import { IsEnum } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
import { shippingTypes } from "../shipping-type.entity";

export class ShippingTypesDto {
    @ApiProperty({description: 'nombre del tipo de envio'})
    @IsEnum(shippingTypes)
    readonly name: shippingTypes;
    @ApiProperty({description: 'descripción del tipo de pago'})
    readonly description : string;
}