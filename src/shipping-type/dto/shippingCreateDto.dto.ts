import { ApiProperty } from "@nestjs/swagger";

export class shippingTypeDto {
    @ApiProperty({ description: 'descripción del tipo de pago' })
    nombre: string;
}