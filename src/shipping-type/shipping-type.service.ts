import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { ShippingTypes } from './shipping-type.entity';

@Injectable()
export class ShippingTypeService extends TypeOrmCrudService<ShippingTypes>{
    constructor(@InjectRepository(ShippingTypes) repo) {
        super(repo)
    }

    public async searchName(nombre: string) {
        let typeEnvio = await this.repo.findOne({ where: { name: nombre } });
        return typeEnvio;
    }
}
