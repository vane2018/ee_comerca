import { Body, Controller, HttpStatus, Post, Res } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Crud } from '@nestjsx/crud';
import { ShippingTypesDto } from './dto/shipping.dto';
import { shippingTypeDto } from './dto/shippingCreateDto.dto';
import { ShippingTypes } from './shipping-type.entity';
import { ShippingTypeService } from './shipping-type.service';
@Crud({
    model: {
        type: ShippingTypes
    },
    routes: {
        exclude: ['createManyBase',
            'deleteOneBase',
            'updateOneBase',
            'replaceOneBase',
            'createOneBase',
            'getOneBase'],
        getManyBase: {
            decorators: [ApiOkResponse({ description: 'datos devueltos correctamente', type: [ShippingTypesDto] })]
        }
    },
    serialize: {
        getMany: ShippingTypesDto,
    },
    params: {
        id: {
            field: 'id',
            type: 'uuid',
            primary: true
        },
    },
})
@ApiTags('Shipping-Types')
@Controller('shipping-type')
export class ShippingTypeController {
    constructor(public service: ShippingTypeService) { }

    @Post('create')
    async createPurchase(@Res() res, @Body() shippingDto: shippingTypeDto) {

        let response = await this.service.searchName(shippingDto.nombre);

        return res.status(HttpStatus.OK).json(response);
    }
}
