import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

export enum shippingTypes {
    OTROS = 'Acordar con el vendedor',
    CADYGO = 'Cadygo'

}
@Entity('SHIPPING_TYPES')
export class ShippingTypes {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column()
    name: shippingTypes;
    @Column()
    description: string;
}