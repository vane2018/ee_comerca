import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ShippingTypeController } from './shipping-type.controller';
import { ShippingTypes } from './shipping-type.entity';
import { ShippingTypeService } from './shipping-type.service';

@Module({
  imports: [TypeOrmModule.forFeature([ShippingTypes])],
  controllers: [ShippingTypeController],
  providers: [ShippingTypeService]
})
export class ShippingTypeModule { }
