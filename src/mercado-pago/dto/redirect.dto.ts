import { ApiProperty } from "@nestjs/swagger";

export class RedirectDto {
    @ApiProperty({ description: 'codigo' })
    code: string;
    @ApiProperty({ description: 'id de comercio' })
    comercioId: string;
}