export interface Address {
    zip_code: string;
    street_name: string;
    street_number: number;
}

interface Phone {
    area_code: string;
    number: number;
}

interface Identification {
    type: string;
    number: number;
}

interface Payer {
    name: string;
    surname: string;
    email: string;
    phone: Phone;
    identification: Identification;
    address: Address;
    date_created: Date;
}

export interface Item {
    id: string;
    title: string;
    description: string;
    picture_url?: string;
    category_id?: string;
    quantity: number;
    currency_id?: string;
    unit_price: number;
}

interface BackUrls {
    success: string;
    pending: string;
    failure: string;
}

interface DifferentialPricing {
    id: number;
}

export interface Preference {
    items: Item[];
    payer: Payer;
    external_reference: string;
    back_urls: BackUrls;
    notification_url: string;
    additional_info: string;
    auto_return: string;
    expires: boolean;
    expiration_date_from: Date;
    expiration_date_to: Date;
    marketplace: string;
    marketplace_fee: number;
    differential_pricing: DifferentialPricing;
    tracks: any[];
}