export class tokenizeWebDto {
    token: string;
    issuer_id: string;
    installments: number;
    payment_method_id: string;
    precio: number;
    precioEnvio: number;
    email: string;
    comercioId: string;
}