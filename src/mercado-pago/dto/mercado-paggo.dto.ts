import { ApiProperty } from "@nestjs/swagger";

export class mercadoDto {
    @ApiProperty({ description: 'precio de productos' })
    token: string;
    @ApiProperty({ description: 'precio de productos' })
    payment_method_id: string;
    @ApiProperty({ description: 'precio de productos' })
    installments: string;
    @ApiProperty({ description: 'precio de productos' })
    issuer_id: string;
} 