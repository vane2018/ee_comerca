import { ApiProperty } from "@nestjs/swagger";

export class tokenizeDto {
    @ApiProperty({ description: 'monto de la compra' })
    token: string;
    issuer_id: string;
    installments: number;
    payment_method_id: string;
    precio: number;
    email: string;
}

