import { Commerce } from "../commerce/commerce.entity";
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";


export enum GrantType {
    AUTHORIZATION_CODE = 'authorization_code',
    REFRESH_TOKEN = 'refresh_token'
}
@Entity('MERCADOPAGO')
export class MercadoPago {

    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column('varchar', { length: 500, nullable: true })
    accessToken: string;
    @Column('varchar', { length: 500, nullable: true })
    tokenType: string;
    @Column('varchar', { length: 500, nullable: true })
    expireIn: number;
    @Column('varchar', { length: 500, nullable: true })
    scope: string;
    @Column('varchar', { length: 500, nullable: true })
    refreshToken: string;
    @Column('varchar', { length: 500, nullable: true })
    comercioId: string;
    @Column('varchar', { length: 500, nullable: true })
    deletedAt: Date;
    @Column('varchar', { length: 500, nullable: true })
    createdAt: Date;
    @OneToOne(() => Commerce, (commerce: Commerce) => commerce.mercado)
    commerce: Commerce;

}