import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GrantType, MercadoPago } from './mercado-pago.entity';
import { Repository } from 'typeorm';
import { PaymentDto } from './dto/paymentDto.dto';
import axios from 'axios';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import * as mercadopago from 'mercadopago';
import { Preference } from './dto/preference.dto';
import { CredentialResponse } from './interface/payerInterface';
import { MercadoPagoDto } from './dto/mercado-pago.dto';
import { mercadoPagoQueryDto } from './dto/mercado-pago-query.dto';
import { mercadoDto } from './dto/mercado-paggo.dto';
import { ENV } from '../common/env.config';

@Injectable()
export class MercadoPagoService extends TypeOrmCrudService<MercadoPago> {

    constructor(
        @InjectRepository(MercadoPago) repo: Repository<MercadoPago>) {
        super(repo)
    }
    async paymentCreateWithToken(payment: PaymentDto, accessToken: string) {
        const res = await axios({
            method: 'post',
            url: ENV.MERCADO_PAGO_API
                + '/v1/payments?access_token=' + accessToken,
            data: payment,
            headers: {
                'accept': 'application/json',
                'content-type': 'application/json'
            }
        });
        console.log("MERCADO PAGO 1", res);

        return res.data;
    }

    async obtieneCode(micode: string) {
        const data = {
            client_secret: ENV.MERCADO_PAGO_ACCESS_TOKEN,
            grant_type: GrantType.AUTHORIZATION_CODE,
            micode,
            redirect_uri: ENV.MERCADO_PAGO_REDIRECT_URI
        };
        console.log("DATOS DE MERCADO PAGO 2 obtieneCode", data);

        const res = await axios({
            method: 'post',
            url: 'https://api.mercadopago.com/oauth/token',
            data,
            headers: {
                'accept': 'application/json',
                'content-type': 'application/x-www-form-urlencoded'
            },
        });
        return res.data;
    }

    async paymentCreate(payment: PaymentDto) {
        const res = await axios({
            method: 'post',
            url: MERCADO_PAGO_API
                + '/v1/payments?access_token=' +
                ENV.MERCADO_PAGO_ACCESS_TOKEN,
            data: payment,
            headers: {
                'accept': 'application/json',
                'content-type': 'application/x-www-form-urlencoded'
            }
        });
        console.log("MECADO PAGO paymentCreate", res.data);

        return res.data;
    }

    async paymentSave(payment: PaymentDto) {
        return await mercadopago.payment.save(payment);
    }

    async createPreference(preference: Preference) {
        return await mercadopago.preferences.create(preference);
    }

    async createPreferenceOfComercio(preference: Preference, accessToken: string) {
        const res = await axios({
            method: 'post',
            url: MERCADO_PAGO_API + 'checkout/preferences?access_token=' + accessToken,
            data: preference,
            headers: {
                'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        });
        return res.data;
    }

    async createCredencial(code: string): Promise<CredentialResponse> {
        const data = {
            client_secret: ENV.MERCADO_PAGO_CLIENT_SECRET,
            grant_type: GrantType.AUTHORIZATION_CODE,
            code,
            redirect_uri: ENV.MERCADO_PAGO_REDIRECT_URI
        };
        console.log("CREA CREDENCIALES MERCADO PAGO", data);
        try {
            const res = await axios({
                method: 'post',
                url: MERCADO_PAGO_API + 'oauth/token',
                data,
                headers: {
                    'accept': 'application/json',
                    'content-type': 'application/x-www-form-urlencoded'
                }
            });
            console.log("CREA CREDENCIALES", res);

            return res.data;
        } catch (error) {
            console.log("error", error);
        }
    }

    async realizarPago(query: mercadoPagoQueryDto, credenciales: mercadoDto) {
        try {
            const token = credenciales.token;
            const payment_method_id = credenciales.payment_method_id;
            const installments = parseInt(credenciales.installments);
            const issuer_id = credenciales.issuer_id;
            const transaction_amoun = Number(query.transaction_amount);
            const shipping_amount = Number(query.shipping_amount); // se agrego el monto de envio desde front
            const product_price = transaction_amoun - shipping_amount; //se calcula el precio del producto
            const comprartir_commission = (product_price * 0.07); // se calcula la comision de comprartir
            const application_fee = shipping_amount + comprartir_commission; //Comisión + envio recolectada por el marketplace
            const email = query.email;
            const commerceId = query.commerceId;
            var sellerAccessToken = 'TEST-4738866408022481-121515-5b0f30a924dc5654d932d5f46c249a95-688246148';

            var payment_data = {
                transaction_amount: transaction_amoun,
                token: token,
                issuer_id: issuer_id,//se agrega
                installments: installments,
                payment_method_id: payment_method_id,
                binary_mode: true,//se agrego
                application_fee: application_fee,//se agrega
                payer: {
                    email: email //'test_user_7450365@testuser.com'
                }
            };
            var commerce_id = {
                comerceId: commerceId
            };
            /*   console.log("PRUEBA DE MERC PAGO",this.envService.read().SERVER_URL); */

            const mivar = await axios({
                method: 'post',
                url: ENV.SERVER_URL
                    + '/api/v1/mercado-pago/apis/v1/mercado-pago/exist/comercioId/{comercioId}',
                data: commerce_id,
                headers: {
                    'accept': 'application/json',
                    'content-type': 'application/json',
                }
            });
            sellerAccessToken = mivar.data.replace(/['"]+/g, '');
            const miletra = await axios({
                method: 'post',
                url: ENV.MERCADO_PAGO_API + 'v1/payments',
                data: payment_data,
                headers: {
                    'accept': 'application/json',
                    'content-type': 'application/json',
                    'Authorization': `Bearer ${sellerAccessToken}`
                }
            })
            return miletra.data.status;
        }
        catch (error) {
            console.log("error", error);
        }
    }

    async refreshCredencial(userRefresToken: string): Promise<CredentialResponse> {
        const data = {
            client_secret: ENV.MERCADO_PAGO_ACCESS_TOKEN,
            grant_type: GrantType.REFRESH_TOKEN,
            refresh_token: userRefresToken,
        };
        const res = await axios({
            method: 'post',
            url: MERCADO_PAGO_API + 'oauth/token',
            data,
            headers: {
                'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        });
        return res.data;
    }

    async createMercadoPago(mercadoPago: MercadoPagoDto): Promise<MercadoPago> {
        return await this.repo.save(mercadoPago)

    }

    public async compruebaExistencia(id: string) {
        return await this.repo.findOne({ where: { comercioId: id } });
    }

    public async updateMercado(ids: string, mercado: MercadoPagoDto): Promise<any> {
        const miComerce = await this.compruebaExistencia(ids);
        if (miComerce) {
            let comerceMod = {
                id: miComerce.id,
                accessToken: mercado.accessToken,
                tokenType: mercado.tokenType,
                expireIn: mercado.expireIn,
                scope: mercado.scope,
                refreshToken: mercado.refreshToken,
                comercioId: mercado.comercioId
            } as MercadoPagoDto
            return await this.repo.save(comerceMod);
        }
    }

    async getMercadoPagoByComercioId(comercioId: string): Promise<MercadoPago> {
        return await this.repo.findOne({ where: { comercioId } });
    }
}
