import * as fs from 'fs-extra';
const winston = require('winston');
interface Logger {
    // Logs methods
    trace(msg: any): void;
    debug(msg: any): void;
    info(msg: any): void;
    warn(msg: any): void;
    error(msg: any): void;
    fatal(msg: any): void;
}

const transports = [];

if (process.env.NODE_ENV === 'production') {
    // Logs error and fatal
    const pm2ErrorFile = !!process.env.NODE_APP_INSTANCE ? `/home/ubuntu/.pm2/logs/vitrinia-server-error-${process.env.NODE_APP_INSTANCE}.log` : '/home/ubuntu/.pm2/logs/vitrinia-server-error.log';
    // Sync porque debemos asegurarnos que se cree para que no crashee al intentar loggear un error
    // Ademas esto se ejecuta solo una vez en la inicalizacion
    fs.ensureFileSync(pm2ErrorFile);
    transports.push(new (winston.transports.File)({ filename: pm2ErrorFile, level: 'error' }));
} else {
    // Logs debug, info, and forward
    transports.push(new (winston.transports.Console)({
        // format: winston.format.simple(),
        level: 'debug',
        colorize: true,
        timestamp: true,
        prettyPrint: true
    }));
}

/**
 * Author: https://gist.github.com/rtgibbons/7354879
 */
export const logger: Logger = new (winston.Logger)({
    colors: {
        trace: 'white',
        debug: 'green',
        info: 'green',
        warn: 'yellow',
        error: 'red',
        fatal: 'red'
    },
    // Redefinimos los niveles de seguridad como yo quiero y de acuerdo con los colores seteados mas arriba
    levels: {
        trace: 5,
        debug: 4,
        info: 3,
        warn: 2,
        error: 1,
        fatal: 0
    },
    transports: transports
});

/**
 * Examples
 */

// logger.log('trace', 'testingx');
// logger.log('debug', 'testingx');
// logger.log('info', 'testingx');
// logger.log('warn', 'testingx');
// logger.log('error', 'testingx');
// logger.log('fatal', 'testingx');

// logger.trace('testing');
// logger.debug('testing');
// logger.info('testing');
// logger.warn('testing');
// logger.error('testing');
// logger.fatal('testing');
