import { Body, Controller, Get, HttpStatus, Post, Response, Query } from '@nestjs/common';
import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { MercadoPagoService } from './mercado-pago.service';
import { RedirectDto } from './dto/redirect.dto';
import { CommerceService } from '../commerce/commerce.service';
import { credenciales } from './dto/credenciales.dto';
import { consultarDto } from './dto/consultarCommerce.dto';
import { MercadoPagoDto } from './dto/mercado-pago.dto';
import { ENV } from '../common/env.config';

@ApiTags('Mercado-Pago')
@Controller('mercado-pago')
export class MercadoPagoController {
    constructor(
        public serviceMercado: MercadoPagoService,
        public commerceService: CommerceService
    ) { }

    @Post('/apis/v1/mercado-pago/get-code')
    @ApiOperation({ summary: 'crea comercio' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async obtenerCode(@Response() res, @Body() escorp: credenciales) {
        try {
            await this.serviceMercado.obtieneCode(escorp.code);
        } catch (error) {
            return error;
        }
    }

    @Post('/apis/v1/mercado-pago/redirect')
    @ApiOperation({ summary: 'crea comercio' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    @ApiBody({ type: RedirectDto })
    public async crearComercios(@Response() res, @Body() commerce: RedirectDto) {
        let comercio;
        try {
            const credentialResponse = await this.serviceMercado.createCredencial(commerce.code);
            const mercadoPago = {
                accessToken: credentialResponse.access_token,
                tokenType: credentialResponse.token_type,
                expireIn: credentialResponse.expires_in,
                scope: credentialResponse.scope,
                refreshToken: credentialResponse.refresh_token,
                comercioId: commerce.comercioId, // afterward we update this
            } as MercadoPagoDto;
            comercio = await this.serviceMercado.compruebaExistencia(commerce.comercioId);
            if (comercio) {
                await this.serviceMercado.updateMercado(commerce.comercioId, mercadoPago);
                return res.status(HttpStatus.OK).json({ mensaje: 'se actualizo con exito', statusCode: 200 });
            } else {
                await this.serviceMercado.createMercadoPago(mercadoPago);
                return res.status(HttpStatus.OK).json({ mensaje: 'se creo con exito', statusCode: 200 });
            }
        } catch (error) {
            console.log(error);
            if (error.response && error.response.data) {
                console.log(error.response.data);
            }
            return res.status(HttpStatus.NOT_FOUND).json({ error: ' no se crearon las credenciales', statusCode: 400 });
        }
    }

    @Post('/apis/v1/mercado-pago/exist/comercioId/:comercioId')
    @ApiOperation({ summary: 'crea comercio' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    @ApiBody({ type: consultarDto })
    public async consultarComercios(@Response() res, @Body() consulta: consultarDto) {
        try {
            const mercadoPago = await this.serviceMercado.getMercadoPagoByComercioId(consulta.comerceId);
            if (mercadoPago) {
                let nuevo = mercadoPago.accessToken.replace(/['"]+/g, '');
                return res.status(200).json(`${nuevo}`);
            } else {
                return res.status(200).json({ hasPagoOnline: false });
            }
        } catch (error) {
            return res.status(500).json({ error: 'internal server error', statusCode: 500 });
        }
    }

    @Get('/apis/v1/mercado-pago/mensaje-exito')
    @ApiOperation({ summary: 'crea comercio' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async obtenerMensajes(@Response() res) {
        try {
            res.redirect(ENV.APP_FRONT_URL + '/mercado-pago/loading-sppiner/?success');
        } catch (error) {
            return error;
        }
    }

    @Get('/apis/v1/mercado-pago/mensaje-failed')
    @ApiOperation({ summary: 'crea comercio' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async obtenerMensajesFailed(@Response() res) {
        try {
            res.redirect(ENV.APP_FRONT_URL + '/mercado-pago/loading-sppiner/?failure');
        } catch (error) {
            return error;
        }
    }
}