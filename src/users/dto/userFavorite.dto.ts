import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty, IsEmail, IsString, IsOptional } from 'class-validator';
import { Product } from '../../products/products.entity'

export class UserFavoriteDto {

    @ApiPropertyOptional({ description: 'array de favorite' })
    favorite: string[];
}