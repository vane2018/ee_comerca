import { ApiProperty } from "@nestjs/swagger";

export class notificarUser {
    @ApiProperty({ description: 'token de firebase del Usuario' })
    id: string;
}