import { ApiProperty } from "@nestjs/swagger";

export class consultaCbu {

    @ApiProperty({ description: 'token de firebase del Usuario' })
    id: string;
}