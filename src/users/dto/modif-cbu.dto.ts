import { ApiProperty } from "@nestjs/swagger";

export class modificarCbuDto {
    @ApiProperty()
    cbu: string;
}