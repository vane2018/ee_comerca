import { ApiProperty } from "@nestjs/swagger";
import Api = require("twilio/lib/rest/Api");

export class bodyMensajeDto{
    @ApiProperty()
    nickName:string;
    @ApiProperty()
    nombre:string;
    @ApiProperty()
    apellido:string;
    @ApiProperty()
    montoExtraccioon:number;
    @ApiProperty()
    cbu:string;
}