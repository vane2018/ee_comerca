import { Injectable, HttpException, HttpStatus, Inject, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { RegistrationStatus } from './interface/reponseStatus.interface';
import { CredentialsDto } from '../auth/dto/credentials.dto';
import * as bcrypt from 'bcrypt';
import { User } from './user.entity';
import { UserDto } from './dto/user.dto';
import * as firebase from 'firebase-admin';
import { TipoNotificacion } from '../chat/chat.entity';
import { UserFirebaseDto } from './dto/user-firebase.dto';
import { bodyMensajeDto } from './dto/notificar.dto';

@Injectable()
export class UsersService extends TypeOrmCrudService<User> {
  constructor(@InjectRepository(User) repo) {
    super(repo)
  }

  public getAllUsers = async (): Promise<any[]> => {
    return await this.repo.find({ relations: ['rols'] });

  }

  public async getUsuarioById(id: string): Promise<any> {
    let usuario = await this.repo.findOne(id, { relations: ['commisions', 'commerce', 'rols'] });

    return usuario;
  }

  public getUserValidateToken = async (id: string): Promise<any> => {
    return await this.repo.findOne(id, { relations: ['rols'] });
  }

  public async findByEmail(userEmail: string): Promise<RegistrationStatus> {
    const status: RegistrationStatus = {
      success: false,
      message: 'user not found',
      user: null,
    };
    try {

      const user = await this.repo.findOne({ email: userEmail });
      if (user) {
        // tslint:disable-next-line: no-shadowed-variable
        const status: RegistrationStatus = {
          success: true,
          message: 'user finded',
          user,
        };
        return status;
      }
      return status;

    } catch (error) {
      console.log('error en buscar por email: ', error);

    }

  }
  public async addCobro(idCustomer: string, comisionn: any): Promise<any> {
    let custom = await this.repo.findOne(idCustomer, { relations: ['cobro'] });
    custom.cobro.push(comisionn)
    await this.repo.save(custom)
  }

  public async findByNumCel(nroCel: string): Promise<RegistrationStatus> {
    const status: RegistrationStatus = {
      success: false,
      message: 'user not found',
      user: null,
    };
    try {
      const user = await this.repo.findOne({ phone: nroCel });
      if (user) {
        // tslint:disable-next-line: no-shadowed-variable
        const status: RegistrationStatus = {
          success: true,
          message: 'user finded',
          user,
        };
        return status;
      }
      return status;
    } catch (error) {
      console.log('error en buscar por nro: ', error);
    }
  }
  public async guardarComision(user: User, total: number) {
    let usuario = await this.repo.findOne(user.id);
    usuario.montoextraido = total;
    return this.repo.save(usuario);
  }
  public async login(login: any): Promise<any> {

    try {
      const user = await this.repo.findOne({ where: [{ email: login.email }, { nickName: login.nickName }], });
      if (user) {
        const pass = await user.comparePassword(login.password);
        const nick = await user.compareNickname(login.nickName)
        if (pass) {
          // tslint:disable-next-line: object-literal-shorthand
          return { success: true, user: user };
        } else {
          return { success: pass, user: null };
        }
      }

      return { success: false, user: null };

    } catch (error) {
      console.log('pass o mail incorrectos');

    }
  }

  public async findByNickName(nickName: string): Promise<User> {
    return await this.repo.findOne({ nickName });
  }

  public async findByEmailForValidate(email: string): Promise<User> {
    return await this.repo.findOne({ email });
  }

  public async findById(id: string): Promise<RegistrationStatus> {
    const user = await this.repo.findOne(id);
    const status: RegistrationStatus = {
      success: true,
      message: 'user finded',
      user,
    };
    return status;

  }
  public async bucarporId(id: string) {
    let user = this.repo.findOne({ where: { id: id } });
    return user;

  }

  public async register(userDto: UserDto): Promise<UserDto> {

    const { email } = userDto;
    let user = await this.repo.findOne({ where: { email } });
    if (user) {
      throw new HttpException(
        'User already exists',
        HttpStatus.BAD_REQUEST,
      );
    }
    try {
      userDto.rols = [];
      user = this.repo.create(userDto);
      return await this.repo.save(user);
    } catch (error) {
      console.log('error: ', error);
    }
  }

  public async updateUser(userDto: UserDto, rolDto: any) {
    const user = await this.repo.findOne({ where: { email: userDto.email }, relations: ['rols'] });
    if (user.rols.find(role => (role.name === "customer"))) {
      user.isCustomerAndSeller = !!userDto.isCustomerAndSeller;
    }
    user.rols.push(rolDto);
    return this.repo.save(user);
  }


  public async updateUserRol(id: string, rolDto: any) {
    const user = await this.repo.findOne({ where: { id: id }, relations: ['rols'] });

    user.rols.push(rolDto);
    return this.repo.save(user);
  }

  public async updatePassword(credential: CredentialsDto) {
    let user;
    if (credential.email) {
      user = await this.repo.findOne({ where: { email: credential.email } });
    } else {
      user = await this.repo.findOne({ where: { phone: credential.nroCel } });
    }
    user.password = await bcrypt.hash(credential.pass, 10);
    await this.repo.save(user);
  }

  public async updateToken(token: string, idUser: string) {
    let user = await this.repo.findOne(idUser);
    user.firebaseRegistrationToken = token;
    return await this.repo.save(user);
  }


  public async addCommisions(idCustomer: string, comisionn: any): Promise<any> {
    let custom = await this.repo.findOne(idCustomer, { relations: ['commisions'] });
    custom.commisions.push(comisionn)
    await this.repo.save(custom)
  }



  public async changePass(user: User, password) {
    user.password = await bcrypt.hash(password, 10);
    return this.repo.save(user);
  }


  public async comparePassword(password1: string, password2: string): Promise<boolean> {
    return password1 === password2
  }

  public async saveProduct(productDto: UserDto): Promise<any> {
    return await this.repo.save(productDto);
  }

  public async getReferrers(nickName: string): Promise<any> {
    const referrers = this.repo.find({ where: { nickReferrer: nickName } });
    return referrers;
  }


  public async descontarComision(user: User) {
    user.commisions.push()

  }

  public async notificar(id: string, datos: bodyMensajeDto) {
    try {
      var usuario = this.repo.findOne(id)
      const notificacionId = `nueva-comision`;
      if ((await usuario).firebaseRegistrationToken) {
        firebase.messaging().sendToDevice((await usuario).firebaseRegistrationToken, {
          data: {
            notificacionId: notificacionId,
            tipo: TipoNotificacion.Comision
          },
          notification: {
            tag: notificacionId,
            title: `Tiene un mensaje cobro de comision`,
            body: `nombre ${datos.nombre}, apellido ${datos.apellido}, nickname ${datos.montoExtraccioon}, cbu ${datos.cbu}`,
          }
        });
      }

    } catch (error) {
      console.log(error);
    }
  }

  public async saveCbuReferido(referer: string, cbu: string) {
    let user = await this.repo.findOne({ where: { id: referer } })
    user.cbu = cbu;
    this.repo.save(user);
  }
  public async modificoUser(user: User) {
    return this.repo.save(user);
  }
  public async consultaMonto(id: string) {
    let usuarios = await this.repo.findOne(id);
    let montoExtraccion = usuarios.montoextraido;
    return montoExtraccion;

  }
  public async getUser() {
    let usuario = await this.repo.findOne({ email: "comprartir@gmail.com" });
    return usuario;
  }

  public async comprueboCbu(id: string) {
    let user = await this.repo.findOne({ where: { id: id } })
    if (user.cbu !== "") {
      return user.cbu;
    } else {
      return false;
    }
  }

  public async getCobroByUserId(userId: string): Promise<any> {
    let comisions = await this.repo.findOne({ where: { user: userId } });
    return comisions;
  }
}
