import { Controller, Response, HttpStatus, Get, Param, Post, Body, Res, Put } from '@nestjs/common';
import { Crud } from '@nestjsx/crud';
import { UserDto } from '../users/dto/user.dto'
import { ApiTags, ApiResponse, ApiBody, ApiOperation, ApiParam, ApiCreatedResponse, ApiConflictResponse } from '@nestjs/swagger';
import { User } from './user.entity';
import { UsersService } from './user.service';
import { UserFavoriteDto } from './dto/userFavorite.dto';
import { ParamsUserDto } from './dto/paramsUserDto';
import { ChangePassDto } from './dto/changePass.dto';
import { ErrorChangePassDto } from './dto/errorChangePass.dto';
import { UserFirebaseDto } from './dto/user-firebase.dto';
import { datosCbu } from './dto/cbu.dto';
import { consultaCbu } from './dto/consulta-cbu.dto';
import { modificarCbuDto } from './dto/modif-cbu.dto';
import { cunsultaDto } from './dto/consultar-extraccion.dto';
import { bodyMensajeDto } from './dto/notificar.dto';
import { addRol } from './dto/datos-user-rol.dto';
import { RolsService } from '../rols/rols.service';

@Crud({
    model: {
        type: User,
    },
    routes: {
        createOneBase: {
            decorators: [ApiBody({ type: UserDto })]
        }
    },
    serialize: {
        create: UserDto,
    },
    params: {
        id: {
            field: 'id',
            type: 'uuid',
            primary: true,
        },
    },

    query: {
        join: {

            commisions: {
                allow: []
            },
            commerce: {}
        },
    }
})

@ApiTags('Users')
@Controller('users')
export class UsersController {
    constructor(public service: UsersService,
        public serviceRol: RolsService

    ) { }

    @Get()

    //@UseGuards(AuthGuard('jwt'))
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })

    public async getUsers(@Response() res): Promise<any> {
        const users = await this.service.getAllUsers();
        return res.status(HttpStatus.OK).json(users);

    }



    @Get('/:userId')
    @ApiOperation({ summary: 'Obtiene un usuario por su Id' })
    @ApiResponse({ status: 200, description: 'Los datos han sido devueltos correctamente.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getCommerce(@Response() res, @Param() param): Promise<any> {
        const user = await this.service.getUsuarioById(param.userId);

        return res.status(HttpStatus.OK).json(user);
    }
    @Post('/monto/consulta/extraccion/userId')
    @ApiOperation({ summary: 'Obtiene un usuario por su Id' })
    @ApiResponse({ status: 200, description: 'Los datos han sido devueltos correctamente.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getUser(@Response() res, @Body() cuerpo: cunsultaDto, @Param() param): Promise<any> {
        const monto = await this.service.consultaMonto(cuerpo.id);
        return res.status(HttpStatus.OK).json(monto);
    }

    @Get('referrers/:nickName')
    @ApiParam({ type: 'string', name: 'nickName' })
    public async getReferrers(@Response() res, @Param() param): Promise<any> {
        const listReferrer = await this.service.getReferrers(param.nickName);
        return res.status(HttpStatus.OK).json(listReferrer);
    }
    @Put('/editUser/:id')
    public async updateUser(@Response() res, @Param() param, @Body() userDto: UserFirebaseDto): Promise<any> {
        try {

            let product = await this.service.findOne(param.id);
            if (product) {
                let productMod = {
                    firebaseRegistrationToken: userDto.firebaseRegistrationToken
                }

                const userUpdate = await this.service.updateToken(param.id, productMod.firebaseRegistrationToken);
                return res.status(HttpStatus.OK).json(userUpdate);
            } else {
                return res.status(HttpStatus.BAD_REQUEST).json({ message: ' user no encontrados' });
            }

        } catch (error) {
            console.log('error al modifcar user: ', error);
            return res.status(HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el user', error });
        }
    }

    @Put('/editCbu/:id')
    public async updateCbu(@Response() res, @Param() param, @Body() userDto: modificarCbuDto): Promise<any> {
        try {
            let user = await this.service.findOne(param.id);
            if (user) {
                user.cbu = userDto.cbu;
                const userUpdate = await this.service.modificoUser(user);
                return res.status(HttpStatus.OK).json(userUpdate);
            } else {
                return res.status(HttpStatus.BAD_REQUEST).json({ message: ' user no encontrados' });
            }
        } catch (error) {
            console.log('error al modifcar user: ', error);
            return res.status(HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el user', error });
        }
    }

    @Post('comprobarCbu')
    @ApiOperation({ summary: 'password change' })
    @ApiResponse({ status: 200, description: 'was changed password' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 403, description: "Forbidden" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiConflictResponse({ description: 'conflict with password form', status: 409, type: ErrorChangePassDto })
    @ApiResponse({ status: 500, description: "internal server error" })
    @ApiBody({ type: consultaCbu })
    async comprobarCbu(@Res() res, @Body() datosCbu: consultaCbu): Promise<any> {
        try {
            let us = await this.service.comprueboCbu(datosCbu.id);
            if (us) {
                return res.status(HttpStatus.OK).json(us);
            } else {
                return res.status(HttpStatus.OK).json("false");
            }
        } catch {
            return false;
        }
    }

    @Post('change-password/:userId')
    @ApiOperation({ summary: 'password change' })
    @ApiResponse({ status: 200, description: 'was changed password' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 403, description: "Forbidden" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiConflictResponse({ description: 'conflict with password form', status: 409, type: ErrorChangePassDto })
    @ApiResponse({ status: 500, description: "internal server error" })
    @ApiBody({ type: ChangePassDto })
    public async ChangePassword(@Response() res, @Param() param: ParamsUserDto, @Body() changePassDto: ChangePassDto) {
        try {
            const resUser = await this.service.findOne({ id: param.userId });
            if (resUser) {
                const isValidPass = await resUser.comparePassword(changePassDto.backPassword);
                const backSamePass = await this.service.comparePassword(changePassDto.backPassword, changePassDto.newPassword);
                const repatPassSame = await this.service.comparePassword(changePassDto.newPassword, changePassDto.repeatPassword);
                if (!isValidPass) {
                    return res.status(HttpStatus.BAD_REQUEST).json({ error: 'invalid password' });
                }
                if (backSamePass) {
                    return res.status(HttpStatus.CONFLICT).json({ error: 'new and back password not should same' });
                }
                if (!repatPassSame) {
                    return res.status(HttpStatus.CONFLICT).json({ error: 'new and repeat password should same' });
                }
                await this.service.changePass(resUser, changePassDto.newPassword);
                return res.status(HttpStatus.OK).json({ message: 'pass changed succesfull' });
            } else {
                return res.status(HttpStatus.NOT_FOUND).json({
                    statusCode: 404,
                    error: 'user not found',
                    message: 'user not found',
                });
            }
        } catch (error) {
            console.log('error: ', error);
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: 'Internal Server Error' })
        }
    }

    @Post('favorito/:id')
    async saveFavorite(@Res() res, @Body() updateUserDto: UserFavoriteDto, @Param() param) {

        try {
            let users = await this.service.findOne(param.id);
            if (users) {
                users.favorite = updateUserDto.favorite;
                let usuario = await this.service.saveProduct(users);
                return res.status(HttpStatus.OK).json(usuario);

            } else {
                return res.status(HttpStatus.BAD_REQUEST).json({ message: 'users no encontrado' });
            }

        } catch (error) {
            console.log('error al crear user: ', error);
            return res.status(HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el user', error });
        }
    }

    @Post('guardarCbu')
    @ApiOperation({ summary: 'password change' })
    @ApiResponse({ status: 200, description: 'was changed password' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 403, description: "Forbidden" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiConflictResponse({ description: 'conflict with password form', status: 409, type: ErrorChangePassDto })
    @ApiResponse({ status: 500, description: "internal server error" })
    @ApiBody({ type: datosCbu })
    async guardarCbu(@Res() res, @Body() datosCbu: datosCbu) {
        try {
            this.service.saveCbuReferido(datosCbu.user, datosCbu.cbu)
            return res.status(HttpStatus.OK).json({ mensaje: 'se guardo correctamente', statusCode: 200 });
        } catch {
            console.log("error");
        }
    }

    @Post('notificarUser')
    @ApiOperation({ summary: 'password change' })
    @ApiResponse({ status: 200, description: 'was changed password' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 403, description: "Forbidden" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiConflictResponse({ description: 'conflict with password form', status: 409, type: ErrorChangePassDto })
    @ApiResponse({ status: 500, description: "internal server error" })
    async notificar(@Res() res, @Body() datos: bodyMensajeDto) {
        try {
            let user = await this.service.getUser();
            await this.service.notificar(user.id, datos);
            return res.status(HttpStatus.OK).json({ message: 'notificacion enviada' });
        } catch (error) {
            console.log("error");
            return res.status(HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al enviar notificacion', error });
        }
    }

    @Post('addUserRol/empleado')
    @ApiOperation({ summary: 'asigna el rol de empleado, ingrese idUser' })
    @ApiResponse({ status: 200, description: 'was changed password' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 403, description: "Forbidden" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiConflictResponse({ description: 'conflict with password form', status: 409, type: ErrorChangePassDto })
    @ApiResponse({ status: 500, description: "internal server error" })
    @ApiBody({ type: addRol })
    async agregorol(@Res() res, @Body() datos: addRol): Promise<any> {
        const rol = await this.serviceRol.findRol({ name: 'empleado' });
        let usuario = await this.service.updateUserRol(datos.idUser, rol);
        return res.status(HttpStatus.OK).json(usuario);
    }

    @Post('addUserRol/superAdmin')
    @ApiOperation({ summary: 'asigna el rol de SuperAdmin, ingrese idUser' })
    @ApiResponse({ status: 200, description: 'was changed password' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 403, description: "Forbidden" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiConflictResponse({ description: 'conflict with password form', status: 409, type: ErrorChangePassDto })
    @ApiResponse({ status: 500, description: "internal server error" })
    @ApiBody({ type: addRol })
    async agregorolSuperAdmin(@Res() res, @Body() datos: addRol): Promise<any> {
        const rol = await this.serviceRol.findRol({ name: 'super' });
        let usuario = await this.service.updateUserRol(datos.idUser, rol);
        return res.status(HttpStatus.OK).json(usuario);
    }
}