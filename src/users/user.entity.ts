import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BeforeInsert,
  OneToOne,
  ManyToMany,
  JoinTable,
  BeforeUpdate,
  OneToMany,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { IsOptional, IsEmail } from 'class-validator';
import * as bcrypt from 'bcrypt';
import { UserDto } from './dto/user.dto';
import { Commerce } from '../commerce/commerce.entity';
import { Rols } from '../rols/rols.entity';
import { Purchase } from '../purchases/purchase.entity';
import { Commision } from '../commissions/commissions.entity';
import { Chat } from '../chat/chat.entity';
import { Cobro } from '../cobrarcomision/cobrarcomision.entity';

// const { CREATE, UPDATE } = CrudValidationGroups;
const enum tipoUsuario { customer = 'customer', seller = 'seller', admin = 'admin' }

@Entity('USERS')
export class User {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { length: 300, unique: true })
  email: string;

  @Column('varchar', { length: 300 })
  password: string;

  @IsOptional(/* { groups: [CREATE] } */)
  @Column('varchar', { length: 300, nullable: true })
  firstName: string;

  @Column('varchar', { length: 300, nullable: true })
  lastName: string;

  @Column('varchar', { length: 300, nullable: true })
  nickReferrer: string;

  @Column('varchar', { length: 300, unique: true, nullable: true })
  nickName: string;

  @Column('varchar', { length: 300, nullable: true })
  phone: string;

  @Column('varchar', { length: 30, nullable: true })
  gender: string;

  @Column('varchar', { length: 300, nullable: true })
  dni: string;

  @Column('varchar', { length: 300, nullable: true })
  cbu: string;

  @Column('varchar', { length: 300, nullable: true })
  profilePic: string;

  @Column('varchar', { length: 10, nullable: true, default: false })
  isCustomerAndSeller: boolean;

  @Column('varchar', { length: 10, nullable: true, default: tipoUsuario.customer })
  lastAccess: tipoUsuario;

  @Column("text", { array: true, default: () => 'array [] :: text []' })
  favorite: string[];

  @Column('varchar', { length: 500, nullable: true })
  firebaseRegistrationToken: string;
  @Column('varchar', { length: 500, nullable: true })
  pais: string;
  @Column('varchar', { length: 500, nullable: true })
  provincia: string;
  @Column('varchar', { length: 500, nullable: true })
  localidad: string;
  @Column('varchar', { length: 500, nullable: true })
  latitud: string;
  @Column('varchar', { length: 500, nullable: true })
  longitud: string;
  @Column('varchar', { length: 500, nullable: true })
  calle: string;
  @Column('varchar', { length: 500, nullable: true })
  numero: string;
  @Column({ type: "float8", nullable: true, default: 0 })
  montoextraido: number;
  @Column({ type: "float8", nullable: true })
  estadocobrocomision: string;
  @Column({ type: Date, default: new Date("2017-03-15T19:47:47.975Z") })
  fechanacimiento: Date;
  @Column('varchar', { length: 500, nullable: true })
  pisodepartamento: string;
  /* @OneToOne(() => Customer, (customer: Customer) => customer.user)
  customer: Customer; */

  /*  @OneToOne(() => Seller, (seller: Seller) => seller.user)
   seller: Seller; */
  @OneToOne(() => Commerce, (commerce: Commerce) => commerce.user)
  commerce: Commerce;
  @OneToMany(() => Chat, (chat: Chat) => chat.remitente)
  chat: Chat[];
  @ManyToMany(() => Rols, (rol: Rols) => rol.users)
  @JoinTable({
    name: 'USERS_ROLS',
    joinColumns: [
      { name: 'userId' },
    ],
    inverseJoinColumns: [
      { name: 'rolId' },
    ],
  })
  rols: Rols[];

  @OneToMany(type => Purchase, (purchase: Purchase) => purchase.user)
  purchase: Purchase;

  @OneToMany(type => Commision, commision => commision.user)
  commisions: Commision[];
  user: any;
  /*  customer: any;
   seller: any; */


  @OneToMany(type => Cobro, cobro => cobro.user)
  cobro: Cobro[];

  @BeforeInsert()
  // @BeforeUpdate()
  async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10);
    // this.nickName= await bcrypt.hash(this.nickName,10)
  }

  async comparePassword(attempt: string): Promise<any> {
    return await bcrypt.compare(attempt, this.password);
  }
  async compareNickname(nick: string) {
    if (nick === this.nickName) {
      return true;
    }
  }

  toResponseObject(showToken: boolean = true): UserDto {
    const { email, password, firstName, lastName, dni, gender, nickName, phone, commisions, commerce, isCustomerAndSeller, profilePic, rols, nickReferrer, calle, numero, latitud, longitud, localidad, pais, provincia, fechanacimiento, pisodepartamento, firebaseRegistrationToken
    } = this;
    const responseObject: UserDto = {
      email,
      password,
      firstName,
      lastName,
      dni,
      firebaseRegistrationToken,
      gender,
      nickReferrer,
      nickName,
      phone,
      profilePic,
      commisions,
      calle,
      numero,
      localidad,
      provincia,
      pais,
      latitud,
      longitud,
      commerce,
      fechanacimiento,
      pisodepartamento,
      isCustomerAndSeller,
      rols,
    };

    return responseObject;
  }
}
