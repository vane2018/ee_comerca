import {MigrationInterface, QueryRunner} from "typeorm";

export class anachanges1616639620214 implements MigrationInterface {
    name = 'anachanges1616639620214'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`COMMENT ON COLUMN "PAYMENTS"."date" IS NULL`);
        await queryRunner.query(`ALTER TABLE "PAYMENTS" ALTER COLUMN "date" SET DEFAULT '"2021-03-25T02:33:50.241Z"'`);
        await queryRunner.query(`COMMENT ON COLUMN "PURCHASES"."date" IS NULL`);
        await queryRunner.query(`ALTER TABLE "PURCHASES" ALTER COLUMN "date" SET DEFAULT '"2021-03-25T02:33:50.271Z"'`);
        await queryRunner.query(`COMMENT ON COLUMN "PURCHASES"."createAt" IS NULL`);
        await queryRunner.query(`ALTER TABLE "PURCHASES" ALTER COLUMN "createAt" SET DEFAULT '"2021-03-25T02:33:50.271Z"'`);
        await queryRunner.query(`COMMENT ON COLUMN "COMMISIONS"."createAt" IS NULL`);
        await queryRunner.query(`ALTER TABLE "COMMISIONS" ALTER COLUMN "createAt" SET DEFAULT '"2021-03-25T02:33:50.276Z"'`);
        await queryRunner.query(`COMMENT ON COLUMN "CHAT"."createAt" IS NULL`);
        await queryRunner.query(`ALTER TABLE "CHAT" ALTER COLUMN "createAt" SET DEFAULT '"2021-03-25T02:33:50.280Z"'`);
        await queryRunner.query(`COMMENT ON COLUMN "USERS"."favorite" IS NULL`);
        await queryRunner.query(`ALTER TABLE "USERS" ALTER COLUMN "favorite" SET DEFAULT array [] :: text []`);
        await queryRunner.query(`COMMENT ON COLUMN "USERS"."fechanacimiento" IS NULL`);
        await queryRunner.query(`ALTER TABLE "USERS" ALTER COLUMN "fechanacimiento" SET DEFAULT '"2017-03-15T19:47:47.975Z"'`);
        await queryRunner.query(`COMMENT ON COLUMN "PRODUCTS"."pic" IS NULL`);
        await queryRunner.query(`ALTER TABLE "PRODUCTS" ALTER COLUMN "pic" SET DEFAULT array [] :: text []`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "PRODUCTS" ALTER COLUMN "pic" SET DEFAULT ARRAY[]`);
        await queryRunner.query(`COMMENT ON COLUMN "PRODUCTS"."pic" IS NULL`);
        await queryRunner.query(`ALTER TABLE "USERS" ALTER COLUMN "fechanacimiento" SET DEFAULT '2017-03-15 19:47:47.975'`);
        await queryRunner.query(`COMMENT ON COLUMN "USERS"."fechanacimiento" IS NULL`);
        await queryRunner.query(`ALTER TABLE "USERS" ALTER COLUMN "favorite" SET DEFAULT ARRAY[]`);
        await queryRunner.query(`COMMENT ON COLUMN "USERS"."favorite" IS NULL`);
        await queryRunner.query(`ALTER TABLE "CHAT" ALTER COLUMN "createAt" SET DEFAULT '2021-03-19 15:20:37.392'`);
        await queryRunner.query(`COMMENT ON COLUMN "CHAT"."createAt" IS NULL`);
        await queryRunner.query(`ALTER TABLE "COMMISIONS" ALTER COLUMN "createAt" SET DEFAULT '2021-03-19 15:20:37.391'`);
        await queryRunner.query(`COMMENT ON COLUMN "COMMISIONS"."createAt" IS NULL`);
        await queryRunner.query(`ALTER TABLE "PURCHASES" ALTER COLUMN "createAt" SET DEFAULT '2021-03-19 15:20:37.395'`);
        await queryRunner.query(`COMMENT ON COLUMN "PURCHASES"."createAt" IS NULL`);
        await queryRunner.query(`ALTER TABLE "PURCHASES" ALTER COLUMN "date" SET DEFAULT '2021-03-19 15:20:37.395'`);
        await queryRunner.query(`COMMENT ON COLUMN "PURCHASES"."date" IS NULL`);
        await queryRunner.query(`ALTER TABLE "PAYMENTS" ALTER COLUMN "date" SET DEFAULT '2021-03-19 15:20:37.337'`);
        await queryRunner.query(`COMMENT ON COLUMN "PAYMENTS"."date" IS NULL`);
    }

}
