import {MigrationInterface, QueryRunner} from "typeorm";

export class cambios1617072984970 implements MigrationInterface {
    name = 'cambios1617072984970'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "DETALLE" ADD "imagenProducto" character varying`);
        await queryRunner.query(`COMMENT ON COLUMN "PAYMENTS"."date" IS NULL`);
        await queryRunner.query(`ALTER TABLE "PAYMENTS" ALTER COLUMN "date" SET DEFAULT '"2021-03-30T02:56:32.704Z"'`);
        await queryRunner.query(`COMMENT ON COLUMN "PURCHASES"."date" IS NULL`);
        await queryRunner.query(`ALTER TABLE "PURCHASES" ALTER COLUMN "date" SET DEFAULT '"2021-03-30T02:56:32.707Z"'`);
        await queryRunner.query(`COMMENT ON COLUMN "PURCHASES"."createAt" IS NULL`);
        await queryRunner.query(`ALTER TABLE "PURCHASES" ALTER COLUMN "createAt" SET DEFAULT '"2021-03-30T02:56:32.707Z"'`);
        await queryRunner.query(`COMMENT ON COLUMN "COMMISIONS"."createAt" IS NULL`);
        await queryRunner.query(`ALTER TABLE "COMMISIONS" ALTER COLUMN "createAt" SET DEFAULT '"2021-03-30T02:56:32.708Z"'`);
        await queryRunner.query(`COMMENT ON COLUMN "CHAT"."createAt" IS NULL`);
        await queryRunner.query(`ALTER TABLE "CHAT" ALTER COLUMN "createAt" SET DEFAULT '"2021-03-30T02:56:32.709Z"'`);
        await queryRunner.query(`COMMENT ON COLUMN "USERS"."favorite" IS NULL`);
        await queryRunner.query(`ALTER TABLE "USERS" ALTER COLUMN "favorite" SET DEFAULT array [] :: text []`);
        await queryRunner.query(`COMMENT ON COLUMN "USERS"."fechanacimiento" IS NULL`);
        await queryRunner.query(`ALTER TABLE "USERS" ALTER COLUMN "fechanacimiento" SET DEFAULT '"2017-03-15T19:47:47.975Z"'`);
        await queryRunner.query(`COMMENT ON COLUMN "PRODUCTS"."pic" IS NULL`);
        await queryRunner.query(`ALTER TABLE "PRODUCTS" ALTER COLUMN "pic" SET DEFAULT array [] :: text []`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "PRODUCTS" ALTER COLUMN "pic" SET DEFAULT ARRAY[]`);
        await queryRunner.query(`COMMENT ON COLUMN "PRODUCTS"."pic" IS NULL`);
        await queryRunner.query(`ALTER TABLE "USERS" ALTER COLUMN "fechanacimiento" SET DEFAULT '2017-03-15 19:47:47.975'`);
        await queryRunner.query(`COMMENT ON COLUMN "USERS"."fechanacimiento" IS NULL`);
        await queryRunner.query(`ALTER TABLE "USERS" ALTER COLUMN "favorite" SET DEFAULT ARRAY[]`);
        await queryRunner.query(`COMMENT ON COLUMN "USERS"."favorite" IS NULL`);
        await queryRunner.query(`ALTER TABLE "CHAT" ALTER COLUMN "createAt" SET DEFAULT '2021-03-25 13:45:33.825'`);
        await queryRunner.query(`COMMENT ON COLUMN "CHAT"."createAt" IS NULL`);
        await queryRunner.query(`ALTER TABLE "COMMISIONS" ALTER COLUMN "createAt" SET DEFAULT '2021-03-25 13:45:33.802'`);
        await queryRunner.query(`COMMENT ON COLUMN "COMMISIONS"."createAt" IS NULL`);
        await queryRunner.query(`ALTER TABLE "PURCHASES" ALTER COLUMN "createAt" SET DEFAULT '2021-03-25 13:45:33.94'`);
        await queryRunner.query(`COMMENT ON COLUMN "PURCHASES"."createAt" IS NULL`);
        await queryRunner.query(`ALTER TABLE "PURCHASES" ALTER COLUMN "date" SET DEFAULT '2021-03-25 13:45:33.94'`);
        await queryRunner.query(`COMMENT ON COLUMN "PURCHASES"."date" IS NULL`);
        await queryRunner.query(`ALTER TABLE "PAYMENTS" ALTER COLUMN "date" SET DEFAULT '2021-03-25 13:45:29.375'`);
        await queryRunner.query(`COMMENT ON COLUMN "PAYMENTS"."date" IS NULL`);
        await queryRunner.query(`ALTER TABLE "DETALLE" DROP COLUMN "imagenProducto"`);
    }

}
