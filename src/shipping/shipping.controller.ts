import { Body, Controller, HttpStatus, Param, Post, Put, Response } from '@nestjs/common';
import { ApiBadRequestResponse, ApiBody, ApiInternalServerErrorResponse, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { calculationDto } from './dto/calculationShipping.dto';
import { pedidoDto } from './dto/pedido.dto';
import { shippingComercio } from './dto/shiping-comercio.dto';
import { shippingCancelar } from './dto/shipping-cancelar.dto';
import { ShippingDto } from './dto/shippingDTO.dto';
import { shippingResponseDto } from './dto/shippingResponse.dto';
import { Shipping } from './shipping.entity';
import { ShippingService } from './shipping.service';
@Crud({
    model: {
        type: Shipping
    },
    routes: {
        exclude: ['createManyBase'],
        createOneBase: {
            decorators: [ApiBadRequestResponse({ description: 'Los datos no son los correctos' }),
            ApiInternalServerErrorResponse({ description: 'Error interno del servidor' })]
        }
    },
    dto: {
        create: ShippingDto,
    },
    serialize: {
        create: shippingResponseDto,
    },
    params: {
        id: {
            field: 'id',
            type: 'uuid',
            primary: true
        }
    },
    query: {
        join: {
            shippingTypes: {}
        }
    }
})
@ApiTags('Shipping')
@Controller('shipping')
export class ShippingController implements CrudController<Shipping> {
    constructor(public service: ShippingService) { }

    @Post('/cadygo/pedidoxComercio')
    @ApiOperation({ summary: 'crea comercio' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    @ApiBody({ type: shippingComercio })
    public async getPedidoComercios(@Response() res, @Body() shippingComer: shippingComercio) {
        let comercios = await this.service.getShippingComercio(shippingComer);
        return res.status(HttpStatus.OK).json(comercios)
    }

    @Post('/cadygo/crear-pedido')
    @ApiOperation({ summary: 'crea comercio' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    @ApiBody({ type: pedidoDto })
    public async crearComercios(@Response() res, @Body() comercio: pedidoDto) {
        let comercios = await this.service.generateShipping(comercio);
        return res.status(HttpStatus.OK).json(comercios)
    }

    @Post('/cadygo/calculation-envio')
    @ApiOperation({ summary: 'calcula el precio de envio' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    @ApiBody({ type: calculationDto })
    public async calculationShipping(@Response() res, @Body() datos: calculationDto) {
        let datesShipping = await this.service.calculation(datos);
        return res.status(HttpStatus.OK).json(datesShipping)
    }

    @Put('/cadygo/cancelar-pedido')
    @ApiOperation({ summary: 'cancelar-pedido' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    @ApiBody({ type: shippingCancelar })
    public async cancelarPedidos(@Response() res, @Body() cancelar: shippingCancelar) {
        let comercios = await this.service.cancelarPedido(cancelar);
        return res.status(HttpStatus.OK).json(comercios)
    }

}
