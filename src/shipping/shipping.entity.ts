import { ShippingTypes } from "../shipping-type/shipping-type.entity";
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToOne } from "typeorm";
import { Purchase } from "../purchases/purchase.entity";

@Entity('SHIPPING')
export class Shipping {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column({ type: "float8", nullable: true })
    costo: number;
    @Column('varchar', { length: 500, nullable: true })
    codigoEnvio: string;
    @Column('varchar', { length: 500, nullable: true })
    origen: string;
    @Column('varchar', { length: 500, nullable: true })
    destino: string;
    @Column('varchar', { length: 500, nullable: true })
    estadoEnvio: string;
    @OneToOne(() => Purchase, (purchases: Purchase) => purchases.shipping)
    purchases: Purchase;
    @ManyToOne(type => ShippingTypes, { cascade: true, eager: false })
    @JoinColumn({ name: 'shipping_types_id' })
    shippingTypes: ShippingTypes;

}