import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ShippingController } from './shipping.controller';
import { Shipping } from './shipping.entity';
import { ShippingService } from './shipping.service';

@Module({
    imports: [TypeOrmModule.forFeature([Shipping])],
    providers: [ShippingService],
    controllers: [ShippingController]
})
export class ShippingModule { }
