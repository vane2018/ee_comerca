import { ApiProperty } from "@nestjs/swagger";

export class calculationDto {
    @ApiProperty({ description: 'latitud de origen' })
    readonly sourceLatitude: number;
    @ApiProperty({ description: 'longitud de origen' })
    readonly sourceLongitude: number;
    @ApiProperty({ description: 'latitud de origen' })
    readonly targetLatitude: number;
    @ApiProperty({ description: 'latitud de origen' })
    readonly targetLongitude: number;


}