import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsUUID } from "class-validator";

export class modShipping {
    @ApiProperty({ description: 'id compra' })
    @IsNotEmpty()
    readonly id:string;
    @ApiProperty({ description: 'monto de la compra' })
    @IsNotEmpty()
    readonly costo: number;
    @ApiProperty({ description: 'monto de la compra' })
    @IsNotEmpty()
    readonly codigoEnvio: string;
    @ApiProperty({ description: 'origen del envio' })
    @IsNotEmpty()
    readonly origen: string;
    @ApiProperty({ description: 'destino del envio' })
    @IsNotEmpty()
    readonly destino: string;
    @ApiProperty({ description: 'estado del envio' })
    @IsNotEmpty()
    readonly estadoEnvio: string;
    @ApiProperty({ description: 'id del tipo de envio', type: String, format: 'uuid' })
    @IsNotEmpty()
    @IsUUID()
    readonly shippingTypes: string;

}