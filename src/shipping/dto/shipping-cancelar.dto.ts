import { ApiPreconditionFailedResponse, ApiProperty } from "@nestjs/swagger";

export class shippingCancelar {
    @ApiProperty({ required: true, description: 'Token provisto en datos de acceso o por mail' })
    externalOrderId: string;
}
