import { ApiProperty } from "@nestjs/swagger";

export class shippingResponseDto {
    @ApiProperty({ description: 'monto de la compra' })
    readonly costo: number;
    @ApiProperty({ description: 'monto de la compra' })
    readonly codigoEnvio: string;
    @ApiProperty({ description: 'origen del envio' })
    readonly origen: string;
    @ApiProperty({ description: 'destino del envio' })
    readonly destino: string;
    @ApiProperty({ description: 'estado del envio' })
    readonly estadoEnvio: string;

}