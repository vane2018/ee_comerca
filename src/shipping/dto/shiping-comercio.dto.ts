import { ApiPreconditionFailedResponse, ApiProperty } from "@nestjs/swagger";

export class shippingComercio {
    @ApiProperty({ required: true, description: 'Token provisto en datos de acceso o por mail' })
    commerceMarketplaceId: string;

}
