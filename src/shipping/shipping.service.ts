import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';
import { pedidoDto } from './dto/pedido.dto';
import { shippingComercio } from './dto/shiping-comercio.dto';
import { shippingCancelar } from './dto/shipping-cancelar.dto';
import { Shipping } from './shipping.entity';
import { calculationDto } from './dto/calculationShipping.dto';
import { ENV } from '../common/env.config';

const fetch = require("node-fetch");

@Injectable()
export class ShippingService extends TypeOrmCrudService<Shipping>{
    constructor(@InjectRepository(Shipping) repo: Repository<Shipping>) {
        super(repo)
    }

    public async getShippingComercio(mybody: shippingComercio) {

        return fetch(ENV.CADYGO_API + '/apis/v1/external-marketplace/orders/commerce/' + mybody.commerceMarketplaceId, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ENV.CADYGO_TOKEN,
            }
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                return data;
            })
            .catch(function (err) {
                console.error(err);
            });

    }


    public async cancelarPedido(mybody: shippingCancelar) {

        return fetch(ENV.CADYGO_API + '/apis/v1/external-marketplace/orders/' + mybody.externalOrderId, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ENV.CADYGO_TOKEN,
            }
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                return data;
            })
            .catch(function (err) {
                console.error(err);
            });

    }

    public async calculation(datos: calculationDto) {
        const mibody = {
            sourceLatitude: this.reduccion(datos.sourceLatitude),
            sourceLongitude: this.reduccion(datos.sourceLongitude),
            targetLatitude: this.reduccion(datos.targetLatitude),
            targetLongitude: this.reduccion(datos.targetLongitude)
        }
        return fetch(ENV.CADYGO_API + '/apis/v1/external-marketplace/shipping', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ENV.CADYGO_TOKEN,
            },
            body: JSON.stringify(mibody),
            cache: 'no-cache'
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                return data;
            })
            .catch(function (err) {
                console.error(err);
            });
    }

    public async generateShipping(pedido: pedidoDto) {
        const bo = {
            commerceMarketplaceId: pedido.commerceMarketplaceId,
            targetStreetName: pedido.targetStreetName,
            targetStreetNumber: pedido.targetStreetNumber,
            targetNeighborhood: pedido.targetNeighborhood,
            targetDirectionDetail: pedido.targetDirectionDetail,
            targetDirectionLatitude: pedido.targetDirectionLatitude,
            targetDirectionLongitude: pedido.targetDirectionLongitude,
            customer: pedido.customer,
            customerPhone: pedido.customerPhone,
            price: pedido.price,
            detail: pedido.detail
        }
        return fetch(ENV.CADYGO_API + '/apis/v1/external-marketplace/orders', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ENV.CADYGO_TOKEN,
            },
            body: JSON.stringify(bo),
            cache: 'no-cache'
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                return data;
            })
            .catch(function (err) {
                console.error(err);
            });

    }
    reduccion(valor: number) {
        let number = valor;

        let letra = number.toString().split("");
        let vect: string[] = [];

        let vect1: string[] = [];

        vect = letra;
        let j = 0;

        for (let index = 0; index < vect.length; index++) {
            if (vect[index] !== '.') {
                vect1[j] = vect[index];
                j = j + 1;
            }
            else {
                if (vect[index] === '.') {
                    vect1[j] = vect[index];
                    vect1[j + 1] = vect[index + 1];
                    vect1[j + 2] = vect[index + 2];
                    vect1[j + 3] = vect[index + 3];
                    vect1[j + 4] = vect[index + 4];
                    index = vect.length + 1;
                }
            }
        }
        if (vect1[7] == "0") [
            vect1[7] = "1"
        ]

        let precio = vect1.join('');
        let abs = parseFloat(precio);

        return abs;
    }
}
