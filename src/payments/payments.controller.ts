import { Controller, Body } from '@nestjs/common';
import { Crud, CrudController, Override, ParsedRequest, ParsedBody, CrudRequest } from '@nestjsx/crud';
import { Payment } from './payment.entity';
import { PaymentsService } from './payments.service'
import { ApiTags, ApiCreatedResponse, ApiBadRequestResponse, ApiOkResponse, ApiInternalServerErrorResponse } from '@nestjs/swagger';
import { PaymentDto } from './dto/payments.dto';
import { PaymentResponseDto } from './dto/paymentResponse.dto';
@Crud({
    model: {
        type: Payment
    },
    routes: {
        exclude: ['createManyBase'],
        createOneBase: {
            decorators: [ApiBadRequestResponse({ description: 'Los datos no son los correctos' }),
            ApiInternalServerErrorResponse({ description: 'Error interno del servidor' })]
        }
    },
    dto: {
        create: PaymentDto,
    },
    serialize: {
        create: PaymentResponseDto,
    },
    params: {
        id: {
            field: 'id',
            type: 'uuid',
            primary: true
        }
    },
    query: {
        join: {
            paymentTypes: {}
        }
    }
})

@ApiTags('Payments')
@Controller('payments')
export class PaymentsController implements CrudController<Payment> {
    constructor(public service: PaymentsService) { }
}
