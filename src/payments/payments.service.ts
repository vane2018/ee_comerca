import { Injectable } from '@nestjs/common';
import { InjectRepository, TypeOrmModule } from '@nestjs/typeorm';
import { Payment } from './payment.entity';
import { Repository } from 'typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';

@Injectable()
export class PaymentsService extends TypeOrmCrudService<Payment>{
    constructor(@InjectRepository(Payment) repo: Repository<Payment> ){
        super(repo)
    }
}
