import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToOne } from "typeorm";
import { PaymentTypes } from "../payment-types/payment-types.entity";

@Entity('PAYMENTS')
export class Payment {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column(({ type: "float8", nullable: true }))
    amount: number;
    @Column({ nullable: true, default: 0 })
    dues: number;
    @Column({ default: new Date() })
    date: Date;
    @ManyToOne(type => PaymentTypes, { cascade: true, eager: false })
    @JoinColumn({ name: 'payment_types_id' })
    paymentTypes: PaymentTypes  
 


}