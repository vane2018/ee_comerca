import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from "typeorm";
import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

@Entity('PAYMENT')
export class PaymentResponseDto {

    @ApiProperty({ description: 'id del pago' })   
    readonly id: number;
    @ApiProperty({ description: 'monto de la compra' })   
    readonly amount: number;
    @ApiPropertyOptional({ description: 'cantidad de cuotas de la compra, por defecto 0' })
    readonly dues: number;
    @ApiPropertyOptional({ description: 'fecha de la compra de la compra, por defecto fecha actual', type: Date })
    readonly date: Date;    
    
}