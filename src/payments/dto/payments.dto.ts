import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from "typeorm";
import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty, IsUUID } from "class-validator";

@Entity('PAYMENT')
export class PaymentDto {

    @ApiProperty({ description: 'monto de la compra' })
    @IsNotEmpty()
    readonly amount: number;
    @ApiPropertyOptional({ description: 'cantidad de cuotas de la compra, por defecto 0' })
    readonly dues: number;
    @ApiPropertyOptional({ description: 'fecha de la compra de la compra, por defecto fecha actual', type: 'string', format: 'date-time' })
    readonly date: Date;
    @ApiProperty({ description: 'id del tipo de pago', type: String, format: 'uuid' })
    @IsNotEmpty()
    @IsUUID()
    readonly paymentTypes: string;
}