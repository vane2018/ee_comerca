import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductTypes } from './product-category.entity';
import { ProductTypesController } from './product-category.controller';
import { ProductTypesService } from './product-category.service';
import { SubcategoryProductsModule } from '../subcategory-products/subcategory-products.module';

@Module({
  imports: [TypeOrmModule.forFeature([ProductTypes]),
    SubcategoryProductsModule],
  controllers: [ProductTypesController],
  providers: [ProductTypesService],
  exports: [ProductTypesService],
})

export class ProductTypesModule { }