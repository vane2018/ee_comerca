import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne, JoinColumn } from 'typeorm';
import { Product } from '../products/products.entity';
import { ProductSubcategory } from '../subcategory-products/subcategory-products.entity';
@Entity('PRODUCT_TYPES')
export class ProductTypes {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column('varchar', { length: 500 })
    name: string;


    @OneToMany(() => Product, (product: Product) => product.productType, {
        cascade: true,
    })
    @JoinColumn()
    products: Product[];

    @ManyToOne(() => ProductSubcategory, (productSubcategory: ProductSubcategory) => productSubcategory.productsTypes)
    @JoinColumn()
    productsSubcategory: ProductSubcategory;
}
