import { ApiProperty } from "@nestjs/swagger";


export class CreateCategoryProductDto {

    @ApiProperty({ description: 'Nombre subcategoria de producto' })
    readonly name: string;

    @ApiProperty({ description: 'id de categoria general' })
    readonly idSubcategoryProduct: string;




}