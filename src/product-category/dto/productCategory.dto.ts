import { ApiProperty } from '@nestjs/swagger';
import { subcategoryProductsDto } from '../../subcategory-products/dto/subcategory-product.dto';


export class ProductTypeDto {
    @ApiProperty({ description: 'Nombre del tipo de producto' })
    readonly name: string;


    @ApiProperty({ description: 'product type id' })
    productsSubcategory: subcategoryProductsDto;
}
