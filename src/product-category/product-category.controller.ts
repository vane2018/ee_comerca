import {
    Controller,
    Get,
    Response,
    HttpStatus,
    UseGuards,
    Post,
    Body,
    Param,
} from '@nestjs/common';
import { Crud } from '@nestjsx/crud';
import { ProductTypesService } from './product-category.service';
import { ApiTags, ApiOperation, ApiBody, ApiResponse } from '@nestjs/swagger';
import { ProductTypes } from './product-category.entity';
import { ProductTypeDto } from './dto/productCategory.dto';
import { CreateCategoryProductDto } from './dto/createCategoryProduct.dto';
import { SubcategoryProductsService } from '../subcategory-products/subcategory-products.service';

@Crud({
    model: {
        //type: ProductTypes,
        type: ProductTypeDto,
    },
    params: {
        id: {
            field: 'id',
            type: 'uuid',
            primary: true,
        },

    },
    query: {
        join: {
            productsSubcategory: {}
        }
    }
})

@ApiTags('Product-Category')
/* @UseGuards(PermissionGuard)
@Permision('Read', 'Write') */
@Controller('product-types')
export class ProductTypesController {
    constructor(
        public service: ProductTypesService,

        public serviceSubcategoria: SubcategoryProductsService
    ) { }

    @Post('register')
    public async saveSubcategory(@Response() res, @Body() createSubcategory: CreateCategoryProductDto): Promise<any> {
        try {
            let category = await this.serviceSubcategoria.findOne(createSubcategory.idSubcategoryProduct)
            if (category) {
                let subcategorySave: ProductTypeDto = {
                    name: createSubcategory.name,

                    productsSubcategory: category

                }
                const subctegory = await this.service.saveSubcategory(subcategorySave)

                return res.status(HttpStatus.OK).json(subctegory);
            } else {
                return res.status(HttpStatus.BAD_REQUEST).json({ message: 'gategoria no encontrada' })
            }
        } catch (error) {
            console.log('error al crear producto: ', error);
            return res.status(HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el producto', error });
        }
    }




    @Get('/listar')
    @ApiResponse({ status: 200, description: "OK" })
    @ApiResponse({ status: 400, description: "Solicitud incorrecta" })
    @ApiResponse({ status: 403, description: "Prohibido" })
    @ApiResponse({ status: 404, description: "No encontrado" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: "error server" })
    @ApiOperation({ summary: 'Creando producto de un  commercio' })
    @ApiBody({ type: ProductTypeDto })
    public async getTypeProducts(@Response() res): Promise<ProductTypes> {
        const productsTypes = await this.service.getAllProductTypes();
        return res.status(HttpStatus.OK).json(productsTypes);
    }


    @Get()
    /* @UseGuards(AuthGuard('jwt')) */
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    @ApiOperation({ summary: 'obtiene todas las categorias de producto' })
    public async getcategoryProduct(@Response() res): Promise<any> {
        const categoriaProduct = await this.service.getcategoryProduct();

        return res.status(HttpStatus.OK).json(categoriaProduct);
    }


    @Get('/:id')
    /* @UseGuards(AuthGuard('jwt')) */
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: '\'The resource was not found\'' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getProductsId(@Response() res, @Param() param): Promise<any> {
        const products = await this.service.abtenerproduct(param.id);
        return res.status(HttpStatus.OK).json(products);
    }
}
