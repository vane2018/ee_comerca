import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductTypes } from './product-category.entity';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { ProductTypeDto } from './dto/productCategory.dto';
import { Product } from '../products/products.entity'

@Injectable()
export class ProductTypesService extends TypeOrmCrudService<ProductTypeDto>{
    constructor(@InjectRepository(ProductTypes) repo) {
        super(repo);
    }

    public async saveSubcategory(subcategoryDto: ProductTypeDto): Promise<ProductTypeDto> {
        try {
            let productoSubcategoria = this.repo.create(subcategoryDto);
            return await this.repo.save(productoSubcategoria);
        } catch (error) {
            console.log('error: ', error);
        }
    }

    public getAllProductTypes = async (): Promise<ProductTypeDto[]> => {
        return await this.repo.find({ relations: ['products'] });
    }

    public async getcategoryProduct(): Promise<ProductTypeDto[]> {
        try {
            return await this.repo.find({ relations: ['products'] });
        } catch (error) {
            console.log('error: ', error);
        }
    }

    public async getProductsCategorId(categoryId: string): Promise<any> {
        try {
            let product: Product[] = [];
            let categorias = await this.repo.findOne(categoryId, { relations: ['products'] });
            return categorias;
        } catch (error) {
            console.log("ocurrio un error en categoryProduct", error);
        }
    }

    public async abtenerproduct(cat: string): Promise<any> {
        var product: Product[] = [];
        try {
            let prod = await this.getProductsCategorId(cat);
            if (prod.products.length > 0) {
                for (let index = 0; index < prod.products.length; index++) {
                    if (prod.products[index].deletedDate === null) {
                        product.push(prod.products[index])
                    }
                }
                prod.products = product;
                return prod;
            } else {
                return prod;
            }
        } catch (error) {
            return error;
        }
    }
}
