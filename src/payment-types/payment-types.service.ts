import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PaymentTypes, paymentTypes } from './payment-types.entity';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';

@Injectable()
export class PaymentTypesService extends TypeOrmCrudService<PaymentTypes>{
    constructor(@InjectRepository(PaymentTypes) repo){
        super(repo)
    }
}