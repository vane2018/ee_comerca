import { Test, TestingModule } from '@nestjs/testing';
import { PaymentTypesController } from './payment-types.controller';

describe('PymentTypes Controller', () => {
  let controller: PaymentTypesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PaymentTypesController],
    }).compile();

    controller = module.get<PaymentTypesController>(PaymentTypesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
