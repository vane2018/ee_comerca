import { paymentTypes } from "../payment-types.entity";
import { IsEnum } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class PaymentTypesDto {
    @ApiProperty({description: 'nombre del tipo de pago'})
    @IsEnum(paymentTypes)
    readonly name: paymentTypes;
    @ApiProperty({description: 'descripción del tipo de pago'})
    readonly description : string;
}