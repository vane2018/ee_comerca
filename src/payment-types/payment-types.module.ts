import { Module } from '@nestjs/common';
import { PaymentTypesService } from './payment-types.service';
import { PaymentTypesController } from './payment-types.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PaymentTypes } from './payment-types.entity';

@Module({
  imports: [TypeOrmModule.forFeature([PaymentTypes])],
  providers: [PaymentTypesService],
  controllers: [ PaymentTypesController]
})
export class PymentTypesModule {}
