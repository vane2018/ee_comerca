import { Controller } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { PaymentTypes } from './payment-types.entity';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { PaymentTypesService } from './payment-types.service';
import { PaymentTypesDto } from './dto/payment-types.dto';

@Crud({
    model: {
        type: PaymentTypes},
    routes: {
        exclude: ['createManyBase', 
        'deleteOneBase', 
        'updateOneBase', 
        'replaceOneBase', 
        'createOneBase', 
        'getOneBase'],
        getManyBase: {
            decorators: [ApiOkResponse({description: 'datos devueltos correctamente', type: [PaymentTypesDto]})]
        }
    },
    serialize: {
        getMany: PaymentTypesDto,
    },
    params: {
        id: {
            field: 'id',
            type: 'uuid',
            primary: true
        },
    },
})
@ApiTags('Payment-Tipes')
@Controller('payment-types')
export class PaymentTypesController implements CrudController<PaymentTypes> {
    constructor(public service: PaymentTypesService ) {}
}
