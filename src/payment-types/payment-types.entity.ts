import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

export enum paymentTypes {
    EFECTIVO = 'Efectivo',
    CREDITO = 'Credito',
    DEBITO = 'Debito'
}
@Entity('PAYMENT_TYPES')
export class PaymentTypes {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column()
    name: paymentTypes;
    @Column()
    description: string;
}