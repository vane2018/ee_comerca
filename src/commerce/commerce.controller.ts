import { Controller, Post, Response, Body, HttpStatus, Get, Param, Put } from '@nestjs/common';
import { Crud } from '@nestjsx/crud';
import { CommerceService } from './commerce.service';
import { ApiTags, ApiOperation, ApiCreatedResponse, ApiBody, ApiResponse } from '@nestjs/swagger';
import { Commerce } from './commerce.entity';
import { CommerceDTO } from './dto/commerce.dto';
import { CreateCommerceDTO } from './dto/createCommerce.dto';
import { UsersService } from '../users/user.service';
import { RolsService } from '../rols/rols.service';
import { localDto } from './dto/localDTO.dto';
import { depaDto } from './dto/departamento.dto';
import { provinciaDto } from './dto/provincia.dto';
import { ubicacionDto } from './dto/ubicacion.dto';
import { updateDTO } from './dto/update.dto';
import { obtenerComercio } from './dto/obtenerComercio.dto';
import { localidadCadygo } from './dto/localidadCadygo.dto';
import { create } from 'node:domain';
import { CreateProductDto } from '../products/dto/createProduct.dto';

@Crud({
    model: {
        type: Commerce,
    },
    params: {
        id: {
            field: 'id',
            type: 'uuid',
            primary: true,
        },
    },
    query: {
        join: {
            users: {
                allow: [],
            },
            products:{
                allow:[]
            }
        },
    },
})

@ApiTags('Commerces')
@Controller('commerces')
export class CommerceController {
    constructor(
        public service: CommerceService,
        public userService: UsersService,
        private readonly rolsService: RolsService
    ) { }

    @Post('register')
    @ApiOperation({ summary: 'Registra un commercio' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    @ApiCreatedResponse({ description: 'The Commerce has been successfully created.', type: CommerceDTO, status: 200 })
    @ApiBody({ type: CreateCommerceDTO })
    public async postCommerce(@Response() res, @Body() createCommerceDto: CreateCommerceDTO): Promise<CommerceDTO> {
        try {
            let seller = await this.userService.findOne(createCommerceDto.sellerId);
            if (seller) {
                const commerceACrear = { seller, ...createCommerceDto.commerce } as unknown as CommerceDTO;
                commerceACrear.user = await this.userService.findOne(createCommerceDto.sellerId);
                const resp = await this.userService.findById(createCommerceDto.sellerId);
                const rol = await this.rolsService.findRol({ name: 'seller' });
                await this.userService.updateUser(resp.user, rol);
                commerceACrear.user = await this.userService.findOne(createCommerceDto.sellerId);
                commerceACrear.pisoDepto= createCommerceDto.commerce.pisoDepto;
                const commerce = await this.service.saveCommerce(commerceACrear);
                console.log("COMERCIO GUARDADO", commerce);

                return res.status(HttpStatus.OK).json(commerce);
            } else {

            }
        } catch (error) {

        }

    }
    @Get()
    @ApiOperation({ summary: 'Obtiene todos los comercios' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getSellers(@Response() res): Promise<any> {
        let commerces = await this.service.getAllCommerces();
        return res.status(HttpStatus.OK).json(commerces);

    }
    @Post('/localidad/cadygo')
    @ApiOperation({ summary: 'Obtiene lacalidades cadygo' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async obtenerLocalidadCadygo(@Response() res, @Body() localidadDTO: localidadCadygo): Promise<any> {
        let commerces = await this.service.obtenerlocalidades(localidadDTO.localidad);
        return res.status(HttpStatus.OK).json(commerces);
    }

    @Post('/cadygo/crear-comercio')
    @ApiOperation({ summary: 'crea comercio' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    @ApiBody({ type: obtenerComercio })
    public async crearComercios(@Response() res, @Body() comercio: obtenerComercio) {
        try {
            let comercios = await this.service.crearComercio(comercio);
            return res.status(HttpStatus.OK).json(comercios)
        } catch (error) {
            console.log(error);
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(error);
        }
    }

    @Post('provincias')
    @ApiOperation({ summary: 'Obtiene todas las provincias' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getProvincias(@Response() res, @Body() provDto: provinciaDto) {
        let provincias = await this.service.obtenerprovincia(provDto.pais);
        return res.status(HttpStatus.OK).json(provincias)
    }

    @Get('pais')
    @ApiOperation({ summary: 'Obtiene todas los Paises' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getPais(@Response() res) {
        let pais = await this.service.getpaises();
        return res.status(HttpStatus.OK).json(pais)
    }

    @Post('localidad')
    @ApiOperation({ summary: 'Obtiene todas las localidades' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getlocalidad(@Response() res, @Body() localidadDTO: localDto) {

        console.log("entro a localidades");

        let pais = await this.service.getlocalidades(localidadDTO.provincia);
        return res.status(HttpStatus.OK).json(pais)
    }

    @Post('departamentos')
    @ApiOperation({ summary: 'Obtiene todos los departamentos' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getDepartamentp(@Response() res, @Body() departamentoDTO: depaDto) {
        let pais = await this.service.getdepartamento(departamentoDTO.provincia);
        return res.status(HttpStatus.OK).json(pais)
    }

    @Post('ubicacion')
    @ApiOperation({ summary: 'busquedax ubicacion' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getProductUbicacion(@Response() res, @Body() ubicDTO: ubicacionDto) {
        let pais = await this.service.searchUbicacion(ubicDTO.provincia);
        return res.status(HttpStatus.OK).json(pais)
    }

    @Get('/paises/provincia')
    @ApiOperation({ summary: 'obtiene paises con provincias' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async gettodos(@Response() res) {
        let tod = await this.service.obtenerTodo();
        return res.status(HttpStatus.OK).json(tod);
    }
    @Get('/:id')
    @ApiOperation({ summary: 'Obtiene un comercio por su Id' })
    @ApiResponse({ status: 200, description: 'Los datos han sido devueltos correctamente.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getCommerce(@Response() res, @Param() param): Promise<any> {
        const commerce = await this.service.getCommerceById(param.commerceId);
        return res.status(HttpStatus.OK).json(commerce);
    }



    @Put('/edit/:id')
    public async putProducts(@Response() res, @Param() param, @Body() createProductDto: updateDTO): Promise<any> {
        try {

            let product = await this.service.findOne(param.id);
            if (product) {
                let productMod = {
                    id: param.id,
                    name: createProductDto.name,
                    adress: createProductDto.adress,
                    numero: createProductDto.numero,
                    cuit: createProductDto.cuit,
                    pais: createProductDto.pais,
                    provincia: createProductDto.provincia,
                    localidad: createProductDto.localidad,
                    latitud: createProductDto.latitud,
                    longitud: createProductDto.longitud,
                    profilePic: createProductDto.profilePic,
                    coverPic: createProductDto.coverPic,
                    pisoDepto: createProductDto.pisoDepto
                }

                const producto = await this.service.updateComerce(productMod);
                return res.status(HttpStatus.OK).json(producto);
            } else {
                return res.status(HttpStatus.BAD_REQUEST).json({ message: ' producto no encontrados' });
            }
        } catch (error) {
            console.log('error al modifcar producto: ', error);
            return res.status(HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el producto', error });
        }
    }
}
