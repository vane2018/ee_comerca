import { Injectable, Inject, forwardRef, HttpService } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Commerce } from './commerce.entity';
import { CommerceDTO } from './dto/commerce.dto';
import { Product } from '../products/products.entity';
import * as countries from '../resource/countries.json';
import * as  paises from '../resource/paises.json'
import * as localidades from '../resource/localidades.json'
import { getRepository } from 'typeorm';
import { updateDTO } from './dto/update.dto';
import { obtenerComercio } from './dto/obtenerComercio.dto';
import { UsersService } from '../users/user.service';
import { ENV } from '../common/env.config';
import { ProductsService } from '../products/products.service';

const fetch = require("node-fetch");

@Injectable()
export class CommerceService extends TypeOrmCrudService<Commerce> {
    constructor(@InjectRepository(Commerce) repo,
        public userService: UsersService

    ) {
        super(repo);
    }

    public async saveCommerce(commerceDto: CommerceDTO): Promise<Commerce> {



        let commerce = this.repo.create(commerceDto);
        console.log("comercio creado", commerceDto);
        return await this.repo.save(commerce);
    }

    public async updateComerce(commerce: updateDTO): Promise<any> {
        let comercio = this.repo.create(commerce);
        return await this.repo.save(comercio);

    }

    public async getAllCommerces(): Promise<any[]> {
        return await this.repo.find({ relations: ['user', 'products'] });
    }

    public async compruebaExistencia(id: string) {
        let commerce = this.repo.findOne(id);
        return commerce;
    }

    public async obtenerlocalidades(localidad: string) {
        var vector = [];
        return fetch(ENV.CADYGO_API + '/apis/localidades/v1', {
            method: 'GET'
        })
            .then((response) => {
                return response.json()
            })
            .then((localidades) => {
                let arrayL = localidades.items;

                var minuscula = localidad.toLowerCase()

                for (let index = 0; index < arrayL.length; index++) {
                    let n = arrayL[index].nombre.toLowerCase();
                    vector[index] = n;
                }
                for (let i = 0; i < vector.length; i++) {
                    if (vector[i] == minuscula) {
                        var localid = arrayL[i];
                    }
                }
                if (localid === undefined) {
                    return [];

                } else {
                    return localid;
                }
            })
    }


    public async crearComercio(comercce: obtenerComercio) {
        const cuer = {
            commerceMarketplaceId: comercce.commerceMarketplaceId,
            marketplace: comercce.marketplace,
            name: comercce.name,
            localidadId: comercce.localidadId,
            streetName: comercce.streetName,
            streetNumber: comercce.streetNumber,
            directionLatitude: comercce.directionLatitude,
            directionLongitude: comercce.directionLongitude,
            phone: comercce.phone,
            adminEmail: comercce.adminEmail,
            adminName: comercce.adminName,
        }
        return fetch(ENV.CADYGO_API + '/apis/v1/external-marketplace/commerce', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ENV.CADYGO_TOKEN,
            },
            body: JSON.stringify(cuer),
            cache: 'no-cache'
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                return data;
            })
            .catch(function (err) {
                console.error(err);
            });

    }



    public async getCommerceById(id: string): Promise<any> {
        let product: Product[] = [];
        let commerce = await this.repo.findOne(id, { relations: ['user', 'products','products.productType'] });
console.log(commerce);

        for (let index = 0; index < commerce.products.length; index++) {
            if (commerce.products[index].deletedDate === null) {
                product.push(commerce.products[index])
            }
        }
        commerce.products = product;
        return commerce;
    }

    public async addProducts(idCommerce: string, product: any): Promise<any> {
        let commerce = await this.repo.findOne(idCommerce, { relations: ['products'] });

        commerce.products.push(product);
        let commerceCreate = this.repo.create(commerce);
        await this.repo.save(commerce);
    }

    public async searchUserCommerce(ids: string[]): Promise<any> {
        let commerce = await this.repo.findByIds(ids, { relations: ['user'] });
        return commerce;
    }

    public async searchUser(ids: string): Promise<any> {
        let commerce = await this.userService.findOne(ids);
        return commerce;
    }



    public async getDistanciaMetros(lat1, lon1, localidad) {
        let distancias: number[] = [];
        let latitud: string[][] = [];
        let i = 0;
        let j = 0;
        let comercios = await getRepository(Commerce)
            .createQueryBuilder("commerce")
            .getMany();
        for (let index = 0; index < comercios.length; index++) {
            if (comercios[index].localidad == localidad) {
                let rad = function (x) { return x * Math.PI / 180; }
                var R = 6378.137; //Radio de la tierra en km 
                let dLat = rad(parseFloat(comercios[index].latitud) - lat1);
                var dLong = rad(parseFloat(comercios[index].longitud) - lon1);

                var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(lat1)) *
                    Math.cos(rad(parseFloat(comercios[index].latitud))) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
                var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

                //aquí obtienes la distancia en metros por la conversion 1Km =1000m
                var d = R * c * 1000;
                if (d < 10) {

                    distancias.push(d);
                }
            }
        }
        return d;
    }



    public async obtenerprovincia(pais: string): Promise<any> {
        let provincia;
        for (let index = 0; index < countries.length; index++) {
            if (countries[index].name === pais) {
                provincia = countries[index].states
            }
        }
        console.log(provincia);

        return provincia;
    }

    public async getpaises(): Promise<any> {
        let pais = paises.countries;

        return pais;
    }

    public async getdepartamento(id: string): Promise<any> {
        return fetch('https://apis.datos.gob.ar/georef/api/departamentos?provincia=' + id + '&aplanar=true&campos=estandar&max=20&exacto=true')
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                return data.departamentos;
            })
            .catch(function (err) {
                console.error(err);
            });
    }


    public async getlocalidades(provin: string): Promise<any> {
        let provincia;
        let number = localidades.length;

        for (let index = 0; index < number; index++) {

            if (localidades[index].provincia === provin) {

                provincia = localidades[index].localidad
                console.log("LAS LOCALIDADES DE LA PROVINCIA SON", provincia);

            }
        }
        return provincia;
    }

    public async searchUbicacion(name: string): Promise<any> {

        let productos: Product[] = [];
        var j = 0;

        let a = await getRepository(Commerce)
            .createQueryBuilder("commerce")
            .innerJoinAndSelect("commerce.products", "products")
            .where("commerce.provincia = :id", { id: name })
            .getMany();


        for (let index = 0; index < a.length; index++) {
            for (let j = 0; j < a[index].products.length; j++) {
                if (a[index].products[j].deletedDate === null) {
                    productos.push(a[index].products[j])
                }
            }
        }
        return productos;
    }

    public async obtenerTodo() {
        return countries;
    }
}
