import { ApiProperty } from "@nestjs/swagger";

export class localidadCadygo {

    @ApiProperty({ required: true, description: 'Nombre del marketplace dado en acceso.' })
    localidad: string;
}