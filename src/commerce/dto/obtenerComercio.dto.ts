import { ApiProperty } from "@nestjs/swagger";

export class obtenerComercio {

    @ApiProperty({ required: true, description: 'Nombre del marketplace dado en acceso.' })
    marketplace: string;
    @ApiProperty({ required: true, description: 'id del vendedor' })
    commerceMarketplaceId: string;
    @ApiProperty({ required: true, description: 'id del vendedor' })
    name: string;
    @ApiProperty({ required: true, description: 'id del vendedor' })
    localidadId: string;
    @ApiProperty({ required: true, description: 'id del vendedor' })
    streetName: string;
    @ApiProperty({ required: true, description: 'id del vendedor' })
    streetNumber: string;
    @ApiProperty({ required: true, description: 'id del vendedor' })
    directionLatitude: string;
    @ApiProperty({ required: true, description: 'id del vendedor' })
    directionLongitude: string;
    @ApiProperty({ required: true, description: 'id del vendedor' })
    phone: string;
    @ApiProperty({ required: true, description: 'id del vendedor' })
    adminEmail: string;
    @ApiProperty({ required: true, description: 'id del vendedor' })
    adminName: string;

}
