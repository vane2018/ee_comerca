import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsString, IsNotEmpty, IsNumber, IsOptional } from "class-validator";

export class updateDTO {
    id: string;
    @ApiProperty({ required: true, description: 'Nombre del comercio' })
    @IsString()
    @IsOptional()
    name: string;

    @ApiProperty({ required: true, description: 'Direccion' })
    @IsString()
    @IsOptional()
    adress: string;

    @ApiProperty({ required: true, description: 'Direccion' })
    @IsString()
    @IsOptional()
    numero: string;

    @ApiProperty({ required: true, description: 'CUIT ' })
    @IsNumber()
    @IsOptional()
    cuit: number;

    @ApiProperty({ required: true, description: 'PAIS' })
    @IsOptional()
    pais: string;

    @ApiProperty({ required: true, description: 'PROVINCIA' })
    @IsOptional()
    provincia: string;

    @ApiProperty({ required: true, description: 'PROVINCIA' })
    @IsOptional()
    localidad: string;

    @ApiProperty({ required: true, description: 'PROVINCIA' })
    @IsOptional()
    latitud: string;
    @ApiProperty({ required: true, description: 'PROVINCIA' })
    @IsOptional()
    longitud: string;

    @ApiPropertyOptional({ description: 'Foto de perfil del comercio' })
    @IsString()
    @IsOptional()
    profilePic: string;

    @ApiPropertyOptional({ description: 'Foto de portada del comercio' })
    @IsString()
    @IsOptional()
    coverPic: string;
    @ApiProperty({ required: true, description: 'PROVINCIA' })
    @IsString()
    @IsOptional()
    pisoDepto: string;
}
