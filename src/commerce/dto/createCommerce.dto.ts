import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsUUID, IsNumber, IsNotEmpty, } from 'class-validator';
import { CommerceDTO } from './commerce.dto';

export class CreateCommerceDTO implements Readonly<CreateCommerceDTO> {

  @ApiProperty({ required: true, description: 'id del vendedor' })
  @IsString()
  @IsNotEmpty()
  readonly sellerId: string;

  @ApiProperty({ required: true, description: 'Commercio' })
  readonly commerce: CommerceDTO;
}
