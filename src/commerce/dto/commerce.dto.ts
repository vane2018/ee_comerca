import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsString, IsUUID, IsNumber, IsOptional, IsNotEmpty, MinLength, Min, Max, } from 'class-validator';
import { Commerce } from '../commerce.entity';
import { User } from '../../users/user.entity';

// import { User } from '../user.decorator';

export class CommerceDTO implements Readonly<CommerceDTO> {

  @ApiProperty({ required: true, description: 'Nombre del comercio' })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({ required: true, description: 'Direccion' })
  @IsString()
  adress: string;

  @ApiProperty({ required: true, description: 'CUIT ' })
  @IsNumber()
  cuit: number;

  @ApiProperty({ required: true, description: 'PAIS' })
  @IsOptional()
  pais: string;

  @ApiProperty({ required: true, description: 'PROVINCIA' })
  @IsOptional()
  provincia: string;

  @ApiProperty({ required: true, description: 'PROVINCIA' })
  @IsOptional()
  localidad: string;

  @ApiProperty({ required: true, description: 'PROVINCIA' })
  @IsOptional()
  numero: string;


  @ApiProperty({ required: true, description: 'PROVINCIA' })
  @IsOptional()
  latitud: string;
  @ApiProperty({ required: true, description: 'PROVINCIA' })
  @IsOptional()
  longitud: string;

  @ApiPropertyOptional({ description: 'Foto de perfil del comercio' })
  @IsString()
  @IsOptional()
  profilePic: string;

  @ApiPropertyOptional({ description: 'Foto de portada del comercio' })
  @IsString()
  @IsOptional()
  coverPic: string;

  @ApiPropertyOptional({ description: 'Foto de portada del comercio' })
  @IsString()
  @IsOptional()
  user: User;

  @ApiProperty({ required: true, description: 'PROVINCIA' })
  @IsString()
  @IsOptional()
  pisoDepto: string;

  /*  @ApiProperty({ description: 'Vendedor', nullable: true })
   
   seller: SellerDto; */


  /* public static from(dto: Partial<CommerceDTO>) {
    const com = new CommerceDTO();
    // com.id = dto.id;
    com.name = dto.name;
    com.adress = dto.adress;
    com.cuit = dto.cuit;
    com.profilePic = dto.profilePic;
    com.coverPic = dto.coverPic;
    return com;
  }
 */
  /* public static fromEntity(entity: Commerce) {
    return this.from({
      // id: entity.id,
      name: entity.name,
      adress: entity.adress,
      cuit: entity.cuit,
      profilePic: entity.profilePic,
      coverPic: entity.coverPic,
  } */

  /* public toEntity() {
    const com = new Commerce();
    // com.id = this.id;
    com.name = this.name;
    com.adress = this.adress;
    com.cuit = this.cuit;
    com.profilePic = this.profilePic;
    com.coverPic = this.coverPic;
    it.createDateTime = new Date();
    it.createdBy = user ? user.id : null;
    it.lastChangedBy = user ? user.id : null;
    return com;
  } */
}
