import { MercadoPago } from '../mercado-pago/mercado-pago.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany, OneToOne, JoinColumn } from 'typeorm';
import { Product } from '../products/products.entity';
import { User } from '../users/user.entity';
import { Mercado } from '../mercado/mercado.entity';

@Entity('COMMERCES')
export class Commerce {

    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column('varchar', { length: 500, unique: true })
    name: string;
    @Column('varchar', { length: 500 })
    adress: string;
    @Column('numeric')
    cuit: number;
    @Column('varchar', { length: 500, nullable: true })
    profilePic: string;
    @Column('varchar', { length: 500, nullable: true })
    coverPic: string;
    @OneToOne(type => User, (user: User) => user.commerce)
    @JoinColumn()
    user: User;
    @Column('varchar', { length: 500, nullable: true })
    pais: string;
    @Column('varchar', { length: 500, nullable: true })
    provincia: string;
    @Column('varchar', { length: 500, nullable: true })
    localidad: string;
    @Column('varchar', { length: 500, nullable: true })
    latitud: string;
    @Column('varchar', { length: 500, nullable: true })
    longitud: string;
    @Column('varchar', { length: 500, nullable: true })
    numero: string;
    @Column('varchar', { length: 500, nullable: true })
    pisoDepto: string;
    @OneToMany(type => Product, product => product.commerce)
    products: Product[];
    @OneToOne(type => MercadoPago, (mercado: MercadoPago) => mercado.commerce)
    @JoinColumn()
    mercado: MercadoPago;
    @OneToOne(type => Mercado, (mercado: Mercado) => mercado.commerce)
    @JoinColumn()
    merc: Mercado;

}
