import { Module } from '@nestjs/common';
import { CommerceService } from './commerce.service';
import { CommerceController } from './commerce.controller';
import { Commerce } from './commerce.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '../users/user.module';
import { RolsModule } from '../rols/rols.module';



@Module({
        imports: [
                TypeOrmModule.forFeature([Commerce]),
                RolsModule,
                UsersModule],
        controllers: [CommerceController],
        providers: [CommerceService],
        exports: [CommerceService]
})
export class CommerceModule { }
