import { PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, JoinColumn, Entity } from "typeorm";
import { ProductTypes } from "../product-category/product-category.entity";
import { ProductGeneral } from "../category-product-general/category-product-general.entity";
@Entity('SUBCATEGORY_PRODUCTS')
export class ProductSubcategory {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column('varchar', { length: 500 })
    name: string;


    @OneToMany(() => ProductTypes, (productType: ProductTypes) => productType.productsSubcategory)
    productsTypes: ProductTypes[];

    @ManyToOne(() => ProductGeneral, (productGeneral: ProductGeneral) => productGeneral.productsSubcategory)
    @JoinColumn()
    productsGeneral: ProductGeneral;
}