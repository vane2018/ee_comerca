import { Module } from '@nestjs/common';
import { SubcategoryProductsController } from './subcategory-products.controller';
import { SubcategoryProductsService } from './subcategory-products.service';
import { ProductSubcategory } from './subcategory-products.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoryGeneralModule } from '../category-product-general/category-product-general.module';

@Module({
  imports: [TypeOrmModule.forFeature([ProductSubcategory]),
    CategoryGeneralModule],
  controllers: [SubcategoryProductsController],
  providers: [SubcategoryProductsService],
  exports: [SubcategoryProductsService],
})
export class SubcategoryProductsModule { }
