import { Controller, Post, Body, HttpStatus, Response, Get, Param } from '@nestjs/common';
import { Crud } from '@nestjsx/crud';
import { subcategoryProductsDto } from './dto/subcategory-product.dto';
import { CategoryGeneralService } from '../category-product-general/category-product-general.service';
import { CreateSucategoryProductDto } from './dto/createSubcategoryProduct.dto';
import { SubcategoryProductsService } from './subcategory-products.service';
import { ApiTags, ApiOperation, ApiResponse, ApiBody } from '@nestjs/swagger';
import { productDto } from './dto/subcategoryDto.dto';

@Crud({
    model: {
        type: subcategoryProductsDto,
    },
    params: {
        id: {
            field: 'id',
            type: 'uuid',
            primary: true,
        },
    },
    query: {
        join: {

            productsGeneral: {},
            productsSubcategory: {
                allow: []
            }
        }
    }
})
@ApiTags('Product-Subcategory')
@Controller('subcategory-products')
export class SubcategoryProductsController {
    constructor(
        public service: SubcategoryProductsService,
        public serviceGeneral: CategoryGeneralService
    ) { }

    @Post('register')
    public async saveSubcategory(@Response() res, @Body() createSubcategory: CreateSucategoryProductDto): Promise<any> {
        try {
            let category = await this.serviceGeneral.findOne(createSubcategory.idProductGeneral)
            if (category) {
                let subcategorySave: subcategoryProductsDto = {
                    name: createSubcategory.name,
                    productsGeneral: category
                }
                const subctegory = await this.service.saveSubcategory(subcategorySave)

                return res.status(HttpStatus.OK).json(subctegory);
            } else {
                return res.status(HttpStatus.BAD_REQUEST).json({ message: 'categoria no encontrada' })
            }
        } catch (error) {
            console.log('error al crear producto: ', error);
            return res.status(HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el producto', error });
        }
    }


    @Get()
    /* @UseGuards(AuthGuard('jwt')) */
    @ApiOperation({ summary: 'obtiene todas las Subcategorias de producto' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })

    public async getSubcategory(@Response() res): Promise<any> {
        const subCategoriaProduct = await this.service.getcategorySubcategory();
        return res.status(HttpStatus.OK).json(subCategoriaProduct);
    }

    @Get('/:id')
    @ApiOperation({ summary: 'Obtiene subcategoria por su Id, de aqui obtengo un [] de categoria de producto' })
    @ApiResponse({ status: 200, description: 'Los datos han sido devueltos correctamente.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getCommerce(@Response() res, @Param() param): Promise<any> {
        console.log("entro a subcategoria");
        
        const categoryProducts = await this.service.getCategoryById(param.id);
        return res.status(HttpStatus.OK).json(categoryProducts);
    }

    @Post('product-list/id')
    @ApiOperation({ summary: 'Obtiene subcategoria por su Id, de aqui obtengo un [] de categoria de producto' })
    @ApiResponse({ status: 200, description: 'Los datos han sido devueltos correctamente.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    @ApiBody({ type: productDto })
    public async getCommerceProduct(@Response() res,@Body() productDto, @Param() param): Promise<any> { 
        const categoryProducts = await this.service.getCategoryByIdProduct(productDto.id);
        return res.status(HttpStatus.OK).json(categoryProducts);
    }
}


