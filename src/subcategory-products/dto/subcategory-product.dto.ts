import { PrimaryGeneratedColumn, Column } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { CategoryProductGeneralDto } from "../../category-product-general/dto/categoryProductGeneral.dto";

export class subcategoryProductsDto {
    @ApiProperty({ description: 'Product name' })

    readonly name: string;


    @ApiProperty({ description: 'product type id' })
    productsGeneral: CategoryProductGeneralDto;


}