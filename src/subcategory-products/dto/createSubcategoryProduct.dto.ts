import { ApiProperty } from "@nestjs/swagger";

export class CreateSucategoryProductDto {

    @ApiProperty({ description: 'Nombre subcategoria de producto' })
    readonly name: string;

    @ApiProperty({ description: 'id de categoria general' })
    readonly idProductGeneral: string;


}