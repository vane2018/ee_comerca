import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { subcategoryProductsDto } from './dto/subcategory-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductSubcategory } from './subcategory-products.entity';
import { CreateSucategoryProductDto } from './dto/createSubcategoryProduct.dto';

@Injectable()
export class SubcategoryProductsService extends TypeOrmCrudService<subcategoryProductsDto>{
    constructor(@InjectRepository(ProductSubcategory) repo) {
        super(repo);
    }

    public async saveSubcategory(subcategoryDto: subcategoryProductsDto): Promise<subcategoryProductsDto> {
        try {
            // productDto.pic = Buffer.from(productDto.pic).toString('base64');
            let productoSubcategoria = this.repo.create(subcategoryDto);
            return await this.repo.save(productoSubcategoria);
        } catch (error) {
            console.log('error: ', error);
        }
    }


    public async getcategorySubcategory(): Promise<subcategoryProductsDto[]> {

        try {
            return await this.repo.find({ relations: ['productsTypes'] });
        } catch (error) {
            console.log('error: ', error);
        }
    }



    public async getSubcategoriaByGeneralId(categoryId: string): Promise<any> {
        let subcategorias = await this.repo.find({ where: { productsGeneral: categoryId }/* , relations: ['user'] */ });
        return subcategorias;
    }

    public async getCategoryById(id: string): Promise<any> {
        let categoriaProduct = await this.repo.findOne(id, { relations: ['productsTypes'] });
        return categoriaProduct;
    }

    public async getCategoryByIdProduct(id: string): Promise<any> {
        let categoriaProduct = await this.repo.findOne(id, { relations: ['productsTypes','productsTypes.products'] });
        return categoriaProduct;
    }
}
