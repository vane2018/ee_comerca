import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import * as basicAuth from 'express-basic-auth';
import { json } from 'express';
import { ValidationPipe } from '@nestjs/common/pipes/validation.pipe';
import * as fbase from 'firebase-admin';
import { ServiceAccount } from 'firebase-admin';
import { ENV } from './common/env.config';

const API_DEFAULT_PREFIX = '/api/v1';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  /*   agrego esta linea para validaciones */
  app.useGlobalPipes(new ValidationPipe());
  app.use(json({ limit: '50mb' }));  
  app.enableCors();
  app.setGlobalPrefix(process.env.API_PREFIX || API_DEFAULT_PREFIX);
  const options = new DocumentBuilder()
    .setTitle('Comprartir Api')
    .setDescription('The API for e-commerce')
    .setVersion('1.0')
    // .addBearerAuth({ type: 'http', scheme: 'bearer', bearerFormat: 'JWT' }, 'JWT')
    .build();

  // Set the config options
  const adminConfig: ServiceAccount = {
    projectId: ENV.FIREBASE_PROJECT_ID,
    clientEmail: ENV.FIREBASE_CLIENT_EMAIL,
    privateKey: ENV.FIREBASE_PRIVATE_KEY.replace(/\\n/g, '\n')
  };
  // Initialize the firebase admin app
  if (!fbase.apps.length) {
    fbase.initializeApp({
      credential: fbase.credential.cert(adminConfig)
    });
  }

  app.use('/doc', basicAuth({
    challenge: true,
    users: { [process.env.USER_SWAGGER]: process.env.USER_SWAGGER_PASSWORD },
  }));

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('/doc', app, document);
  await app.listen(3000);
}
bootstrap();

