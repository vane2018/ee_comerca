import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Purchase } from '../purchases/purchase.entity';
import { Product } from '../products/products.entity';

@Entity('DETALLE')
export class Detalle {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column()
    productoId: string;
    @Column()
    purchaseId: string;
    @Column()
    productoName: string;
    @Column()
    productoPrice: number;
    @Column({ nullable: true })
    imagenProducto: string;
    @ManyToOne(() => Purchase, purchase => purchase.detalle)
    public purchase!: Purchase;
    @ManyToOne(() => Product, product => product.detalle)
    public producto!: Product;

}
