import { ApiProperty } from "@nestjs/swagger";

export class detalleDto{

    @ApiProperty({ description: 'precio de productos' })
    productoId: string;
    @ApiProperty({ description: 'precio de productos' })
    productoName: string;
    @ApiProperty({ description: 'precio de productos' })
    productoPrice: number;
    @ApiProperty({ description: 'precio de productos' })
    purchaseId: string;
    @ApiProperty({ description: 'precio de productos' })
    imagenProducto: string;
}