import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Detalle } from './detalle.entity';
import { detalleDto } from './dto/detalleDto.dto';
import { ProductsService } from '../products/products.service';

@Injectable()
export class DetalleService extends TypeOrmCrudService<Detalle>{
    constructor(@InjectRepository(Detalle) repo,
        productService: ProductsService
    ) {
        super(repo);

    }

    public async saveDetalle(detalle: detalleDto): Promise<detalleDto> {
        let detalles = this.repo.create(detalle);
        return await this.repo.save(detalles);
    }
}


