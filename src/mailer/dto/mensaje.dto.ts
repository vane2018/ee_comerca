import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsEmail } from 'class-validator';

export class NotificarDto {
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    name: string;

    /*    @ApiProperty()
       @IsNotEmpty()
       @IsString()
       email: string; */

    @ApiProperty({ description: 'Email del usuario a enviar el codigo' })
    @IsNotEmpty()
    apellido: string;

    @ApiProperty({ description: 'Cuerpo para el mensaje' })
    @IsNotEmpty()
    @IsString()
    cbu: string;

    @ApiProperty({ description: 'email body' })
    @IsNotEmpty()
    @IsString()
    message: string;

    @ApiProperty({ description: 'Cuerpo para el mensaje' })
    @IsNotEmpty()
    @IsString()
    subject: string;

    @ApiProperty({ description: 'Mensaje del email' })
    @IsNotEmpty()
    @IsString()
    monto: string;
    @ApiProperty({ description: 'Mensaje del email' })
    @IsNotEmpty()
    @IsString()
    nickname: string;
}