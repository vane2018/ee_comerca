import { Injectable } from '@nestjs/common';
import { MailerService, ISendMailOptions } from '@nest-modules/mailer';
import { MailerOptions } from './mailer.interface';


@Injectable()
export class NodeMailerService {
    constructor(private readonly mailerService: MailerService) { }

    async sendMail(mailer: ISendMailOptions) {
        return await this.mailerService.sendMail(mailer);
    }
}
