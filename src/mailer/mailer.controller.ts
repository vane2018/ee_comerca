import { Controller, Post, Res, Body, HttpStatus, UsePipes, ValidationPipe } from '@nestjs/common';
import { NodeMailerService } from './mailer.service';
import { MailerOptions } from './mailer.interface';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { MailerRecommendDto } from './dto/mailerRecommend.dto';
import { ENV } from '../common/env.config';

@ApiTags('Mailers')
@Controller('mailers')
export class MailerController {
    constructor(
        private mailerService: NodeMailerService
    ) { }
    @Post('/notificar')
    @UsePipes(new ValidationPipe({ transform: true }))
    @ApiResponse({ status: 201, description: 'The record has been successfully created.' })
    @ApiResponse({ status: 500, description: 'Internal Server Error' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    public async postMailerComprartir(@Res() res, @Body() bodys: MailerRecommendDto) {
        const mailer = {
            to: ENV.USER_COBRO_COMISION,
            from: ENV.USER_COBRO_COMISION,
            subject: 'De: ' + ENV.USER_COBRO_COMISION + ' - Motivo: ' + bodys.subject,
            text: `${bodys.message}.`,
            html: `${bodys.message}.`,
        } as MailerOptions;
        try {
            const mail = await this.mailerService.sendMail(mailer);
            return res.status(HttpStatus.OK).json(mail);
        } catch (error) {
            console.log('error', error);
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ err: true, msg: 'error al enviar mail' });
        }
    }
}
