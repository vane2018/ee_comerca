import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Purchase } from './purchase.entity';
import { PurchaseDto } from './dto/purchase.dto';
import { ProductsService } from '../products/products.service';
import { UsersService } from '../users/user.service';
import { CommissionsService } from '../commissions/commissions.service';
import { CommissionDto } from '../commissions/dto/commmissions.dto';
import { CommerceService } from '../commerce/commerce.service';
import { QueryDto } from './dto/queryDto.dto';
import * as firebase from 'firebase-admin';
import { TipoNotificacion } from '../chat/chat.entity';
import { Detalle } from '../detalle/detalle.entity';
import { DetalleService } from '../detalle/detalle.service';
import { purchaseDetalleDto } from './dto/purchases-detalle.dto';



@Injectable()
export class PurchasesService extends TypeOrmCrudService<PurchaseDto>{
    constructor(@InjectRepository(Purchase) repo,
        public commercesService: CommerceService,
        public userService: UsersService,
        public detalleService: DetalleService,
        public productService: ProductsService,
        public commisionsService: CommissionsService) {
        super(repo)
    }

    public async saveDetails(idPurchase: string, products: any): Promise<any> {
        try {
            let purchase = await this.repo.findOne(idPurchase, { relations: ['products'] });
            for (let i = 0; i < products.length; i++) {
                purchase.detalles.push(products)
            }
            let newPurchase = await this.repo.save(purchase);
            return newPurchase;
        } catch (error) {
            return error;
        }
    }

    public async getPurchases() {
        return await this.repo.find({ relations: ['detalle'] });
    }

    public async create(data: PurchaseDto): Promise<PurchaseDto> {
        try {
            let { detalles } = { ...data }

            //data.products = [];
            let porcentaje = 0;
            let band2 = false;
            let band3 = false;
            let responseProducts = [];
            let commision = 0;
            let dimension = detalles.length;
            for (let index = 0; index < detalles.length; index++) {
                let mproduct = await this.productService.searchCommerceProducts(detalles[index].idProducto);
                responseProducts.push(mproduct)
            }

            let porc = process.env.PORCENTAJE;

            let comprartir = await this.userService.findOne({ where: { email: "comprartir@gmail.com" } });

            for (let i = 0; i < dimension; i++) {
                data.detalles.push(responseProducts[i])

                if (detalles[i].priceOfert === 0) {
                    commision += responseProducts[i].price * data.detalles[i].cantidad;
                } else {
                    commision += responseProducts[i].priceOffer * data.detalles[i].cantidad;
                }
            }

            let commisionDto = new CommissionDto();
            let commisionDto2 = new CommissionDto();
            let commisionDto3 = new CommissionDto();

            commisionDto3.commission = 0;
            commisionDto3.user = comprartir;
            commisionDto3.typeReferido = "comprartir";

            let user = await this.userService.findOne(data.user)

            let description: string[] = [];
            let description2: string[] = [];
            let description3: string[] = [];
            description3.push("comprartir");
            if (user.nickReferrer) {
                description.push(`${user.firstName} ${user.lastName}`);
                description.push(`${user.profilePic}`);

                let referente = await this.userService.findOne({ where: { nickName: user.nickReferrer } });

                commisionDto.user = referente;

                if (referente) {
                    commisionDto.commission = this.reduccion(commision * 0.01);
                    commisionDto.typeReferido = "comprador";
                } else {
                    band2 = true;
                }

                description.push(`${responseProducts.map(item => item.id)}`);
                commisionDto.description = description;
                commisionDto.createAt = new Date();

                await this.commisionsService.saveCommission(commisionDto);
            } else {
                band2 = true;
            }
            let commerce = responseProducts[0].commerce;
            let comercio = await this.commercesService.searchUserCommerce(commerce.id);
            var idVendedor = comercio[0].user.id;

            if (comercio[0].user.nickReferrer) {
                description2.push(`${comercio[0].user.firstName} ${comercio[0].user.lastName}`);
                description2.push(`${comercio[0].user.profilePic}`);
                let referentedeSeller = await this.userService.findOne({ where: { nickName: comercio[0].user.nickReferrer } });
                commisionDto2.user = referentedeSeller;
                commisionDto2.user = await this.userService.findOne({ where: { nickName: comercio[0].user.nickReferrer } });

                if (referentedeSeller) {
                    commisionDto2.commission = this.reduccion(commision * 0.01);
                    commisionDto2.typeReferido = "vendedor";
                } else {
                    band3 = true;
                }
                description2.push(`${responseProducts.map(item => item.id)}`);
                commisionDto2.description = description2;
                commisionDto2.createAt = new Date();
                await this.commisionsService.saveCommission(commisionDto2);
            } else {
                band3 = true;
            }
            data.commerce = commerce;

            data.date = new Date();
            commisionDto3.createAt = new Date();

            if (band2 && band3) {
                porcentaje = parseFloat(porcentaje + porc + 0.02);
            } else {
                if (band2 || band3) {
                    porcentaje = parseFloat(porcentaje + porc + 0.01);
                } else {
                    porcentaje = parseFloat(porcentaje + porc);
                }
            }
            commisionDto3.description = description3;
            commisionDto3.commission = this.reduccion(commisionDto3.commission + commision * porcentaje);
            let productoOrdenado = detalles.sort();

            for (let i = 0; i < dimension; i++) {
                console.log("DATA DETALLE",data.detalles[i].cantidad);
                console.log("DATA DETALLE",responseProducts[i].stock);
                
                responseProducts[i].stock = responseProducts[i].stock - data.detalles[i].cantidad;
                console.log("PRODUCTOS",responseProducts[i].stock);
                
                await this.productService.saveProduct(responseProducts[i])
            }
            let purchase = await this.repo.create(data);

            await this.commisionsService.saveCommission(commisionDto3);
            console.log("COMPRARTIR",comprartir.id);
            

            await this.userService.addCommisions(comprartir.id, commisionDto3.commission);

            const mipurchase = await this.repo.save(purchase);
            let arrayDetalle: purchaseDetalleDto[];

            for (let index = 0; index < dimension; index++) {
                let detalle = new Detalle();
                detalle.productoId = data.detalles[index].idProducto;
                detalle.productoName = data.detalles[index].nombreProducto;
                detalle.productoPrice = data.detalles[index].precioProducto;
                detalle.imagenProducto= data.detalles[index].imagenProducto;
                detalle.purchaseId = purchase.id;

                let vect = await this.detalleService.saveDetalle(detalle);
            }


            this.envianotificacionVendedor2(mipurchase.id, idVendedor);
            this.notificarReferido(commisionDto3)
            if (commisionDto.commission !== null) {
                this.notificarReferido(commisionDto);
            }
            if (commisionDto2.commission !== null) {
                this.notificarReferido(commisionDto2);
            }
            return mipurchase;

        } catch (error) {
            console.log('error: ', error);
            throw new HttpException(`${error}`, HttpStatus.BAD_REQUEST);
        }
    }

    async envianotificacionVendedor2(mipurchase: string, idVendedor: string) {
        var usuario = this.userService.findOne(idVendedor)
        try {
            const notificacionId = `nuevo-venta`;
            if ((await usuario).firebaseRegistrationToken) {
                firebase.messaging().sendToDevice((await usuario).firebaseRegistrationToken, {
                    data: {
                        notificacionId: notificacionId,
                        tipo: TipoNotificacion.Venta
                    },
                    notification: {
                        tag: notificacionId,
                        title: `Nuevo venta registrada nro  ${mipurchase}`,
                        body: `se registro una nueva venta`,
                    }
                });
            }

        } catch (error) {
            console.log("se detecto un error", error);
        }
    }

    public async notificarReferido(purchass: CommissionDto) {
        try {
            var usuario = this.userService.findOne(purchass.user.id)

            const notificacionId = `nueva-comision`;
            if ((await purchass.user).firebaseRegistrationToken) {
                firebase.messaging().sendToDevice((await purchass.user).firebaseRegistrationToken, {
                    data: {
                        notificacionId: notificacionId,
                        tipo: TipoNotificacion.Comision
                    },
                    notification: {
                        tag: notificacionId,
                        title: `Tiene una nueva comision de $ ${purchass.commission}`,
                        body: `se registro una nueva comision`,
                    }
                });
            }
        } catch (error) {
            console.log(error);
        }
    }


    reduccion(valor: number) {
        let number = valor;

        let letra = number.toString().split("");
        let vect: string[] = [];

        let vect1: string[] = [];
        vect = letra;
        let j = 0;

        for (let index = 0; index < vect.length; index++) {
            if (vect[index] !== '.') {
                vect1[j] = vect[index];
                j = j + 1;
            }
            else {
                if (vect[index] === '.') {
                    vect1[j] = vect[index];
                    vect1[j + 1] = vect[index + 1];
                    vect1[j + 2] = vect[index + 2];
                    index = vect.length + 1;
                }
            }
        }
        let precio = vect1.join('');
        let abs = parseFloat(precio);
        return abs;
    }
    async showByCommerceId(commerceId: string, queryDto: QueryDto): Promise<PurchaseDto[]> {
        try {
            const purchases = await this.repo.find({ where: { commerce: commerceId }, relations: ["detalle", "user", "payment"] });
            // const purchases = await (await this.repo.find({ where: { user: commerceId }, relations: ["products"], order: { date: 'DESC' } }));            

            return purchases;
        } catch (error) {
            console.log('error: ', error);
        }
    }

    async showByCustomerId(customerId: string, queryDto: QueryDto): Promise<PurchaseDto[]> {
        console.log('query: ', queryDto);



        try {
            // let purchases;
            const purchases = await this.repo.find({ where: { user: customerId }, relations: ["detalle", "user", "payment", "shipping"] });
        
            return purchases;
        } catch (error) {
            console.log('error: ', error);
        }
    }


}