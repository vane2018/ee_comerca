import { Controller, Post, Body, Res, Response, HttpStatus, Get, Param, Query } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { Purchase } from './purchase.entity';
import { ApiTags, ApiParam, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { PurchasesService } from './purchases.service';
import { PurchaseCreateDto } from './dto/purchaseCreate.dto';
import { ProductsService } from '../products/products.service';
import { PurchaseDto } from './dto/purchase.dto';
import { QueryDto } from './dto/queryDto.dto';
import * as firebase from 'firebase-admin';
import { CommerceService } from '../commerce/commerce.service';
import { UsersService } from '../users/user.service';
import { notificacionVendedor } from './dto/enviar-notificaciones.dto';

@Crud({
    model: {
        type: Purchase,
    },
    routes: { exclude: ['createManyBase', 'createOneBase'] },
    params: {
        id: {
            field: 'id',
            type: 'uuid',
            primary: true,
        },
    },
})
@ApiTags('Purchase')
@Controller('purchases')
export class PurchasesController implements CrudController<PurchaseDto> {
    constructor(public service: PurchasesService,
        public serviceComercio: CommerceService,
        public serviceUser: UsersService) { }

    @Post('create')
    async createPurchase(@Res() res, @Body() purchaseDto: PurchaseCreateDto) {
        let response = await this.service.create(purchaseDto as PurchaseDto);
        return res.status(HttpStatus.OK).json(response);
    }
    @Get()
    @ApiOperation({ summary: 'Obtiene todos los comercios' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getSellers(@Response() res): Promise<any> {
        let purchases = await this.service.getPurchases();
        return res.status(HttpStatus.OK).json(purchases);

    }


    @Post('notificaciones')
    async enviarNotif(@Res() res, @Body() vendedor: notificacionVendedor) {
        var usuario = this.serviceUser.findOne(vendedor.vendedorId)
        try {
            // Send notificacion firebase
            const notificacionId = `nuevo-pedido`;
            if ((await usuario).firebaseRegistrationToken) {
                const message = {
                    notification: {
                        // tag: notificacionId,
                        title: `venta realizada`,
                        body: 'se ha registardo una nueva venta',
                    },
                    data: {
                        score: '850',
                        time: '2:45',
                        link: '',
                        notificacionId: notificacionId,

                    },
                    fcm_options: {
                        link: ''
                    },
                    token: (await usuario).firebaseRegistrationToken
                };
                // Send a message to the device corresponding to the provided
                // registration token.
                firebase.messaging().send(message)
                    .then((response) => {
                        // Response is a message ID string.
                        console.log('Firebase successfully sent message:', response);
                    })
                    .catch((error) => {
                        console.log('message:', error);
                    });
            };
        } catch (error) {
            console.log(error);
        }
    }


    @Get('/commerce/:commerceId')
    @ApiParam({ type: "string", name: 'commerceId' })
    async showPurchaseCommerce(@Res() res, @Param() param, @Query() queryDto: QueryDto) {


        let response = await this.service.showByCommerceId(param.commerceId, queryDto);
        return res.status(HttpStatus.OK).json(response);

    }

    @Get('/customer/:customerId')
    @ApiParam({ type: "string", name: 'customerId' })
    async showPurchaseCustomer(@Res() res, @Param() param, @Query() queryDto: QueryDto) {
        let response = await this.service.showByCustomerId(param.customerId, queryDto);
        return res.status(HttpStatus.OK).json(response);

    }
    

}
