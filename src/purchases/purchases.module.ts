import { Module } from '@nestjs/common';
import { PurchasesService } from './purchases.service';
import { PurchasesController } from './purchases.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Purchase } from './purchase.entity';
import { ProductsModule } from '../products/products.module';

import { UsersModule } from '../users/user.module';

import { CommissionsModule } from '../commissions/commissions.module';
import { CommerceModule } from '../commerce/commerce.module';
import { ShippingModule } from '../shipping/shipping.module';
import { DetalleModule } from '../detalle/detalle.module';
import { DetalleService } from '../detalle/detalle.service';
import { Detalle } from '../detalle/detalle.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Purchase, Detalle]),
    DetalleModule,
    UsersModule,
    CommerceModule,
    ProductsModule,
    CommissionsModule],
  providers: [PurchasesService,DetalleService],
  controllers: [PurchasesController]
})
export class PurchasesModule { }
