import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToMany, JoinTable, AfterLoad, ManyToOne, OneToMany } from "typeorm";
import { Payment } from "../payments/payment.entity";
import { Product } from "../products/products.entity";
import { Commerce } from "../commerce/commerce.entity";
import { User } from "../users/user.entity";
import { Shipping } from "../shipping/shipping.entity";
import { Detalle } from "../detalle/detalle.entity";


@Entity({ name: 'PURCHASES' })
export class Purchase {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column({ type: Date, default: new Date, nullable: true })
    date: Date;

    /*  @Column({ nullable: true })
     cantidad: number; */

    @ManyToOne(type => User, (user: User) => user.purchase)
    @JoinColumn()
    user: User;
    /*   customer: Customer; */
    @OneToOne(type => Payment/* , { cascade: true, eager: true } */)
    @JoinColumn()
    payment: Payment;

    @ManyToOne(type => Commerce)
    @JoinColumn()
    commerce: Commerce;

    @Column({ default: new Date() })
    createAt: Date;
    @OneToOne(() => Shipping)
    @JoinColumn()
    shipping: Shipping;
    @OneToMany(() => Detalle, detalle => detalle.purchase)
    public detalle!: Detalle[];
}