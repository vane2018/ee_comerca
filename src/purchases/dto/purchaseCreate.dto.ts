import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { purchaseDetalleDto } from './purchases-detalle.dto';


export class PurchaseCreateDto {
    @ApiPropertyOptional({ description: 'purchase date', type: 'string', format: 'date-time' })
    readonly date: Date;
    @ApiProperty({ description: 'id payment', format: 'uuid' })
    readonly payment: string;

    @ApiProperty({ description: 'id payment', format: 'uuid' })
    readonly shipping: string;
    // @IsString()
    @ApiProperty({ description: 'id customer', format: 'uuid' })
    readonly user: string;


    /*  @ApiPropertyOptional({ description: 'id customer', format: 'uuid' })
     @Exclude()
     commerce: string; */
    @ApiProperty({ description: 'id customer', type: [purchaseDetalleDto] })
    readonly detalles: purchaseDetalleDto[];
}