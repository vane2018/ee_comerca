import { ApiProperty } from "@nestjs/swagger";
import { purchaseDetalleDto } from "./purchases-detalle.dto";

export class PurchaseDetailDto {
    @ApiProperty({ description: 'purchase date', type: 'string', format: 'uuid' })
    readonly idPurchase: string;
    @ApiProperty({ description: 'selected products ids ' })
    readonly products: purchaseDetalleDto[];

    @ApiProperty({ description: 'cantidad' })
    readonly cantidad: number[];
}