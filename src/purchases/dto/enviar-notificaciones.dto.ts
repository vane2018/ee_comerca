import { ApiProperty } from "@nestjs/swagger";

export class notificacionVendedor {
    @ApiProperty({ description: 'id purchases', format: 'uuid' })
    purchaseId: string;
    @ApiProperty({ description: 'id usuario vendedor', format: 'uuid' })
    vendedorId: string;

}