import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";

export enum order {
    ASC = "ASC",
    DESC = "DESC"
}
export class QueryDto {
    @ApiPropertyOptional({ description: 'name of field to order' })
    readonly field: string;
    @ApiPropertyOptional({ description: 'order type', enum: order, default:'ASC'})
    readonly order: order;

}