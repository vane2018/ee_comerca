import { ApiProperty } from "@nestjs/swagger";

export class purchaseDetalleDto {
    @ApiProperty({ description: 'purchase id' })
    idProducto: string;
    @ApiProperty({ description: 'purchase id' })
    nombreProducto: string;
    @ApiProperty({ description: 'purchase id' })
    precioProducto: number;
    @ApiProperty({ description: 'purchase id' })
    cantidad: number;
    @ApiProperty({ description: 'purchase id' })
    priceOfert: number;
    @ApiProperty({ description: 'purchase id' })
    idComercio: string;
    @ApiProperty({ description: 'purchase id' })
    imagenProducto: string;
  
}