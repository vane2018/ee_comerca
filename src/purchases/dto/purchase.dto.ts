import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Commerce } from "../../commerce/commerce.entity";
import { Detalle } from '../../detalle/detalle.entity';
import { purchaseDetalleDto } from "./purchases-detalle.dto";


export class PurchaseDto {
    @ApiProperty({ description: 'purchase id' })
    readonly id: string;
    @ApiPropertyOptional({ description: 'purchase date', type: 'string', format: 'date-time' })
    date: Date;
    @ApiProperty({ description: 'payment id' })
    readonly payment: string;
    @ApiProperty({ description: 'payment id' })
    readonly shipping: string;
    @ApiProperty({ description: 'id user' })
    readonly user: string;
    @ApiProperty({ description: 'selected products ids' })
    detalles: purchaseDetalleDto[];
    @ApiProperty({ description: 'commerce' })
    commerce: Commerce;
}