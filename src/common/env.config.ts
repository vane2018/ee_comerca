import 'dotenv/config';

interface EnvData {
    // application
    APP_ENV: string;
    APP_DEBUG: boolean;
    SERVER_URL: string;
    LANDING_URL: string;
    APP_FRONT_URL: string;

    //firebase
    FIREBASE_PROJECT_ID: string;
    FIREBASE_PRIVATE_KEY: string;
    FIREBASE_CLIENT_EMAIL: string;
    FIREBASE_DATABASE_URL: string;

    //mercado pago
    MERCADO_PAGO_PUBLIC_KEY: string;
    MERCADO_PAGO_API: string;
    MERCADO_PAGO_ACCESS_TOKEN: string;
    MERCADO_PAGO_CLIENT_ID: string;
    MERCADO_PAGO_CLIENT_SECRET: string;
    MERCADO_PAGO_APLICATION_FEE_PERCENTAJE: string;
    URL_API: string;
    APP_ID: number;
    URL_FRONT: string;
    MERCADO_PAGO_REDIRECT_URI: string;

    // database
    DB_HOST?: string;
    DB_NAME: string;
    DB_PORT?: number;
    DB_USER: string;
    DB_PASSWORD: string;
    DB_SINCRONIZE: boolean;

    // cadygo
    CADYGO_TOKEN: string;
    CADYGO_API: string;

    // twilio
    TWILIO_ACCOUNT_SID: string;
    TWILIO_AUTH_TOKEN: string;

    PORCENTAJE: number;

    USER_NO_REPLY: string;
    USER_NO_REPLY_PASSWORD: string;
    USER_COBRO_COMISION: string;
    USER_COBRO_COMISION_PASSWORD: string;

    G_STORAGE_BUCKET_NAME: string,
    G_STORAGE_BASE_URI: string
    G_STORAGE_CREDENTIAL_PRIVATE_KEY: string,
    G_STORAGE_CREDENTIAL_CLIENT_EMAIL: string,
}

export const ENV: EnvData = {
    SERVER_URL: process.env.SERVER_URL,
    LANDING_URL: process.env.LANDING_URL,
    APP_FRONT_URL: process.env.APP_FRONT_URL,
    FIREBASE_PROJECT_ID: process.env.FIREBASE_PROJECT_ID,
    FIREBASE_PRIVATE_KEY: process.env.FIREBASE_PRIVATE_KEY,
    FIREBASE_CLIENT_EMAIL: process.env.FIREBASE_CLIENT_EMAIL,
    FIREBASE_DATABASE_URL: process.env.FIREBASE_DATABASE_URL,
    CADYGO_TOKEN: process.env.CADYGO_TOKEN,
    CADYGO_API: process.env.CADYGO_API,
    MERCADO_PAGO_PUBLIC_KEY: process.env.MERCADO_PAGO_PUBLIC_KEY,
    MERCADO_PAGO_API: process.env.MERCADO_PAGO_API,
    MERCADO_PAGO_ACCESS_TOKEN: process.env.MERCADO_PAGO_ACCESS_TOKEN,
    MERCADO_PAGO_CLIENT_ID: process.env.MERCADO_PAGO_CLIENT_ID,
    MERCADO_PAGO_CLIENT_SECRET: process.env.MERCADO_PAGO_CLIENT_SECRET,
    MERCADO_PAGO_REDIRECT_URI: process.env.MERCADO_PAGO_REDIRECT_URI,
    TWILIO_ACCOUNT_SID: process.env.TWILIO_ACCOUNT_SID,
    TWILIO_AUTH_TOKEN: process.env.TWILIO_AUTH_TOKEN,
    APP_ID: parseInt(process.env.APP_ID),
    MERCADO_PAGO_APLICATION_FEE_PERCENTAJE: process.env.MERCADO_PAGO_APLICATION_FEE_PERCENTAJE,
    URL_FRONT: process.env.URL_FRONT,
    URL_API: process.env.URL_API,
    APP_ENV: process.env.NODE_ENV,
    APP_DEBUG: process.env.APP_DEBUG === 'true',
    DB_HOST: process.env.DB_HOST,
    DB_NAME: process.env.DB_NAME,
    DB_PORT: parseInt(process.env.DB_PORT),
    DB_USER: process.env.DB_USER,
    DB_PASSWORD: process.env.DB_PASSWORD,
    DB_SINCRONIZE: process.env.DB_SINCRONIZE === 'true',
    PORCENTAJE: parseFloat(process.env.PORCENTAJE),
    USER_NO_REPLY: process.env.USER_NO_REPLY,
    USER_NO_REPLY_PASSWORD: process.env.USER_NO_REPLY_PASSWORD,
    USER_COBRO_COMISION: process.env.USER_COBRO_COMISION,
    USER_COBRO_COMISION_PASSWORD: process.env.USER_COBRO_COMISION_PASSWORD,
    G_STORAGE_BUCKET_NAME: process.env.G_STORAGE_BUCKET_NAME,
    G_STORAGE_BASE_URI: process.env.G_STORAGE_BASE_URI,
    G_STORAGE_CREDENTIAL_PRIVATE_KEY: process.env.G_STORAGE_CREDENTIAL_PRIVATE_KEY,
    G_STORAGE_CREDENTIAL_CLIENT_EMAIL: process.env.G_STORAGE_CREDENTIAL_CLIENT_EMAIL,
}