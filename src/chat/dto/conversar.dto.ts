import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";


export class conversarDto {
    @ApiProperty({ required: true, description: 'cuerpo del mensaje' })
    readonly id: string;
    @ApiPropertyOptional({ description: 'fecha de la compra de la compra, por defecto fecha actual', type: Date })
    createdAt: Date;
    @ApiProperty({ required: true, description: 'id del remitente' })
    readonly text: string;
}