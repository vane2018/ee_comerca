import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";

export class chatDto {
    @ApiProperty({ required: true, description: 'cuerpo del mensaje' })
    readonly texto: string;
    @ApiPropertyOptional({ description: 'fecha de la compra de la compra, por defecto fecha actual', type: Date })
    createdAt: Date;
    @ApiProperty({ required: true, description: 'id del remitente' })
    readonly remitenteId: string;
    @ApiProperty({ required: true, description: 'id del destinatario' })
    readonly destinatarioId: string;
}