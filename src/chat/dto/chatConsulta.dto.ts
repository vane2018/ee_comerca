import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Entity } from "typeorm";

export class chatConsulta {

    @ApiProperty({ description: 'id del remitente' })
    readonly remitenteId: string;
    @ApiProperty({ description: 'id del destinatario' })
    readonly destinatarioId: string;
}