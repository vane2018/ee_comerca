import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Entity } from "typeorm";

export class Consulta {

    @ApiProperty({ description: 'id del remitente' })
    readonly remitenteId: string;

}