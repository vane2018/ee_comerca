// get health of application
var mercadopago = require('mercadopago');
const axios = require('axios');
const MERCADO_PAGO_API = 'https://api.mercadopago.com';

mercadopago.configure({
  access_token: 'TEST-4738866408022481-121515-c178c5dab0518ce3b9b4aec1a32e7eb4-687561679'
});

// .../mercadopago
exports.postMercadopago = async (req, res) => { //Registrar el pago

  const token = req.body.token;
  const payment_method_id = req.body.payment_method_id;
  const installments = parseInt(req.body.installments);
  const issuer_id = req.body.issuer_id;
  const transaction_amoun = Number(req.query.transaction_amount);
  const email = req.query.email;
  const commerceId = req.query.commerceId;
  var sellerAccessToken = 'TEST-4738866408022481-121515-5b0f30a924dc5654d932d5f46c249a95-688246148';
  // se generan las propiedades del el pago
  var payment_data = {
    transaction_amount: transaction_amoun,
    token: token,
    description: 'Small Wool Pants',
    installments: installments,
    payment_method_id: payment_method_id,
    payer: {
      email: email //'test_user_7450365@testuser.com'
    }
  };
  // se genera el parametro para buscar el Tokenseller
  var commerce_id = {
      comerceId: commerceId    
  };
  await axios({
    method: 'post',
    url: 'https://api.comprartir-staging.com.ar/api/v1/mercado-pago/apis/v1/mercado-pago/exist/comercioId/{comercioId}',
    data: commerce_id,
    headers: {
      'accept': 'application/json',
      'content-type': 'application/json',
    }
  }).then(response => {
    sellerAccessToken = response.data
  }).catch(err => {
    console.error(err.response.data)
  });
 sellerAccessToken= 'TEST-4738866408022481-121521-8d16c6a45ec868a2dc8dbe77d5174e5e-688246148'
 //llamodo a api de mercado pago para registrar el pago
  await axios({
    method: 'post',
    url: 'https://api.mercadopago.com/v1/payments',
    data: payment_data,
    headers: {
      'accept': 'application/json',
      'content-type': 'application/json',
      'Authorization': `Bearer ${sellerAccessToken}`
    }
  }).then(response => {
    res.redirect('http://10.0.2.2:3000/health/mercadopago/success');
  }).catch(err => {
    console.error(err.response.data)
    res.redirect('http://10.0.2.2:3000/health/mercadopago/failure');
  });
};

// .../health/mercadopago/success
exports.getSucces = async (req, res) => {
  res.json({
    status: 'Success',
  });
};

// .../health/mercadopago/failure
exports.getFailure = async (req, res) => {
  res.json({
    status: 'Failure',
  });
};
