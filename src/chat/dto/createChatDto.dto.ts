import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";


export class createChatDto implements Readonly<createChatDto> {
    @ApiProperty({ required: true, description: 'cuerpo del mensaje' })
    readonly texto: string;
    @ApiPropertyOptional({ description: 'fecha de la compra de la compra, por defecto fecha actual', type: Date })
    createdAt: Date;
    @ApiProperty({ required: true, description: 'id del destinatario' })
    readonly destinatarioId: string;

}

