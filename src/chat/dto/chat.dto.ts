import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Moment } from "moment";


export class chatDto implements Readonly<chatDto> {
    @ApiProperty({ required: true, description: 'cuerpo del mensaje' })
    readonly texto: string;
    @ApiPropertyOptional({ description: 'fecha de la compra de la compra, por defecto fecha actual' })
    createAt: Date;
    @ApiPropertyOptional({ description: 'purchase date', type: 'string', format: 'date-time' })
    readonly date: Date;
    @ApiProperty({ required: true, description: 'id del remitente' })
    remitenteId: string;
    @ApiProperty({ required: true, description: 'id del destinatario' })
    destinatarioId: string;
    @ApiProperty({ required: true, description: 'id del destinatario' })
    readonly token: string;

}