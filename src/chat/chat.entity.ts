import { Moment } from "moment";
import moment = require("moment");
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "../users/user.entity";
export enum TipoNotificacion {
    Venta = 'purchases',
    Chat = 'chat',
    Comision = 'commision',

}
@Entity('CHAT')
export class Chat {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column('varchar', { length: 10000, nullable: true })
    texto: string;

    @Column({ type: Date, default: new Date, nullable: true })
    createAt: Date;

    /*  @Column('varchar', { length: 300, nullable: true })
     remitenteId: string;
     @Column('varchar', { length: 300, nullable: true })
     destinatarioId: string; */
    @ManyToOne(() => User, { onUpdate: 'CASCADE', onDelete: 'CASCADE' })
    @JoinColumn({ name: 'remitenteId' })
    remitente: User;

    @ManyToOne(() => User, { onUpdate: 'CASCADE', onDelete: 'CASCADE' })
    @JoinColumn({ name: 'destinatarioId' })
    destinatario: User;
}