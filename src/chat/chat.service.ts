import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import moment = require('moment');
import { UsersService } from '../users/user.service';
import { User } from '../users/user.entity';
import { Chat, TipoNotificacion } from './chat.entity';
import { chatDto } from './dto/chat.dto';
import * as firebase from 'firebase-admin';

@Injectable()
export class ChatService extends TypeOrmCrudService<Chat> {

    constructor(@InjectRepository(Chat) repo,
        public usersService: UsersService) {
        super(repo)
    }

    public async saveChat(chats: chatDto): Promise<Chat> {
        let miChat = this.repo.create(chats);
        let r = await this.repo.save(miChat);
        this.envianotificacionVendedor2(chats.destinatarioId)

        return r;
    }


    async envianotificacionVendedor2(idVendedor: string) {
        var usuario = this.usersService.findOne(idVendedor)
        try {
            const notificacionId = `nuevo--mensaje`;
            if ((await usuario).firebaseRegistrationToken) {
                firebase.messaging().sendToDevice((await usuario).firebaseRegistrationToken, {
                    data: {
                        notificacionId: notificacionId,
                        tipo: TipoNotificacion.Chat
                    },
                    notification: {
                        tag: notificacionId,
                        title: `Nuevo mensaje `,
                        body: `tiene un nuevo mensaje`,
                    }
                });
            }
        } catch (error) {
            console.log("se detecto un error", error);
        }
    }

    public async obtenerChat() {
        try {
            var fechaInicio = moment().format('YYYY-MM-DDTHH:mm')
            let general = await this.repo.find({ relations: ['remitente', 'destinatario'] });
            return general;

        } catch (error) {
            return error;
        }
    }

    public async updateChat(chatDto: Chat, rem: User, des: User): Promise<any> {
        const user = await this.repo.findOne(chatDto.id);
        user.remitente = rem;
        user.destinatario = des;
        if (user) {
            let f = await this.repo.save(user);
            return f;
        }
        else {
            console.log("error");
        }
    }

    public async getChat(ids: string, id2: string): Promise<any> {
        let loschats: Chat[] = [];
        let q = await this.repo.find({
            relations: ['remitente', 'destinatario'], order: {
                createAt: "ASC"
            }
        });
        for (let i = 0; i < q.length; i++) {
            if (((q[i].remitente.id == ids) && (q[i].destinatario.id == id2)) || ((q[i].remitente.id == id2) && (q[i].destinatario.id == ids))) {
                loschats.push(q[i])
            }
        }
        return loschats;
    }


    public async getchats(id: string): Promise<any> {
        let prod: Chat[] = [];
        let r = await this.repo.find({ relations: ['remitente', 'destinatario'] });
        for (let index = 0; index < r.length; index++) {
            if (r[index].remitente.id == id || r[index].destinatario.id == id) {
                prod.push(r[index]);
            }
        }
        return prod;
    }

    public async getChatId(ids: string): Promise<any> {
        let chats = await this.repo.findOne(ids, { relations: ['remitente', 'destinatario'] });
        return chats;
    }

    public async getPreviewTexto(texto: string) {
        if (!texto) {
            return '';
        }
        const maxlength = 200;
        // Menos 3 para poder agregar los 3 puntos sin que supere maxlength
        return texto.length > maxlength ? `${texto.slice(0, maxlength - 3)}...` : texto;
    }
}