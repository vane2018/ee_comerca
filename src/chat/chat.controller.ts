import { Body, Controller, Response, Post, Get, HttpStatus, Param } from '@nestjs/common';
import { ApiBody, ApiCreatedResponse, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Crud } from '@nestjsx/crud';
import moment = require('moment');
import * as firebase from 'firebase-admin';
import { UsersService } from '../users/user.service';
import { Chat, TipoNotificacion } from './chat.entity';
import { ChatService } from './chat.service';
import { chatDto } from './dto/chat.dto';
import { chatConsulta } from './dto/chatConsulta.dto';
import { Consulta } from './dto/consulta.dto'
let admin = require("firebase-admin");
@Crud({
    model: {
        type: Chat,
    },
    params: {
        id: {
            field: 'id',
            type: 'uuid',
            primary: true,
        },
    },
    query: {

    },
})

@ApiTags('Chats')
@Controller('chat')
export class ChatController {
    constructor(public service: ChatService,
        public userService: UsersService
    ) { }

    @Post('/chatRemitente')
    @ApiOperation({ summary: 'Registra un commercio' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    @ApiBody({ type: Consulta })
    public async obtenerChat(@Response() res, @Param() param, @Body() chats: Consulta): Promise<any> {
        const chatss = await this.service.getchats(chats.remitenteId);
        return res.status(HttpStatus.OK).json(chatss);
    }

    @Post('/chatDestandRemittente')
    @ApiOperation({ summary: 'Registra un commercio' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    @ApiBody({ type: chatConsulta })
    public async losChat(@Response() res, @Param() param, @Body() chats: chatConsulta): Promise<any> {
        const chatss = await this.service.getChat(chats.remitenteId, chats.destinatarioId);
        return res.status(HttpStatus.OK).json(chatss);
    }

    @Post('register/mensajeChat')
    @ApiOperation({ summary: 'Registra un commercio' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    @ApiCreatedResponse({ description: 'The Commerce has been successfully created.', type: chatDto, status: 200 })
    @ApiBody({ type: chatDto })
    public async postChat(@Response() res, @Body() chats: chatDto): Promise<void> {
        let rem = await this.userService.findOne(chats.remitenteId);
        let des = await this.userService.findOne(chats.destinatarioId);
        chats.createAt = new Date();
        let item = await this.service.saveChat(chats);
        item.destinatario = des;
        item.remitente = rem;
        let loschat = await this.service.updateChat(item, rem, des)
        res.json({ status: 'ok', loschat });
    }

    @Get()
    @ApiOperation({ summary: 'Registra un commercio' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getGeneral(@Response() res): Promise<void> {
        let u = await this.service.obtenerChat();
        res.json({ status: 'ok', u });
    }

    @Get('/:id')
    /* @UseGuards(AuthGuard('jwt')) */
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: '\'The resource was not found\'' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getChatId(@Response() res, @Param() param): Promise<any> {
        const products = await this.service.getChatId(param.id);
        return res.status(HttpStatus.OK).json(products);
    }
}