import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { productView } from "./productUser.entity";
import { productViewDto } from "./dto/productxUserDto.dto";
import { getRepository } from "typeorm";

@Injectable()
export class ProductsViewService extends TypeOrmCrudService<productView> {
    constructor(@InjectRepository(productView) repo) {
        super(repo);
    }
    public async guardarView(prod: productViewDto) {
        try {
            let prodV = this.repo.create(prod);
            return await this.repo.save(prodV);
        } catch (error) {
            console.log('error', error);
        }
    }

    public async comparar(prod: productViewDto) {
        let band = false;
        let r = await getRepository(productView)
            .createQueryBuilder("productView")
            .where("productView.idUser = :prod", { prod: prod.idUser })
            .getMany()

        for (let index = 0; index < r.length; index++) {
            if (r[index].idProduct === prod.idProduct) {
                band = true;
            }
        }
        if (band) {
            return band;
        } else {
            let prodV = this.repo.create(prod);
            return await this.repo.save(prodV);
        }
    }
}