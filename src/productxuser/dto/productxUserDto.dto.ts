import { ApiProperty } from "@nestjs/swagger";

export class productViewDto {
    @ApiProperty({ description: 'id del user' })
    readonly idUser: string;
    @ApiProperty({ description: 'id del producto' })
    readonly idProduct: string;
}