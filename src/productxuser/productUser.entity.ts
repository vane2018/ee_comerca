import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from "typeorm";

@Entity('PRODUCTSVIEW')
export class productView {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column({ nullable: true })
    idUser: string;
    @Column({ nullable: true })
    idProduct: string;
}