import { Module } from '@nestjs/common';
import { productView } from './productUser.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductsViewController } from './productxuser.controller';
import { ProductsViewService } from './productxuser.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([productView])
    ],
    controllers: [ProductsViewController],
    providers: [ProductsViewService],
    exports: [ProductsViewService]
})
export class ProductxuserModule { }
