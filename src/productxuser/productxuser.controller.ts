import { Crud } from "@nestjsx/crud";
import { ApiTags, ApiOperation, ApiResponse, ApiBody } from "@nestjs/swagger";
import { Controller, Get, Post, Response, Body, HttpStatus } from "@nestjs/common";
import { productView } from "./productUser.entity";
import { ProductsViewService } from "./productxuser.service";
import { productViewDto } from "./dto/productxUserDto.dto";


@Crud({
    model: {
        type: productView,
    },
    params: {
        id: {
            field: 'id',
            type: 'uuid',
            primary: true,
        },
    }
})
@ApiTags('ProductsViewxUser')
@Controller('productsViewxuser')
export class ProductsViewController {
    constructor(
        public service: ProductsViewService,
    ) { }

    @Post('viewxuser')
    @ApiOperation({ summary: 'comprueba si el producto fue visto por usuario' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiBody({ type: productViewDto })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getProvincias(@Response() res, @Body() prodViewDto: productViewDto): Promise<any> {
        try {
            let provincias = await this.service.guardarView(prodViewDto);
            return res.status(HttpStatus.OK).json(provincias)
        } catch (error) {
            console.log('error', error);
        }
    }


    @Post('/comp')
    @ApiOperation({ summary: 'compara los view' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiBody({ type: productViewDto })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async comparacion(@Response() res, @Body() prodViewDto: productViewDto) {
        try {
            let estado = await this.service.comparar(prodViewDto)
            if (estado) {

                return res.status(HttpStatus.OK).json({ "mensaje": estado })
            } else {
                return res.status(HttpStatus.OK).json({ "mensaje": estado })
            }
        } catch (error) {
            console.log("error", error);
        }
    }
}