import { ApiProperty } from '@nestjs/swagger';

export class mercadoPagoQueryDto {

    @ApiProperty({ description: 'monto', type: String })
    transaction_amount: string;
    @ApiProperty({ description: 'monto', type: String })
    email: string;
    @ApiProperty({ description: 'monto', type: String })
    commerceId: string;
    @ApiProperty({ description: 'monto', type: String })
    shipping_amount: string;

}