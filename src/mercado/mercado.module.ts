import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommerceModule } from '../commerce/commerce.module';

import { MercadoController } from './mercado.controller';
import { Mercado } from './mercado.entity';
import { MercadoService } from './mercado.service';

@Module({
  imports: [TypeOrmModule.forFeature([Mercado]), CommerceModule],
  controllers: [MercadoController],
  providers: [MercadoService]
})
export class MercadoModule { }
