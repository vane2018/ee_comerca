import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import axios from 'axios';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { mercadoPagoQueryDto } from './dto/mercado-pago-query.dto';
import { mercadoDto } from './dto/mercado-paggo.dto';
import { Mercado } from './mercado.entity';
import { mercadDto } from './dto/mercad.dto';
import { ENV } from '../common/env.config';

@Injectable()
export class MercadoService extends TypeOrmCrudService<Mercado>{

    constructor(@InjectRepository(Mercado) repo: Repository<Mercado>) {
        super(repo)
    }

    async realizarPago(query: mercadoPagoQueryDto, credenciales: mercadoDto) {
        try {
            const token = credenciales.token;
            const payment_method_id = credenciales.payment_method_id;
            const installments = parseInt(credenciales.installments);
            const issuer_id = credenciales.issuer_id;
            const transaction_amoun = Number(query.transaction_amount);
            const shipping_amount = Number(query.shipping_amount); // se agrego el monto de envio desde front
            const product_price = transaction_amoun - shipping_amount; //se calcula el precio del producto
            const comprartir_commission = (product_price * 0.07); // se calcula la comision de comprartir
            const application_fee = shipping_amount + comprartir_commission; //Comisión + envio recolectada por el marketplace
            const email = query.email;
            const commerceId = query.commerceId;

            var payment_data = {
                transaction_amount: transaction_amoun,
                token: token,
                issuer_id: issuer_id,//se agrega
                installments: installments,
                payment_method_id: payment_method_id,
                binary_mode: true,//se agrego
                application_fee: application_fee,//se agrega
                payer: {
                    email: email //'test_user_7450365@testuser.com'
                }
            };
            var commerce_id = {
                comerceId: commerceId
            };
            const mivar = await axios({
                method: 'post',
                url: ENV.URL_API + '/api/v1/mercado-pago/apis/v1/mercado-pago/exist/comercioId/{comercioId}',
                data: commerce_id,
                headers: {
                    'accept': 'application/json',
                    'content-type': 'application/json',
                }
            });
            var sellerAccessToken = mivar.data
            const miletra = await axios({
                method: 'post',
                url: 'https://api.mercadopago.com/v1/payments',
                data: payment_data,
                headers: {
                    'accept': 'application/json',
                    'content-type': 'application/json',
                    'Authorization': `Bearer ${sellerAccessToken}`
                }
            })
            var productMod: mercadDto = {
                email: miletra.data.payer.email,
                transaction_amount: miletra.data.transaction_amount,
                status: miletra.data.status,
                application_fee: payment_data.application_fee,
                fee_details: miletra.data.fee_details

            }
            this.guardar(productMod);

            return miletra.data;
        }

        catch (error) {
            console.log("error", error);
        }
    }

    async guardar(midtao: mercadDto) {
        await this.repo.save(midtao);
    }
    public async getdatos(): Promise<Mercado[]> {
        let vector = await this.repo.find();
        return vector;
    }
}

