import { Commerce } from "../commerce/commerce.entity";
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity("MERCADO")
export class Mercado {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column('varchar', { length: 500, nullable: true })
    email: string;
    @Column({ type: "float8", nullable: true })
    transaction_amount: number;
    @Column('varchar', { length: 500, nullable: true })
    status: string;
    @Column({ type: "float8", nullable: true })
    application_fee: number;
    @Column('varchar', { length: 500, nullable: true })
    fee_details: [];
    @OneToOne(() => Commerce, (commerce: Commerce) => commerce.merc)
    commerce: Commerce;
}