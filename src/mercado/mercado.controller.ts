import { Body, Controller, Get, HttpStatus, Post, Response, Query, Param } from '@nestjs/common';
import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { mercadoPagoQueryDto } from './dto/mercado-pago-query.dto';
import { mercadoDto } from './dto/mercado-paggo.dto';
import { MercadoService } from './mercado.service';
import { CommerceService } from '../commerce/commerce.service';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { PermissionGuard } from '../common/guards/permission.guards';
import { Permision } from '../common/permission/permission.decorator';
import { ENV } from '../common/env.config';

@ApiTags('Mercado')
@Controller('mercado')
export class MercadoController {
    constructor(
        public serviceMercado: MercadoService,
        public commerceService: CommerceService
    ) { }

    @Post('/apis/v1/mercado-pago/gestion-pagos')
    @ApiOperation({ summary: 'crea comercio' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    @ApiBody({ type: mercadoDto && mercadoPagoQueryDto })
    public async gestionarPago(@Response() res, @Query() query: mercadoPagoQueryDto, @Body() credenciales: mercadoDto) {
        let mercado = await this.serviceMercado.realizarPago(query, credenciales);
        if (mercado.status === 'approved') {
            return res.redirect(ENV.APP_FRONT_URL + '/mercado-pago/loading-sppiner/?success');
        } else {
            if (mercado.status === 'rejected') {
                this.serviceMercado.guardar(mercado);
                return res.redirect(ENV.APP_FRONT_URL + '/mercado-pago/loading-sppiner/?failure');
            }
        }
    }

    @Get('/apis/v1/mercado-pago/gestion')
    @UseGuards(AuthGuard('jwt'), PermissionGuard)
    @Permision('admin', 'super')
    @ApiOperation({ summary: 'crea comercio' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async obtenerDatos(@Response() res) {
        let datos = await this.serviceMercado.getdatos();
        return res.status(HttpStatus.OK).json(datos);
    }
}
