import { Test, TestingModule } from '@nestjs/testing';
import { MercadoController } from './mercado.controller';

describe('Mercado Controller', () => {
  let controller: MercadoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MercadoController],
    }).compile();

    controller = module.get<MercadoController>(MercadoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
