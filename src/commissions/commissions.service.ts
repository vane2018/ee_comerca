import { Injectable, Inject } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Commision } from './commissions.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { CommissionDto } from './dto/commmissions.dto';
import * as firebase from 'firebase-admin';
import { UsersService } from '../users/user.service';


@Injectable()
export class CommissionsService extends TypeOrmCrudService<CommissionDto>{
    constructor(@InjectRepository(Commision) repo, public serviceUser: UsersService) {
        super(repo)
    }

    public async saveCommission(commisionDto: CommissionDto): Promise<CommissionDto> {
        try {
            let comision = this.repo.create(commisionDto);
            return await this.repo.save(comision);
        } catch (error) {
            console.log('error', error);
        }
    }

    public async notificarComercioSobreNuevoVenta(purchass: CommissionDto) {
        try {
            // Send notificacion firebase
            const notificacionId = `nuevo-pedido`;
            if (purchass.user.firebaseRegistrationToken) {
                const message = {
                    notification: {
                        // tag: notificacionId,
                        title: `Nueva comision`,
                        body: 'comision asignada',
                    },
                    data: {
                        score: '850',
                        time: '2:45',
                        link: '',
                        notificacionId: notificacionId,

                    },
                    fcm_options: {
                        link: ''
                    },
                    token: purchass.user.firebaseRegistrationToken
                };
                firebase.messaging().send(message)
                    .then((response) => {
                        console.log('Firebase successfully sent message:', response);
                    })
                    .catch((error) => {
                        console.log('message:', error);
                    });
            };
        } catch (error) {
            console.log(error);
        }
    }


    public async cobrarCbu(monto: number, cbu: string) {
        var user = await this.serviceUser.findOne({ where: { cbu: cbu } })
        let tot = monto + user.montoextraido;

        return await this.serviceUser.guardarComision(user, tot);
    }

    public async getCommission(): Promise<CommissionDto[]> {
        try {
            return await this.repo.find({ relations: ['user'] })
        } catch (error) {
            console.log('error', error);
        }
    }

    public async getCommissionByIds(ids: string[]): Promise<any> {
        let comisions = await this.repo.findByIds(ids);
        return comisions;
    }

    public async getCommissionByUserId(userId: string): Promise<any> {
        let comisions = await this.repo.find({ where: { user: userId }/* , relations: ['user'] */ });
        return comisions;
    }

    public async deleteCommission(id: string) {
        await this.repo.delete(id);
    }
}




