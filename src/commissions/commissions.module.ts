import { Module } from '@nestjs/common';
import { CommissionsService } from './commissions.service';
import { CommissionsController } from './commissions.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Commision } from './commissions.entity';
import { UsersModule } from '../users/user.module';

@Module({
  imports: [TypeOrmModule.forFeature([Commision]),
    UsersModule],
  providers: [CommissionsService],
  controllers: [CommissionsController],
  exports: [CommissionsService]
})
export class CommissionsModule { }
