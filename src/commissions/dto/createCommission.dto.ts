import { ApiProperty } from "@nestjs/swagger";
import { Length, Min, IsString } from "class-validator";


export class CreateCommissionDto {
    @ApiProperty({ description: 'Commission name' })
    readonly commission: number;

    @ApiProperty({ description: 'Description commission' })
    readonly description: string[];

    @ApiProperty({ description: 'commission id' })
    readonly idUser: string;

    @ApiProperty({ description: 'commission id' })
    readonly createAt: Date;

    @ApiProperty({ description: 'tipo de referido' })
    readonly typeReferido: string;
}