import { ApiProperty } from "@nestjs/swagger";

export class comisionDescontar {
    @ApiProperty()
    monto: number;
    @ApiProperty()
    cbu: string;
}