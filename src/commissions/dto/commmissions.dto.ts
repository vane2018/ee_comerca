import { ApiProperty } from "@nestjs/swagger";

import { User } from "../../users/user.entity";

export class CommissionDto {

    @ApiProperty({ description: 'commission amount ' })

    commission: number;

    description: string[];

    user: User;

    createAt: Date;

    typeReferido: string;

}
