import { Controller, Post, UseGuards, Response, Body, HttpStatus, Get, Delete, Param, Res } from '@nestjs/common';
import { CommissionsService } from './commissions.service';
import { ApiOperation, ApiResponse, ApiBody, ApiCreatedResponse, ApiTags, ApiParam } from '@nestjs/swagger';
import { CommissionDto } from './dto/commmissions.dto';
import { CreateCommissionDto } from './dto/createCommission.dto';
import { Crud } from '@nestjsx/crud';
import { UsersService } from '../users/user.service';
import { comisionDescontar } from './dto/descontar-comision.dto';
import { PermissionGuard } from '../common/guards/permission.guards';
import { Permision } from '../common/permission/permission.decorator';
import { AuthGuard } from '@nestjs/passport';


@Crud({
    model: {
        type: CommissionDto,
    },
    params: {
        id: {
            field: 'id',
            type: 'uuid',
            primary: true,
        },
    },
    query: {
        join: {

            user: {}
        },
        sort: [{
            field: 'createAt',
            order: 'DESC'
        }]
    }
})
@ApiTags('Commissions')
@Controller('commission')
export class CommissionsController {

    constructor(
        public service: CommissionsService,
        public serviceUser: UsersService,

    ) { }

    @Post('register')

    @ApiOperation({ summary: 'Registra una comision' })
    @ApiCreatedResponse({ description: 'The product has been successfully created.', type: CommissionDto })
    @ApiBody({ type: CreateCommissionDto })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })

    public async saveCommissions(@Response() res, @Body() createCommissionDto: CreateCommissionDto): Promise<any> {
        try {
            let customerResp = await this.serviceUser.findOne(createCommissionDto.idUser);
            if (customerResp) {
                let commissionSave: CommissionDto = {
                    commission: createCommissionDto.commission,
                    description: createCommissionDto.description,
                    user: customerResp,
                    createAt: createCommissionDto.createAt,
                    typeReferido: createCommissionDto.typeReferido
                }

                const comision = await this.service.saveCommission(commissionSave);
                return res.status(HttpStatus.OK).json(comision);
            } else {
                return res.status(HttpStatus.BAD_REQUEST).json({ message: 'customer no encontrados' });
            }
        } catch (error) {
            console.log('error al crear producto: ', error);
            return res.status(HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar comision', error });
        }
    }


    @Get()
    /*     @UseGuards(AuthGuard('jwt'), PermissionGuard)
        @Permision('super') */
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getProducts(@Response() res): Promise<any> {
        const commissions = await this.service.getCommission();
        return res.status(HttpStatus.OK).json(commissions);
    }

    @Delete('/:id')
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async deleteCommisions(@Response() res, @Param() param): Promise<any> {
        await this.service.deleteCommission(param.id);
        return res.status(HttpStatus.OK).json({ message: 'commision deleted' });
    }

    @Get(':idUser')
    @ApiParam({ type: 'string', name: 'idUser' })
    public async getByUser(@Response() res, @Param() param): Promise<any> {
        const commissions = await this.service.getCommissionByUserId(param.idUser);
        return res.status(HttpStatus.OK).json(commissions);
    }


    @Post('descontar-comision')
    @ApiOperation({ summary: 'password change' })
    @ApiResponse({ status: 200, description: 'was changed password' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 403, description: "Forbidden" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 500, description: "internal server error" })
    @ApiBody({ type: comisionDescontar })
    async descontarComision(@Res() res, @Body() datos: comisionDescontar): Promise<any> {
        try {
            const usuario = await this.service.cobrarCbu(datos.monto, datos.cbu);
            return res.status(HttpStatus.OK).json(usuario);
        } catch {
            console.log("error");
        }
    }
}
