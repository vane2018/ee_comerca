import { Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn, Column } from "typeorm";
import { ParseUUIDPipe } from "@nestjs/common";
import { User } from "../users/user.entity";
import * as moment from 'moment';

@Entity('COMMISIONS')
export class Commision {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column({ type: "float8", nullable: true })
    commission: number;
    /* @Column("text", { array: true, default: () => 'array [] :: text []' })
     description: string[]; */
    @Column()
    description: string;
    @Column({ default: new Date() })
    createAt: Date;
    @ManyToOne(() => User, (user: User) => user.commisions)
    @JoinColumn()
    user: User;
    @Column({ default: "" })
    typeReferido: string;

}