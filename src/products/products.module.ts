import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from './products.entity';
import { ProductTypesModule } from '../product-category/product-category.module';
import { CommerceModule } from '../commerce/commerce.module';


@Module({
  imports: [
    TypeOrmModule.forFeature([Product]),
    ProductTypesModule, CommerceModule
  ],
  controllers: [ProductsController],
  providers: [ProductsService],
  exports: [ProductsService]
})
export class ProductsModule { }
