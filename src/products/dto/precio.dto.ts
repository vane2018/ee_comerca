import { ApiProperty } from "@nestjs/swagger";
import { IsString, IsNumber } from "class-validator";

export class PriceDto {

    @ApiProperty({ description: 'precio de productos' })
    readonly price: number;


}