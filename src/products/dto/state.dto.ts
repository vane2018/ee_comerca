import { ApiProperty } from "@nestjs/swagger";
export class EstadoDto {
    @ApiProperty({ description: 'estado del producto' })
    readonly statte: string;
}