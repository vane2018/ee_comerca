import { IsOptional, IsNumber } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class viewDto {
    @ApiProperty({ description: 'id del producto' })
    id: string;
    @ApiProperty({ description: 'Precio del producto' })
    @IsOptional()
    view: number;
}