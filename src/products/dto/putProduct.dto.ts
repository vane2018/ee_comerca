import { ApiProperty } from "@nestjs/swagger";
import { IsString, IsOptional } from "class-validator";

export class PutProductDto {
    id: string;
    @ApiProperty({ description: 'Product name' })

    readonly name: string;
    @ApiProperty({ description: 'Description product' })
    readonly description: string;
    @ApiProperty({ description: 'Price  product' })

    readonly price: number;
    @ApiProperty({ description: ' product photo' })
    pic: string[];
    @ApiProperty({ description: 'product type id' })
    readonly idProductType: string;
    @ApiProperty({ description: 'commerce id' })
    @IsString()
    readonly idCommerce: string;
    @ApiProperty({ description: 'precio de oferta' })
    @IsOptional()
    readonly priceOffer: number;
    @ApiProperty({ description: 'porcentaje de descuento' })
    @IsOptional()
    readonly porcentajeDesc: number;
    @ApiProperty({ description: 'stock de product' })
    @IsOptional()
    readonly stock: number;
    @ApiProperty({ description: 'estado del product' })
    @IsOptional()
    readonly estado: string;

}