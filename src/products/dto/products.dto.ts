import { ApiProperty } from '@nestjs/swagger';
import { ProductTypeDto } from '../../product-category/dto/productCategory.dto';
import { IsString, IsNumber, IsArray, IsOptional } from 'class-validator';
export class ProductDto {
    @ApiProperty({ description: 'Nombre del Producto' })
    @IsString()
    readonly name: string;
    @ApiProperty({ description: 'Descripcion del prodcuto' })
    @IsString()
    readonly description: string;
    @ApiProperty({ description: 'Precio del producto' })
    @IsNumber()
    readonly price: number;
    @ApiProperty({ description: 'Foto del producto' })
    //    @IsArray()
    pic: string[];
    @IsOptional()
    view: number;
    @IsOptional()
    like: number;
    @IsOptional()
    priceOffer: number;
    @IsOptional()
    porcentajeDesc: number;
    @IsOptional()
    stock: number;
    @IsOptional()
    estado: string;



    // @ApiProperty({description: 'Tipo de producto'})
    // readonly producType: ProductTypeDto;

}
