import { ApiProperty } from "@nestjs/swagger";
import { IsString, IsNumber } from "class-validator";

export class VectProductDto {
    @ApiProperty({ description: 'id de productos' })
    readonly vector: string[];
}