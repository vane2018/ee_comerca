import { IsOptional, IsNumber } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class viewUser {
    @ApiProperty({ description: 'id del producto' })
    id: string;
    @ApiProperty({ description: 'token del user' })
    user: string;
    @ApiProperty({ description: 'Precio del producto' })
    @IsOptional()
    view: number;
}