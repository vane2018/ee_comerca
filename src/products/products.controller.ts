import { Controller, Inject, UseGuards, Post, Response, Body, HttpStatus, Get, Delete, Param, Put, Patch } from '@nestjs/common';
import { Crud } from '@nestjsx/crud';
import { ApiTags, ApiOperation, ApiCreatedResponse, ApiBody, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { ProductDto } from './dto/products.dto';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/createProduct.dto';
import { CommerceService } from '../commerce/commerce.service';
import { ProductTypesService } from '../product-category/product-category.service';
import { PutProductDto } from './dto/putProduct.dto';
import { ContProductDto } from './dto/contProduct.dto';
import { Product } from './products.entity';
import { VectProductDto } from './dto/vectProduct.dto';
import { PriceDto } from './dto/precio.dto';
import { EstadoDto } from './dto/state.dto';
import { viewUser } from './dto/viewProduct.dto';
import { viewDto } from './dto/viewUser.dto';
import { AuthGuard } from '@nestjs/passport';


@Crud({
    model: {
        type: ProductDto,
    },
    params: {
        id: {
            field: 'id',
            type: 'uuid',
            primary: true,
        },
    },
    query: {
        join: {
            'purchase': {
                allow: []
            },
            commerce: {}
        }
    }
})
@ApiTags('Products')
@Controller('products')
export class ProductsController {
    constructor(
        public service: ProductsService,
        public serviceCommerce: CommerceService,
        public serviceProductType: ProductTypesService,
    ) { }

    @Post('register')
    @ApiOperation({ summary: 'Registra un producto en un commercio' })
    @ApiCreatedResponse({ description: 'The product has been successfully created.', type: ProductDto })
    @ApiBody({ type: CreateProductDto })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: '\'The resource was not found\'' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })

    public async saveProducts(@Response() res, @Body() createProductDto: CreateProductDto): Promise<any> {

        try {
            let commerceResp = await this.serviceCommerce.findOne(createProductDto.idCommerce);
            let typeProductResp = await this.serviceProductType.findOne(createProductDto.idProductType);
            if (typeProductResp && commerceResp) {
                let productSave = {
                    name: createProductDto.name,
                    description: createProductDto.description,
                    pic: createProductDto.pic,
                    price: this.reduccion(createProductDto.price),
                    productType: typeProductResp,
                    commerce: commerceResp,
                    view: 0,
                    like: 0,
                    priceOffer: this.reduccion(createProductDto.priceOffer),
                    porcentajeDesc: this.reduccion(createProductDto.porcentajeDesc),
                    stock: createProductDto.stock,
                    estado: createProductDto.estado
                }

                const product = await this.service.saveProduct(productSave);

                //no devuelve commerce upadate
                const commerce = await this.serviceCommerce.addProducts(createProductDto.idCommerce, product);
                return res.status(HttpStatus.OK).json(product);
            } else {
                return res.status(HttpStatus.BAD_REQUEST).json({ message: 'comercio o tipo de producto no encontrados' });
            }

        } catch (error) {
            console.log('error al crear producto: ', error);
            return res.status(HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el producto', error });
        }
    }

    reduccion(valor: number) {
        let number = valor;

        let letra = number.toString().split('');
        let vect: string[] = [];

        let vect1: string[] = [];
        vect = letra;
        let j = 0;

        for (let index = 0; index < vect.length; index++) {
            if (vect[index] !== '.') {
                vect1[j] = vect[index];
                j = j + 1;
            }
            else {
                if (vect[index] === '.') {
                    vect1[j] = vect[index];
                    vect1[j + 1] = vect[index + 1];
                    vect1[j + 2] = vect[index + 2];
                    index = vect.length + 1;
                }
            }
        }
        let precio = vect1.join('');
        let abs = parseFloat(precio);
        return abs;
    }

    @Get()
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: '\'The resource was not found\'' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getProducts(@Response() res): Promise<any> {
        const products = await this.service.getProducts();
        return res.status(HttpStatus.OK).json(products);
    }


    @Get('/product/ofertas')
    /* @UseGuards(AuthGuard('jwt')) */
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: '\'The resource was not found\'' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getProductsOffert(@Response() res): Promise<any> {
        const products = await this.service.getProductsOffer();
        return res.status(HttpStatus.OK).json(products);
    }

    @Post('/array')
    @ApiOperation({ summary: 'obtiene Array de productos, cuando le pasan el id' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: '\'The resource was not found\'' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async obtenerProductos(@Response() res, @Body() vect: VectProductDto): Promise<any> {
        try {
            let product = await this.service.obtener(vect.vector);
            return res.status(HttpStatus.OK).json(product);
        } catch (error) {
            console.log('error al crear producto: ', error);
            return res.status(HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el producto', error });
        }
    }

    @Post('/search/price')
    @ApiOperation({ summary: 'obtiene Array de productos, cuando le pasan precio' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: '\'The resource was not found\'' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async productsPrice(@Response() res, @Body() price: PriceDto): Promise<any> {
        try {
            let product = await this.service.busquedaPrecio(price.price);
            return res.status(HttpStatus.OK).json(product);
        } catch (error) {
            console.log('error al buscar producto: ', error);
            return res.status(HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el producto', error });
        }
    }
    @Get('/popular')
    /* @UseGuards(AuthGuard('jwt')) */
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: '\'The resource was not found\'' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getPopular(@Response() res, @Param() param): Promise<any> {
        try {
            const products = await this.service.obtenerFavorito();
            return res.status(HttpStatus.OK).json(products);
        } catch (error) {
            console.log("error", error);
        }
    }

    @Get('/:id')
    /* @UseGuards(AuthGuard('jwt')) */
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: '\'The resource was not found\'' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getProductsId(@Response() res, @Param() param): Promise<any> {
        const products = await this.service.getProductsId(param.id);
        return res.status(HttpStatus.OK).json(products);
    }

    @Delete('/:id')
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: '\'The resource was not found\'' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async deleteProducts(@Response() res, @Param() param): Promise<any> {
        await this.service.deleteProduct(param.id);
        return res.status(HttpStatus.OK).json({ message: 'product deleted' });
    }

    @Put('/edit/:id')
    public async putProducts(@Response() res, @Param() param, @Body() createProductDto: PutProductDto): Promise<any> {
        try {
            let product = await this.service.findOne(param.id);
            if (product) {
                let productMod = {
                    id: param.id,
                    name: createProductDto.name,
                    description: createProductDto.description,
                    pic: createProductDto.pic,
                    price: this.reduccion(createProductDto.price),
                    idProductType: createProductDto.idProductType,
                    idCommerce: createProductDto.idCommerce,
                    priceOffer: this.reduccion(createProductDto.priceOffer),
                    porcentajeDesc: this.reduccion(createProductDto.porcentajeDesc),
                    stock: createProductDto.stock,
                    estado: createProductDto.estado
                }
                const producto = await this.service.updateProduct(productMod);
                return res.status(HttpStatus.OK).json(producto);
            } else {
                return res.status(HttpStatus.BAD_REQUEST).json({ message: ' producto no encontrados' });
            }
        } catch (error) {
            console.log('error al modifcar producto: ', error);
            return res.status(HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el producto', error });
        }
    }

    @Post('/cont/:id')
    public async cont(@Response() res, @Body() contProductDto: ContProductDto, @Param() param): Promise<any> {
        try {
            let product = await this.service.findOne(param.id);
            if (product) {
                let productnuevo: ContProductDto = {
                    id: param.id,

                    view: product.view + contProductDto.view,

                    like: product.like + contProductDto.like

                }
                const producto = await this.service.updateProductCont(productnuevo);
                return res.status(HttpStatus.OK).json(producto as Product);
            }
        }
        catch (error) {
            console.log('error al crear producto: ', error);
            return res.status(HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el producto', error });
        }
    }

    @Post('search/estado')
    /* @UseGuards(AuthGuard('jwt')) */
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: '\'The resource was not found\'' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getProductEstado(@Response() res, @Param() param, @Body() estadoDto: EstadoDto): Promise<any> {
        const products = await this.service.busquedaEstado(estadoDto.statte);
        return res.status(HttpStatus.OK).json(products);
    }
}