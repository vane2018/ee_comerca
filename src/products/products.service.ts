import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Product } from './products.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductDto } from './dto/products.dto';
import { PutProductDto } from './dto/putProduct.dto';
import { CreateProductDto } from './dto/createProduct.dto';
import { ContProductDto } from './dto/contProduct.dto';
import { createQueryBuilder, getRepository, IsNull } from 'typeorm';
import { isNull } from 'util';
import { viewDto } from './dto/viewUser.dto';


@Injectable()
export class ProductsService extends TypeOrmCrudService<ProductDto> {
  constructor(@InjectRepository(Product) repo) {
    super(repo);
  }

  public async saveProduct(productDto: ProductDto): Promise<ProductDto> {
    try {
      // productDto.pic = Buffer.from(productDto.pic).toString('base64');
      let producto = this.repo.create(productDto);
      return await this.repo.save(producto);
    } catch (error) {
      console.log('error: ', error);
    }
  }

  public async updateProduct(productDto: PutProductDto) {
    return await this.repo.save(productDto);
  }



  public async getProducts(): Promise<ProductDto[]> {
    try {
      return await this.repo.find({ relations: ['commerce', 'productType'] });
    } catch (error) {
      console.log('error: ', error);
    }
  }
  public async getProductsOffer(): Promise<ProductDto[]> {
    let productos: ProductDto[] = [];
    try {
      let prodOfer = await this.repo.find();

      for (let index = 0; index < prodOfer.length; index++) {


        if (!isNull(prodOfer[index].priceOffer)) {
          if (prodOfer[index].priceOffer !== 0) {
            productos.push(prodOfer[index])
          }
        }

      }
      return productos;

    }
    catch (error) {
      return error;

    }
  }

  public async searchCommerceProducts(ids: string): Promise<any> {
    let products = await this.repo.findOne(ids, { relations: ['commerce'] });
    return products;
  }

  public async deleteProduct(id: string) {
    await this.repo.softDelete(id);
  }

  public async getProductsId(ids: string): Promise<any> {
    let product = await this.repo.findOne(ids, { relations: ['productType', 'commerce'] });
    return product;
  }


  public async updateProductCont(productdto: ContProductDto): Promise<Product> {

    let producto = await this.repo.findOne({ where: { id: productdto.id }, relations: ['commerce', 'productType'] });

    producto.like = productdto.like;
    producto.view = productdto.view;

    let nuevo = await this.repo.save(producto as Product);
    return nuevo;
  }

  public async updateProductView(productdto: viewDto): Promise<Product> {
    let producto = await this.repo.findOne({ where: { id: productdto.id }, relations: ['commerce', 'productType'] });
    producto.view = productdto.view;
    let nuevo = await this.repo.save(producto as Product);
    return nuevo;
  }

  public async updateStock(id: string, cant: number): Promise<any> {
    try {
      let product = await this.repo.findOne(id);
      product.stock = product.stock - cant;
      let miProduct = await this.repo.save(product);
    } catch (error) {
      return error;
    }
  }
  public async addOffert(idProduct: string, ofert: any): Promise<any> {
    let custom = await this.repo.findOne(idProduct, { relations: ['offer'] });

    await this.repo.save(custom)
  }

  public async obtener(prod: any) {
    let productos: Product[] = [];
    for (let index = 0; index < prod.length; index++) {
      productos.push(await getRepository(Product)
        .createQueryBuilder("user")
        .where("user.id= :id", { id: prod[index] })
        .getOne());
    }
    return productos;
  }

  public async obtenerFavorito() {
    let productos: Product[] = [];
    let r = await getRepository(Product)
      .createQueryBuilder("product")
      .where("product.like >= :precio", { precio: 4 })
      .getMany()
    return r;
  }

  public async busquedaPrecio(prec: number) {

    let productos: Product[] = [];

    let r = await getRepository(Product)
      .createQueryBuilder("product")
      .where("product.price <= :precio OR product.priceOffer <= :precio", { precio: prec })
      .getMany()
    for (let index = 0; index < r.length; index++) {

      if (r[index].priceOffer === 0) {
        if (r[index].price <= prec) {
          productos.push(r[index])
        }
      } else {
        if ((r[index].price <= prec) || (r[index].priceOffer <= prec)) {
          productos.push(r[index])
        }
      }
    }
    return productos;
  }

  public async busquedaEstado(calidad: string) {
    let productos: Product[] = [];
    let r = await getRepository(Product)
      .createQueryBuilder("product")
      .where("product.estado= :cal", { cal: calidad })
      .getMany()
    return r;
  }
}
