import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, JoinColumn, ManyToMany, DeleteDateColumn } from 'typeorm';
import { Commerce } from '../commerce/commerce.entity';
import { Purchase } from '../purchases/purchase.entity';
import { ProductTypes } from '../product-category/product-category.entity';
import { Detalle } from '../detalle/detalle.entity';

@Entity('PRODUCTS')
export class Product {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column()
    name: string;
    @Column()
    description: string;
    @Column({ type: "float8", nullable: true })
    price: number;
    @Column("text", { array: true, default: () => 'array [] :: text []' })
    pic: string[];
    @Column({ nullable: true })
    view: number;
    @Column({ nullable: true })
    like: number;
    @Column({ type: "float8", nullable: true })
    priceOffer: number;
    @Column({ type: "float8", nullable: true })
    porcentajeDesc: number;
    @Column({ nullable: true })
    stock: number;
    @Column({ nullable: true })
    estado: string;
    @ManyToOne(() => Commerce, (commerce: Commerce) => commerce.products)
    @JoinColumn()
    commerce: Commerce;
    @ManyToOne(() => ProductTypes, (productType: ProductTypes) => productType.products)
    
    productType: ProductTypes;
    @DeleteDateColumn()
    deletedDate: Date;
    @OneToMany(() => Detalle, detalle => detalle.producto)
    public detalle!: Detalle[];

}
