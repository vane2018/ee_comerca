import {
    Controller,
    UseGuards,
    HttpStatus,
    Request,
    Response,
    Post,
    Body,
    Get,
    Query,
    Param,
    Logger,
    Put,
} from '@nestjs/common';
import { Crud } from '@nestjsx/crud';
import { User } from '../users/user.entity';
import {
    ApiTags,
    ApiResponse,
    ApiBody,
    ApiOperation,
    ApiOkResponse,
    ApiBadRequestResponse,
    ApiCreatedResponse,
} from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { UsersService } from '../users/user.service';
import { UserDto } from '../users/dto/user.dto';
import { LoginUserDto } from './dto/login.dto';
import { AuthGuard } from '@nestjs/passport';
import { debuglog } from 'util';
import { RegistrationResponse } from './interfaces/registartionResponse.interface';
import { RolsService } from '../rols/rols.service';
import { CodeDto } from '../code/dto/code.dto';
import { CodeService } from '../code/code.service';
import { CredentialsDto } from './dto/credentials.dto';
import * as moment from 'moment';
import { ParamRecoveryDto, NumberRecoveryDto } from './dto/param-recovery.dto';
import { ValidateDto } from '../code/dto/validateDto.dto';
import { ValidateResponseDto } from './dto/responseDto.dto';
import { ValidateMailDto } from './dto/param-validate-mail.dto';
import { validateNickEmailQueryDto } from './dto/validation.dto';
import { UserFirebaseDto } from '../users/dto/user-firebase.dto';
import { ENV } from '../common/env.config';
import { ISendMailOptions } from '@nest-modules/mailer';

@Crud({
    model: {
        type: User,
    },
    params: {
        id: {
            field: 'id',
            type: 'uuid',
            primary: true,
        },
    },
})

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
    private logger = new Logger('AuthController');
    private twilioClient;
    constructor(
        private readonly authService: AuthService,
        private readonly usersService: UsersService,
        private readonly rolsService: RolsService,
        private readonly codeService: CodeService
    ) {
        this.twilioClient = require('twilio')(
            ENV.TWILIO_ACCOUNT_SID,
            ENV.TWILIO_AUTH_TOKEN);
    }

    @Get('validate')
    @ApiOperation({ summary: 'End point para validar email o nick name', description: 'validate email' })
    @ApiOkResponse({ description: 'El usuario o mail existe.', type: [UserDto], status: 200 })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 403, description: "The logged user has no rights" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: "Internal server error" })
    public async validateNickEmail(@Response() res, @Query() query: validateNickEmailQueryDto) {
        try {
            const result = await this.authService.validateNickEmail(query.email);
            if (!result.success) { return res.status(HttpStatus.OK).json(result); }
            return res.status(HttpStatus.OK).json(result);
        } catch (error) {
            return res.status(HttpStatus.FORBIDDEN).json();
        }
    }

    @Post('register')
    @ApiOperation({ summary: 'Registra una cuenta' })
    @ApiCreatedResponse({ description: 'The Account has been successfully created.', type: UserDto })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 403, description: "The logged user has no rights" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: "internal server error" })
    @ApiBody({ type: UserDto })
    public async register(@Response() res, @Body() userDto: UserDto) {
        try {
            const resp = await this.usersService.findByEmail(userDto.email);
            if (resp.success) {
                const response: RegistrationResponse = {
                    success: false,
                    message: 'user already exist',
                    user: null,
                };
                return res.status(HttpStatus.OK).json(response);
            } else {
                const rol = await this.rolsService.findRol({ name: 'customer' });
                const user = await this.usersService.register(userDto);

                await this.usersService.updateUser(user, rol);
                const response: RegistrationResponse = {
                    success: true,
                    message: 'user registered sucesfull',
                    user,
                };
                return res.status(HttpStatus.OK).json(response);
            }
        } catch (err) {
            debuglog(err);
            const response: RegistrationResponse = { success: false, message: err, user: null };
            return res.status(HttpStatus.OK).json(response);
        }
    }

    @Post('registerAdmin')
    @ApiOperation({ summary: 'Registra una cuenta' })
    @ApiCreatedResponse({ description: 'The Account has been successfully created.', type: UserDto })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 403, description: "The logged user has no rights" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: "internal server error" })
    @ApiBody({ type: UserDto })
    public async registerAdmin(@Response() res, @Body() userDto: UserDto) {
        try {
            const resp = await this.usersService.findByEmail(userDto.email);
            if (resp.success) {
                const response: RegistrationResponse = {
                    success: false,
                    message: 'user already exist',
                    user: null,
                };
                return res.status(HttpStatus.OK).json(response);
            } else {
                const rol = await this.rolsService.findRol({ name: 'admin' });
                const user = await this.usersService.register(userDto);
                await this.usersService.updateUser(user, rol);
                const response: RegistrationResponse = {
                    success: true,
                    message: 'user registered sucesfull',
                    user,

                };
                return res.status(HttpStatus.OK).json(response);
            }
        } catch (err) {
            debuglog(err);
            const response: RegistrationResponse = { success: false, message: err, user: null };
            return res.status(HttpStatus.OK).json(response);
        }
    }

    @Put('/edit/user/:id')
    public async updateUser(@Response() res, @Param() param, @Body() userDto: UserFirebaseDto): Promise<any> {
        try {
            const userUpdate = await this.usersService.updateToken(userDto.firebaseRegistrationToken, param.id);
            return res.status(HttpStatus.OK).json(userUpdate);
        } catch (error) {
            console.log('error al modifcar user: ', error);
            return res.status(HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el user', error });
        }
    }

    // @UseGuards(AuthGuard('local'))
    @Post('login')
    @ApiOperation({ summary: 'Se loguea una cuenta' })
    @ApiCreatedResponse({ description: 'The Account has been successfully login.', type: LoginUserDto, status: 200 })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 403, description: "Forbidden" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: "internal server error" })
    @ApiBody({ type: LoginUserDto })
    public async login(@Response() res, @Body() login: LoginUserDto) {
        const resp = await this.authService.login(login);
        if (resp.success) {
            const token = this.authService.createToken(resp.user);
            return res.status(HttpStatus.OK).json(token);
        } else {
            res.status(HttpStatus.NOT_FOUND).json({ message: 'User o pass incorrect' });
        }
    }

    @Get('google/callback')
    @UseGuards(AuthGuard('google'))
    @ApiOperation({ summary: 'Se loguea una cuenta con google', description: 'login account google' })
    @ApiResponse({ status: 200, description: 'Los datos han sido devueltos correctamente.' })
    @ApiResponse({ status: 400, description: "bad request" })
    @ApiResponse({ status: 403, description: "forbidden" })
    @ApiResponse({ status: 404, description: "Not found" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async googleCallback(@Request() req, @Response() res) {
        const token = this.authService.createToken(req.user);
        res.redirect(ENV.LANDING_URL + 'verifytoken/' + token.accessToken);
    }

    @Get('google')
    @UseGuards(AuthGuard('google'))
    @ApiResponse({ status: 200, description: 'Los datos han sido devueltos correctamente.' })
    @ApiResponse({ status: 400, description: "bad request" })
    @ApiResponse({ status: 403, description: "Forbidden" })
    @ApiResponse({ status: 404, description: "Not found" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async googleSignIn() {
        console.log('get google debe andar el guard');
    }

    @Get('recovery/:email')
    @ApiOperation({ summary: 'End point para enviar el email' })
    @ApiOkResponse({ description: 'the email was sent correctly', type: [ParamRecoveryDto], status: 200 })
    @ApiResponse({ status: 400, description: "bad request" })
    @ApiResponse({ status: 403, description: "forbidden" })
    @ApiResponse({ status: 404, description: "Email Not found" })
    @ApiResponse({ status: 409, description: "Conflict" })
    public async validateMail(@Response() res, @Param() mail: ParamRecoveryDto) {
        this.logger.log(mail.email);
        const result = await this.usersService.findByEmail(mail.email);
        let codigo = '';
        const ref = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        for (let i = 0; i < 6; i++) {
            codigo += ref.charAt(Math.floor(Math.random() * ref.length));
        }
        const code: CodeDto = {
            email: mail.email,
            nroCel: null,
            code: codigo,
            validate: false,
            expiredAt: moment().add(30, 'm').format('YYYY-MM-DD HH:mm'),
            uso: false,
        };
        if (result.success) {
            try {
                const mailer = {
                    to: mail.email,
                    from: ENV.USER_NO_REPLY,
                    subject: 'COMPRARTIR - Código de seguridad',
                    template: 'recovery',
                    context: {
                        code: codigo
                    }
                } as ISendMailOptions;
                try {
                    await this.authService.sendMail(mailer);
                } catch (error) {
                    return res.status(HttpStatus.BAD_REQUEST).json({ error, message: 'Ocurrio un error al enviar el mail' });
                }
                // tslint:disable-next-line: no-shadowed-variable
                const result = await this.codeService.saveCode(code);
                if (!result) {
                    return res.status(HttpStatus.BAD_REQUEST).json(result);
                }
                return res.status(HttpStatus.OK).json('Se guardo el codigo correctamente');
            } catch (error) {
                return res.status(HttpStatus.NOT_FOUND).json({ message: 'No se encontre el email' });
            }
        } else {
            return res.status(HttpStatus.NOT_FOUND).json({ error: 'user not exist', message: 'usario no existente' });
        }
    }
    
    @Post('validate-code')
    @ApiOperation({ summary: 'Validation the code the recovery the  password' })
    @ApiOkResponse({ description: 'The code has been checked successfully.', type: ValidateResponseDto, status: 200 })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 403, description: "ProhibidoForbidden" })
    @ApiResponse({ status: 404, description: "Not found" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: "internal server error" })
    @ApiBody({ type: ValidateDto })
    public async validateCode(@Response() res, @Body() validate: ValidateDto) {
        const resp = await this.codeService.validateMailandCodigo(validate);
        if (!resp.succeses) {
            res.status(HttpStatus.BAD_REQUEST).json({ ...resp, message: ' mail or code invalid, o code has expired' });
        } else {
            await this.codeService.actualizarCodigo(resp.code);
            return res.status(HttpStatus.OK).json({ ...resp, message: ' el codigo se valido con exito' });
        }
    }

    @Post('change-password')
    @ApiOperation({ summary: 'Cambiar contraseña' })
    @ApiCreatedResponse({ description: 'The password has been successfully change.', type: CredentialsDto, status: 200 })
    @ApiBadRequestResponse({ description: 'Contraseña distintas' })
    @ApiResponse({ status: 400, description: "password different" })
    @ApiResponse({ status: 403, description: "ProhibidoForbidden" })
    @ApiResponse({ status: 404, description: "Not found" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: "internal server error" })
    @ApiBody({ type: CredentialsDto })
    public async changuePass(@Response() res, @Body() credentials: CredentialsDto) {
        const resp = await this.codeService.findOne(credentials.codeID);
        const respuesta = await this.codeService.validateMailandCodigo(credentials as unknown as ValidateDto);
        if ((credentials.pass === credentials.repeatPass)) {
            if (resp && resp.validate === true && resp.uso === false) {
                await this.usersService.updatePassword(credentials);
                await this.codeService.actualizarUsoCodigo(respuesta.code);
                return res.status(HttpStatus.OK).json({ message: 'se cambio la contraseña con exito' });
            } else {
                return res.status(HttpStatus.NOT_FOUND).json({ message: 'Error al cambiar la contraseña, codigo no valido o codigo ya usado' });
            }
        } else {
            return res.status(HttpStatus.BAD_REQUEST).json({ message: 'Error: Contraseña distintas' });
        }
    }

    @Get('validate-email/:email')
    @ApiOperation({ summary: 'End point para validar email' })
    @ApiOkResponse({ description: 'Email existe.', status: 200 })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 403, description: "Forbidden" })
    @ApiResponse({ status: 404, description: "Not found" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: "internal server error" })
    public async validateEmail(@Response() res, @Param() param: ValidateMailDto) {
        const result = await this.usersService.findByEmailForValidate(param.email);
        if (result) {
            return res.status(HttpStatus.OK).json({
                validate: true,
            });
        } else {
            return res.status(HttpStatus.NOT_FOUND).json({
                statusCode: 404,
                error: 'email_not_found',
                message: 'Email not found',
            });
        }
    }

    @Get('validate-nickname/:nickname')
    @ApiOperation({ summary: 'End point para validations nickName' })
    @ApiResponse({ description: 'nickName existe.', status: 200 })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 403, description: "Forbidden" })
    @ApiResponse({ status: 404, description: "nickName Not found" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: "internal server error" })
    public async validateNick(@Response() res, @Param('nickname') nickname: any) {
        const result = await this.usersService.findByNickName(nickname);
        if (result) {
            return res.status(HttpStatus.OK).json({
                validate: true,
            });
        } else {
            return res.status(HttpStatus.NOT_FOUND).json({
                statusCode: 404,
                error: 'nickname_not_found',
                message: 'Nickname not found',
            });
        }
    }

    @Get('recovery/sms/:nroCel')
    @ApiOperation({ summary: 'End point para enviar el numero' })
    @ApiResponse({ description: 'the sms was sent successfully.', status: 200 })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 403, description: "Forbidden" })
    @ApiResponse({ status: 404, description: "number Not found" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: "internal server error" })
    public async validateNumber(@Response() res, @Param() numberRecovery: NumberRecoveryDto) {
        this.logger.log(numberRecovery.nroCel);
        const result = await this.usersService.findByNumCel(numberRecovery.nroCel);

        let codigo = '';
        const ref = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZ0123456789';
        for (let i = 0; i < 6; i++) {
            codigo += ref.charAt(Math.floor(Math.random() * ref.length));
        }
        const code: CodeDto = {
            email: null,
            nroCel: numberRecovery.nroCel,
            code: codigo,
            validate: false,
            expiredAt: moment().add(30, 'm').format('YYYY-MM-DD HH:mm'),
            uso: false,
        };

        if (result.success) {
            this.twilioClient.messages
                .create({
                    body: 'codigo de restablecimiento de contraseña: ' + codigo,
                    from: '+12054489977',
                    to: numberRecovery.nroCel,
                })
                .then(message => {
                    // tslint:disable-next-line: semicolon
                    console.log(message.sid)
                    const result = this.codeService.saveCode(code);
                    return res.status(HttpStatus.OK).json('Se guardo el codigo correctamente');
                })
                .then(err => {
                    console.log('error: ', err);
                    return res.status(HttpStatus.BAD_REQUEST).json({ err, message: 'Ocurrio un error al enviar el sms' });
                });
        } else {
            return res.status(HttpStatus.NOT_FOUND).json({ error: 'user not exist', message: 'usario no existente' });
        }
    }
}
