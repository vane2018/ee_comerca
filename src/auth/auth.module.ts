import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { User } from "../users/user.entity";
import { TypeOrmModule } from '@nestjs/typeorm';
import { LocalStrategy } from '../auth/passport/local.strategy';
import { UsersModule } from '../users/user.module';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from '../auth/passport/jwt.strategy';
import { GoogleStrategy } from './passport/google.strategy';
import { RolsModule } from '../rols/rols.module';
import { CodeModule } from '../code/code.module';
import { MailerNestModule } from '../mailer/mailer.module';
import { MailerModule, HandlebarsAdapter } from '@nest-modules/mailer';
import { ENV } from '../common/env.config';

const mail = ENV.USER_NO_REPLY;
const password = ENV.USER_NO_REPLY_PASSWORD;
const mailProvider = 'mail.comprartir.com';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]), MailerModule.forRoot({
      transport: {
        host: mailProvider, port: Number(587),
        auth: { user: mail, pass: password },
        tls: {
          // do not fail on invalid certs
          rejectUnauthorized: false
        }
      },
      defaults: {
        from: '"nest-modules" <modules@nestjs.com>',
      },
      template: {
        dir: 'src/email-templates',
        adapter: new HandlebarsAdapter(), // or new PugAdapter()
        options: {
          strict: true,
        },
      },
    }),
    UsersModule,
    RolsModule,   
    PassportModule,
    CodeModule,
    MailerNestModule,
    JwtModule.register({
      secret: 'Comprartir',
      signOptions: { expiresIn: 3600 },
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, LocalStrategy, JwtStrategy, GoogleStrategy],
  exports: [AuthService, LocalStrategy, JwtStrategy],
})
export class AuthModule { }