import { Injectable } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';
import { Storage, Bucket, GetSignedUrlConfig, GetSignedUrlResponse } from '@google-cloud/storage';
import { ENV } from '../common/env.config';

const FOLDER_NAME = 'upload';

const CREDENTIAL = {
    private_key: ENV.G_STORAGE_CREDENTIAL_PRIVATE_KEY.replace(/\\n/g, '\n'),
    client_email: ENV.G_STORAGE_CREDENTIAL_CLIENT_EMAIL
}

@Injectable()
export class FileService {
    private storage: Storage;
    private bucket: Bucket;
    constructor() {
        this.storage = new Storage({ credentials: CREDENTIAL });
        this.bucket = this.storage.bucket(ENV.G_STORAGE_BUCKET_NAME);
    }

    async getReadSignedUrl(fileName: string, cfg?: GetSignedUrlConfig): Promise<GetSignedUrlResponse> {
        const now = new Date();
        const nextDay = now.setHours(now.getHours() + 24);
        const defaultReadConfig = {
            action: 'read',
            expires: nextDay
        } as GetSignedUrlConfig;
        const file = this.bucket.file(`${FOLDER_NAME}/${fileName}`);
        const config = cfg ? cfg : defaultReadConfig;
        return await file.getSignedUrl(config)
    }

    async getWriteSignedUrl(fileName?: string, cfg?: GetSignedUrlConfig): Promise<any> {
        const now = new Date();
        const nextHour = now.setHours(now.getHours() + 1);
        const defaultWriteConfig = {
            action: 'write',
            expires: nextHour
        } as GetSignedUrlConfig;
        const name = fileName ? fileName : uuidv4();
        const file = this.bucket.file(`${FOLDER_NAME}/${name}`);
        const config = cfg ? cfg : defaultWriteConfig;
        const signedUrl = await file.getSignedUrl(config)
        return { fileName: name, signedUrl: signedUrl[0] }
    }
}
