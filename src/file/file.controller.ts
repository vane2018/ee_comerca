import { Controller, Post, UseInterceptors, UploadedFile, Get, UseGuards, Response, Param, HttpStatus } from '@nestjs/common';
import { GCloudStorageFileInterceptor, UploadedFileMetadata } from '@aginix/nestjs-gcloud-storage';
import { AuthGuard } from '@nestjs/passport';
import { PermissionGuard } from '../common/guards/permission.guards';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { FileService } from './file.service';

@ApiTags('File')
@Controller('file')
export class FileController {
    constructor(
        public fileService: FileService
    ) { }

    @Post('/upload')
    @UseInterceptors(GCloudStorageFileInterceptor(
        'file', undefined, { prefix: 'upload/app' }))
    UploadedFilesUsingInterceptor(
        @UploadedFile()
        file: UploadedFileMetadata,
    ) {
        console.log(`Storage URL: ${file.storageUrl}`, 'AppController');
    }

    @Get('/signed-url/read/:fileName')
    // @UseGuards(AuthGuard('jwt'), PermissionGuard)
    // @Permision('admin', 'super')
    @ApiOperation({ summary: 'test get signed url' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getReadSignedUrl(@Response() res, @Param('fileName') fileName) {
        const signed = await this.fileService.getReadSignedUrl(fileName);
        return res.status(HttpStatus.OK).json(signed);
    }

    @Get('/signed-url/write')
    // @UseGuards(AuthGuard('jwt'), PermissionGuard)
    // @Permision('admin', 'super')
    @ApiOperation({ summary: 'test get signed url' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getWriteSignedUrl(@Response() res) {
        const signed = await this.fileService.getWriteSignedUrl();
        return res.status(HttpStatus.OK).json(signed);
    }
}
