import { Module } from '@nestjs/common';
import { FileController } from './file.controller';
import { FileService } from './file.service';
import { GCloudStorageModule } from '@aginix/nestjs-gcloud-storage';
import { ENV } from '../common/env.config';

@Module({
  controllers: [FileController],
  providers: [FileService],
  imports: [
    GCloudStorageModule.withConfig({
      // keyFilename: 'COMPRARTIR-PRODUCTION-dd83a0aeb0d0.json',
      // keyFile: '../../COMPRARTIR-PRODUCTION-dd83a0aeb0d0.json',
      defaultBucketname: ENV.G_STORAGE_BUCKET_NAME,
      storageBaseUri: ENV.G_STORAGE_BASE_URI,
      predefinedAcl: 'private' // Default is publicRead
    })
  ],
})
export class FileModule { }
