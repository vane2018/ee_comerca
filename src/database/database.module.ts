import { Module, Global, DynamicModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as ormconfig from './orm.config';

function DatabaseOrmModule(): DynamicModule {
  return TypeOrmModule.forRoot({
    ...ormconfig,
    keepConnectionAlive: true
  });
}

@Global()
@Module({
  imports: [DatabaseOrmModule()],

  exports: [],
})
export class DatabaseModule { }
