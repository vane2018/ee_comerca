import { Delete, UseGuards } from '@nestjs/common';
import { Body, Controller, Post, Response, HttpStatus, Get, Param, Put } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBody, ApiCreatedResponse, ApiOperation, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Crud } from '@nestjsx/crud';
import { PermissionGuard } from '../common/guards/permission.guards';
import { Permision } from '../common/permission/permission.decorator';
import { UsersService } from '../users/user.service';
import { Cobro } from './cobrarcomision.entity';
import { CobrarcomisionService } from './cobrarcomision.service';
import { cobroComision } from './dto/consulta.dto';
import { estadoDto } from './dto/estado.dto';
import { prueba } from './dto/prueba.dto';

@Crud({
    model: { type: Cobro },
    params: {
        id: {
            field: 'id',
            type: 'uuid',
            primary: true,
        },
    },
    query: {
        join: {
            user: {
                allow: [],
            },
        },
    },
})

@ApiTags('CobrarComision')
@Controller('cobrarcomision')
export class CobrarcomisionController {
    constructor(

        public cobrarService: CobrarcomisionService,
        public userServic: UsersService

    ) { }
    @Post('/cobro/comision')
    @ApiOperation({ summary: 'Registra un cobro de comision' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async crearCobro(@Response() res, @Body() datos: cobroComision): Promise<any> {
        let commerces = await this.cobrarService.guardadCobro(datos);
        return res.status(HttpStatus.OK).json(commerces);

    }

    @Post('/guardarCobro')
    @ApiOperation({ summary: 'Registra una comision' })
    @ApiCreatedResponse({ description: 'The product has been successfully created.', type: prueba })
    @ApiBody({ type: cobroComision })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async saveCobbro(@Response() res, @Body() createCommissionDto: cobroComision): Promise<any> {
        try {
            let customerResp = await this.userServic.findOne(createCommissionDto.idUser);
            if (customerResp) {
                let commissionSave: prueba = {
                    monto: createCommissionDto.monto,
                    estado: createCommissionDto.estado,
                    user: customerResp
                }
                const comision = await this.cobrarService.saveCobro(commissionSave);
                return res.status(HttpStatus.OK).json(comision);
            } else {
                return res.status(HttpStatus.BAD_REQUEST).json({ message: 'customer no encontrados' });
            }
        } catch (error) {
            console.log('error al crear producto: ', error);
            return res.status(HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar comision', error });
        }
    }
    @Get('/obtener/comision')
    /* @UseGuards(AuthGuard('jwt')) */
    @UseGuards(AuthGuard('jwt'), PermissionGuard)
    @Permision('admin', 'empleado')
    @ApiOperation({ summary: 'Obtiene todas las comision' })
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: '\'The resource was not found\'' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getProductsOffert(@Response() res): Promise<Cobro> {
        const products = await this.cobrarService.getcobro();
        return res.status(HttpStatus.OK).json(products);
    }
    @Get(':idUser')
    @ApiParam({ type: 'string', name: 'idUser' })
    public async getByUser(@Response() res, @Param() param): Promise<any> {
        const commissions = await this.cobrarService.getCobroByUserId(param.idUser);
        return res.status(HttpStatus.OK).json(commissions);
    }

    @Get('/:id')
    /* @UseGuards(AuthGuard('jwt')) */
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: 'Bad Request' })
    @ApiResponse({ status: 404, description: '\'The resource was not found\'' })
    @ApiResponse({ status: 409, description: 'Conflict' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getProductsId(@Response() res, @Param() param): Promise<any> {
        const products = await this.cobrarService.getCobroId(param.id);
        return res.status(HttpStatus.OK).json(products);
    }

    @Put('/edit/:id')
    public async putProducts(@Response() res, @Param() param, @Body() estado: estadoDto): Promise<any> {
        try {
            let cobro = await this.cobrarService.findOne(param.id);
            if (cobro) {
                cobro.estado = estado.estado;
                cobro.id = param.id;
                const producto = await this.cobrarService.actualizar(param.id, estado)
                return res.status(HttpStatus.OK).json(producto);
            } else {
                return res.status(HttpStatus.BAD_REQUEST).json({ message: ' producto no encontrados' });
            }
        } catch (error) {
            console.log('error al modifcar producto: ', error);
            return res.status(HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el producto', error });
        }
    }

    @Delete('/:id')
    @ApiResponse({ status: 200, description: 'data returned correctly' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 400, description: "Bad Request" })
    @ApiResponse({ status: 404, description: "'The resource was not found'" })
    @ApiResponse({ status: 409, description: "Conflict" })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async deleteCommisions(@Response() res, @Param() param): Promise<any> {
        await this.cobrarService.deleteCobro(param.id);
        return res.status(HttpStatus.OK).json({ message: 'commision deleted' });
    }
}
