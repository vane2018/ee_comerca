import { ApiProperty } from "@nestjs/swagger";
import Api = require("twilio/lib/rest/Api");

export class cobroComision {
    @ApiProperty()
    idUser: string;
    @ApiProperty()
    monto: number;
    @ApiProperty()
    estado: string;
}