import { ApiProperty } from "@nestjs/swagger";

export class estadoDto {

    @ApiProperty()
    estado: string;
}