import { User } from "../users/user.entity";
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
@Entity('COBRO')
export class Cobro {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column()
    monto: number;
    @Column('varchar', { length: 500, nullable: true })
    estado: string;
    @Column('varchar', { length: 500, nullable: true })
    idusuario: string;

    @ManyToOne(() => User, (user: User) => user.cobro)
    @JoinColumn()
    user: User;

}