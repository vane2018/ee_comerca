import { Test, TestingModule } from '@nestjs/testing';
import { CobrarcomisionController } from './cobrarcomision.controller';

describe('Cobrarcomision Controller', () => {
  let controller: CobrarcomisionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CobrarcomisionController],
    }).compile();

    controller = module.get<CobrarcomisionController>(CobrarcomisionController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
