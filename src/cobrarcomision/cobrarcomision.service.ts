import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { UsersService } from '../users/user.service';
import { Cobro } from './cobrarcomision.entity';
import { cobroComision } from './dto/consulta.dto';
import { estadoDto } from './dto/estado.dto';
import { prueba } from './dto/prueba.dto';

@Injectable()
export class CobrarcomisionService extends TypeOrmCrudService<Cobro> {
    constructor(@InjectRepository(Cobro) repo, public serviceUser: UsersService) {
        super(repo)
    }

    public async guardadCobro(datos: cobroComision): Promise<Cobro> {
        let cobro = await this.repo.create(datos);
        cobro.idusuario = datos.idUser;
        return await this.repo.save(cobro);


    }

    public async saveCobro(commisionDto: prueba): Promise<prueba> {
        try {
            let comision = this.repo.create(commisionDto);
            return await this.repo.save(comision);
        } catch (error) {
            console.log('error', error);
        }
    }


    public async getcobro(): Promise<Cobro[]> {
        try {
            let usuarios = await this.repo.find({ relations: ['user'] });
            return usuarios;
        } catch (error) {
            console.log('error: ', error);
        }
    }
    public async getCobroId(id: string) {
        return await this.repo.findOne(id);
    }

    public async actualizar(id: string, estado: estadoDto) {

        let cobro = await this.repo.findOne(id);

        cobro.estado = estado.estado;
        cobro.id = id;




        return await this.repo.save(cobro);



    }
    public async getCobroByUserId(userId: string): Promise<any> {
        let comisions = await this.repo.find({ where: { user: userId } });
        return comisions;
    }

    public async getCobroByIds(ids: string): Promise<any> {
        let comisions = await this.repo.findOne(ids);
        return comisions;
    }
    public async deleteCobro(id: string) {
        await this.repo.delete(id);
    }
}
