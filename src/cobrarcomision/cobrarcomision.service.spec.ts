import { Test, TestingModule } from '@nestjs/testing';
import { CobrarcomisionService } from './cobrarcomision.service';

describe('CobrarcomisionService', () => {
  let service: CobrarcomisionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CobrarcomisionService],
    }).compile();

    service = module.get<CobrarcomisionService>(CobrarcomisionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
