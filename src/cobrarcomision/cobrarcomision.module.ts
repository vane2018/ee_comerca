import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '../users/user.module';
import { CobrarcomisionController } from './cobrarcomision.controller';
import { Cobro } from './cobrarcomision.entity';
import { CobrarcomisionService } from './cobrarcomision.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Cobro]), UsersModule

  ],
  controllers: [CobrarcomisionController],
  providers: [CobrarcomisionService]
})
export class CobrarcomisionModule { }
