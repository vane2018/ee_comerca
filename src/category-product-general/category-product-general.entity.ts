import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { ProductSubcategory } from "../subcategory-products/subcategory-products.entity";
@Entity('CATEGORIA_GRAL_PRODUCTS')
export class ProductGeneral {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column('varchar', { length: 500 })
    name: string;

    @OneToMany(() => ProductSubcategory, (productSubcategory: ProductSubcategory) => productSubcategory.productsGeneral)
    productsSubcategory: ProductSubcategory[];
}