import { Module } from '@nestjs/common';
import { ProductGeneral } from './category-product-general.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoryGeneralController } from './category-product-general.controller';
import { CategoryGeneralService } from './category-product-general.service';

@Module({
    imports: [TypeOrmModule.forFeature([ProductGeneral])],
    controllers: [CategoryGeneralController],
    providers: [CategoryGeneralService],
    exports: [CategoryGeneralService],
})
export class CategoryGeneralModule { }
