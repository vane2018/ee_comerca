import {
    Controller,
    Get,
    Response,
    HttpStatus,
    Param,
} from '@nestjs/common';
import { Crud } from '@nestjsx/crud';
import { CategoryProductGeneralDto } from './dto/categoryProductGeneral.dto';
import { ApiTags, ApiResponse, ApiOperation, ApiBody } from '@nestjs/swagger';
import { CategoryGeneralService } from './category-product-general.service';
@Crud({
    model: {

        type: CategoryProductGeneralDto,
    },
    params: {
        id: {
            field: 'id',
            type: 'uuid',
            primary: true,
        },
    },
    query: {
        join: {

            productsSubcategory: {
                allow: []
            }
        },
    }
})


@ApiTags('Product-Category-General')
@Controller('category-general')
export class CategoryGeneralController {

    constructor(public service: CategoryGeneralService) { }
    @Get('/:id')
    @ApiOperation({ summary: 'Obtiene una categoria General por Id, con su [] de subcategoria' })
    @ApiResponse({ status: 200, description: 'Los datos han sido devueltos correctamente.' })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({ status: 500, description: 'Internal Server Error.' })
    public async getCommerce(@Response() res, @Param() param): Promise<any> {
        const categoryGeneral = await this.service.getCategoryById(param.id);
        return res.status(HttpStatus.OK).json(categoryGeneral);
    }
}

