import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { CategoryProductGeneralDto } from './dto/categoryProductGeneral.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductGeneral } from './category-product-general.entity';

@Injectable()
export class CategoryGeneralService extends TypeOrmCrudService<CategoryProductGeneralDto> {

    constructor(@InjectRepository(ProductGeneral) repo) {
        super(repo);
    }

    public async getCategoryGeneral(): Promise<CategoryProductGeneralDto[]> {

        try {
            return await this.repo.find({ relations: ['productsSubcategory'] });
        } catch (error) {
            console.log('error: ', error);
        }
    }

    public async getCategoryById(id: string): Promise<any> {
        return await this.repo.findOne(id, { relations: ['productsSubcategory'] });
    }
}
