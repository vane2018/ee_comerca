import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Rols } from './rols.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { RolDto } from './dto/rol.dto';

@Injectable()
export class RolsService extends TypeOrmCrudService<RolDto> {
    constructor(@InjectRepository(Rols) repo) {
        super(repo);
    }

    public async findRol(name: any): Promise<any> {
        try {
            const rolEncontrado = await this.repo.findOne({ where: name });
            return rolEncontrado;
        } catch (err) {
            console.log('error: ', err);
        }
    }
}
