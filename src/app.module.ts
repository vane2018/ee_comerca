import { Module, RequestMethod, MiddlewareConsumer } from '@nestjs/common';
import { Connection } from 'typeorm';
import { CommerceModule } from './commerce/commerce.module';
import { UsersModule } from './users/user.module';
import { AuthModule } from './auth/auth.module';
/* import { SellerModule } from './sellers/seller.module'; */
import { ProductsModule } from './products/products.module';
import { ProductTypesModule } from './product-category/product-category.module';
/* import { CustomersModule } from './customers/customers.module'; */
import { RolsModule } from './rols/rols.module';
import { PermissionsModule } from './permissions/permissions.module';
import { MailerNestModule } from './mailer/mailer.module';
import { CodeModule } from './code/code.module';
import { MailerModule } from '@nest-modules/mailer';
import { CommissionsModule } from './commissions/commissions.module';
import { PurchasesModule } from './purchases/purchases.module'
import { PymentsModule } from './payments/payments.module';
import { PymentTypesModule } from './payment-types/payment-types.module';
import { DatabaseModule } from './database/database.module';
import { CategoryGeneralModule } from './category-product-general/category-product-general.module';
import { SubcategoryProductsModule } from './subcategory-products/subcategory-products.module';
import { ProductxuserModule } from './productxuser/productxuser.module';
import { ChatModule } from './chat/chat.module';
import { MensajesModule } from './mensajes/mensajes.module';
import { NotificacionesModule } from './notificaciones/notificaciones.module';
import { ShippingTypeModule } from './shipping-type/shipping-type.module';
import { ShippingModule } from './shipping/shipping.module';
import { IntegracionModule } from './integracion/integracion.module';
import { MercadoPagoModule } from './mercado-pago/mercado-pago.module';
import { MercadoModule } from './mercado/mercado.module';
import { CobrarcomisionModule } from './cobrarcomision/cobrarcomision.module';
import { FileModule } from './file/file.module';

@Module({
  imports: [
    DatabaseModule,
    CommerceModule,
    UsersModule,
    AuthModule,
    /*  CustomersModule, */
    /*  SellerModule, */
    ProductsModule,
    ProductTypesModule,

    /*   CustomersModule, */
    RolsModule,
    PermissionsModule,
    MailerNestModule,
    MailerModule,
    CodeModule,
    CommissionsModule,
    PurchasesModule,
    PymentsModule,
    PymentTypesModule,
    CategoryGeneralModule,
    SubcategoryProductsModule,
    ProductxuserModule,
    ChatModule,
    MensajesModule,
    NotificacionesModule,
    ShippingTypeModule,
    ShippingModule,
    IntegracionModule,
    MercadoPagoModule,
    MercadoModule,
    CobrarcomisionModule,
    FileModule],
  controllers: [],
  providers: [],
  exports: []
})
export class AppModule {
  constructor(private readonly connection: Connection) { }
}
