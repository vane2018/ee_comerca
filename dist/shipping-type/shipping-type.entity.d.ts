export declare enum shippingTypes {
    OTROS = "Acordar con el vendedor",
    CADYGO = "Cadygo"
}
export declare class ShippingTypes {
    id: string;
    name: shippingTypes;
    description: string;
}
