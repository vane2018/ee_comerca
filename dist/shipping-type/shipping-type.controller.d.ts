import { shippingTypeDto } from './dto/shippingCreateDto.dto';
import { ShippingTypeService } from './shipping-type.service';
export declare class ShippingTypeController {
    service: ShippingTypeService;
    constructor(service: ShippingTypeService);
    createPurchase(res: any, shippingDto: shippingTypeDto): Promise<any>;
}
