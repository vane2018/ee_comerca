"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShippingTypeController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const crud_1 = require("@nestjsx/crud");
const shipping_dto_1 = require("./dto/shipping.dto");
const shippingCreateDto_dto_1 = require("./dto/shippingCreateDto.dto");
const shipping_type_entity_1 = require("./shipping-type.entity");
const shipping_type_service_1 = require("./shipping-type.service");
let ShippingTypeController = class ShippingTypeController {
    constructor(service) {
        this.service = service;
    }
    async createPurchase(res, shippingDto) {
        let response = await this.service.searchName(shippingDto.nombre);
        return res.status(common_1.HttpStatus.OK).json(response);
    }
};
__decorate([
    common_1.Post('create'),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, shippingCreateDto_dto_1.shippingTypeDto]),
    __metadata("design:returntype", Promise)
], ShippingTypeController.prototype, "createPurchase", null);
ShippingTypeController = __decorate([
    crud_1.Crud({
        model: {
            type: shipping_type_entity_1.ShippingTypes
        },
        routes: {
            exclude: ['createManyBase',
                'deleteOneBase',
                'updateOneBase',
                'replaceOneBase',
                'createOneBase',
                'getOneBase'],
            getManyBase: {
                decorators: [swagger_1.ApiOkResponse({ description: 'datos devueltos correctamente', type: [shipping_dto_1.ShippingTypesDto] })]
            }
        },
        serialize: {
            getMany: shipping_dto_1.ShippingTypesDto,
        },
        params: {
            id: {
                field: 'id',
                type: 'uuid',
                primary: true
            },
        },
    }),
    swagger_1.ApiTags('Shipping-Types'),
    common_1.Controller('shipping-type'),
    __metadata("design:paramtypes", [shipping_type_service_1.ShippingTypeService])
], ShippingTypeController);
exports.ShippingTypeController = ShippingTypeController;
//# sourceMappingURL=shipping-type.controller.js.map