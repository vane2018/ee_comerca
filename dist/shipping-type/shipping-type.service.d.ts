import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { ShippingTypes } from './shipping-type.entity';
export declare class ShippingTypeService extends TypeOrmCrudService<ShippingTypes> {
    constructor(repo: any);
    searchName(nombre: string): Promise<ShippingTypes>;
}
