"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShippingTypeModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const shipping_type_controller_1 = require("./shipping-type.controller");
const shipping_type_entity_1 = require("./shipping-type.entity");
const shipping_type_service_1 = require("./shipping-type.service");
let ShippingTypeModule = class ShippingTypeModule {
};
ShippingTypeModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([shipping_type_entity_1.ShippingTypes])],
        controllers: [shipping_type_controller_1.ShippingTypeController],
        providers: [shipping_type_service_1.ShippingTypeService]
    })
], ShippingTypeModule);
exports.ShippingTypeModule = ShippingTypeModule;
//# sourceMappingURL=shipping-type.module.js.map