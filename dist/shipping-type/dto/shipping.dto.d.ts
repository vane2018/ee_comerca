import { shippingTypes } from "../shipping-type.entity";
export declare class ShippingTypesDto {
    readonly name: shippingTypes;
    readonly description: string;
}
