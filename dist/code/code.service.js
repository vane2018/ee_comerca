"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CodeService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const code_entity_1 = require("./code.entity");
const crud_typeorm_1 = require("@nestjsx/crud-typeorm");
const moment = require("moment");
let CodeService = class CodeService extends crud_typeorm_1.TypeOrmCrudService {
    constructor(repo) {
        super(repo);
    }
    async saveCode(codigo) {
        const newCodigo = this.repo.create(codigo);
        try {
            return await this.repo.save(newCodigo);
        }
        catch (error) {
            console.log('error: ', error);
        }
    }
    async validateMailandCodigo(validate) {
        const resp = validate.email ? await this.repo.findOne({ where: { email: validate.email, code: validate.code } }) : await this.repo.findOne({ where: { nroCel: validate.nroCel, code: validate.code } });
        let validateResp = {
            succeses: false,
            code: null,
        };
        if (resp) {
            const fecha1 = moment(resp.expiredAt);
            const fecha2 = moment();
            if ((resp.email === validate.email || resp.nroCel === validate.nroCel) && resp.code === validate.code) {
                if (fecha1 >= fecha2) {
                    validateResp.succeses = true;
                    validateResp.code = resp;
                    return validateResp;
                }
                else {
                    await this.repo.delete({ id: resp.id });
                    return validateResp;
                }
            }
        }
        return validateResp;
    }
    async validateExpired(validate) {
        const resp = await this.repo.findOne({ where: { email: validate.email, code: validate.code } });
        const fecha1 = moment(resp.expiredAt);
        const fecha2 = moment();
        let validateResp = {
            succeses: false,
            code: null,
        };
        if (fecha1 <= fecha2) {
            validateResp.succeses = true;
            validateResp.code = resp;
            return validateResp;
        }
        return validateResp;
    }
    async actualizarCodigo(codigo) {
        const id = codigo.id;
        await this.repo.update({ id }, { validate: !codigo.validate });
    }
    async actualizarUsoCodigo(codigo) {
        const id = codigo.id;
        await this.repo.update({ id }, { uso: !codigo.uso });
    }
};
CodeService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(code_entity_1.Code)),
    __metadata("design:paramtypes", [Object])
], CodeService);
exports.CodeService = CodeService;
//# sourceMappingURL=code.service.js.map