export declare class Code {
    id: string;
    email: string;
    nroCel: string;
    code: string;
    validate: boolean;
    expiredAt: string;
    uso: boolean;
}
