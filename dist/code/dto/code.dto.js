"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CodeDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class CodeDto {
}
__decorate([
    swagger_1.ApiProperty(),
    __metadata("design:type", String)
], CodeDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Email del usuario que desea recuperar la contraseña' }),
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], CodeDto.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'nro del usuario que desea recuperar la contraseña' }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CodeDto.prototype, "nroCel", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Codigo generado para reguperar la contraseña' }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CodeDto.prototype, "code", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Variable para ver si el codigo esta valido para usar o no' }),
    __metadata("design:type", Boolean)
], CodeDto.prototype, "validate", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Fecha de expiracion del codigo, 30min' }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CodeDto.prototype, "expiredAt", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Variable que me indica si ya se uso o no el codigo' }),
    __metadata("design:type", Boolean)
], CodeDto.prototype, "uso", void 0);
exports.CodeDto = CodeDto;
//# sourceMappingURL=code.dto.js.map