export declare class ValidateDto {
    readonly email: string;
    readonly nroCel: string;
    readonly code: string;
    readonly uso: string;
}
