import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { CodeDto } from './dto/code.dto';
import { ICode } from './interface/code.interface';
import { ValidateDto } from './dto/validateDto.dto';
export interface IValidateResp {
    succeses: boolean;
    code: ICode;
}
export declare class CodeService extends TypeOrmCrudService<ICode> {
    constructor(repo: any);
    saveCode(codigo: CodeDto): Promise<any>;
    validateMailandCodigo(validate: ValidateDto): Promise<IValidateResp>;
    validateExpired(validate: ValidateDto): Promise<IValidateResp>;
    actualizarCodigo(codigo: CodeDto): Promise<any>;
    actualizarUsoCodigo(codigo: CodeDto): Promise<any>;
}
