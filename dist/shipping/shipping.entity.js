"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Shipping = void 0;
const shipping_type_entity_1 = require("../shipping-type/shipping-type.entity");
const typeorm_1 = require("typeorm");
const purchase_entity_1 = require("../purchases/purchase.entity");
let Shipping = class Shipping {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Shipping.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: "float8", nullable: true }),
    __metadata("design:type", Number)
], Shipping.prototype, "costo", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], Shipping.prototype, "codigoEnvio", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], Shipping.prototype, "origen", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], Shipping.prototype, "destino", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], Shipping.prototype, "estadoEnvio", void 0);
__decorate([
    typeorm_1.OneToOne(() => purchase_entity_1.Purchase, (purchases) => purchases.shipping),
    __metadata("design:type", purchase_entity_1.Purchase)
], Shipping.prototype, "purchases", void 0);
__decorate([
    typeorm_1.ManyToOne(type => shipping_type_entity_1.ShippingTypes, { cascade: true, eager: false }),
    typeorm_1.JoinColumn({ name: 'shipping_types_id' }),
    __metadata("design:type", shipping_type_entity_1.ShippingTypes)
], Shipping.prototype, "shippingTypes", void 0);
Shipping = __decorate([
    typeorm_1.Entity('SHIPPING')
], Shipping);
exports.Shipping = Shipping;
//# sourceMappingURL=shipping.entity.js.map