"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShippingDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class ShippingDto {
}
__decorate([
    swagger_1.ApiProperty({ description: 'monto de la compra' }),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", Number)
], ShippingDto.prototype, "costo", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'monto de la compra' }),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], ShippingDto.prototype, "codigoEnvio", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'origen del envio' }),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], ShippingDto.prototype, "origen", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'destino del envio' }),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], ShippingDto.prototype, "destino", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'estado del envio' }),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], ShippingDto.prototype, "estadoEnvio", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'id del tipo de envio', type: String, format: 'uuid' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsUUID(),
    __metadata("design:type", String)
], ShippingDto.prototype, "shippingTypes", void 0);
exports.ShippingDto = ShippingDto;
//# sourceMappingURL=shippingDTO.dto.js.map