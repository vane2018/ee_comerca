export declare class calculationDto {
    readonly sourceLatitude: number;
    readonly sourceLongitude: number;
    readonly targetLatitude: number;
    readonly targetLongitude: number;
}
