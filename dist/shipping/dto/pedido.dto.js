"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.pedidoDto = void 0;
const swagger_1 = require("@nestjs/swagger");
class pedidoDto {
}
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'Token provisto en datos de acceso o por mail' }),
    __metadata("design:type", String)
], pedidoDto.prototype, "commerceMarketplaceId", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'Token provisto en datos de acceso o por mail' }),
    __metadata("design:type", String)
], pedidoDto.prototype, "targetStreetName", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'Token provisto en datos de acceso o por mail' }),
    __metadata("design:type", String)
], pedidoDto.prototype, "targetStreetNumber", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'Token provisto en datos de acceso o por mail' }),
    __metadata("design:type", String)
], pedidoDto.prototype, "targetNeighborhood", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'Token provisto en datos de acceso o por mail' }),
    __metadata("design:type", String)
], pedidoDto.prototype, "targetDirectionDetail", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'Token provisto en datos de acceso o por mail' }),
    __metadata("design:type", String)
], pedidoDto.prototype, "targetDirectionLatitude", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'Token provisto en datos de acceso o por mail' }),
    __metadata("design:type", String)
], pedidoDto.prototype, "targetDirectionLongitude", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'Token provisto en datos de acceso o por mail' }),
    __metadata("design:type", String)
], pedidoDto.prototype, "customer", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'Token provisto en datos de acceso o por mail' }),
    __metadata("design:type", String)
], pedidoDto.prototype, "customerPhone", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'Token provisto en datos de acceso o por mail' }),
    __metadata("design:type", String)
], pedidoDto.prototype, "price", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'Token provisto en datos de acceso o por mail' }),
    __metadata("design:type", String)
], pedidoDto.prototype, "detail", void 0);
exports.pedidoDto = pedidoDto;
//# sourceMappingURL=pedido.dto.js.map