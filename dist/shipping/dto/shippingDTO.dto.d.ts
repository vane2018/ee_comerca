export declare class ShippingDto {
    readonly costo: number;
    readonly codigoEnvio: string;
    readonly origen: string;
    readonly destino: string;
    readonly estadoEnvio: string;
    readonly shippingTypes: string;
}
