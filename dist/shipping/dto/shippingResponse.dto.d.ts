export declare class shippingResponseDto {
    readonly costo: number;
    readonly codigoEnvio: string;
    readonly origen: string;
    readonly destino: string;
    readonly estadoEnvio: string;
}
