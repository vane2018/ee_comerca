export declare class modShipping {
    readonly id: string;
    readonly costo: number;
    readonly codigoEnvio: string;
    readonly origen: string;
    readonly destino: string;
    readonly estadoEnvio: string;
    readonly shippingTypes: string;
}
