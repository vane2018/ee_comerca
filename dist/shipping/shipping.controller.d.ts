import { CrudController } from '@nestjsx/crud';
import { calculationDto } from './dto/calculationShipping.dto';
import { pedidoDto } from './dto/pedido.dto';
import { shippingComercio } from './dto/shiping-comercio.dto';
import { shippingCancelar } from './dto/shipping-cancelar.dto';
import { Shipping } from './shipping.entity';
import { ShippingService } from './shipping.service';
export declare class ShippingController implements CrudController<Shipping> {
    service: ShippingService;
    constructor(service: ShippingService);
    getPedidoComercios(res: any, shippingComer: shippingComercio): Promise<any>;
    crearComercios(res: any, comercio: pedidoDto): Promise<any>;
    calculationShipping(res: any, datos: calculationDto): Promise<any>;
    cancelarPedidos(res: any, cancelar: shippingCancelar): Promise<any>;
}
