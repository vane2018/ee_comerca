import { ShippingTypes } from "../shipping-type/shipping-type.entity";
import { Purchase } from "../purchases/purchase.entity";
export declare class Shipping {
    id: string;
    costo: number;
    codigoEnvio: string;
    origen: string;
    destino: string;
    estadoEnvio: string;
    purchases: Purchase;
    shippingTypes: ShippingTypes;
}
