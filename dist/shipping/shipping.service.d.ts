import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';
import { pedidoDto } from './dto/pedido.dto';
import { shippingComercio } from './dto/shiping-comercio.dto';
import { shippingCancelar } from './dto/shipping-cancelar.dto';
import { Shipping } from './shipping.entity';
import { calculationDto } from './dto/calculationShipping.dto';
export declare class ShippingService extends TypeOrmCrudService<Shipping> {
    constructor(repo: Repository<Shipping>);
    getShippingComercio(mybody: shippingComercio): Promise<any>;
    cancelarPedido(mybody: shippingCancelar): Promise<any>;
    calculation(datos: calculationDto): Promise<any>;
    generateShipping(pedido: pedidoDto): Promise<any>;
    reduccion(valor: number): number;
}
