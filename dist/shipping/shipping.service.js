"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShippingService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const crud_typeorm_1 = require("@nestjsx/crud-typeorm");
const typeorm_2 = require("typeorm");
const shipping_entity_1 = require("./shipping.entity");
const env_config_1 = require("../common/env.config");
const fetch = require("node-fetch");
let ShippingService = class ShippingService extends crud_typeorm_1.TypeOrmCrudService {
    constructor(repo) {
        super(repo);
    }
    async getShippingComercio(mybody) {
        return fetch(env_config_1.ENV.CADYGO_API + '/apis/v1/external-marketplace/orders/commerce/' + mybody.commerceMarketplaceId, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + env_config_1.ENV.CADYGO_TOKEN,
            }
        })
            .then(function (response) {
            return response.json();
        })
            .then(function (data) {
            return data;
        })
            .catch(function (err) {
            console.error(err);
        });
    }
    async cancelarPedido(mybody) {
        return fetch(env_config_1.ENV.CADYGO_API + '/apis/v1/external-marketplace/orders/' + mybody.externalOrderId, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + env_config_1.ENV.CADYGO_TOKEN,
            }
        })
            .then(function (response) {
            return response.json();
        })
            .then(function (data) {
            return data;
        })
            .catch(function (err) {
            console.error(err);
        });
    }
    async calculation(datos) {
        const mibody = {
            sourceLatitude: this.reduccion(datos.sourceLatitude),
            sourceLongitude: this.reduccion(datos.sourceLongitude),
            targetLatitude: this.reduccion(datos.targetLatitude),
            targetLongitude: this.reduccion(datos.targetLongitude)
        };
        return fetch(env_config_1.ENV.CADYGO_API + '/apis/v1/external-marketplace/shipping', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + env_config_1.ENV.CADYGO_TOKEN,
            },
            body: JSON.stringify(mibody),
            cache: 'no-cache'
        })
            .then(function (response) {
            return response.json();
        })
            .then(function (data) {
            return data;
        })
            .catch(function (err) {
            console.error(err);
        });
    }
    async generateShipping(pedido) {
        const bo = {
            commerceMarketplaceId: pedido.commerceMarketplaceId,
            targetStreetName: pedido.targetStreetName,
            targetStreetNumber: pedido.targetStreetNumber,
            targetNeighborhood: pedido.targetNeighborhood,
            targetDirectionDetail: pedido.targetDirectionDetail,
            targetDirectionLatitude: pedido.targetDirectionLatitude,
            targetDirectionLongitude: pedido.targetDirectionLongitude,
            customer: pedido.customer,
            customerPhone: pedido.customerPhone,
            price: pedido.price,
            detail: pedido.detail
        };
        return fetch(env_config_1.ENV.CADYGO_API + '/apis/v1/external-marketplace/orders', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + env_config_1.ENV.CADYGO_TOKEN,
            },
            body: JSON.stringify(bo),
            cache: 'no-cache'
        })
            .then(function (response) {
            return response.json();
        })
            .then(function (data) {
            return data;
        })
            .catch(function (err) {
            console.error(err);
        });
    }
    reduccion(valor) {
        let number = valor;
        let letra = number.toString().split("");
        let vect = [];
        let vect1 = [];
        vect = letra;
        let j = 0;
        for (let index = 0; index < vect.length; index++) {
            if (vect[index] !== '.') {
                vect1[j] = vect[index];
                j = j + 1;
            }
            else {
                if (vect[index] === '.') {
                    vect1[j] = vect[index];
                    vect1[j + 1] = vect[index + 1];
                    vect1[j + 2] = vect[index + 2];
                    vect1[j + 3] = vect[index + 3];
                    vect1[j + 4] = vect[index + 4];
                    index = vect.length + 1;
                }
            }
        }
        if (vect1[7] == "0")
            [
                vect1[7] = "1"
            ];
        let precio = vect1.join('');
        let abs = parseFloat(precio);
        return abs;
    }
};
ShippingService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(shipping_entity_1.Shipping)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], ShippingService);
exports.ShippingService = ShippingService;
//# sourceMappingURL=shipping.service.js.map