"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShippingModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const shipping_controller_1 = require("./shipping.controller");
const shipping_entity_1 = require("./shipping.entity");
const shipping_service_1 = require("./shipping.service");
let ShippingModule = class ShippingModule {
};
ShippingModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([shipping_entity_1.Shipping])],
        providers: [shipping_service_1.ShippingService],
        controllers: [shipping_controller_1.ShippingController]
    })
], ShippingModule);
exports.ShippingModule = ShippingModule;
//# sourceMappingURL=shipping.module.js.map