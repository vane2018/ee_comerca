"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShippingController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const crud_1 = require("@nestjsx/crud");
const calculationShipping_dto_1 = require("./dto/calculationShipping.dto");
const pedido_dto_1 = require("./dto/pedido.dto");
const shiping_comercio_dto_1 = require("./dto/shiping-comercio.dto");
const shipping_cancelar_dto_1 = require("./dto/shipping-cancelar.dto");
const shippingDTO_dto_1 = require("./dto/shippingDTO.dto");
const shippingResponse_dto_1 = require("./dto/shippingResponse.dto");
const shipping_entity_1 = require("./shipping.entity");
const shipping_service_1 = require("./shipping.service");
let ShippingController = class ShippingController {
    constructor(service) {
        this.service = service;
    }
    async getPedidoComercios(res, shippingComer) {
        let comercios = await this.service.getShippingComercio(shippingComer);
        return res.status(common_1.HttpStatus.OK).json(comercios);
    }
    async crearComercios(res, comercio) {
        let comercios = await this.service.generateShipping(comercio);
        return res.status(common_1.HttpStatus.OK).json(comercios);
    }
    async calculationShipping(res, datos) {
        let datesShipping = await this.service.calculation(datos);
        return res.status(common_1.HttpStatus.OK).json(datesShipping);
    }
    async cancelarPedidos(res, cancelar) {
        let comercios = await this.service.cancelarPedido(cancelar);
        return res.status(common_1.HttpStatus.OK).json(comercios);
    }
};
__decorate([
    common_1.Post('/cadygo/pedidoxComercio'),
    swagger_1.ApiOperation({ summary: 'crea comercio' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    swagger_1.ApiBody({ type: shiping_comercio_dto_1.shippingComercio }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, shiping_comercio_dto_1.shippingComercio]),
    __metadata("design:returntype", Promise)
], ShippingController.prototype, "getPedidoComercios", null);
__decorate([
    common_1.Post('/cadygo/crear-pedido'),
    swagger_1.ApiOperation({ summary: 'crea comercio' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    swagger_1.ApiBody({ type: pedido_dto_1.pedidoDto }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, pedido_dto_1.pedidoDto]),
    __metadata("design:returntype", Promise)
], ShippingController.prototype, "crearComercios", null);
__decorate([
    common_1.Post('/cadygo/calculation-envio'),
    swagger_1.ApiOperation({ summary: 'calcula el precio de envio' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    swagger_1.ApiBody({ type: calculationShipping_dto_1.calculationDto }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, calculationShipping_dto_1.calculationDto]),
    __metadata("design:returntype", Promise)
], ShippingController.prototype, "calculationShipping", null);
__decorate([
    common_1.Put('/cadygo/cancelar-pedido'),
    swagger_1.ApiOperation({ summary: 'cancelar-pedido' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    swagger_1.ApiBody({ type: shipping_cancelar_dto_1.shippingCancelar }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, shipping_cancelar_dto_1.shippingCancelar]),
    __metadata("design:returntype", Promise)
], ShippingController.prototype, "cancelarPedidos", null);
ShippingController = __decorate([
    crud_1.Crud({
        model: {
            type: shipping_entity_1.Shipping
        },
        routes: {
            exclude: ['createManyBase'],
            createOneBase: {
                decorators: [swagger_1.ApiBadRequestResponse({ description: 'Los datos no son los correctos' }),
                    swagger_1.ApiInternalServerErrorResponse({ description: 'Error interno del servidor' })]
            }
        },
        dto: {
            create: shippingDTO_dto_1.ShippingDto,
        },
        serialize: {
            create: shippingResponse_dto_1.shippingResponseDto,
        },
        params: {
            id: {
                field: 'id',
                type: 'uuid',
                primary: true
            }
        },
        query: {
            join: {
                shippingTypes: {}
            }
        }
    }),
    swagger_1.ApiTags('Shipping'),
    common_1.Controller('shipping'),
    __metadata("design:paramtypes", [shipping_service_1.ShippingService])
], ShippingController);
exports.ShippingController = ShippingController;
//# sourceMappingURL=shipping.controller.js.map