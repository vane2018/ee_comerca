import { Commerce } from '../commerce/commerce.entity';
import { ProductTypes } from '../product-category/product-category.entity';
import { Detalle } from '../detalle/detalle.entity';
export declare class Product {
    id: string;
    name: string;
    description: string;
    price: number;
    pic: string[];
    view: number;
    like: number;
    priceOffer: number;
    porcentajeDesc: number;
    stock: number;
    estado: string;
    commerce: Commerce;
    productType: ProductTypes;
    deletedDate: Date;
    detalle: Detalle[];
}
