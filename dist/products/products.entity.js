"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Product = void 0;
const typeorm_1 = require("typeorm");
const commerce_entity_1 = require("../commerce/commerce.entity");
const product_category_entity_1 = require("../product-category/product-category.entity");
const detalle_entity_1 = require("../detalle/detalle.entity");
let Product = class Product {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Product.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Product.prototype, "name", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Product.prototype, "description", void 0);
__decorate([
    typeorm_1.Column({ type: "float8", nullable: true }),
    __metadata("design:type", Number)
], Product.prototype, "price", void 0);
__decorate([
    typeorm_1.Column("text", { array: true, default: () => 'array [] :: text []' }),
    __metadata("design:type", Array)
], Product.prototype, "pic", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], Product.prototype, "view", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], Product.prototype, "like", void 0);
__decorate([
    typeorm_1.Column({ type: "float8", nullable: true }),
    __metadata("design:type", Number)
], Product.prototype, "priceOffer", void 0);
__decorate([
    typeorm_1.Column({ type: "float8", nullable: true }),
    __metadata("design:type", Number)
], Product.prototype, "porcentajeDesc", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], Product.prototype, "stock", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], Product.prototype, "estado", void 0);
__decorate([
    typeorm_1.ManyToOne(() => commerce_entity_1.Commerce, (commerce) => commerce.products),
    typeorm_1.JoinColumn(),
    __metadata("design:type", commerce_entity_1.Commerce)
], Product.prototype, "commerce", void 0);
__decorate([
    typeorm_1.ManyToOne(() => product_category_entity_1.ProductTypes, (productType) => productType.products),
    __metadata("design:type", product_category_entity_1.ProductTypes)
], Product.prototype, "productType", void 0);
__decorate([
    typeorm_1.DeleteDateColumn(),
    __metadata("design:type", Date)
], Product.prototype, "deletedDate", void 0);
__decorate([
    typeorm_1.OneToMany(() => detalle_entity_1.Detalle, detalle => detalle.producto),
    __metadata("design:type", Array)
], Product.prototype, "detalle", void 0);
Product = __decorate([
    typeorm_1.Entity('PRODUCTS')
], Product);
exports.Product = Product;
//# sourceMappingURL=products.entity.js.map