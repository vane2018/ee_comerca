"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsService = void 0;
const common_1 = require("@nestjs/common");
const crud_typeorm_1 = require("@nestjsx/crud-typeorm");
const products_entity_1 = require("./products.entity");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const util_1 = require("util");
let ProductsService = class ProductsService extends crud_typeorm_1.TypeOrmCrudService {
    constructor(repo) {
        super(repo);
    }
    async saveProduct(productDto) {
        try {
            let producto = this.repo.create(productDto);
            return await this.repo.save(producto);
        }
        catch (error) {
            console.log('error: ', error);
        }
    }
    async updateProduct(productDto) {
        return await this.repo.save(productDto);
    }
    async getProducts() {
        try {
            return await this.repo.find({ relations: ['commerce', 'productType'] });
        }
        catch (error) {
            console.log('error: ', error);
        }
    }
    async getProductsOffer() {
        let productos = [];
        try {
            let prodOfer = await this.repo.find();
            for (let index = 0; index < prodOfer.length; index++) {
                if (!util_1.isNull(prodOfer[index].priceOffer)) {
                    if (prodOfer[index].priceOffer !== 0) {
                        productos.push(prodOfer[index]);
                    }
                }
            }
            return productos;
        }
        catch (error) {
            return error;
        }
    }
    async searchCommerceProducts(ids) {
        let products = await this.repo.findOne(ids, { relations: ['commerce'] });
        return products;
    }
    async deleteProduct(id) {
        await this.repo.softDelete(id);
    }
    async getProductsId(ids) {
        let product = await this.repo.findOne(ids, { relations: ['productType', 'commerce'] });
        return product;
    }
    async updateProductCont(productdto) {
        let producto = await this.repo.findOne({ where: { id: productdto.id }, relations: ['commerce', 'productType'] });
        producto.like = productdto.like;
        producto.view = productdto.view;
        let nuevo = await this.repo.save(producto);
        return nuevo;
    }
    async updateProductView(productdto) {
        let producto = await this.repo.findOne({ where: { id: productdto.id }, relations: ['commerce', 'productType'] });
        producto.view = productdto.view;
        let nuevo = await this.repo.save(producto);
        return nuevo;
    }
    async updateStock(id, cant) {
        try {
            let product = await this.repo.findOne(id);
            product.stock = product.stock - cant;
            let miProduct = await this.repo.save(product);
        }
        catch (error) {
            return error;
        }
    }
    async addOffert(idProduct, ofert) {
        let custom = await this.repo.findOne(idProduct, { relations: ['offer'] });
        await this.repo.save(custom);
    }
    async obtener(prod) {
        let productos = [];
        for (let index = 0; index < prod.length; index++) {
            productos.push(await typeorm_2.getRepository(products_entity_1.Product)
                .createQueryBuilder("user")
                .where("user.id= :id", { id: prod[index] })
                .getOne());
        }
        return productos;
    }
    async obtenerFavorito() {
        let productos = [];
        let r = await typeorm_2.getRepository(products_entity_1.Product)
            .createQueryBuilder("product")
            .where("product.like >= :precio", { precio: 4 })
            .getMany();
        return r;
    }
    async busquedaPrecio(prec) {
        let productos = [];
        let r = await typeorm_2.getRepository(products_entity_1.Product)
            .createQueryBuilder("product")
            .where("product.price <= :precio OR product.priceOffer <= :precio", { precio: prec })
            .getMany();
        for (let index = 0; index < r.length; index++) {
            if (r[index].priceOffer === 0) {
                if (r[index].price <= prec) {
                    productos.push(r[index]);
                }
            }
            else {
                if ((r[index].price <= prec) || (r[index].priceOffer <= prec)) {
                    productos.push(r[index]);
                }
            }
        }
        return productos;
    }
    async busquedaEstado(calidad) {
        let productos = [];
        let r = await typeorm_2.getRepository(products_entity_1.Product)
            .createQueryBuilder("product")
            .where("product.estado= :cal", { cal: calidad })
            .getMany();
        return r;
    }
};
ProductsService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(products_entity_1.Product)),
    __metadata("design:paramtypes", [Object])
], ProductsService);
exports.ProductsService = ProductsService;
//# sourceMappingURL=products.service.js.map