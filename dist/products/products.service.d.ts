import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Product } from './products.entity';
import { ProductDto } from './dto/products.dto';
import { PutProductDto } from './dto/putProduct.dto';
import { ContProductDto } from './dto/contProduct.dto';
import { viewDto } from './dto/viewUser.dto';
export declare class ProductsService extends TypeOrmCrudService<ProductDto> {
    constructor(repo: any);
    saveProduct(productDto: ProductDto): Promise<ProductDto>;
    updateProduct(productDto: PutProductDto): Promise<PutProductDto & ProductDto>;
    getProducts(): Promise<ProductDto[]>;
    getProductsOffer(): Promise<ProductDto[]>;
    searchCommerceProducts(ids: string): Promise<any>;
    deleteProduct(id: string): Promise<void>;
    getProductsId(ids: string): Promise<any>;
    updateProductCont(productdto: ContProductDto): Promise<Product>;
    updateProductView(productdto: viewDto): Promise<Product>;
    updateStock(id: string, cant: number): Promise<any>;
    addOffert(idProduct: string, ofert: any): Promise<any>;
    obtener(prod: any): Promise<Product[]>;
    obtenerFavorito(): Promise<Product[]>;
    busquedaPrecio(prec: number): Promise<Product[]>;
    busquedaEstado(calidad: string): Promise<Product[]>;
}
