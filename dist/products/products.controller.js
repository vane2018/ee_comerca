"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsController = void 0;
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const swagger_1 = require("@nestjs/swagger");
const products_dto_1 = require("./dto/products.dto");
const products_service_1 = require("./products.service");
const createProduct_dto_1 = require("./dto/createProduct.dto");
const commerce_service_1 = require("../commerce/commerce.service");
const product_category_service_1 = require("../product-category/product-category.service");
const putProduct_dto_1 = require("./dto/putProduct.dto");
const contProduct_dto_1 = require("./dto/contProduct.dto");
const vectProduct_dto_1 = require("./dto/vectProduct.dto");
const precio_dto_1 = require("./dto/precio.dto");
const state_dto_1 = require("./dto/state.dto");
let ProductsController = class ProductsController {
    constructor(service, serviceCommerce, serviceProductType) {
        this.service = service;
        this.serviceCommerce = serviceCommerce;
        this.serviceProductType = serviceProductType;
    }
    async saveProducts(res, createProductDto) {
        try {
            let commerceResp = await this.serviceCommerce.findOne(createProductDto.idCommerce);
            let typeProductResp = await this.serviceProductType.findOne(createProductDto.idProductType);
            if (typeProductResp && commerceResp) {
                let productSave = {
                    name: createProductDto.name,
                    description: createProductDto.description,
                    pic: createProductDto.pic,
                    price: this.reduccion(createProductDto.price),
                    productType: typeProductResp,
                    commerce: commerceResp,
                    view: 0,
                    like: 0,
                    priceOffer: this.reduccion(createProductDto.priceOffer),
                    porcentajeDesc: this.reduccion(createProductDto.porcentajeDesc),
                    stock: createProductDto.stock,
                    estado: createProductDto.estado
                };
                const product = await this.service.saveProduct(productSave);
                const commerce = await this.serviceCommerce.addProducts(createProductDto.idCommerce, product);
                return res.status(common_1.HttpStatus.OK).json(product);
            }
            else {
                return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'comercio o tipo de producto no encontrados' });
            }
        }
        catch (error) {
            console.log('error al crear producto: ', error);
            return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el producto', error });
        }
    }
    reduccion(valor) {
        let number = valor;
        let letra = number.toString().split('');
        let vect = [];
        let vect1 = [];
        vect = letra;
        let j = 0;
        for (let index = 0; index < vect.length; index++) {
            if (vect[index] !== '.') {
                vect1[j] = vect[index];
                j = j + 1;
            }
            else {
                if (vect[index] === '.') {
                    vect1[j] = vect[index];
                    vect1[j + 1] = vect[index + 1];
                    vect1[j + 2] = vect[index + 2];
                    index = vect.length + 1;
                }
            }
        }
        let precio = vect1.join('');
        let abs = parseFloat(precio);
        return abs;
    }
    async getProducts(res) {
        const products = await this.service.getProducts();
        return res.status(common_1.HttpStatus.OK).json(products);
    }
    async getProductsOffert(res) {
        const products = await this.service.getProductsOffer();
        return res.status(common_1.HttpStatus.OK).json(products);
    }
    async obtenerProductos(res, vect) {
        try {
            let product = await this.service.obtener(vect.vector);
            return res.status(common_1.HttpStatus.OK).json(product);
        }
        catch (error) {
            console.log('error al crear producto: ', error);
            return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el producto', error });
        }
    }
    async productsPrice(res, price) {
        try {
            let product = await this.service.busquedaPrecio(price.price);
            return res.status(common_1.HttpStatus.OK).json(product);
        }
        catch (error) {
            console.log('error al buscar producto: ', error);
            return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el producto', error });
        }
    }
    async getPopular(res, param) {
        try {
            const products = await this.service.obtenerFavorito();
            return res.status(common_1.HttpStatus.OK).json(products);
        }
        catch (error) {
            console.log("error", error);
        }
    }
    async getProductsId(res, param) {
        const products = await this.service.getProductsId(param.id);
        return res.status(common_1.HttpStatus.OK).json(products);
    }
    async deleteProducts(res, param) {
        await this.service.deleteProduct(param.id);
        return res.status(common_1.HttpStatus.OK).json({ message: 'product deleted' });
    }
    async putProducts(res, param, createProductDto) {
        try {
            let product = await this.service.findOne(param.id);
            if (product) {
                let productMod = {
                    id: param.id,
                    name: createProductDto.name,
                    description: createProductDto.description,
                    pic: createProductDto.pic,
                    price: this.reduccion(createProductDto.price),
                    idProductType: createProductDto.idProductType,
                    idCommerce: createProductDto.idCommerce,
                    priceOffer: this.reduccion(createProductDto.priceOffer),
                    porcentajeDesc: this.reduccion(createProductDto.porcentajeDesc),
                    stock: createProductDto.stock,
                    estado: createProductDto.estado
                };
                const producto = await this.service.updateProduct(productMod);
                return res.status(common_1.HttpStatus.OK).json(producto);
            }
            else {
                return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: ' producto no encontrados' });
            }
        }
        catch (error) {
            console.log('error al modifcar producto: ', error);
            return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el producto', error });
        }
    }
    async cont(res, contProductDto, param) {
        try {
            let product = await this.service.findOne(param.id);
            if (product) {
                let productnuevo = {
                    id: param.id,
                    view: product.view + contProductDto.view,
                    like: product.like + contProductDto.like
                };
                const producto = await this.service.updateProductCont(productnuevo);
                return res.status(common_1.HttpStatus.OK).json(producto);
            }
        }
        catch (error) {
            console.log('error al crear producto: ', error);
            return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el producto', error });
        }
    }
    async getProductEstado(res, param, estadoDto) {
        const products = await this.service.busquedaEstado(estadoDto.statte);
        return res.status(common_1.HttpStatus.OK).json(products);
    }
};
__decorate([
    common_1.Post('register'),
    swagger_1.ApiOperation({ summary: 'Registra un producto en un commercio' }),
    swagger_1.ApiCreatedResponse({ description: 'The product has been successfully created.', type: products_dto_1.ProductDto }),
    swagger_1.ApiBody({ type: createProduct_dto_1.CreateProductDto }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiResponse({ status: 404, description: '\'The resource was not found\'' }),
    swagger_1.ApiResponse({ status: 409, description: 'Conflict' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, createProduct_dto_1.CreateProductDto]),
    __metadata("design:returntype", Promise)
], ProductsController.prototype, "saveProducts", null);
__decorate([
    common_1.Get(),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiResponse({ status: 404, description: '\'The resource was not found\'' }),
    swagger_1.ApiResponse({ status: 409, description: 'Conflict' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ProductsController.prototype, "getProducts", null);
__decorate([
    common_1.Get('/product/ofertas'),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiResponse({ status: 404, description: '\'The resource was not found\'' }),
    swagger_1.ApiResponse({ status: 409, description: 'Conflict' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ProductsController.prototype, "getProductsOffert", null);
__decorate([
    common_1.Post('/array'),
    swagger_1.ApiOperation({ summary: 'obtiene Array de productos, cuando le pasan el id' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiResponse({ status: 404, description: '\'The resource was not found\'' }),
    swagger_1.ApiResponse({ status: 409, description: 'Conflict' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, vectProduct_dto_1.VectProductDto]),
    __metadata("design:returntype", Promise)
], ProductsController.prototype, "obtenerProductos", null);
__decorate([
    common_1.Post('/search/price'),
    swagger_1.ApiOperation({ summary: 'obtiene Array de productos, cuando le pasan precio' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiResponse({ status: 404, description: '\'The resource was not found\'' }),
    swagger_1.ApiResponse({ status: 409, description: 'Conflict' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, precio_dto_1.PriceDto]),
    __metadata("design:returntype", Promise)
], ProductsController.prototype, "productsPrice", null);
__decorate([
    common_1.Get('/popular'),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiResponse({ status: 404, description: '\'The resource was not found\'' }),
    swagger_1.ApiResponse({ status: 409, description: 'Conflict' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ProductsController.prototype, "getPopular", null);
__decorate([
    common_1.Get('/:id'),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiResponse({ status: 404, description: '\'The resource was not found\'' }),
    swagger_1.ApiResponse({ status: 409, description: 'Conflict' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ProductsController.prototype, "getProductsId", null);
__decorate([
    common_1.Delete('/:id'),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiResponse({ status: 404, description: '\'The resource was not found\'' }),
    swagger_1.ApiResponse({ status: 409, description: 'Conflict' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ProductsController.prototype, "deleteProducts", null);
__decorate([
    common_1.Put('/edit/:id'),
    __param(0, common_1.Response()), __param(1, common_1.Param()), __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, putProduct_dto_1.PutProductDto]),
    __metadata("design:returntype", Promise)
], ProductsController.prototype, "putProducts", null);
__decorate([
    common_1.Post('/cont/:id'),
    __param(0, common_1.Response()), __param(1, common_1.Body()), __param(2, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, contProduct_dto_1.ContProductDto, Object]),
    __metadata("design:returntype", Promise)
], ProductsController.prototype, "cont", null);
__decorate([
    common_1.Post('search/estado'),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiResponse({ status: 404, description: '\'The resource was not found\'' }),
    swagger_1.ApiResponse({ status: 409, description: 'Conflict' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Param()), __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, state_dto_1.EstadoDto]),
    __metadata("design:returntype", Promise)
], ProductsController.prototype, "getProductEstado", null);
ProductsController = __decorate([
    crud_1.Crud({
        model: {
            type: products_dto_1.ProductDto,
        },
        params: {
            id: {
                field: 'id',
                type: 'uuid',
                primary: true,
            },
        },
        query: {
            join: {
                'purchase': {
                    allow: []
                },
                commerce: {}
            }
        }
    }),
    swagger_1.ApiTags('Products'),
    common_1.Controller('products'),
    __metadata("design:paramtypes", [products_service_1.ProductsService,
        commerce_service_1.CommerceService,
        product_category_service_1.ProductTypesService])
], ProductsController);
exports.ProductsController = ProductsController;
//# sourceMappingURL=products.controller.js.map