import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/createProduct.dto';
import { CommerceService } from '../commerce/commerce.service';
import { ProductTypesService } from '../product-category/product-category.service';
import { PutProductDto } from './dto/putProduct.dto';
import { ContProductDto } from './dto/contProduct.dto';
import { VectProductDto } from './dto/vectProduct.dto';
import { PriceDto } from './dto/precio.dto';
import { EstadoDto } from './dto/state.dto';
export declare class ProductsController {
    service: ProductsService;
    serviceCommerce: CommerceService;
    serviceProductType: ProductTypesService;
    constructor(service: ProductsService, serviceCommerce: CommerceService, serviceProductType: ProductTypesService);
    saveProducts(res: any, createProductDto: CreateProductDto): Promise<any>;
    reduccion(valor: number): number;
    getProducts(res: any): Promise<any>;
    getProductsOffert(res: any): Promise<any>;
    obtenerProductos(res: any, vect: VectProductDto): Promise<any>;
    productsPrice(res: any, price: PriceDto): Promise<any>;
    getPopular(res: any, param: any): Promise<any>;
    getProductsId(res: any, param: any): Promise<any>;
    deleteProducts(res: any, param: any): Promise<any>;
    putProducts(res: any, param: any, createProductDto: PutProductDto): Promise<any>;
    cont(res: any, contProductDto: ContProductDto, param: any): Promise<any>;
    getProductEstado(res: any, param: any, estadoDto: EstadoDto): Promise<any>;
}
