export declare class ProductDto {
    readonly name: string;
    readonly description: string;
    readonly price: number;
    pic: string[];
    view: number;
    like: number;
    priceOffer: number;
    porcentajeDesc: number;
    stock: number;
    estado: string;
}
