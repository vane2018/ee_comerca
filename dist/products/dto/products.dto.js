"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class ProductDto {
}
__decorate([
    swagger_1.ApiProperty({ description: 'Nombre del Producto' }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], ProductDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Descripcion del prodcuto' }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], ProductDto.prototype, "description", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Precio del producto' }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], ProductDto.prototype, "price", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Foto del producto' }),
    __metadata("design:type", Array)
], ProductDto.prototype, "pic", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], ProductDto.prototype, "view", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], ProductDto.prototype, "like", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], ProductDto.prototype, "priceOffer", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], ProductDto.prototype, "porcentajeDesc", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], ProductDto.prototype, "stock", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], ProductDto.prototype, "estado", void 0);
exports.ProductDto = ProductDto;
//# sourceMappingURL=products.dto.js.map