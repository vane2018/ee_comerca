export declare class PutProductDto {
    id: string;
    readonly name: string;
    readonly description: string;
    readonly price: number;
    pic: string[];
    readonly idProductType: string;
    readonly idCommerce: string;
    readonly priceOffer: number;
    readonly porcentajeDesc: number;
    readonly stock: number;
    readonly estado: string;
}
