"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PutProductDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class PutProductDto {
}
__decorate([
    swagger_1.ApiProperty({ description: 'Product name' }),
    __metadata("design:type", String)
], PutProductDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Description product' }),
    __metadata("design:type", String)
], PutProductDto.prototype, "description", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Price  product' }),
    __metadata("design:type", Number)
], PutProductDto.prototype, "price", void 0);
__decorate([
    swagger_1.ApiProperty({ description: ' product photo' }),
    __metadata("design:type", Array)
], PutProductDto.prototype, "pic", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'product type id' }),
    __metadata("design:type", String)
], PutProductDto.prototype, "idProductType", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'commerce id' }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], PutProductDto.prototype, "idCommerce", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'precio de oferta' }),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], PutProductDto.prototype, "priceOffer", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'porcentaje de descuento' }),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], PutProductDto.prototype, "porcentajeDesc", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'stock de product' }),
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], PutProductDto.prototype, "stock", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'estado del product' }),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], PutProductDto.prototype, "estado", void 0);
exports.PutProductDto = PutProductDto;
//# sourceMappingURL=putProduct.dto.js.map