"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const commerce_module_1 = require("./commerce/commerce.module");
const user_module_1 = require("./users/user.module");
const auth_module_1 = require("./auth/auth.module");
const products_module_1 = require("./products/products.module");
const product_category_module_1 = require("./product-category/product-category.module");
const rols_module_1 = require("./rols/rols.module");
const permissions_module_1 = require("./permissions/permissions.module");
const mailer_module_1 = require("./mailer/mailer.module");
const code_module_1 = require("./code/code.module");
const mailer_1 = require("@nest-modules/mailer");
const commissions_module_1 = require("./commissions/commissions.module");
const purchases_module_1 = require("./purchases/purchases.module");
const payments_module_1 = require("./payments/payments.module");
const payment_types_module_1 = require("./payment-types/payment-types.module");
const database_module_1 = require("./database/database.module");
const category_product_general_module_1 = require("./category-product-general/category-product-general.module");
const subcategory_products_module_1 = require("./subcategory-products/subcategory-products.module");
const productxuser_module_1 = require("./productxuser/productxuser.module");
const chat_module_1 = require("./chat/chat.module");
const mensajes_module_1 = require("./mensajes/mensajes.module");
const notificaciones_module_1 = require("./notificaciones/notificaciones.module");
const shipping_type_module_1 = require("./shipping-type/shipping-type.module");
const shipping_module_1 = require("./shipping/shipping.module");
const integracion_module_1 = require("./integracion/integracion.module");
const mercado_pago_module_1 = require("./mercado-pago/mercado-pago.module");
const mercado_module_1 = require("./mercado/mercado.module");
const cobrarcomision_module_1 = require("./cobrarcomision/cobrarcomision.module");
const file_module_1 = require("./file/file.module");
let AppModule = class AppModule {
    constructor(connection) {
        this.connection = connection;
    }
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            database_module_1.DatabaseModule,
            commerce_module_1.CommerceModule,
            user_module_1.UsersModule,
            auth_module_1.AuthModule,
            products_module_1.ProductsModule,
            product_category_module_1.ProductTypesModule,
            rols_module_1.RolsModule,
            permissions_module_1.PermissionsModule,
            mailer_module_1.MailerNestModule,
            mailer_1.MailerModule,
            code_module_1.CodeModule,
            commissions_module_1.CommissionsModule,
            purchases_module_1.PurchasesModule,
            payments_module_1.PymentsModule,
            payment_types_module_1.PymentTypesModule,
            category_product_general_module_1.CategoryGeneralModule,
            subcategory_products_module_1.SubcategoryProductsModule,
            productxuser_module_1.ProductxuserModule,
            chat_module_1.ChatModule,
            mensajes_module_1.MensajesModule,
            notificaciones_module_1.NotificacionesModule,
            shipping_type_module_1.ShippingTypeModule,
            shipping_module_1.ShippingModule,
            integracion_module_1.IntegracionModule,
            mercado_pago_module_1.MercadoPagoModule,
            mercado_module_1.MercadoModule,
            cobrarcomision_module_1.CobrarcomisionModule,
            file_module_1.FileModule
        ],
        controllers: [],
        providers: [],
        exports: []
    }),
    __metadata("design:paramtypes", [typeorm_1.Connection])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map