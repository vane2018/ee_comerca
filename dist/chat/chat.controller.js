"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const crud_1 = require("@nestjsx/crud");
const user_service_1 = require("../users/user.service");
const chat_entity_1 = require("./chat.entity");
const chat_service_1 = require("./chat.service");
const chat_dto_1 = require("./dto/chat.dto");
const chatConsulta_dto_1 = require("./dto/chatConsulta.dto");
const consulta_dto_1 = require("./dto/consulta.dto");
let admin = require("firebase-admin");
let ChatController = class ChatController {
    constructor(service, userService) {
        this.service = service;
        this.userService = userService;
    }
    async obtenerChat(res, param, chats) {
        const chatss = await this.service.getchats(chats.remitenteId);
        return res.status(common_1.HttpStatus.OK).json(chatss);
    }
    async losChat(res, param, chats) {
        const chatss = await this.service.getChat(chats.remitenteId, chats.destinatarioId);
        return res.status(common_1.HttpStatus.OK).json(chatss);
    }
    async postChat(res, chats) {
        let rem = await this.userService.findOne(chats.remitenteId);
        let des = await this.userService.findOne(chats.destinatarioId);
        chats.createAt = new Date();
        let item = await this.service.saveChat(chats);
        item.destinatario = des;
        item.remitente = rem;
        let loschat = await this.service.updateChat(item, rem, des);
        res.json({ status: 'ok', loschat });
    }
    async getGeneral(res) {
        let u = await this.service.obtenerChat();
        res.json({ status: 'ok', u });
    }
    async getChatId(res, param) {
        const products = await this.service.getChatId(param.id);
        return res.status(common_1.HttpStatus.OK).json(products);
    }
};
__decorate([
    common_1.Post('/chatRemitente'),
    swagger_1.ApiOperation({ summary: 'Registra un commercio' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    swagger_1.ApiBody({ type: consulta_dto_1.Consulta }),
    __param(0, common_1.Response()), __param(1, common_1.Param()), __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, consulta_dto_1.Consulta]),
    __metadata("design:returntype", Promise)
], ChatController.prototype, "obtenerChat", null);
__decorate([
    common_1.Post('/chatDestandRemittente'),
    swagger_1.ApiOperation({ summary: 'Registra un commercio' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    swagger_1.ApiBody({ type: chatConsulta_dto_1.chatConsulta }),
    __param(0, common_1.Response()), __param(1, common_1.Param()), __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, chatConsulta_dto_1.chatConsulta]),
    __metadata("design:returntype", Promise)
], ChatController.prototype, "losChat", null);
__decorate([
    common_1.Post('register/mensajeChat'),
    swagger_1.ApiOperation({ summary: 'Registra un commercio' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    swagger_1.ApiCreatedResponse({ description: 'The Commerce has been successfully created.', type: chat_dto_1.chatDto, status: 200 }),
    swagger_1.ApiBody({ type: chat_dto_1.chatDto }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, chat_dto_1.chatDto]),
    __metadata("design:returntype", Promise)
], ChatController.prototype, "postChat", null);
__decorate([
    common_1.Get(),
    swagger_1.ApiOperation({ summary: 'Registra un commercio' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ChatController.prototype, "getGeneral", null);
__decorate([
    common_1.Get('/:id'),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiResponse({ status: 404, description: '\'The resource was not found\'' }),
    swagger_1.ApiResponse({ status: 409, description: 'Conflict' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ChatController.prototype, "getChatId", null);
ChatController = __decorate([
    crud_1.Crud({
        model: {
            type: chat_entity_1.Chat,
        },
        params: {
            id: {
                field: 'id',
                type: 'uuid',
                primary: true,
            },
        },
        query: {},
    }),
    swagger_1.ApiTags('Chats'),
    common_1.Controller('chat'),
    __metadata("design:paramtypes", [chat_service_1.ChatService,
        user_service_1.UsersService])
], ChatController);
exports.ChatController = ChatController;
//# sourceMappingURL=chat.controller.js.map