import { UsersService } from '../users/user.service';
import { ChatService } from './chat.service';
import { chatDto } from './dto/chat.dto';
import { chatConsulta } from './dto/chatConsulta.dto';
import { Consulta } from './dto/consulta.dto';
export declare class ChatController {
    service: ChatService;
    userService: UsersService;
    constructor(service: ChatService, userService: UsersService);
    obtenerChat(res: any, param: any, chats: Consulta): Promise<any>;
    losChat(res: any, param: any, chats: chatConsulta): Promise<any>;
    postChat(res: any, chats: chatDto): Promise<void>;
    getGeneral(res: any): Promise<void>;
    getChatId(res: any, param: any): Promise<any>;
}
