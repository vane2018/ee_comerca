import { User } from "../users/user.entity";
export declare enum TipoNotificacion {
    Venta = "purchases",
    Chat = "chat",
    Comision = "commision"
}
export declare class Chat {
    id: string;
    texto: string;
    createAt: Date;
    remitente: User;
    destinatario: User;
}
