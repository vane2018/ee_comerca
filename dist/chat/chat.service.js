"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const crud_typeorm_1 = require("@nestjsx/crud-typeorm");
const moment = require("moment");
const user_service_1 = require("../users/user.service");
const chat_entity_1 = require("./chat.entity");
const firebase = require("firebase-admin");
let ChatService = class ChatService extends crud_typeorm_1.TypeOrmCrudService {
    constructor(repo, usersService) {
        super(repo);
        this.usersService = usersService;
    }
    async saveChat(chats) {
        let miChat = this.repo.create(chats);
        let r = await this.repo.save(miChat);
        this.envianotificacionVendedor2(chats.destinatarioId);
        return r;
    }
    async envianotificacionVendedor2(idVendedor) {
        var usuario = this.usersService.findOne(idVendedor);
        try {
            const notificacionId = `nuevo--mensaje`;
            if ((await usuario).firebaseRegistrationToken) {
                firebase.messaging().sendToDevice((await usuario).firebaseRegistrationToken, {
                    data: {
                        notificacionId: notificacionId,
                        tipo: chat_entity_1.TipoNotificacion.Chat
                    },
                    notification: {
                        tag: notificacionId,
                        title: `Nuevo mensaje `,
                        body: `tiene un nuevo mensaje`,
                    }
                });
            }
        }
        catch (error) {
            console.log("se detecto un error", error);
        }
    }
    async obtenerChat() {
        try {
            var fechaInicio = moment().format('YYYY-MM-DDTHH:mm');
            let general = await this.repo.find({ relations: ['remitente', 'destinatario'] });
            return general;
        }
        catch (error) {
            return error;
        }
    }
    async updateChat(chatDto, rem, des) {
        const user = await this.repo.findOne(chatDto.id);
        user.remitente = rem;
        user.destinatario = des;
        if (user) {
            let f = await this.repo.save(user);
            return f;
        }
        else {
            console.log("error");
        }
    }
    async getChat(ids, id2) {
        let loschats = [];
        let q = await this.repo.find({
            relations: ['remitente', 'destinatario'], order: {
                createAt: "ASC"
            }
        });
        for (let i = 0; i < q.length; i++) {
            if (((q[i].remitente.id == ids) && (q[i].destinatario.id == id2)) || ((q[i].remitente.id == id2) && (q[i].destinatario.id == ids))) {
                loschats.push(q[i]);
            }
        }
        return loschats;
    }
    async getchats(id) {
        let prod = [];
        let r = await this.repo.find({ relations: ['remitente', 'destinatario'] });
        for (let index = 0; index < r.length; index++) {
            if (r[index].remitente.id == id || r[index].destinatario.id == id) {
                prod.push(r[index]);
            }
        }
        return prod;
    }
    async getChatId(ids) {
        let chats = await this.repo.findOne(ids, { relations: ['remitente', 'destinatario'] });
        return chats;
    }
    async getPreviewTexto(texto) {
        if (!texto) {
            return '';
        }
        const maxlength = 200;
        return texto.length > maxlength ? `${texto.slice(0, maxlength - 3)}...` : texto;
    }
};
ChatService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(chat_entity_1.Chat)),
    __metadata("design:paramtypes", [Object, user_service_1.UsersService])
], ChatService);
exports.ChatService = ChatService;
//# sourceMappingURL=chat.service.js.map