"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.chatDto = void 0;
const swagger_1 = require("@nestjs/swagger");
class chatDto {
}
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'cuerpo del mensaje' }),
    __metadata("design:type", String)
], chatDto.prototype, "texto", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'fecha de la compra de la compra, por defecto fecha actual' }),
    __metadata("design:type", Date)
], chatDto.prototype, "createAt", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'purchase date', type: 'string', format: 'date-time' }),
    __metadata("design:type", Date)
], chatDto.prototype, "date", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'id del remitente' }),
    __metadata("design:type", String)
], chatDto.prototype, "remitenteId", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'id del destinatario' }),
    __metadata("design:type", String)
], chatDto.prototype, "destinatarioId", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'id del destinatario' }),
    __metadata("design:type", String)
], chatDto.prototype, "token", void 0);
exports.chatDto = chatDto;
//# sourceMappingURL=chat.dto.js.map