export declare class chatDto {
    readonly texto: string;
    createdAt: Date;
    readonly remitenteId: string;
    readonly destinatarioId: string;
}
