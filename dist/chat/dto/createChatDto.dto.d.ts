export declare class createChatDto implements Readonly<createChatDto> {
    readonly texto: string;
    createdAt: Date;
    readonly destinatarioId: string;
}
