export declare class chatDto implements Readonly<chatDto> {
    readonly texto: string;
    createAt: Date;
    readonly date: Date;
    remitenteId: string;
    destinatarioId: string;
    readonly token: string;
}
