import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { UsersService } from '../users/user.service';
import { User } from '../users/user.entity';
import { Chat } from './chat.entity';
import { chatDto } from './dto/chat.dto';
export declare class ChatService extends TypeOrmCrudService<Chat> {
    usersService: UsersService;
    constructor(repo: any, usersService: UsersService);
    saveChat(chats: chatDto): Promise<Chat>;
    envianotificacionVendedor2(idVendedor: string): Promise<void>;
    obtenerChat(): Promise<any>;
    updateChat(chatDto: Chat, rem: User, des: User): Promise<any>;
    getChat(ids: string, id2: string): Promise<any>;
    getchats(id: string): Promise<any>;
    getChatId(ids: string): Promise<any>;
    getPreviewTexto(texto: string): Promise<string>;
}
