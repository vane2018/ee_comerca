"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Chat = exports.TipoNotificacion = void 0;
const typeorm_1 = require("typeorm");
const user_entity_1 = require("../users/user.entity");
var TipoNotificacion;
(function (TipoNotificacion) {
    TipoNotificacion["Venta"] = "purchases";
    TipoNotificacion["Chat"] = "chat";
    TipoNotificacion["Comision"] = "commision";
})(TipoNotificacion = exports.TipoNotificacion || (exports.TipoNotificacion = {}));
let Chat = class Chat {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Chat.prototype, "id", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 10000, nullable: true }),
    __metadata("design:type", String)
], Chat.prototype, "texto", void 0);
__decorate([
    typeorm_1.Column({ type: Date, default: new Date, nullable: true }),
    __metadata("design:type", Date)
], Chat.prototype, "createAt", void 0);
__decorate([
    typeorm_1.ManyToOne(() => user_entity_1.User, { onUpdate: 'CASCADE', onDelete: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: 'remitenteId' }),
    __metadata("design:type", user_entity_1.User)
], Chat.prototype, "remitente", void 0);
__decorate([
    typeorm_1.ManyToOne(() => user_entity_1.User, { onUpdate: 'CASCADE', onDelete: 'CASCADE' }),
    typeorm_1.JoinColumn({ name: 'destinatarioId' }),
    __metadata("design:type", user_entity_1.User)
], Chat.prototype, "destinatario", void 0);
Chat = __decorate([
    typeorm_1.Entity('CHAT')
], Chat);
exports.Chat = Chat;
//# sourceMappingURL=chat.entity.js.map