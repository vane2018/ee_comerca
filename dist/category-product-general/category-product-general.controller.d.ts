import { CategoryGeneralService } from './category-product-general.service';
export declare class CategoryGeneralController {
    service: CategoryGeneralService;
    constructor(service: CategoryGeneralService);
    getCommerce(res: any, param: any): Promise<any>;
}
