"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoryGeneralModule = void 0;
const common_1 = require("@nestjs/common");
const category_product_general_entity_1 = require("./category-product-general.entity");
const typeorm_1 = require("@nestjs/typeorm");
const category_product_general_controller_1 = require("./category-product-general.controller");
const category_product_general_service_1 = require("./category-product-general.service");
let CategoryGeneralModule = class CategoryGeneralModule {
};
CategoryGeneralModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([category_product_general_entity_1.ProductGeneral])],
        controllers: [category_product_general_controller_1.CategoryGeneralController],
        providers: [category_product_general_service_1.CategoryGeneralService],
        exports: [category_product_general_service_1.CategoryGeneralService],
    })
], CategoryGeneralModule);
exports.CategoryGeneralModule = CategoryGeneralModule;
//# sourceMappingURL=category-product-general.module.js.map