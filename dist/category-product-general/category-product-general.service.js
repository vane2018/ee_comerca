"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoryGeneralService = void 0;
const common_1 = require("@nestjs/common");
const crud_typeorm_1 = require("@nestjsx/crud-typeorm");
const typeorm_1 = require("@nestjs/typeorm");
const category_product_general_entity_1 = require("./category-product-general.entity");
let CategoryGeneralService = class CategoryGeneralService extends crud_typeorm_1.TypeOrmCrudService {
    constructor(repo) {
        super(repo);
    }
    async getCategoryGeneral() {
        try {
            return await this.repo.find({ relations: ['productsSubcategory'] });
        }
        catch (error) {
            console.log('error: ', error);
        }
    }
    async getCategoryById(id) {
        return await this.repo.findOne(id, { relations: ['productsSubcategory'] });
    }
};
CategoryGeneralService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(category_product_general_entity_1.ProductGeneral)),
    __metadata("design:paramtypes", [Object])
], CategoryGeneralService);
exports.CategoryGeneralService = CategoryGeneralService;
//# sourceMappingURL=category-product-general.service.js.map