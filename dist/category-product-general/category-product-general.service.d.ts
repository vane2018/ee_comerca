import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { CategoryProductGeneralDto } from './dto/categoryProductGeneral.dto';
export declare class CategoryGeneralService extends TypeOrmCrudService<CategoryProductGeneralDto> {
    constructor(repo: any);
    getCategoryGeneral(): Promise<CategoryProductGeneralDto[]>;
    getCategoryById(id: string): Promise<any>;
}
