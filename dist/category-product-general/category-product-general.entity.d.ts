import { ProductSubcategory } from "../subcategory-products/subcategory-products.entity";
export declare class ProductGeneral {
    id: string;
    name: string;
    productsSubcategory: ProductSubcategory[];
}
