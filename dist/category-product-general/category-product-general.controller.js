"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoryGeneralController = void 0;
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const categoryProductGeneral_dto_1 = require("./dto/categoryProductGeneral.dto");
const swagger_1 = require("@nestjs/swagger");
const category_product_general_service_1 = require("./category-product-general.service");
let CategoryGeneralController = class CategoryGeneralController {
    constructor(service) {
        this.service = service;
    }
    async getCommerce(res, param) {
        const categoryGeneral = await this.service.getCategoryById(param.id);
        return res.status(common_1.HttpStatus.OK).json(categoryGeneral);
    }
};
__decorate([
    common_1.Get('/:id'),
    swagger_1.ApiOperation({ summary: 'Obtiene una categoria General por Id, con su [] de subcategoria' }),
    swagger_1.ApiResponse({ status: 200, description: 'Los datos han sido devueltos correctamente.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CategoryGeneralController.prototype, "getCommerce", null);
CategoryGeneralController = __decorate([
    crud_1.Crud({
        model: {
            type: categoryProductGeneral_dto_1.CategoryProductGeneralDto,
        },
        params: {
            id: {
                field: 'id',
                type: 'uuid',
                primary: true,
            },
        },
        query: {
            join: {
                productsSubcategory: {
                    allow: []
                }
            },
        }
    }),
    swagger_1.ApiTags('Product-Category-General'),
    common_1.Controller('category-general'),
    __metadata("design:paramtypes", [category_product_general_service_1.CategoryGeneralService])
], CategoryGeneralController);
exports.CategoryGeneralController = CategoryGeneralController;
//# sourceMappingURL=category-product-general.controller.js.map