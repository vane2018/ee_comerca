"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommissionsController = void 0;
const common_1 = require("@nestjs/common");
const commissions_service_1 = require("./commissions.service");
const swagger_1 = require("@nestjs/swagger");
const commmissions_dto_1 = require("./dto/commmissions.dto");
const createCommission_dto_1 = require("./dto/createCommission.dto");
const crud_1 = require("@nestjsx/crud");
const user_service_1 = require("../users/user.service");
const descontar_comision_dto_1 = require("./dto/descontar-comision.dto");
let CommissionsController = class CommissionsController {
    constructor(service, serviceUser) {
        this.service = service;
        this.serviceUser = serviceUser;
    }
    async saveCommissions(res, createCommissionDto) {
        try {
            let customerResp = await this.serviceUser.findOne(createCommissionDto.idUser);
            if (customerResp) {
                let commissionSave = {
                    commission: createCommissionDto.commission,
                    description: createCommissionDto.description,
                    user: customerResp,
                    createAt: createCommissionDto.createAt,
                    typeReferido: createCommissionDto.typeReferido
                };
                const comision = await this.service.saveCommission(commissionSave);
                return res.status(common_1.HttpStatus.OK).json(comision);
            }
            else {
                return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'customer no encontrados' });
            }
        }
        catch (error) {
            console.log('error al crear producto: ', error);
            return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar comision', error });
        }
    }
    async getProducts(res) {
        const commissions = await this.service.getCommission();
        return res.status(common_1.HttpStatus.OK).json(commissions);
    }
    async deleteCommisions(res, param) {
        await this.service.deleteCommission(param.id);
        return res.status(common_1.HttpStatus.OK).json({ message: 'commision deleted' });
    }
    async getByUser(res, param) {
        const commissions = await this.service.getCommissionByUserId(param.idUser);
        return res.status(common_1.HttpStatus.OK).json(commissions);
    }
    async descontarComision(res, datos) {
        try {
            const usuario = await this.service.cobrarCbu(datos.monto, datos.cbu);
            return res.status(common_1.HttpStatus.OK).json(usuario);
        }
        catch (_a) {
            console.log("error");
        }
    }
};
__decorate([
    common_1.Post('register'),
    swagger_1.ApiOperation({ summary: 'Registra una comision' }),
    swagger_1.ApiCreatedResponse({ description: 'The product has been successfully created.', type: commmissions_dto_1.CommissionDto }),
    swagger_1.ApiBody({ type: createCommission_dto_1.CreateCommissionDto }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, createCommission_dto_1.CreateCommissionDto]),
    __metadata("design:returntype", Promise)
], CommissionsController.prototype, "saveCommissions", null);
__decorate([
    common_1.Get(),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CommissionsController.prototype, "getProducts", null);
__decorate([
    common_1.Delete('/:id'),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CommissionsController.prototype, "deleteCommisions", null);
__decorate([
    common_1.Get(':idUser'),
    swagger_1.ApiParam({ type: 'string', name: 'idUser' }),
    __param(0, common_1.Response()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CommissionsController.prototype, "getByUser", null);
__decorate([
    common_1.Post('descontar-comision'),
    swagger_1.ApiOperation({ summary: 'password change' }),
    swagger_1.ApiResponse({ status: 200, description: 'was changed password' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 403, description: "Forbidden" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 500, description: "internal server error" }),
    swagger_1.ApiBody({ type: descontar_comision_dto_1.comisionDescontar }),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, descontar_comision_dto_1.comisionDescontar]),
    __metadata("design:returntype", Promise)
], CommissionsController.prototype, "descontarComision", null);
CommissionsController = __decorate([
    crud_1.Crud({
        model: {
            type: commmissions_dto_1.CommissionDto,
        },
        params: {
            id: {
                field: 'id',
                type: 'uuid',
                primary: true,
            },
        },
        query: {
            join: {
                user: {}
            },
            sort: [{
                    field: 'createAt',
                    order: 'DESC'
                }]
        }
    }),
    swagger_1.ApiTags('Commissions'),
    common_1.Controller('commission'),
    __metadata("design:paramtypes", [commissions_service_1.CommissionsService,
        user_service_1.UsersService])
], CommissionsController);
exports.CommissionsController = CommissionsController;
//# sourceMappingURL=commissions.controller.js.map