import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { CommissionDto } from './dto/commmissions.dto';
import { UsersService } from '../users/user.service';
export declare class CommissionsService extends TypeOrmCrudService<CommissionDto> {
    serviceUser: UsersService;
    constructor(repo: any, serviceUser: UsersService);
    saveCommission(commisionDto: CommissionDto): Promise<CommissionDto>;
    notificarComercioSobreNuevoVenta(purchass: CommissionDto): Promise<void>;
    cobrarCbu(monto: number, cbu: string): Promise<import("../users/user.entity").User>;
    getCommission(): Promise<CommissionDto[]>;
    getCommissionByIds(ids: string[]): Promise<any>;
    getCommissionByUserId(userId: string): Promise<any>;
    deleteCommission(id: string): Promise<void>;
}
