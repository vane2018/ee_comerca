import { CommissionsService } from './commissions.service';
import { CreateCommissionDto } from './dto/createCommission.dto';
import { UsersService } from '../users/user.service';
import { comisionDescontar } from './dto/descontar-comision.dto';
export declare class CommissionsController {
    service: CommissionsService;
    serviceUser: UsersService;
    constructor(service: CommissionsService, serviceUser: UsersService);
    saveCommissions(res: any, createCommissionDto: CreateCommissionDto): Promise<any>;
    getProducts(res: any): Promise<any>;
    deleteCommisions(res: any, param: any): Promise<any>;
    getByUser(res: any, param: any): Promise<any>;
    descontarComision(res: any, datos: comisionDescontar): Promise<any>;
}
