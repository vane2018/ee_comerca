export declare class CreateCommissionDto {
    readonly commission: number;
    readonly description: string[];
    readonly idUser: string;
    readonly createAt: Date;
    readonly typeReferido: string;
}
