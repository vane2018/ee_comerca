"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateCommissionDto = void 0;
const swagger_1 = require("@nestjs/swagger");
class CreateCommissionDto {
}
__decorate([
    swagger_1.ApiProperty({ description: 'Commission name' }),
    __metadata("design:type", Number)
], CreateCommissionDto.prototype, "commission", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Description commission' }),
    __metadata("design:type", Array)
], CreateCommissionDto.prototype, "description", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'commission id' }),
    __metadata("design:type", String)
], CreateCommissionDto.prototype, "idUser", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'commission id' }),
    __metadata("design:type", Date)
], CreateCommissionDto.prototype, "createAt", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'tipo de referido' }),
    __metadata("design:type", String)
], CreateCommissionDto.prototype, "typeReferido", void 0);
exports.CreateCommissionDto = CreateCommissionDto;
//# sourceMappingURL=createCommission.dto.js.map