import { User } from "../../users/user.entity";
export declare class CommissionDto {
    commission: number;
    description: string[];
    user: User;
    createAt: Date;
    typeReferido: string;
}
