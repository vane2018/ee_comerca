"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommissionsService = void 0;
const common_1 = require("@nestjs/common");
const crud_typeorm_1 = require("@nestjsx/crud-typeorm");
const commissions_entity_1 = require("./commissions.entity");
const typeorm_1 = require("@nestjs/typeorm");
const firebase = require("firebase-admin");
const user_service_1 = require("../users/user.service");
let CommissionsService = class CommissionsService extends crud_typeorm_1.TypeOrmCrudService {
    constructor(repo, serviceUser) {
        super(repo);
        this.serviceUser = serviceUser;
    }
    async saveCommission(commisionDto) {
        try {
            let comision = this.repo.create(commisionDto);
            return await this.repo.save(comision);
        }
        catch (error) {
            console.log('error', error);
        }
    }
    async notificarComercioSobreNuevoVenta(purchass) {
        try {
            const notificacionId = `nuevo-pedido`;
            if (purchass.user.firebaseRegistrationToken) {
                const message = {
                    notification: {
                        title: `Nueva comision`,
                        body: 'comision asignada',
                    },
                    data: {
                        score: '850',
                        time: '2:45',
                        link: '',
                        notificacionId: notificacionId,
                    },
                    fcm_options: {
                        link: ''
                    },
                    token: purchass.user.firebaseRegistrationToken
                };
                firebase.messaging().send(message)
                    .then((response) => {
                    console.log('Firebase successfully sent message:', response);
                })
                    .catch((error) => {
                    console.log('message:', error);
                });
            }
            ;
        }
        catch (error) {
            console.log(error);
        }
    }
    async cobrarCbu(monto, cbu) {
        var user = await this.serviceUser.findOne({ where: { cbu: cbu } });
        let tot = monto + user.montoextraido;
        return await this.serviceUser.guardarComision(user, tot);
    }
    async getCommission() {
        try {
            return await this.repo.find({ relations: ['user'] });
        }
        catch (error) {
            console.log('error', error);
        }
    }
    async getCommissionByIds(ids) {
        let comisions = await this.repo.findByIds(ids);
        return comisions;
    }
    async getCommissionByUserId(userId) {
        let comisions = await this.repo.find({ where: { user: userId } });
        return comisions;
    }
    async deleteCommission(id) {
        await this.repo.delete(id);
    }
};
CommissionsService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(commissions_entity_1.Commision)),
    __metadata("design:paramtypes", [Object, user_service_1.UsersService])
], CommissionsService);
exports.CommissionsService = CommissionsService;
//# sourceMappingURL=commissions.service.js.map