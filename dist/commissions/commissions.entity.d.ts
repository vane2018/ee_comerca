import { User } from "../users/user.entity";
export declare class Commision {
    id: string;
    commission: number;
    description: string;
    createAt: Date;
    user: User;
    typeReferido: string;
}
