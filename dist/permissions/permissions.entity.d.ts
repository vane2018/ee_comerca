import { Rols } from '../rols/rols.entity';
export declare class Permission {
    id: string;
    name: string;
    rols: Rols[];
}
