import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { subcategoryProductsDto } from './dto/subcategory-product.dto';
export declare class SubcategoryProductsService extends TypeOrmCrudService<subcategoryProductsDto> {
    constructor(repo: any);
    saveSubcategory(subcategoryDto: subcategoryProductsDto): Promise<subcategoryProductsDto>;
    getcategorySubcategory(): Promise<subcategoryProductsDto[]>;
    getSubcategoriaByGeneralId(categoryId: string): Promise<any>;
    getCategoryById(id: string): Promise<any>;
    getCategoryByIdProduct(id: string): Promise<any>;
}
