"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubcategoryProductsController = void 0;
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const subcategory_product_dto_1 = require("./dto/subcategory-product.dto");
const category_product_general_service_1 = require("../category-product-general/category-product-general.service");
const createSubcategoryProduct_dto_1 = require("./dto/createSubcategoryProduct.dto");
const subcategory_products_service_1 = require("./subcategory-products.service");
const swagger_1 = require("@nestjs/swagger");
const subcategoryDto_dto_1 = require("./dto/subcategoryDto.dto");
let SubcategoryProductsController = class SubcategoryProductsController {
    constructor(service, serviceGeneral) {
        this.service = service;
        this.serviceGeneral = serviceGeneral;
    }
    async saveSubcategory(res, createSubcategory) {
        try {
            let category = await this.serviceGeneral.findOne(createSubcategory.idProductGeneral);
            if (category) {
                let subcategorySave = {
                    name: createSubcategory.name,
                    productsGeneral: category
                };
                const subctegory = await this.service.saveSubcategory(subcategorySave);
                return res.status(common_1.HttpStatus.OK).json(subctegory);
            }
            else {
                return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'categoria no encontrada' });
            }
        }
        catch (error) {
            console.log('error al crear producto: ', error);
            return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el producto', error });
        }
    }
    async getSubcategory(res) {
        const subCategoriaProduct = await this.service.getcategorySubcategory();
        return res.status(common_1.HttpStatus.OK).json(subCategoriaProduct);
    }
    async getCommerce(res, param) {
        console.log("entro a subcategoria");
        const categoryProducts = await this.service.getCategoryById(param.id);
        return res.status(common_1.HttpStatus.OK).json(categoryProducts);
    }
    async getCommerceProduct(res, productDto, param) {
        const categoryProducts = await this.service.getCategoryByIdProduct(productDto.id);
        return res.status(common_1.HttpStatus.OK).json(categoryProducts);
    }
};
__decorate([
    common_1.Post('register'),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, createSubcategoryProduct_dto_1.CreateSucategoryProductDto]),
    __metadata("design:returntype", Promise)
], SubcategoryProductsController.prototype, "saveSubcategory", null);
__decorate([
    common_1.Get(),
    swagger_1.ApiOperation({ summary: 'obtiene todas las Subcategorias de producto' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], SubcategoryProductsController.prototype, "getSubcategory", null);
__decorate([
    common_1.Get('/:id'),
    swagger_1.ApiOperation({ summary: 'Obtiene subcategoria por su Id, de aqui obtengo un [] de categoria de producto' }),
    swagger_1.ApiResponse({ status: 200, description: 'Los datos han sido devueltos correctamente.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], SubcategoryProductsController.prototype, "getCommerce", null);
__decorate([
    common_1.Post('product-list/id'),
    swagger_1.ApiOperation({ summary: 'Obtiene subcategoria por su Id, de aqui obtengo un [] de categoria de producto' }),
    swagger_1.ApiResponse({ status: 200, description: 'Los datos han sido devueltos correctamente.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    swagger_1.ApiBody({ type: subcategoryDto_dto_1.productDto }),
    __param(0, common_1.Response()), __param(1, common_1.Body()), __param(2, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], SubcategoryProductsController.prototype, "getCommerceProduct", null);
SubcategoryProductsController = __decorate([
    crud_1.Crud({
        model: {
            type: subcategory_product_dto_1.subcategoryProductsDto,
        },
        params: {
            id: {
                field: 'id',
                type: 'uuid',
                primary: true,
            },
        },
        query: {
            join: {
                productsGeneral: {},
                productsSubcategory: {
                    allow: []
                }
            }
        }
    }),
    swagger_1.ApiTags('Product-Subcategory'),
    common_1.Controller('subcategory-products'),
    __metadata("design:paramtypes", [subcategory_products_service_1.SubcategoryProductsService,
        category_product_general_service_1.CategoryGeneralService])
], SubcategoryProductsController);
exports.SubcategoryProductsController = SubcategoryProductsController;
//# sourceMappingURL=subcategory-products.controller.js.map