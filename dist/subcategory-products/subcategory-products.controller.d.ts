import { CategoryGeneralService } from '../category-product-general/category-product-general.service';
import { CreateSucategoryProductDto } from './dto/createSubcategoryProduct.dto';
import { SubcategoryProductsService } from './subcategory-products.service';
export declare class SubcategoryProductsController {
    service: SubcategoryProductsService;
    serviceGeneral: CategoryGeneralService;
    constructor(service: SubcategoryProductsService, serviceGeneral: CategoryGeneralService);
    saveSubcategory(res: any, createSubcategory: CreateSucategoryProductDto): Promise<any>;
    getSubcategory(res: any): Promise<any>;
    getCommerce(res: any, param: any): Promise<any>;
    getCommerceProduct(res: any, productDto: any, param: any): Promise<any>;
}
