"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.subcategoryProductsDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const categoryProductGeneral_dto_1 = require("../../category-product-general/dto/categoryProductGeneral.dto");
class subcategoryProductsDto {
}
__decorate([
    swagger_1.ApiProperty({ description: 'Product name' }),
    __metadata("design:type", String)
], subcategoryProductsDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'product type id' }),
    __metadata("design:type", categoryProductGeneral_dto_1.CategoryProductGeneralDto)
], subcategoryProductsDto.prototype, "productsGeneral", void 0);
exports.subcategoryProductsDto = subcategoryProductsDto;
//# sourceMappingURL=subcategory-product.dto.js.map