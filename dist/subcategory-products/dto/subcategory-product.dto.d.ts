import { CategoryProductGeneralDto } from "../../category-product-general/dto/categoryProductGeneral.dto";
export declare class subcategoryProductsDto {
    readonly name: string;
    productsGeneral: CategoryProductGeneralDto;
}
