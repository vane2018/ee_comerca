"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubcategoryProductsModule = void 0;
const common_1 = require("@nestjs/common");
const subcategory_products_controller_1 = require("./subcategory-products.controller");
const subcategory_products_service_1 = require("./subcategory-products.service");
const subcategory_products_entity_1 = require("./subcategory-products.entity");
const typeorm_1 = require("@nestjs/typeorm");
const category_product_general_module_1 = require("../category-product-general/category-product-general.module");
let SubcategoryProductsModule = class SubcategoryProductsModule {
};
SubcategoryProductsModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([subcategory_products_entity_1.ProductSubcategory]),
            category_product_general_module_1.CategoryGeneralModule],
        controllers: [subcategory_products_controller_1.SubcategoryProductsController],
        providers: [subcategory_products_service_1.SubcategoryProductsService],
        exports: [subcategory_products_service_1.SubcategoryProductsService],
    })
], SubcategoryProductsModule);
exports.SubcategoryProductsModule = SubcategoryProductsModule;
//# sourceMappingURL=subcategory-products.module.js.map