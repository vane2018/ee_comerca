import { ProductTypes } from "../product-category/product-category.entity";
import { ProductGeneral } from "../category-product-general/category-product-general.entity";
export declare class ProductSubcategory {
    id: string;
    name: string;
    productsTypes: ProductTypes[];
    productsGeneral: ProductGeneral;
}
