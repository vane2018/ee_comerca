"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductSubcategory = void 0;
const typeorm_1 = require("typeorm");
const product_category_entity_1 = require("../product-category/product-category.entity");
const category_product_general_entity_1 = require("../category-product-general/category-product-general.entity");
let ProductSubcategory = class ProductSubcategory {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], ProductSubcategory.prototype, "id", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500 }),
    __metadata("design:type", String)
], ProductSubcategory.prototype, "name", void 0);
__decorate([
    typeorm_1.OneToMany(() => product_category_entity_1.ProductTypes, (productType) => productType.productsSubcategory),
    __metadata("design:type", Array)
], ProductSubcategory.prototype, "productsTypes", void 0);
__decorate([
    typeorm_1.ManyToOne(() => category_product_general_entity_1.ProductGeneral, (productGeneral) => productGeneral.productsSubcategory),
    typeorm_1.JoinColumn(),
    __metadata("design:type", category_product_general_entity_1.ProductGeneral)
], ProductSubcategory.prototype, "productsGeneral", void 0);
ProductSubcategory = __decorate([
    typeorm_1.Entity('SUBCATEGORY_PRODUCTS')
], ProductSubcategory);
exports.ProductSubcategory = ProductSubcategory;
//# sourceMappingURL=subcategory-products.entity.js.map