"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentsController = void 0;
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const payment_entity_1 = require("./payment.entity");
const payments_service_1 = require("./payments.service");
const swagger_1 = require("@nestjs/swagger");
const payments_dto_1 = require("./dto/payments.dto");
const paymentResponse_dto_1 = require("./dto/paymentResponse.dto");
let PaymentsController = class PaymentsController {
    constructor(service) {
        this.service = service;
    }
};
PaymentsController = __decorate([
    crud_1.Crud({
        model: {
            type: payment_entity_1.Payment
        },
        routes: {
            exclude: ['createManyBase'],
            createOneBase: {
                decorators: [swagger_1.ApiBadRequestResponse({ description: 'Los datos no son los correctos' }),
                    swagger_1.ApiInternalServerErrorResponse({ description: 'Error interno del servidor' })]
            }
        },
        dto: {
            create: payments_dto_1.PaymentDto,
        },
        serialize: {
            create: paymentResponse_dto_1.PaymentResponseDto,
        },
        params: {
            id: {
                field: 'id',
                type: 'uuid',
                primary: true
            }
        },
        query: {
            join: {
                paymentTypes: {}
            }
        }
    }),
    swagger_1.ApiTags('Payments'),
    common_1.Controller('payments'),
    __metadata("design:paramtypes", [payments_service_1.PaymentsService])
], PaymentsController);
exports.PaymentsController = PaymentsController;
//# sourceMappingURL=payments.controller.js.map