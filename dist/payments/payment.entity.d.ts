import { PaymentTypes } from "../payment-types/payment-types.entity";
export declare class Payment {
    id: string;
    amount: number;
    dues: number;
    date: Date;
    paymentTypes: PaymentTypes;
}
