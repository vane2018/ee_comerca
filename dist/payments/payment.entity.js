"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Payment = void 0;
const typeorm_1 = require("typeorm");
const payment_types_entity_1 = require("../payment-types/payment-types.entity");
let Payment = class Payment {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Payment.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(({ type: "float8", nullable: true })),
    __metadata("design:type", Number)
], Payment.prototype, "amount", void 0);
__decorate([
    typeorm_1.Column({ nullable: true, default: 0 }),
    __metadata("design:type", Number)
], Payment.prototype, "dues", void 0);
__decorate([
    typeorm_1.Column({ default: new Date() }),
    __metadata("design:type", Date)
], Payment.prototype, "date", void 0);
__decorate([
    typeorm_1.ManyToOne(type => payment_types_entity_1.PaymentTypes, { cascade: true, eager: false }),
    typeorm_1.JoinColumn({ name: 'payment_types_id' }),
    __metadata("design:type", payment_types_entity_1.PaymentTypes)
], Payment.prototype, "paymentTypes", void 0);
Payment = __decorate([
    typeorm_1.Entity('PAYMENTS')
], Payment);
exports.Payment = Payment;
//# sourceMappingURL=payment.entity.js.map