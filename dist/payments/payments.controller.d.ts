import { CrudController } from '@nestjsx/crud';
import { Payment } from './payment.entity';
import { PaymentsService } from './payments.service';
export declare class PaymentsController implements CrudController<Payment> {
    service: PaymentsService;
    constructor(service: PaymentsService);
}
