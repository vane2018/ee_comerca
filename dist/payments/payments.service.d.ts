import { Payment } from './payment.entity';
import { Repository } from 'typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
export declare class PaymentsService extends TypeOrmCrudService<Payment> {
    constructor(repo: Repository<Payment>);
}
