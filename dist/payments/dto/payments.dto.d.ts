export declare class PaymentDto {
    readonly amount: number;
    readonly dues: number;
    readonly date: Date;
    readonly paymentTypes: string;
}
