export declare class PaymentResponseDto {
    readonly id: number;
    readonly amount: number;
    readonly dues: number;
    readonly date: Date;
}
