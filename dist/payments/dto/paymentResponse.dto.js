"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentResponseDto = void 0;
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
let PaymentResponseDto = class PaymentResponseDto {
};
__decorate([
    swagger_1.ApiProperty({ description: 'id del pago' }),
    __metadata("design:type", Number)
], PaymentResponseDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'monto de la compra' }),
    __metadata("design:type", Number)
], PaymentResponseDto.prototype, "amount", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'cantidad de cuotas de la compra, por defecto 0' }),
    __metadata("design:type", Number)
], PaymentResponseDto.prototype, "dues", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'fecha de la compra de la compra, por defecto fecha actual', type: Date }),
    __metadata("design:type", Date)
], PaymentResponseDto.prototype, "date", void 0);
PaymentResponseDto = __decorate([
    typeorm_1.Entity('PAYMENT')
], PaymentResponseDto);
exports.PaymentResponseDto = PaymentResponseDto;
//# sourceMappingURL=paymentResponse.dto.js.map