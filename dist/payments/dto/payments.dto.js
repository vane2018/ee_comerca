"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentDto = void 0;
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
let PaymentDto = class PaymentDto {
};
__decorate([
    swagger_1.ApiProperty({ description: 'monto de la compra' }),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", Number)
], PaymentDto.prototype, "amount", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'cantidad de cuotas de la compra, por defecto 0' }),
    __metadata("design:type", Number)
], PaymentDto.prototype, "dues", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'fecha de la compra de la compra, por defecto fecha actual', type: 'string', format: 'date-time' }),
    __metadata("design:type", Date)
], PaymentDto.prototype, "date", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'id del tipo de pago', type: String, format: 'uuid' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsUUID(),
    __metadata("design:type", String)
], PaymentDto.prototype, "paymentTypes", void 0);
PaymentDto = __decorate([
    typeorm_1.Entity('PAYMENT')
], PaymentDto);
exports.PaymentDto = PaymentDto;
//# sourceMappingURL=payments.dto.js.map