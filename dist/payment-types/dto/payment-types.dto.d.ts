import { paymentTypes } from "../payment-types.entity";
export declare class PaymentTypesDto {
    readonly name: paymentTypes;
    readonly description: string;
}
