"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentTypesDto = void 0;
const payment_types_entity_1 = require("../payment-types.entity");
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
class PaymentTypesDto {
}
__decorate([
    swagger_1.ApiProperty({ description: 'nombre del tipo de pago' }),
    class_validator_1.IsEnum(payment_types_entity_1.paymentTypes),
    __metadata("design:type", String)
], PaymentTypesDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'descripción del tipo de pago' }),
    __metadata("design:type", String)
], PaymentTypesDto.prototype, "description", void 0);
exports.PaymentTypesDto = PaymentTypesDto;
//# sourceMappingURL=payment-types.dto.js.map