import { CrudController } from '@nestjsx/crud';
import { PaymentTypes } from './payment-types.entity';
import { PaymentTypesService } from './payment-types.service';
export declare class PaymentTypesController implements CrudController<PaymentTypes> {
    service: PaymentTypesService;
    constructor(service: PaymentTypesService);
}
