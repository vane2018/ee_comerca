import { PaymentTypes } from './payment-types.entity';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
export declare class PaymentTypesService extends TypeOrmCrudService<PaymentTypes> {
    constructor(repo: any);
}
