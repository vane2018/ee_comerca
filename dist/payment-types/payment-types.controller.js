"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentTypesController = void 0;
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const payment_types_entity_1 = require("./payment-types.entity");
const swagger_1 = require("@nestjs/swagger");
const payment_types_service_1 = require("./payment-types.service");
const payment_types_dto_1 = require("./dto/payment-types.dto");
let PaymentTypesController = class PaymentTypesController {
    constructor(service) {
        this.service = service;
    }
};
PaymentTypesController = __decorate([
    crud_1.Crud({
        model: {
            type: payment_types_entity_1.PaymentTypes
        },
        routes: {
            exclude: ['createManyBase',
                'deleteOneBase',
                'updateOneBase',
                'replaceOneBase',
                'createOneBase',
                'getOneBase'],
            getManyBase: {
                decorators: [swagger_1.ApiOkResponse({ description: 'datos devueltos correctamente', type: [payment_types_dto_1.PaymentTypesDto] })]
            }
        },
        serialize: {
            getMany: payment_types_dto_1.PaymentTypesDto,
        },
        params: {
            id: {
                field: 'id',
                type: 'uuid',
                primary: true
            },
        },
    }),
    swagger_1.ApiTags('Payment-Tipes'),
    common_1.Controller('payment-types'),
    __metadata("design:paramtypes", [payment_types_service_1.PaymentTypesService])
], PaymentTypesController);
exports.PaymentTypesController = PaymentTypesController;
//# sourceMappingURL=payment-types.controller.js.map