export declare enum paymentTypes {
    EFECTIVO = "Efectivo",
    CREDITO = "Credito",
    DEBITO = "Debito"
}
export declare class PaymentTypes {
    id: string;
    name: paymentTypes;
    description: string;
}
