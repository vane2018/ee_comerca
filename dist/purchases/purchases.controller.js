"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchasesController = void 0;
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const purchase_entity_1 = require("./purchase.entity");
const swagger_1 = require("@nestjs/swagger");
const purchases_service_1 = require("./purchases.service");
const purchaseCreate_dto_1 = require("./dto/purchaseCreate.dto");
const queryDto_dto_1 = require("./dto/queryDto.dto");
const firebase = require("firebase-admin");
const commerce_service_1 = require("../commerce/commerce.service");
const user_service_1 = require("../users/user.service");
const enviar_notificaciones_dto_1 = require("./dto/enviar-notificaciones.dto");
let PurchasesController = class PurchasesController {
    constructor(service, serviceComercio, serviceUser) {
        this.service = service;
        this.serviceComercio = serviceComercio;
        this.serviceUser = serviceUser;
    }
    async createPurchase(res, purchaseDto) {
        let response = await this.service.create(purchaseDto);
        return res.status(common_1.HttpStatus.OK).json(response);
    }
    async getSellers(res) {
        let purchases = await this.service.getPurchases();
        return res.status(common_1.HttpStatus.OK).json(purchases);
    }
    async enviarNotif(res, vendedor) {
        var usuario = this.serviceUser.findOne(vendedor.vendedorId);
        try {
            const notificacionId = `nuevo-pedido`;
            if ((await usuario).firebaseRegistrationToken) {
                const message = {
                    notification: {
                        title: `venta realizada`,
                        body: 'se ha registardo una nueva venta',
                    },
                    data: {
                        score: '850',
                        time: '2:45',
                        link: '',
                        notificacionId: notificacionId,
                    },
                    fcm_options: {
                        link: ''
                    },
                    token: (await usuario).firebaseRegistrationToken
                };
                firebase.messaging().send(message)
                    .then((response) => {
                    console.log('Firebase successfully sent message:', response);
                })
                    .catch((error) => {
                    console.log('message:', error);
                });
            }
            ;
        }
        catch (error) {
            console.log(error);
        }
    }
    async showPurchaseCommerce(res, param, queryDto) {
        let response = await this.service.showByCommerceId(param.commerceId, queryDto);
        return res.status(common_1.HttpStatus.OK).json(response);
    }
    async showPurchaseCustomer(res, param, queryDto) {
        let response = await this.service.showByCustomerId(param.customerId, queryDto);
        return res.status(common_1.HttpStatus.OK).json(response);
    }
};
__decorate([
    common_1.Post('create'),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, purchaseCreate_dto_1.PurchaseCreateDto]),
    __metadata("design:returntype", Promise)
], PurchasesController.prototype, "createPurchase", null);
__decorate([
    common_1.Get(),
    swagger_1.ApiOperation({ summary: 'Obtiene todos los comercios' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PurchasesController.prototype, "getSellers", null);
__decorate([
    common_1.Post('notificaciones'),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, enviar_notificaciones_dto_1.notificacionVendedor]),
    __metadata("design:returntype", Promise)
], PurchasesController.prototype, "enviarNotif", null);
__decorate([
    common_1.Get('/commerce/:commerceId'),
    swagger_1.ApiParam({ type: "string", name: 'commerceId' }),
    __param(0, common_1.Res()), __param(1, common_1.Param()), __param(2, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, queryDto_dto_1.QueryDto]),
    __metadata("design:returntype", Promise)
], PurchasesController.prototype, "showPurchaseCommerce", null);
__decorate([
    common_1.Get('/customer/:customerId'),
    swagger_1.ApiParam({ type: "string", name: 'customerId' }),
    __param(0, common_1.Res()), __param(1, common_1.Param()), __param(2, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, queryDto_dto_1.QueryDto]),
    __metadata("design:returntype", Promise)
], PurchasesController.prototype, "showPurchaseCustomer", null);
PurchasesController = __decorate([
    crud_1.Crud({
        model: {
            type: purchase_entity_1.Purchase,
        },
        routes: { exclude: ['createManyBase', 'createOneBase'] },
        params: {
            id: {
                field: 'id',
                type: 'uuid',
                primary: true,
            },
        },
    }),
    swagger_1.ApiTags('Purchase'),
    common_1.Controller('purchases'),
    __metadata("design:paramtypes", [purchases_service_1.PurchasesService,
        commerce_service_1.CommerceService,
        user_service_1.UsersService])
], PurchasesController);
exports.PurchasesController = PurchasesController;
//# sourceMappingURL=purchases.controller.js.map