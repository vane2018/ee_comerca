"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchasesModule = void 0;
const common_1 = require("@nestjs/common");
const purchases_service_1 = require("./purchases.service");
const purchases_controller_1 = require("./purchases.controller");
const typeorm_1 = require("@nestjs/typeorm");
const purchase_entity_1 = require("./purchase.entity");
const products_module_1 = require("../products/products.module");
const user_module_1 = require("../users/user.module");
const commissions_module_1 = require("../commissions/commissions.module");
const commerce_module_1 = require("../commerce/commerce.module");
const detalle_module_1 = require("../detalle/detalle.module");
const detalle_service_1 = require("../detalle/detalle.service");
const detalle_entity_1 = require("../detalle/detalle.entity");
let PurchasesModule = class PurchasesModule {
};
PurchasesModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([purchase_entity_1.Purchase, detalle_entity_1.Detalle]),
            detalle_module_1.DetalleModule,
            user_module_1.UsersModule,
            commerce_module_1.CommerceModule,
            products_module_1.ProductsModule,
            commissions_module_1.CommissionsModule],
        providers: [purchases_service_1.PurchasesService, detalle_service_1.DetalleService],
        controllers: [purchases_controller_1.PurchasesController]
    })
], PurchasesModule);
exports.PurchasesModule = PurchasesModule;
//# sourceMappingURL=purchases.module.js.map