import { Payment } from "../payments/payment.entity";
import { Commerce } from "../commerce/commerce.entity";
import { User } from "../users/user.entity";
import { Shipping } from "../shipping/shipping.entity";
import { Detalle } from "../detalle/detalle.entity";
export declare class Purchase {
    id: string;
    date: Date;
    user: User;
    payment: Payment;
    commerce: Commerce;
    createAt: Date;
    shipping: Shipping;
    detalle: Detalle[];
}
