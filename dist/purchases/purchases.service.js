"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchasesService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const crud_typeorm_1 = require("@nestjsx/crud-typeorm");
const purchase_entity_1 = require("./purchase.entity");
const products_service_1 = require("../products/products.service");
const user_service_1 = require("../users/user.service");
const commissions_service_1 = require("../commissions/commissions.service");
const commmissions_dto_1 = require("../commissions/dto/commmissions.dto");
const commerce_service_1 = require("../commerce/commerce.service");
const firebase = require("firebase-admin");
const chat_entity_1 = require("../chat/chat.entity");
const detalle_entity_1 = require("../detalle/detalle.entity");
const detalle_service_1 = require("../detalle/detalle.service");
let PurchasesService = class PurchasesService extends crud_typeorm_1.TypeOrmCrudService {
    constructor(repo, commercesService, userService, detalleService, productService, commisionsService) {
        super(repo);
        this.commercesService = commercesService;
        this.userService = userService;
        this.detalleService = detalleService;
        this.productService = productService;
        this.commisionsService = commisionsService;
    }
    async saveDetails(idPurchase, products) {
        try {
            let purchase = await this.repo.findOne(idPurchase, { relations: ['products'] });
            for (let i = 0; i < products.length; i++) {
                purchase.detalles.push(products);
            }
            let newPurchase = await this.repo.save(purchase);
            return newPurchase;
        }
        catch (error) {
            return error;
        }
    }
    async getPurchases() {
        return await this.repo.find({ relations: ['detalle'] });
    }
    async create(data) {
        try {
            let { detalles } = Object.assign({}, data);
            let porcentaje = 0;
            let band2 = false;
            let band3 = false;
            let responseProducts = [];
            let commision = 0;
            let dimension = detalles.length;
            for (let index = 0; index < detalles.length; index++) {
                let mproduct = await this.productService.searchCommerceProducts(detalles[index].idProducto);
                responseProducts.push(mproduct);
            }
            let porc = process.env.PORCENTAJE;
            let comprartir = await this.userService.findOne({ where: { email: "comprartir@gmail.com" } });
            for (let i = 0; i < dimension; i++) {
                data.detalles.push(responseProducts[i]);
                if (detalles[i].priceOfert === 0) {
                    commision += responseProducts[i].price * data.detalles[i].cantidad;
                }
                else {
                    commision += responseProducts[i].priceOffer * data.detalles[i].cantidad;
                }
            }
            let commisionDto = new commmissions_dto_1.CommissionDto();
            let commisionDto2 = new commmissions_dto_1.CommissionDto();
            let commisionDto3 = new commmissions_dto_1.CommissionDto();
            commisionDto3.commission = 0;
            commisionDto3.user = comprartir;
            commisionDto3.typeReferido = "comprartir";
            let user = await this.userService.findOne(data.user);
            let description = [];
            let description2 = [];
            let description3 = [];
            description3.push("comprartir");
            if (user.nickReferrer) {
                description.push(`${user.firstName} ${user.lastName}`);
                description.push(`${user.profilePic}`);
                let referente = await this.userService.findOne({ where: { nickName: user.nickReferrer } });
                commisionDto.user = referente;
                if (referente) {
                    commisionDto.commission = this.reduccion(commision * 0.01);
                    commisionDto.typeReferido = "comprador";
                }
                else {
                    band2 = true;
                }
                description.push(`${responseProducts.map(item => item.id)}`);
                commisionDto.description = description;
                commisionDto.createAt = new Date();
                await this.commisionsService.saveCommission(commisionDto);
            }
            else {
                band2 = true;
            }
            let commerce = responseProducts[0].commerce;
            let comercio = await this.commercesService.searchUserCommerce(commerce.id);
            var idVendedor = comercio[0].user.id;
            if (comercio[0].user.nickReferrer) {
                description2.push(`${comercio[0].user.firstName} ${comercio[0].user.lastName}`);
                description2.push(`${comercio[0].user.profilePic}`);
                let referentedeSeller = await this.userService.findOne({ where: { nickName: comercio[0].user.nickReferrer } });
                commisionDto2.user = referentedeSeller;
                commisionDto2.user = await this.userService.findOne({ where: { nickName: comercio[0].user.nickReferrer } });
                if (referentedeSeller) {
                    commisionDto2.commission = this.reduccion(commision * 0.01);
                    commisionDto2.typeReferido = "vendedor";
                }
                else {
                    band3 = true;
                }
                description2.push(`${responseProducts.map(item => item.id)}`);
                commisionDto2.description = description2;
                commisionDto2.createAt = new Date();
                await this.commisionsService.saveCommission(commisionDto2);
            }
            else {
                band3 = true;
            }
            data.commerce = commerce;
            data.date = new Date();
            commisionDto3.createAt = new Date();
            if (band2 && band3) {
                porcentaje = parseFloat(porcentaje + porc + 0.02);
            }
            else {
                if (band2 || band3) {
                    porcentaje = parseFloat(porcentaje + porc + 0.01);
                }
                else {
                    porcentaje = parseFloat(porcentaje + porc);
                }
            }
            commisionDto3.description = description3;
            commisionDto3.commission = this.reduccion(commisionDto3.commission + commision * porcentaje);
            let productoOrdenado = detalles.sort();
            for (let i = 0; i < dimension; i++) {
                console.log("DATA DETALLE", data.detalles[i].cantidad);
                console.log("DATA DETALLE", responseProducts[i].stock);
                responseProducts[i].stock = responseProducts[i].stock - data.detalles[i].cantidad;
                console.log("PRODUCTOS", responseProducts[i].stock);
                await this.productService.saveProduct(responseProducts[i]);
            }
            let purchase = await this.repo.create(data);
            await this.commisionsService.saveCommission(commisionDto3);
            console.log("COMPRARTIR", comprartir.id);
            await this.userService.addCommisions(comprartir.id, commisionDto3.commission);
            const mipurchase = await this.repo.save(purchase);
            let arrayDetalle;
            for (let index = 0; index < dimension; index++) {
                let detalle = new detalle_entity_1.Detalle();
                detalle.productoId = data.detalles[index].idProducto;
                detalle.productoName = data.detalles[index].nombreProducto;
                detalle.productoPrice = data.detalles[index].precioProducto;
                detalle.imagenProducto = data.detalles[index].imagenProducto;
                detalle.purchaseId = purchase.id;
                let vect = await this.detalleService.saveDetalle(detalle);
            }
            this.envianotificacionVendedor2(mipurchase.id, idVendedor);
            this.notificarReferido(commisionDto3);
            if (commisionDto.commission !== null) {
                this.notificarReferido(commisionDto);
            }
            if (commisionDto2.commission !== null) {
                this.notificarReferido(commisionDto2);
            }
            return mipurchase;
        }
        catch (error) {
            console.log('error: ', error);
            throw new common_1.HttpException(`${error}`, common_1.HttpStatus.BAD_REQUEST);
        }
    }
    async envianotificacionVendedor2(mipurchase, idVendedor) {
        var usuario = this.userService.findOne(idVendedor);
        try {
            const notificacionId = `nuevo-venta`;
            if ((await usuario).firebaseRegistrationToken) {
                firebase.messaging().sendToDevice((await usuario).firebaseRegistrationToken, {
                    data: {
                        notificacionId: notificacionId,
                        tipo: chat_entity_1.TipoNotificacion.Venta
                    },
                    notification: {
                        tag: notificacionId,
                        title: `Nuevo venta registrada nro  ${mipurchase}`,
                        body: `se registro una nueva venta`,
                    }
                });
            }
        }
        catch (error) {
            console.log("se detecto un error", error);
        }
    }
    async notificarReferido(purchass) {
        try {
            var usuario = this.userService.findOne(purchass.user.id);
            const notificacionId = `nueva-comision`;
            if ((await purchass.user).firebaseRegistrationToken) {
                firebase.messaging().sendToDevice((await purchass.user).firebaseRegistrationToken, {
                    data: {
                        notificacionId: notificacionId,
                        tipo: chat_entity_1.TipoNotificacion.Comision
                    },
                    notification: {
                        tag: notificacionId,
                        title: `Tiene una nueva comision de $ ${purchass.commission}`,
                        body: `se registro una nueva comision`,
                    }
                });
            }
        }
        catch (error) {
            console.log(error);
        }
    }
    reduccion(valor) {
        let number = valor;
        let letra = number.toString().split("");
        let vect = [];
        let vect1 = [];
        vect = letra;
        let j = 0;
        for (let index = 0; index < vect.length; index++) {
            if (vect[index] !== '.') {
                vect1[j] = vect[index];
                j = j + 1;
            }
            else {
                if (vect[index] === '.') {
                    vect1[j] = vect[index];
                    vect1[j + 1] = vect[index + 1];
                    vect1[j + 2] = vect[index + 2];
                    index = vect.length + 1;
                }
            }
        }
        let precio = vect1.join('');
        let abs = parseFloat(precio);
        return abs;
    }
    async showByCommerceId(commerceId, queryDto) {
        try {
            const purchases = await this.repo.find({ where: { commerce: commerceId }, relations: ["detalle", "user", "payment"] });
            return purchases;
        }
        catch (error) {
            console.log('error: ', error);
        }
    }
    async showByCustomerId(customerId, queryDto) {
        console.log('query: ', queryDto);
        try {
            const purchases = await this.repo.find({ where: { user: customerId }, relations: ["detalle", "user", "payment", "shipping"] });
            return purchases;
        }
        catch (error) {
            console.log('error: ', error);
        }
    }
};
PurchasesService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(purchase_entity_1.Purchase)),
    __metadata("design:paramtypes", [Object, commerce_service_1.CommerceService,
        user_service_1.UsersService,
        detalle_service_1.DetalleService,
        products_service_1.ProductsService,
        commissions_service_1.CommissionsService])
], PurchasesService);
exports.PurchasesService = PurchasesService;
//# sourceMappingURL=purchases.service.js.map