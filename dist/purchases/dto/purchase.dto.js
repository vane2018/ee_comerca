"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const commerce_entity_1 = require("../../commerce/commerce.entity");
class PurchaseDto {
}
__decorate([
    swagger_1.ApiProperty({ description: 'purchase id' }),
    __metadata("design:type", String)
], PurchaseDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'purchase date', type: 'string', format: 'date-time' }),
    __metadata("design:type", Date)
], PurchaseDto.prototype, "date", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'payment id' }),
    __metadata("design:type", String)
], PurchaseDto.prototype, "payment", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'payment id' }),
    __metadata("design:type", String)
], PurchaseDto.prototype, "shipping", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'id user' }),
    __metadata("design:type", String)
], PurchaseDto.prototype, "user", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'selected products ids' }),
    __metadata("design:type", Array)
], PurchaseDto.prototype, "detalles", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'commerce' }),
    __metadata("design:type", commerce_entity_1.Commerce)
], PurchaseDto.prototype, "commerce", void 0);
exports.PurchaseDto = PurchaseDto;
//# sourceMappingURL=purchase.dto.js.map