import { Commerce } from "../../commerce/commerce.entity";
import { purchaseDetalleDto } from "./purchases-detalle.dto";
export declare class PurchaseDto {
    readonly id: string;
    date: Date;
    readonly payment: string;
    readonly shipping: string;
    readonly user: string;
    detalles: purchaseDetalleDto[];
    commerce: Commerce;
}
