import { purchaseDetalleDto } from './purchases-detalle.dto';
export declare class PurchaseCreateDto {
    readonly date: Date;
    readonly payment: string;
    readonly shipping: string;
    readonly user: string;
    readonly detalles: purchaseDetalleDto[];
}
