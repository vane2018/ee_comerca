export declare class purchaseDetalleDto {
    idProducto: string;
    nombreProducto: string;
    precioProducto: number;
    cantidad: number;
    priceOfert: number;
    idComercio: string;
    imagenProducto: string;
}
