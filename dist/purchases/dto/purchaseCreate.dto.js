"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseCreateDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const purchases_detalle_dto_1 = require("./purchases-detalle.dto");
class PurchaseCreateDto {
}
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'purchase date', type: 'string', format: 'date-time' }),
    __metadata("design:type", Date)
], PurchaseCreateDto.prototype, "date", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'id payment', format: 'uuid' }),
    __metadata("design:type", String)
], PurchaseCreateDto.prototype, "payment", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'id payment', format: 'uuid' }),
    __metadata("design:type", String)
], PurchaseCreateDto.prototype, "shipping", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'id customer', format: 'uuid' }),
    __metadata("design:type", String)
], PurchaseCreateDto.prototype, "user", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'id customer', type: [purchases_detalle_dto_1.purchaseDetalleDto] }),
    __metadata("design:type", Array)
], PurchaseCreateDto.prototype, "detalles", void 0);
exports.PurchaseCreateDto = PurchaseCreateDto;
//# sourceMappingURL=purchaseCreate.dto.js.map