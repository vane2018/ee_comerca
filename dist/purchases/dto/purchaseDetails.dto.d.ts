import { purchaseDetalleDto } from "./purchases-detalle.dto";
export declare class PurchaseDetailDto {
    readonly idPurchase: string;
    readonly products: purchaseDetalleDto[];
    readonly cantidad: number[];
}
