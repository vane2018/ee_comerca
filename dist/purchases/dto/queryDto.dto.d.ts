export declare enum order {
    ASC = "ASC",
    DESC = "DESC"
}
export declare class QueryDto {
    readonly field: string;
    readonly order: order;
}
