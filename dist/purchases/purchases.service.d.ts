import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { PurchaseDto } from './dto/purchase.dto';
import { ProductsService } from '../products/products.service';
import { UsersService } from '../users/user.service';
import { CommissionsService } from '../commissions/commissions.service';
import { CommissionDto } from '../commissions/dto/commmissions.dto';
import { CommerceService } from '../commerce/commerce.service';
import { QueryDto } from './dto/queryDto.dto';
import { DetalleService } from '../detalle/detalle.service';
export declare class PurchasesService extends TypeOrmCrudService<PurchaseDto> {
    commercesService: CommerceService;
    userService: UsersService;
    detalleService: DetalleService;
    productService: ProductsService;
    commisionsService: CommissionsService;
    constructor(repo: any, commercesService: CommerceService, userService: UsersService, detalleService: DetalleService, productService: ProductsService, commisionsService: CommissionsService);
    saveDetails(idPurchase: string, products: any): Promise<any>;
    getPurchases(): Promise<PurchaseDto[]>;
    create(data: PurchaseDto): Promise<PurchaseDto>;
    envianotificacionVendedor2(mipurchase: string, idVendedor: string): Promise<void>;
    notificarReferido(purchass: CommissionDto): Promise<void>;
    reduccion(valor: number): number;
    showByCommerceId(commerceId: string, queryDto: QueryDto): Promise<PurchaseDto[]>;
    showByCustomerId(customerId: string, queryDto: QueryDto): Promise<PurchaseDto[]>;
}
