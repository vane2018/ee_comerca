import { CrudController } from '@nestjsx/crud';
import { PurchasesService } from './purchases.service';
import { PurchaseCreateDto } from './dto/purchaseCreate.dto';
import { PurchaseDto } from './dto/purchase.dto';
import { QueryDto } from './dto/queryDto.dto';
import { CommerceService } from '../commerce/commerce.service';
import { UsersService } from '../users/user.service';
import { notificacionVendedor } from './dto/enviar-notificaciones.dto';
export declare class PurchasesController implements CrudController<PurchaseDto> {
    service: PurchasesService;
    serviceComercio: CommerceService;
    serviceUser: UsersService;
    constructor(service: PurchasesService, serviceComercio: CommerceService, serviceUser: UsersService);
    createPurchase(res: any, purchaseDto: PurchaseCreateDto): Promise<any>;
    getSellers(res: any): Promise<any>;
    enviarNotif(res: any, vendedor: notificacionVendedor): Promise<void>;
    showPurchaseCommerce(res: any, param: any, queryDto: QueryDto): Promise<any>;
    showPurchaseCustomer(res: any, param: any, queryDto: QueryDto): Promise<any>;
}
