"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Purchase = void 0;
const typeorm_1 = require("typeorm");
const payment_entity_1 = require("../payments/payment.entity");
const commerce_entity_1 = require("../commerce/commerce.entity");
const user_entity_1 = require("../users/user.entity");
const shipping_entity_1 = require("../shipping/shipping.entity");
const detalle_entity_1 = require("../detalle/detalle.entity");
let Purchase = class Purchase {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Purchase.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ type: Date, default: new Date, nullable: true }),
    __metadata("design:type", Date)
], Purchase.prototype, "date", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.User, (user) => user.purchase),
    typeorm_1.JoinColumn(),
    __metadata("design:type", user_entity_1.User)
], Purchase.prototype, "user", void 0);
__decorate([
    typeorm_1.OneToOne(type => payment_entity_1.Payment),
    typeorm_1.JoinColumn(),
    __metadata("design:type", payment_entity_1.Payment)
], Purchase.prototype, "payment", void 0);
__decorate([
    typeorm_1.ManyToOne(type => commerce_entity_1.Commerce),
    typeorm_1.JoinColumn(),
    __metadata("design:type", commerce_entity_1.Commerce)
], Purchase.prototype, "commerce", void 0);
__decorate([
    typeorm_1.Column({ default: new Date() }),
    __metadata("design:type", Date)
], Purchase.prototype, "createAt", void 0);
__decorate([
    typeorm_1.OneToOne(() => shipping_entity_1.Shipping),
    typeorm_1.JoinColumn(),
    __metadata("design:type", shipping_entity_1.Shipping)
], Purchase.prototype, "shipping", void 0);
__decorate([
    typeorm_1.OneToMany(() => detalle_entity_1.Detalle, detalle => detalle.purchase),
    __metadata("design:type", Array)
], Purchase.prototype, "detalle", void 0);
Purchase = __decorate([
    typeorm_1.Entity({ name: 'PURCHASES' })
], Purchase);
exports.Purchase = Purchase;
//# sourceMappingURL=purchase.entity.js.map