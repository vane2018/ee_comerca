"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductTypesService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const product_category_entity_1 = require("./product-category.entity");
const crud_typeorm_1 = require("@nestjsx/crud-typeorm");
let ProductTypesService = class ProductTypesService extends crud_typeorm_1.TypeOrmCrudService {
    constructor(repo) {
        super(repo);
        this.getAllProductTypes = async () => {
            return await this.repo.find({ relations: ['products'] });
        };
    }
    async saveSubcategory(subcategoryDto) {
        try {
            let productoSubcategoria = this.repo.create(subcategoryDto);
            return await this.repo.save(productoSubcategoria);
        }
        catch (error) {
            console.log('error: ', error);
        }
    }
    async getcategoryProduct() {
        try {
            return await this.repo.find({ relations: ['products'] });
        }
        catch (error) {
            console.log('error: ', error);
        }
    }
    async getProductsCategorId(categoryId) {
        try {
            let product = [];
            let categorias = await this.repo.findOne(categoryId, { relations: ['products'] });
            return categorias;
        }
        catch (error) {
            console.log("ocurrio un error en categoryProduct", error);
        }
    }
    async abtenerproduct(cat) {
        var product = [];
        try {
            let prod = await this.getProductsCategorId(cat);
            if (prod.products.length > 0) {
                for (let index = 0; index < prod.products.length; index++) {
                    if (prod.products[index].deletedDate === null) {
                        product.push(prod.products[index]);
                    }
                }
                prod.products = product;
                return prod;
            }
            else {
                return prod;
            }
        }
        catch (error) {
            return error;
        }
    }
};
ProductTypesService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(product_category_entity_1.ProductTypes)),
    __metadata("design:paramtypes", [Object])
], ProductTypesService);
exports.ProductTypesService = ProductTypesService;
//# sourceMappingURL=product-category.service.js.map