export declare class CreateCategoryProductDto {
    readonly name: string;
    readonly idSubcategoryProduct: string;
}
