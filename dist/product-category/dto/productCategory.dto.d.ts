import { subcategoryProductsDto } from '../../subcategory-products/dto/subcategory-product.dto';
export declare class ProductTypeDto {
    readonly name: string;
    productsSubcategory: subcategoryProductsDto;
}
