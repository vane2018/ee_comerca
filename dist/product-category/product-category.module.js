"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductTypesModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const product_category_entity_1 = require("./product-category.entity");
const product_category_controller_1 = require("./product-category.controller");
const product_category_service_1 = require("./product-category.service");
const subcategory_products_module_1 = require("../subcategory-products/subcategory-products.module");
let ProductTypesModule = class ProductTypesModule {
};
ProductTypesModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([product_category_entity_1.ProductTypes]),
            subcategory_products_module_1.SubcategoryProductsModule],
        controllers: [product_category_controller_1.ProductTypesController],
        providers: [product_category_service_1.ProductTypesService],
        exports: [product_category_service_1.ProductTypesService],
    })
], ProductTypesModule);
exports.ProductTypesModule = ProductTypesModule;
//# sourceMappingURL=product-category.module.js.map