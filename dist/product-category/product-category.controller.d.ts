import { ProductTypesService } from './product-category.service';
import { ProductTypes } from './product-category.entity';
import { CreateCategoryProductDto } from './dto/createCategoryProduct.dto';
import { SubcategoryProductsService } from '../subcategory-products/subcategory-products.service';
export declare class ProductTypesController {
    service: ProductTypesService;
    serviceSubcategoria: SubcategoryProductsService;
    constructor(service: ProductTypesService, serviceSubcategoria: SubcategoryProductsService);
    saveSubcategory(res: any, createSubcategory: CreateCategoryProductDto): Promise<any>;
    getTypeProducts(res: any): Promise<ProductTypes>;
    getcategoryProduct(res: any): Promise<any>;
    getProductsId(res: any, param: any): Promise<any>;
}
