import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { ProductTypeDto } from './dto/productCategory.dto';
export declare class ProductTypesService extends TypeOrmCrudService<ProductTypeDto> {
    constructor(repo: any);
    saveSubcategory(subcategoryDto: ProductTypeDto): Promise<ProductTypeDto>;
    getAllProductTypes: () => Promise<ProductTypeDto[]>;
    getcategoryProduct(): Promise<ProductTypeDto[]>;
    getProductsCategorId(categoryId: string): Promise<any>;
    abtenerproduct(cat: string): Promise<any>;
}
