"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductTypes = void 0;
const typeorm_1 = require("typeorm");
const products_entity_1 = require("../products/products.entity");
const subcategory_products_entity_1 = require("../subcategory-products/subcategory-products.entity");
let ProductTypes = class ProductTypes {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], ProductTypes.prototype, "id", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500 }),
    __metadata("design:type", String)
], ProductTypes.prototype, "name", void 0);
__decorate([
    typeorm_1.OneToMany(() => products_entity_1.Product, (product) => product.productType, {
        cascade: true,
    }),
    typeorm_1.JoinColumn(),
    __metadata("design:type", Array)
], ProductTypes.prototype, "products", void 0);
__decorate([
    typeorm_1.ManyToOne(() => subcategory_products_entity_1.ProductSubcategory, (productSubcategory) => productSubcategory.productsTypes),
    typeorm_1.JoinColumn(),
    __metadata("design:type", subcategory_products_entity_1.ProductSubcategory)
], ProductTypes.prototype, "productsSubcategory", void 0);
ProductTypes = __decorate([
    typeorm_1.Entity('PRODUCT_TYPES')
], ProductTypes);
exports.ProductTypes = ProductTypes;
//# sourceMappingURL=product-category.entity.js.map