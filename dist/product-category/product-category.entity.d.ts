import { Product } from '../products/products.entity';
import { ProductSubcategory } from '../subcategory-products/subcategory-products.entity';
export declare class ProductTypes {
    id: string;
    name: string;
    products: Product[];
    productsSubcategory: ProductSubcategory;
}
