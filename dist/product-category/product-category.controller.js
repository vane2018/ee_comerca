"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductTypesController = void 0;
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const product_category_service_1 = require("./product-category.service");
const swagger_1 = require("@nestjs/swagger");
const productCategory_dto_1 = require("./dto/productCategory.dto");
const createCategoryProduct_dto_1 = require("./dto/createCategoryProduct.dto");
const subcategory_products_service_1 = require("../subcategory-products/subcategory-products.service");
let ProductTypesController = class ProductTypesController {
    constructor(service, serviceSubcategoria) {
        this.service = service;
        this.serviceSubcategoria = serviceSubcategoria;
    }
    async saveSubcategory(res, createSubcategory) {
        try {
            let category = await this.serviceSubcategoria.findOne(createSubcategory.idSubcategoryProduct);
            if (category) {
                let subcategorySave = {
                    name: createSubcategory.name,
                    productsSubcategory: category
                };
                const subctegory = await this.service.saveSubcategory(subcategorySave);
                return res.status(common_1.HttpStatus.OK).json(subctegory);
            }
            else {
                return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'gategoria no encontrada' });
            }
        }
        catch (error) {
            console.log('error al crear producto: ', error);
            return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el producto', error });
        }
    }
    async getTypeProducts(res) {
        const productsTypes = await this.service.getAllProductTypes();
        return res.status(common_1.HttpStatus.OK).json(productsTypes);
    }
    async getcategoryProduct(res) {
        const categoriaProduct = await this.service.getcategoryProduct();
        return res.status(common_1.HttpStatus.OK).json(categoriaProduct);
    }
    async getProductsId(res, param) {
        const products = await this.service.abtenerproduct(param.id);
        return res.status(common_1.HttpStatus.OK).json(products);
    }
};
__decorate([
    common_1.Post('register'),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, createCategoryProduct_dto_1.CreateCategoryProductDto]),
    __metadata("design:returntype", Promise)
], ProductTypesController.prototype, "saveSubcategory", null);
__decorate([
    common_1.Get('/listar'),
    swagger_1.ApiResponse({ status: 200, description: "OK" }),
    swagger_1.ApiResponse({ status: 400, description: "Solicitud incorrecta" }),
    swagger_1.ApiResponse({ status: 403, description: "Prohibido" }),
    swagger_1.ApiResponse({ status: 404, description: "No encontrado" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: "error server" }),
    swagger_1.ApiOperation({ summary: 'Creando producto de un  commercio' }),
    swagger_1.ApiBody({ type: productCategory_dto_1.ProductTypeDto }),
    __param(0, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ProductTypesController.prototype, "getTypeProducts", null);
__decorate([
    common_1.Get(),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    swagger_1.ApiOperation({ summary: 'obtiene todas las categorias de producto' }),
    __param(0, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ProductTypesController.prototype, "getcategoryProduct", null);
__decorate([
    common_1.Get('/:id'),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiResponse({ status: 404, description: '\'The resource was not found\'' }),
    swagger_1.ApiResponse({ status: 409, description: 'Conflict' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ProductTypesController.prototype, "getProductsId", null);
ProductTypesController = __decorate([
    crud_1.Crud({
        model: {
            type: productCategory_dto_1.ProductTypeDto,
        },
        params: {
            id: {
                field: 'id',
                type: 'uuid',
                primary: true,
            },
        },
        query: {
            join: {
                productsSubcategory: {}
            }
        }
    }),
    swagger_1.ApiTags('Product-Category'),
    common_1.Controller('product-types'),
    __metadata("design:paramtypes", [product_category_service_1.ProductTypesService,
        subcategory_products_service_1.SubcategoryProductsService])
], ProductTypesController);
exports.ProductTypesController = ProductTypesController;
//# sourceMappingURL=product-category.controller.js.map