"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cobro = void 0;
const user_entity_1 = require("../users/user.entity");
const typeorm_1 = require("typeorm");
let Cobro = class Cobro {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Cobro.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Cobro.prototype, "monto", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], Cobro.prototype, "estado", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], Cobro.prototype, "idusuario", void 0);
__decorate([
    typeorm_1.ManyToOne(() => user_entity_1.User, (user) => user.cobro),
    typeorm_1.JoinColumn(),
    __metadata("design:type", user_entity_1.User)
], Cobro.prototype, "user", void 0);
Cobro = __decorate([
    typeorm_1.Entity('COBRO')
], Cobro);
exports.Cobro = Cobro;
//# sourceMappingURL=cobrarcomision.entity.js.map