import { UsersService } from '../users/user.service';
import { Cobro } from './cobrarcomision.entity';
import { CobrarcomisionService } from './cobrarcomision.service';
import { cobroComision } from './dto/consulta.dto';
import { estadoDto } from './dto/estado.dto';
export declare class CobrarcomisionController {
    cobrarService: CobrarcomisionService;
    userServic: UsersService;
    constructor(cobrarService: CobrarcomisionService, userServic: UsersService);
    crearCobro(res: any, datos: cobroComision): Promise<any>;
    saveCobbro(res: any, createCommissionDto: cobroComision): Promise<any>;
    getProductsOffert(res: any): Promise<Cobro>;
    getByUser(res: any, param: any): Promise<any>;
    getProductsId(res: any, param: any): Promise<any>;
    putProducts(res: any, param: any, estado: estadoDto): Promise<any>;
    deleteCommisions(res: any, param: any): Promise<any>;
}
