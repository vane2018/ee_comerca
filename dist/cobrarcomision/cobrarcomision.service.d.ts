import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { UsersService } from '../users/user.service';
import { Cobro } from './cobrarcomision.entity';
import { cobroComision } from './dto/consulta.dto';
import { estadoDto } from './dto/estado.dto';
import { prueba } from './dto/prueba.dto';
export declare class CobrarcomisionService extends TypeOrmCrudService<Cobro> {
    serviceUser: UsersService;
    constructor(repo: any, serviceUser: UsersService);
    guardadCobro(datos: cobroComision): Promise<Cobro>;
    saveCobro(commisionDto: prueba): Promise<prueba>;
    getcobro(): Promise<Cobro[]>;
    getCobroId(id: string): Promise<Cobro>;
    actualizar(id: string, estado: estadoDto): Promise<Cobro>;
    getCobroByUserId(userId: string): Promise<any>;
    getCobroByIds(ids: string): Promise<any>;
    deleteCobro(id: string): Promise<void>;
}
