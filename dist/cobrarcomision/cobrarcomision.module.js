"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CobrarcomisionModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_module_1 = require("../users/user.module");
const cobrarcomision_controller_1 = require("./cobrarcomision.controller");
const cobrarcomision_entity_1 = require("./cobrarcomision.entity");
const cobrarcomision_service_1 = require("./cobrarcomision.service");
let CobrarcomisionModule = class CobrarcomisionModule {
};
CobrarcomisionModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([cobrarcomision_entity_1.Cobro]), user_module_1.UsersModule
        ],
        controllers: [cobrarcomision_controller_1.CobrarcomisionController],
        providers: [cobrarcomision_service_1.CobrarcomisionService]
    })
], CobrarcomisionModule);
exports.CobrarcomisionModule = CobrarcomisionModule;
//# sourceMappingURL=cobrarcomision.module.js.map