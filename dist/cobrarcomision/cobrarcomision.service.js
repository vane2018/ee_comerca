"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CobrarcomisionService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const crud_typeorm_1 = require("@nestjsx/crud-typeorm");
const user_service_1 = require("../users/user.service");
const cobrarcomision_entity_1 = require("./cobrarcomision.entity");
let CobrarcomisionService = class CobrarcomisionService extends crud_typeorm_1.TypeOrmCrudService {
    constructor(repo, serviceUser) {
        super(repo);
        this.serviceUser = serviceUser;
    }
    async guardadCobro(datos) {
        let cobro = await this.repo.create(datos);
        cobro.idusuario = datos.idUser;
        return await this.repo.save(cobro);
    }
    async saveCobro(commisionDto) {
        try {
            let comision = this.repo.create(commisionDto);
            return await this.repo.save(comision);
        }
        catch (error) {
            console.log('error', error);
        }
    }
    async getcobro() {
        try {
            let usuarios = await this.repo.find({ relations: ['user'] });
            return usuarios;
        }
        catch (error) {
            console.log('error: ', error);
        }
    }
    async getCobroId(id) {
        return await this.repo.findOne(id);
    }
    async actualizar(id, estado) {
        let cobro = await this.repo.findOne(id);
        cobro.estado = estado.estado;
        cobro.id = id;
        return await this.repo.save(cobro);
    }
    async getCobroByUserId(userId) {
        let comisions = await this.repo.find({ where: { user: userId } });
        return comisions;
    }
    async getCobroByIds(ids) {
        let comisions = await this.repo.findOne(ids);
        return comisions;
    }
    async deleteCobro(id) {
        await this.repo.delete(id);
    }
};
CobrarcomisionService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(cobrarcomision_entity_1.Cobro)),
    __metadata("design:paramtypes", [Object, user_service_1.UsersService])
], CobrarcomisionService);
exports.CobrarcomisionService = CobrarcomisionService;
//# sourceMappingURL=cobrarcomision.service.js.map