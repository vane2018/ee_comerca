import { User } from "../users/user.entity";
export declare class Cobro {
    id: string;
    monto: number;
    estado: string;
    idusuario: string;
    user: User;
}
