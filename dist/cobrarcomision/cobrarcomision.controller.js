"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CobrarcomisionController = void 0;
const common_1 = require("@nestjs/common");
const common_2 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const swagger_1 = require("@nestjs/swagger");
const crud_1 = require("@nestjsx/crud");
const permission_guards_1 = require("../common/guards/permission.guards");
const permission_decorator_1 = require("../common/permission/permission.decorator");
const user_service_1 = require("../users/user.service");
const cobrarcomision_entity_1 = require("./cobrarcomision.entity");
const cobrarcomision_service_1 = require("./cobrarcomision.service");
const consulta_dto_1 = require("./dto/consulta.dto");
const estado_dto_1 = require("./dto/estado.dto");
const prueba_dto_1 = require("./dto/prueba.dto");
let CobrarcomisionController = class CobrarcomisionController {
    constructor(cobrarService, userServic) {
        this.cobrarService = cobrarService;
        this.userServic = userServic;
    }
    async crearCobro(res, datos) {
        let commerces = await this.cobrarService.guardadCobro(datos);
        return res.status(common_2.HttpStatus.OK).json(commerces);
    }
    async saveCobbro(res, createCommissionDto) {
        try {
            let customerResp = await this.userServic.findOne(createCommissionDto.idUser);
            if (customerResp) {
                let commissionSave = {
                    monto: createCommissionDto.monto,
                    estado: createCommissionDto.estado,
                    user: customerResp
                };
                const comision = await this.cobrarService.saveCobro(commissionSave);
                return res.status(common_2.HttpStatus.OK).json(comision);
            }
            else {
                return res.status(common_2.HttpStatus.BAD_REQUEST).json({ message: 'customer no encontrados' });
            }
        }
        catch (error) {
            console.log('error al crear producto: ', error);
            return res.status(common_2.HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar comision', error });
        }
    }
    async getProductsOffert(res) {
        const products = await this.cobrarService.getcobro();
        return res.status(common_2.HttpStatus.OK).json(products);
    }
    async getByUser(res, param) {
        const commissions = await this.cobrarService.getCobroByUserId(param.idUser);
        return res.status(common_2.HttpStatus.OK).json(commissions);
    }
    async getProductsId(res, param) {
        const products = await this.cobrarService.getCobroId(param.id);
        return res.status(common_2.HttpStatus.OK).json(products);
    }
    async putProducts(res, param, estado) {
        try {
            let cobro = await this.cobrarService.findOne(param.id);
            if (cobro) {
                cobro.estado = estado.estado;
                cobro.id = param.id;
                const producto = await this.cobrarService.actualizar(param.id, estado);
                return res.status(common_2.HttpStatus.OK).json(producto);
            }
            else {
                return res.status(common_2.HttpStatus.BAD_REQUEST).json({ message: ' producto no encontrados' });
            }
        }
        catch (error) {
            console.log('error al modifcar producto: ', error);
            return res.status(common_2.HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el producto', error });
        }
    }
    async deleteCommisions(res, param) {
        await this.cobrarService.deleteCobro(param.id);
        return res.status(common_2.HttpStatus.OK).json({ message: 'commision deleted' });
    }
};
__decorate([
    common_2.Post('/cobro/comision'),
    swagger_1.ApiOperation({ summary: 'Registra un cobro de comision' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_2.Response()), __param(1, common_2.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, consulta_dto_1.cobroComision]),
    __metadata("design:returntype", Promise)
], CobrarcomisionController.prototype, "crearCobro", null);
__decorate([
    common_2.Post('/guardarCobro'),
    swagger_1.ApiOperation({ summary: 'Registra una comision' }),
    swagger_1.ApiCreatedResponse({ description: 'The product has been successfully created.', type: prueba_dto_1.prueba }),
    swagger_1.ApiBody({ type: consulta_dto_1.cobroComision }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_2.Response()), __param(1, common_2.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, consulta_dto_1.cobroComision]),
    __metadata("design:returntype", Promise)
], CobrarcomisionController.prototype, "saveCobbro", null);
__decorate([
    common_2.Get('/obtener/comision'),
    common_1.UseGuards(passport_1.AuthGuard('jwt'), permission_guards_1.PermissionGuard),
    permission_decorator_1.Permision('admin', 'empleado'),
    swagger_1.ApiOperation({ summary: 'Obtiene todas las comision' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiResponse({ status: 404, description: '\'The resource was not found\'' }),
    swagger_1.ApiResponse({ status: 409, description: 'Conflict' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_2.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CobrarcomisionController.prototype, "getProductsOffert", null);
__decorate([
    common_2.Get(':idUser'),
    swagger_1.ApiParam({ type: 'string', name: 'idUser' }),
    __param(0, common_2.Response()), __param(1, common_2.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CobrarcomisionController.prototype, "getByUser", null);
__decorate([
    common_2.Get('/:id'),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiResponse({ status: 404, description: '\'The resource was not found\'' }),
    swagger_1.ApiResponse({ status: 409, description: 'Conflict' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_2.Response()), __param(1, common_2.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CobrarcomisionController.prototype, "getProductsId", null);
__decorate([
    common_2.Put('/edit/:id'),
    __param(0, common_2.Response()), __param(1, common_2.Param()), __param(2, common_2.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, estado_dto_1.estadoDto]),
    __metadata("design:returntype", Promise)
], CobrarcomisionController.prototype, "putProducts", null);
__decorate([
    common_1.Delete('/:id'),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_2.Response()), __param(1, common_2.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CobrarcomisionController.prototype, "deleteCommisions", null);
CobrarcomisionController = __decorate([
    crud_1.Crud({
        model: { type: cobrarcomision_entity_1.Cobro },
        params: {
            id: {
                field: 'id',
                type: 'uuid',
                primary: true,
            },
        },
        query: {
            join: {
                user: {
                    allow: [],
                },
            },
        },
    }),
    swagger_1.ApiTags('CobrarComision'),
    common_2.Controller('cobrarcomision'),
    __metadata("design:paramtypes", [cobrarcomision_service_1.CobrarcomisionService,
        user_service_1.UsersService])
], CobrarcomisionController);
exports.CobrarcomisionController = CobrarcomisionController;
//# sourceMappingURL=cobrarcomision.controller.js.map