"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MercadoPagoController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const mercado_pago_service_1 = require("./mercado-pago.service");
const redirect_dto_1 = require("./dto/redirect.dto");
const commerce_service_1 = require("../commerce/commerce.service");
const credenciales_dto_1 = require("./dto/credenciales.dto");
const consultarCommerce_dto_1 = require("./dto/consultarCommerce.dto");
const env_config_1 = require("../common/env.config");
let MercadoPagoController = class MercadoPagoController {
    constructor(serviceMercado, commerceService) {
        this.serviceMercado = serviceMercado;
        this.commerceService = commerceService;
    }
    async obtenerCode(res, escorp) {
        try {
            await this.serviceMercado.obtieneCode(escorp.code);
        }
        catch (error) {
            return error;
        }
    }
    async crearComercios(res, commerce) {
        let comercio;
        try {
            const credentialResponse = await this.serviceMercado.createCredencial(commerce.code);
            const mercadoPago = {
                accessToken: credentialResponse.access_token,
                tokenType: credentialResponse.token_type,
                expireIn: credentialResponse.expires_in,
                scope: credentialResponse.scope,
                refreshToken: credentialResponse.refresh_token,
                comercioId: commerce.comercioId,
            };
            comercio = await this.serviceMercado.compruebaExistencia(commerce.comercioId);
            if (comercio) {
                await this.serviceMercado.updateMercado(commerce.comercioId, mercadoPago);
                return res.status(common_1.HttpStatus.OK).json({ mensaje: 'se actualizo con exito', statusCode: 200 });
            }
            else {
                await this.serviceMercado.createMercadoPago(mercadoPago);
                return res.status(common_1.HttpStatus.OK).json({ mensaje: 'se creo con exito', statusCode: 200 });
            }
        }
        catch (error) {
            console.log(error);
            if (error.response && error.response.data) {
                console.log(error.response.data);
            }
            return res.status(common_1.HttpStatus.NOT_FOUND).json({ error: ' no se crearon las credenciales', statusCode: 400 });
        }
    }
    async consultarComercios(res, consulta) {
        try {
            const mercadoPago = await this.serviceMercado.getMercadoPagoByComercioId(consulta.comerceId);
            if (mercadoPago) {
                let nuevo = mercadoPago.accessToken.replace(/['"]+/g, '');
                return res.status(200).json(`${nuevo}`);
            }
            else {
                return res.status(200).json({ hasPagoOnline: false });
            }
        }
        catch (error) {
            return res.status(500).json({ error: 'internal server error', statusCode: 500 });
        }
    }
    async obtenerMensajes(res) {
        try {
            res.redirect(env_config_1.ENV.APP_FRONT_URL + '/mercado-pago/loading-sppiner/?success');
        }
        catch (error) {
            return error;
        }
    }
    async obtenerMensajesFailed(res) {
        try {
            res.redirect(env_config_1.ENV.APP_FRONT_URL + '/mercado-pago/loading-sppiner/?failure');
        }
        catch (error) {
            return error;
        }
    }
};
__decorate([
    common_1.Post('/apis/v1/mercado-pago/get-code'),
    swagger_1.ApiOperation({ summary: 'crea comercio' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, credenciales_dto_1.credenciales]),
    __metadata("design:returntype", Promise)
], MercadoPagoController.prototype, "obtenerCode", null);
__decorate([
    common_1.Post('/apis/v1/mercado-pago/redirect'),
    swagger_1.ApiOperation({ summary: 'crea comercio' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    swagger_1.ApiBody({ type: redirect_dto_1.RedirectDto }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, redirect_dto_1.RedirectDto]),
    __metadata("design:returntype", Promise)
], MercadoPagoController.prototype, "crearComercios", null);
__decorate([
    common_1.Post('/apis/v1/mercado-pago/exist/comercioId/:comercioId'),
    swagger_1.ApiOperation({ summary: 'crea comercio' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    swagger_1.ApiBody({ type: consultarCommerce_dto_1.consultarDto }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, consultarCommerce_dto_1.consultarDto]),
    __metadata("design:returntype", Promise)
], MercadoPagoController.prototype, "consultarComercios", null);
__decorate([
    common_1.Get('/apis/v1/mercado-pago/mensaje-exito'),
    swagger_1.ApiOperation({ summary: 'crea comercio' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], MercadoPagoController.prototype, "obtenerMensajes", null);
__decorate([
    common_1.Get('/apis/v1/mercado-pago/mensaje-failed'),
    swagger_1.ApiOperation({ summary: 'crea comercio' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], MercadoPagoController.prototype, "obtenerMensajesFailed", null);
MercadoPagoController = __decorate([
    swagger_1.ApiTags('Mercado-Pago'),
    common_1.Controller('mercado-pago'),
    __metadata("design:paramtypes", [mercado_pago_service_1.MercadoPagoService,
        commerce_service_1.CommerceService])
], MercadoPagoController);
exports.MercadoPagoController = MercadoPagoController;
//# sourceMappingURL=mercado-pago.controller.js.map