"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TypeBarcode = exports.TypeOrder = exports.TypeIdentification = exports.TypePayer = exports.EntityType = void 0;
var EntityType;
(function (EntityType) {
    EntityType["INDIVIDUAL"] = "individual";
    EntityType["ASSOCIATION"] = "association";
})(EntityType = exports.EntityType || (exports.EntityType = {}));
var TypePayer;
(function (TypePayer) {
    TypePayer["CUSTOMER"] = "customer";
    TypePayer["REGISTERED"] = "registered";
    TypePayer["GUEST"] = "guest";
})(TypePayer = exports.TypePayer || (exports.TypePayer = {}));
var TypeIdentification;
(function (TypeIdentification) {
    TypeIdentification["DNI"] = "DNI";
})(TypeIdentification = exports.TypeIdentification || (exports.TypeIdentification = {}));
var TypeOrder;
(function (TypeOrder) {
    TypeOrder["MERCADO_LIBRE"] = "mercadolibre";
    TypeOrder["MERCADO_PAGO"] = "mercadopago";
})(TypeOrder = exports.TypeOrder || (exports.TypeOrder = {}));
var TypeBarcode;
(function (TypeBarcode) {
    TypeBarcode["UCC_EAN_128"] = "UCC/EAN 128";
    TypeBarcode["CODE_128_C"] = "Code128C";
    TypeBarcode["CODE_39"] = "Code39";
})(TypeBarcode = exports.TypeBarcode || (exports.TypeBarcode = {}));
//# sourceMappingURL=payerInterface.js.map