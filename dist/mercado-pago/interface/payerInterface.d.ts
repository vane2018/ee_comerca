import { Address, Item } from "../dto/preference.dto";
export declare enum EntityType {
    INDIVIDUAL = "individual",
    ASSOCIATION = "association"
}
export declare enum TypePayer {
    CUSTOMER = "customer",
    REGISTERED = "registered",
    GUEST = "guest"
}
export interface CredentialResponse {
    access_token: string;
    public_key: string;
    refresh_token: string;
    live_mode: boolean;
    user_id: string;
    token_type: string;
    expires_in: number;
    scope: string;
}
export declare enum TypeIdentification {
    DNI = "DNI"
}
export declare enum TypeOrder {
    MERCADO_LIBRE = "mercadolibre",
    MERCADO_PAGO = "mercadopago"
}
export declare enum TypeBarcode {
    UCC_EAN_128 = "UCC/EAN 128",
    CODE_128_C = "Code128C",
    CODE_39 = "Code39"
}
interface Phone {
    area_code: string;
    number: string;
    extension: string;
}
interface Identification {
    type: TypeIdentification;
    number: string;
}
export interface Payer {
    entity_type?: EntityType;
    type?: TypePayer;
    id?: string;
    email: string;
    identification?: Identification;
    phone?: Phone;
    first_name?: string;
    last_name?: string;
    address?: Address;
    registration_date?: Date;
}
interface ReceiverAddress {
    zip_code: string;
    state_name: string;
    city_name: string;
    street_name: string;
    street_number: number;
    floor: string;
    apartment: string;
}
export interface Shipments {
    receiver_address: ReceiverAddress;
}
export interface Barcode {
}
export interface AdditionalInfo {
    ip_address: string;
    items: Item[];
    payer: Payer;
    shipments: Shipments;
    barcode: Barcode;
}
export interface CredentialResponse {
    access_token: string;
    public_key: string;
    refresh_token: string;
    live_mode: boolean;
    user_id: string;
    token_type: string;
    expires_in: number;
    scope: string;
}
export interface PhoneDto {
    area_code: string;
    number: string;
}
export interface IdentificationDto {
    type: string;
    number: string;
}
export interface AddressDto {
    id?: string;
    zip_code: string;
    street_name: string;
    street_number?: string;
}
export interface PaymentMethodDto {
    id: string;
    name: string;
    payment_type_id: string;
    thumbnail: string;
    secure_thumbnail: string;
}
export interface CreateCustomerDto {
    email: string;
    first_name: string;
    last_name: string;
    phone: PhoneDto;
    identification: IdentificationDto;
    address: AddressDto;
    description: string;
}
export interface CustomerDto {
    id: string;
    email: string;
    first_name: string;
    last_name: string;
    phone: PhoneDto;
    identification: IdentificationDto;
    address: AddressDto;
    date_registered: string;
    description: string;
    date_created: string;
    date_last_updated: string;
    metadata: any;
    default_card: string;
    default_address: string;
    cards: CardDto[];
    addresses: AddressDto[];
    live_mode: boolean;
}
export interface CreateCardDto {
    token: string;
    customer_id: string;
}
export interface CardDto {
    id: string;
    expiration_month: number;
    expiration_year: number;
    first_six_digits: string;
    last_four_digits: string;
    payment_method: PaymentMethodDto;
    security_code: {
        length: number;
        card_location: string;
    };
    issuer: {
        id: number;
        name: string;
    };
    cardholder: {
        name: string;
        identification: {
            number: string;
            type: string;
        };
    };
    date_created: string;
    date_last_updated: string;
    customer_id: string;
    user_id: string;
    live_mode: boolean;
}
export interface ResponseCustomersDto {
    paging: {
        limit: number;
        offset: number;
        total: number;
    };
    results: CustomerDto[];
}
export interface CredentialResponse {
    access_token: string;
    public_key: string;
    refresh_token: string;
    live_mode: boolean;
    user_id: string;
    token_type: string;
    expires_in: number;
    scope: string;
}
export interface PhoneDto {
    area_code: string;
    number: string;
}
export interface IdentificationDto {
    type: string;
    number: string;
}
export interface AddressDto {
    id?: string;
    zip_code: string;
    street_name: string;
    street_number?: string;
}
export interface PaymentMethodDto {
    id: string;
    name: string;
    payment_type_id: string;
    thumbnail: string;
    secure_thumbnail: string;
}
export interface CreateCustomerDto {
    email: string;
    first_name: string;
    last_name: string;
    phone: PhoneDto;
    identification: IdentificationDto;
    address: AddressDto;
    description: string;
}
export interface CustomerDto {
    id: string;
    email: string;
    first_name: string;
    last_name: string;
    phone: PhoneDto;
    identification: IdentificationDto;
    address: AddressDto;
    date_registered: string;
    description: string;
    date_created: string;
    date_last_updated: string;
    metadata: any;
    default_card: string;
    default_address: string;
    cards: CardDto[];
    addresses: AddressDto[];
    live_mode: boolean;
}
export interface CreateCardDto {
    token: string;
    customer_id: string;
}
export interface CardDto {
    id: string;
    expiration_month: number;
    expiration_year: number;
    first_six_digits: string;
    last_four_digits: string;
    payment_method: PaymentMethodDto;
    security_code: {
        length: number;
        card_location: string;
    };
    issuer: {
        id: number;
        name: string;
    };
    cardholder: {
        name: string;
        identification: {
            number: string;
            type: string;
        };
    };
    date_created: string;
    date_last_updated: string;
    customer_id: string;
    user_id: string;
    live_mode: boolean;
}
export interface ResponseCustomersDto {
    paging: {
        limit: number;
        offset: number;
        total: number;
    };
    results: CustomerDto[];
}
export {};
