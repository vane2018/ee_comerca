"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MercadoPagoModule = void 0;
const common_1 = require("@nestjs/common");
const mercado_pago_service_1 = require("./mercado-pago.service");
const mercado_pago_controller_1 = require("./mercado-pago.controller");
const mercado_pago_entity_1 = require("./mercado-pago.entity");
const typeorm_1 = require("@nestjs/typeorm");
const commerce_module_1 = require("../commerce/commerce.module");
let MercadoPagoModule = class MercadoPagoModule {
};
MercadoPagoModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([mercado_pago_entity_1.MercadoPago]), commerce_module_1.CommerceModule],
        providers: [mercado_pago_service_1.MercadoPagoService],
        controllers: [mercado_pago_controller_1.MercadoPagoController]
    })
], MercadoPagoModule);
exports.MercadoPagoModule = MercadoPagoModule;
//# sourceMappingURL=mercado-pago.module.js.map