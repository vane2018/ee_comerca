"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MercadoPago = exports.GrantType = void 0;
const commerce_entity_1 = require("../commerce/commerce.entity");
const typeorm_1 = require("typeorm");
var GrantType;
(function (GrantType) {
    GrantType["AUTHORIZATION_CODE"] = "authorization_code";
    GrantType["REFRESH_TOKEN"] = "refresh_token";
})(GrantType = exports.GrantType || (exports.GrantType = {}));
let MercadoPago = class MercadoPago {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], MercadoPago.prototype, "id", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], MercadoPago.prototype, "accessToken", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], MercadoPago.prototype, "tokenType", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", Number)
], MercadoPago.prototype, "expireIn", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], MercadoPago.prototype, "scope", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], MercadoPago.prototype, "refreshToken", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], MercadoPago.prototype, "comercioId", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", Date)
], MercadoPago.prototype, "deletedAt", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", Date)
], MercadoPago.prototype, "createdAt", void 0);
__decorate([
    typeorm_1.OneToOne(() => commerce_entity_1.Commerce, (commerce) => commerce.mercado),
    __metadata("design:type", commerce_entity_1.Commerce)
], MercadoPago.prototype, "commerce", void 0);
MercadoPago = __decorate([
    typeorm_1.Entity('MERCADOPAGO')
], MercadoPago);
exports.MercadoPago = MercadoPago;
//# sourceMappingURL=mercado-pago.entity.js.map