import { MercadoPago } from './mercado-pago.entity';
import { Repository } from 'typeorm';
import { PaymentDto } from './dto/paymentDto.dto';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Preference } from './dto/preference.dto';
import { CredentialResponse } from './interface/payerInterface';
import { MercadoPagoDto } from './dto/mercado-pago.dto';
import { mercadoPagoQueryDto } from './dto/mercado-pago-query.dto';
import { mercadoDto } from './dto/mercado-paggo.dto';
export declare class MercadoPagoService extends TypeOrmCrudService<MercadoPago> {
    constructor(repo: Repository<MercadoPago>);
    paymentCreateWithToken(payment: PaymentDto, accessToken: string): Promise<any>;
    obtieneCode(micode: string): Promise<any>;
    paymentCreate(payment: PaymentDto): Promise<any>;
    paymentSave(payment: PaymentDto): Promise<any>;
    createPreference(preference: Preference): Promise<any>;
    createPreferenceOfComercio(preference: Preference, accessToken: string): Promise<any>;
    createCredencial(code: string): Promise<CredentialResponse>;
    realizarPago(query: mercadoPagoQueryDto, credenciales: mercadoDto): Promise<any>;
    refreshCredencial(userRefresToken: string): Promise<CredentialResponse>;
    createMercadoPago(mercadoPago: MercadoPagoDto): Promise<MercadoPago>;
    compruebaExistencia(id: string): Promise<MercadoPago>;
    updateMercado(ids: string, mercado: MercadoPagoDto): Promise<any>;
    getMercadoPagoByComercioId(comercioId: string): Promise<MercadoPago>;
}
