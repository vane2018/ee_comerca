"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.logger = void 0;
const fs = require("fs-extra");
const winston = require('winston');
const transports = [];
if (process.env.NODE_ENV === 'production') {
    const pm2ErrorFile = !!process.env.NODE_APP_INSTANCE ? `/home/ubuntu/.pm2/logs/vitrinia-server-error-${process.env.NODE_APP_INSTANCE}.log` : '/home/ubuntu/.pm2/logs/vitrinia-server-error.log';
    fs.ensureFileSync(pm2ErrorFile);
    transports.push(new (winston.transports.File)({ filename: pm2ErrorFile, level: 'error' }));
}
else {
    transports.push(new (winston.transports.Console)({
        level: 'debug',
        colorize: true,
        timestamp: true,
        prettyPrint: true
    }));
}
exports.logger = new (winston.Logger)({
    colors: {
        trace: 'white',
        debug: 'green',
        info: 'green',
        warn: 'yellow',
        error: 'red',
        fatal: 'red'
    },
    levels: {
        trace: 5,
        debug: 4,
        info: 3,
        warn: 2,
        error: 1,
        fatal: 0
    },
    transports: transports
});
//# sourceMappingURL=logger.js.map