import { MercadoPagoService } from './mercado-pago.service';
import { RedirectDto } from './dto/redirect.dto';
import { CommerceService } from '../commerce/commerce.service';
import { credenciales } from './dto/credenciales.dto';
import { consultarDto } from './dto/consultarCommerce.dto';
export declare class MercadoPagoController {
    serviceMercado: MercadoPagoService;
    commerceService: CommerceService;
    constructor(serviceMercado: MercadoPagoService, commerceService: CommerceService);
    obtenerCode(res: any, escorp: credenciales): Promise<any>;
    crearComercios(res: any, commerce: RedirectDto): Promise<any>;
    consultarComercios(res: any, consulta: consultarDto): Promise<any>;
    obtenerMensajes(res: any): Promise<any>;
    obtenerMensajesFailed(res: any): Promise<any>;
}
