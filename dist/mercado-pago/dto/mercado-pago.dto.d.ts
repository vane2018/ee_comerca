export declare class MercadoPagoDto {
    id: string;
    accessToken: string;
    tokenType: string;
    expireIn: number;
    scope: string;
    refreshToken: string;
    comercioId?: string;
}
