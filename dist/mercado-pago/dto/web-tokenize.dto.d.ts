export declare class tokenizeDto {
    token: string;
    issuer_id: string;
    installments: number;
    payment_method_id: string;
    precio: number;
    email: string;
}
