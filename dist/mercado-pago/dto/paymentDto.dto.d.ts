import { Barcode, Payer, Shipments, TypeOrder } from "../interface/payerInterface";
import { Item } from "./preference.dto";
interface AdditionalInfo {
    ip_address: string;
    items: Item[];
    payer: Payer;
    shipments: Shipments;
    barcode: Barcode;
}
interface Order {
    type: TypeOrder;
    id: number;
}
export declare class PaymentDto {
    payer?: Payer;
    binary_mode?: boolean;
    order?: Order;
    external_reference?: string;
    description: string;
    metadata?: any;
    transaction_amount: number;
    coupon_amount?: number;
    campaign_id?: number;
    coupon_code?: string;
    differential_pricing_id?: number;
    application_fee?: number;
    capture?: boolean;
    payment_method_id: string;
    issuer_id?: string;
    token?: string;
    statement_descriptor?: string;
    installments: number;
    notification_url?: string;
    callback_url?: string;
    additional_info?: AdditionalInfo;
}
export {};
