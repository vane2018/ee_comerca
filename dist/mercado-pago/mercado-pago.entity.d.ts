import { Commerce } from "../commerce/commerce.entity";
export declare enum GrantType {
    AUTHORIZATION_CODE = "authorization_code",
    REFRESH_TOKEN = "refresh_token"
}
export declare class MercadoPago {
    id: string;
    accessToken: string;
    tokenType: string;
    expireIn: number;
    scope: string;
    refreshToken: string;
    comercioId: string;
    deletedAt: Date;
    createdAt: Date;
    commerce: Commerce;
}
