"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MercadoPagoService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const mercado_pago_entity_1 = require("./mercado-pago.entity");
const typeorm_2 = require("typeorm");
const axios_1 = require("axios");
const crud_typeorm_1 = require("@nestjsx/crud-typeorm");
const mercadopago = require("mercadopago");
const env_config_1 = require("../common/env.config");
let MercadoPagoService = class MercadoPagoService extends crud_typeorm_1.TypeOrmCrudService {
    constructor(repo) {
        super(repo);
    }
    async paymentCreateWithToken(payment, accessToken) {
        const res = await axios_1.default({
            method: 'post',
            url: env_config_1.ENV.MERCADO_PAGO_API
                + '/v1/payments?access_token=' + accessToken,
            data: payment,
            headers: {
                'accept': 'application/json',
                'content-type': 'application/json'
            }
        });
        console.log("MERCADO PAGO 1", res);
        return res.data;
    }
    async obtieneCode(micode) {
        const data = {
            client_secret: env_config_1.ENV.MERCADO_PAGO_ACCESS_TOKEN,
            grant_type: mercado_pago_entity_1.GrantType.AUTHORIZATION_CODE,
            micode,
            redirect_uri: env_config_1.ENV.MERCADO_PAGO_REDIRECT_URI
        };
        console.log("DATOS DE MERCADO PAGO 2 obtieneCode", data);
        const res = await axios_1.default({
            method: 'post',
            url: 'https://api.mercadopago.com/oauth/token',
            data,
            headers: {
                'accept': 'application/json',
                'content-type': 'application/x-www-form-urlencoded'
            },
        });
        return res.data;
    }
    async paymentCreate(payment) {
        const res = await axios_1.default({
            method: 'post',
            url: MERCADO_PAGO_API
                + '/v1/payments?access_token=' +
                env_config_1.ENV.MERCADO_PAGO_ACCESS_TOKEN,
            data: payment,
            headers: {
                'accept': 'application/json',
                'content-type': 'application/x-www-form-urlencoded'
            }
        });
        console.log("MECADO PAGO paymentCreate", res.data);
        return res.data;
    }
    async paymentSave(payment) {
        return await mercadopago.payment.save(payment);
    }
    async createPreference(preference) {
        return await mercadopago.preferences.create(preference);
    }
    async createPreferenceOfComercio(preference, accessToken) {
        const res = await axios_1.default({
            method: 'post',
            url: MERCADO_PAGO_API + 'checkout/preferences?access_token=' + accessToken,
            data: preference,
            headers: {
                'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        });
        return res.data;
    }
    async createCredencial(code) {
        const data = {
            client_secret: env_config_1.ENV.MERCADO_PAGO_CLIENT_SECRET,
            grant_type: mercado_pago_entity_1.GrantType.AUTHORIZATION_CODE,
            code,
            redirect_uri: env_config_1.ENV.MERCADO_PAGO_REDIRECT_URI
        };
        console.log("CREA CREDENCIALES MERCADO PAGO", data);
        try {
            const res = await axios_1.default({
                method: 'post',
                url: MERCADO_PAGO_API + 'oauth/token',
                data,
                headers: {
                    'accept': 'application/json',
                    'content-type': 'application/x-www-form-urlencoded'
                }
            });
            console.log("CREA CREDENCIALES", res);
            return res.data;
        }
        catch (error) {
            console.log("error", error);
        }
    }
    async realizarPago(query, credenciales) {
        try {
            const token = credenciales.token;
            const payment_method_id = credenciales.payment_method_id;
            const installments = parseInt(credenciales.installments);
            const issuer_id = credenciales.issuer_id;
            const transaction_amoun = Number(query.transaction_amount);
            const shipping_amount = Number(query.shipping_amount);
            const product_price = transaction_amoun - shipping_amount;
            const comprartir_commission = (product_price * 0.07);
            const application_fee = shipping_amount + comprartir_commission;
            const email = query.email;
            const commerceId = query.commerceId;
            var sellerAccessToken = 'TEST-4738866408022481-121515-5b0f30a924dc5654d932d5f46c249a95-688246148';
            var payment_data = {
                transaction_amount: transaction_amoun,
                token: token,
                issuer_id: issuer_id,
                installments: installments,
                payment_method_id: payment_method_id,
                binary_mode: true,
                application_fee: application_fee,
                payer: {
                    email: email
                }
            };
            var commerce_id = {
                comerceId: commerceId
            };
            const mivar = await axios_1.default({
                method: 'post',
                url: env_config_1.ENV.SERVER_URL
                    + '/api/v1/mercado-pago/apis/v1/mercado-pago/exist/comercioId/{comercioId}',
                data: commerce_id,
                headers: {
                    'accept': 'application/json',
                    'content-type': 'application/json',
                }
            });
            sellerAccessToken = mivar.data.replace(/['"]+/g, '');
            const miletra = await axios_1.default({
                method: 'post',
                url: env_config_1.ENV.MERCADO_PAGO_API + 'v1/payments',
                data: payment_data,
                headers: {
                    'accept': 'application/json',
                    'content-type': 'application/json',
                    'Authorization': `Bearer ${sellerAccessToken}`
                }
            });
            return miletra.data.status;
        }
        catch (error) {
            console.log("error", error);
        }
    }
    async refreshCredencial(userRefresToken) {
        const data = {
            client_secret: env_config_1.ENV.MERCADO_PAGO_ACCESS_TOKEN,
            grant_type: mercado_pago_entity_1.GrantType.REFRESH_TOKEN,
            refresh_token: userRefresToken,
        };
        const res = await axios_1.default({
            method: 'post',
            url: MERCADO_PAGO_API + 'oauth/token',
            data,
            headers: {
                'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        });
        return res.data;
    }
    async createMercadoPago(mercadoPago) {
        return await this.repo.save(mercadoPago);
    }
    async compruebaExistencia(id) {
        return await this.repo.findOne({ where: { comercioId: id } });
    }
    async updateMercado(ids, mercado) {
        const miComerce = await this.compruebaExistencia(ids);
        if (miComerce) {
            let comerceMod = {
                id: miComerce.id,
                accessToken: mercado.accessToken,
                tokenType: mercado.tokenType,
                expireIn: mercado.expireIn,
                scope: mercado.scope,
                refreshToken: mercado.refreshToken,
                comercioId: mercado.comercioId
            };
            return await this.repo.save(comerceMod);
        }
    }
    async getMercadoPagoByComercioId(comercioId) {
        return await this.repo.findOne({ where: { comercioId } });
    }
};
MercadoPagoService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(mercado_pago_entity_1.MercadoPago)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], MercadoPagoService);
exports.MercadoPagoService = MercadoPagoService;
//# sourceMappingURL=mercado-pago.service.js.map