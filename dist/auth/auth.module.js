"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthModule = void 0;
const common_1 = require("@nestjs/common");
const auth_service_1 = require("./auth.service");
const auth_controller_1 = require("./auth.controller");
const user_entity_1 = require("../users/user.entity");
const typeorm_1 = require("@nestjs/typeorm");
const local_strategy_1 = require("../auth/passport/local.strategy");
const user_module_1 = require("../users/user.module");
const passport_1 = require("@nestjs/passport");
const jwt_1 = require("@nestjs/jwt");
const jwt_strategy_1 = require("../auth/passport/jwt.strategy");
const google_strategy_1 = require("./passport/google.strategy");
const rols_module_1 = require("../rols/rols.module");
const code_module_1 = require("../code/code.module");
const mailer_module_1 = require("../mailer/mailer.module");
const mailer_1 = require("@nest-modules/mailer");
const env_config_1 = require("../common/env.config");
const mail = env_config_1.ENV.USER_NO_REPLY;
const password = env_config_1.ENV.USER_NO_REPLY_PASSWORD;
const mailProvider = 'mail.comprartir.com';
let AuthModule = class AuthModule {
};
AuthModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([user_entity_1.User]), mailer_1.MailerModule.forRoot({
                transport: {
                    host: mailProvider, port: Number(587),
                    auth: { user: mail, pass: password },
                    tls: {
                        rejectUnauthorized: false
                    }
                },
                defaults: {
                    from: '"nest-modules" <modules@nestjs.com>',
                },
                template: {
                    dir: 'src/email-templates',
                    adapter: new mailer_1.HandlebarsAdapter(),
                    options: {
                        strict: true,
                    },
                },
            }),
            user_module_1.UsersModule,
            rols_module_1.RolsModule,
            passport_1.PassportModule,
            code_module_1.CodeModule,
            mailer_module_1.MailerNestModule,
            jwt_1.JwtModule.register({
                secret: 'Comprartir',
                signOptions: { expiresIn: 3600 },
            }),
        ],
        controllers: [auth_controller_1.AuthController],
        providers: [auth_service_1.AuthService, local_strategy_1.LocalStrategy, jwt_strategy_1.JwtStrategy, google_strategy_1.GoogleStrategy],
        exports: [auth_service_1.AuthService, local_strategy_1.LocalStrategy, jwt_strategy_1.JwtStrategy],
    })
], AuthModule);
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth.module.js.map