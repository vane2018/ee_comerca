"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GoogleStrategy = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const passport_google_oauth20_1 = require("passport-google-oauth20");
const auth_service_1 = require("../auth.service");
const user_service_1 = require("../../users/user.service");
const CLIENT_ID = '32078188478-467qmu7rurm96q4q2k9r0hbhqter6uvo.apps.googleusercontent.com';
const CLIENT_SECRET = 'QlBW-C3XYmcuAJR7C0ibddBM';
const CALLBACKURL = process.env.NODE_ENV === 'production' ?
    'https://api.comprartir-staging.tk/auth/google/callback/v1' :
    'http://localhost:3000/api/v1/auth/google/callback';
let GoogleStrategy = class GoogleStrategy extends passport_1.PassportStrategy(passport_google_oauth20_1.Strategy, 'google') {
    constructor(authService, userService) {
        super({
            clientID: CLIENT_ID,
            clientSecret: CLIENT_SECRET,
            callbackURL: CALLBACKURL,
            userProfileURL: 'https://www.googleapis.com/oauth2/v3/userinfo',
            passReqToCallback: true,
            scope: ['profile email'],
        });
        this.authService = authService;
        this.userService = userService;
    }
    async validate(request, accessToken, refreshToken, profile, done) {
        try {
            let user = await this.authService.findUserByMail(profile.emails[0].value);
            if (!user) {
                try {
                    const userRegistered = {
                        firstName: profile.name.familyName,
                        lastName: profile.name.givenName,
                        email: profile.emails[0].value,
                        password: Math.random().toString(36).slice(-8),
                    };
                    user = await this.userService.register(userRegistered);
                }
                catch (error) {
                    done(error, false);
                }
            }
            done(null, user);
        }
        catch (err) {
            done(err, false);
        }
    }
};
GoogleStrategy = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [auth_service_1.AuthService, user_service_1.UsersService])
], GoogleStrategy);
exports.GoogleStrategy = GoogleStrategy;
//# sourceMappingURL=google.strategy.js.map