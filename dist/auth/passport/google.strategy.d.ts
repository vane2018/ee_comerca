import { AuthService } from '../auth.service';
import { UsersService } from '../../users/user.service';
declare const GoogleStrategy_base: new (...args: any[]) => any;
export declare class GoogleStrategy extends GoogleStrategy_base {
    private readonly authService;
    private readonly userService;
    constructor(authService: AuthService, userService: UsersService);
    validate(request: any, accessToken: string, refreshToken: string, profile: any, done: Function): Promise<void>;
}
export {};
