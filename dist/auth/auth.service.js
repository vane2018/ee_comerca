"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var AuthService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const jwt = require("jsonwebtoken");
const common_1 = require("@nestjs/common");
const user_service_1 = require("../users/user.service");
const mailer_service_1 = require("../mailer/mailer.service");
let AuthService = AuthService_1 = class AuthService {
    constructor(usersService, mailerService) {
        this.usersService = usersService;
        this.mailerService = mailerService;
        this.logger = new common_1.Logger(AuthService_1.name);
    }
    createToken(user) {
        const expiresIn = 3600;
        const accessToken = jwt.sign({
            id: user.id,
            email: user.email,
            profilePic: user.profilePic,
            lastAcces: user.lastAccess
        }, 'Comprartir', { expiresIn });
        return {
            expiresIn,
            accessToken,
        };
    }
    async validateUserToken(payload) {
        return await this.usersService.getUserValidateToken(payload.id);
    }
    async validateUser(email, password) {
        const resp = await this.usersService.findByEmail(email);
        if (resp.user && resp.user.comparePassword(password)) {
            this.logger.log('password check success');
            const _a = resp.user, { password } = _a, result = __rest(_a, ["password"]);
            return result;
        }
        return null;
    }
    async findUserByMail(email) {
        return await this.usersService.findByEmail(email);
    }
    async validateNickEmail(nickEmail) {
        try {
            let status = {
                success: true,
                message: 'nick or email is disposition',
            };
            const user = await this.usersService.findByEmail(nickEmail);
            if (user) {
                status = { success: false, message: 'El nickname or email already exist' };
            }
            return status;
        }
        catch (error) {
            console.log('Error', error);
        }
    }
    async login(login) {
        return await this.usersService.login(login);
    }
    async sendMail(mailer) {
        return await this.mailerService.sendMail(mailer);
    }
};
AuthService = AuthService_1 = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [user_service_1.UsersService,
        mailer_service_1.NodeMailerService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map