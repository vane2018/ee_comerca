"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const user_entity_1 = require("../users/user.entity");
const swagger_1 = require("@nestjs/swagger");
const auth_service_1 = require("./auth.service");
const user_service_1 = require("../users/user.service");
const user_dto_1 = require("../users/dto/user.dto");
const login_dto_1 = require("./dto/login.dto");
const passport_1 = require("@nestjs/passport");
const util_1 = require("util");
const rols_service_1 = require("../rols/rols.service");
const code_service_1 = require("../code/code.service");
const credentials_dto_1 = require("./dto/credentials.dto");
const moment = require("moment");
const param_recovery_dto_1 = require("./dto/param-recovery.dto");
const validateDto_dto_1 = require("../code/dto/validateDto.dto");
const responseDto_dto_1 = require("./dto/responseDto.dto");
const param_validate_mail_dto_1 = require("./dto/param-validate-mail.dto");
const validation_dto_1 = require("./dto/validation.dto");
const user_firebase_dto_1 = require("../users/dto/user-firebase.dto");
const env_config_1 = require("../common/env.config");
let AuthController = class AuthController {
    constructor(authService, usersService, rolsService, codeService) {
        this.authService = authService;
        this.usersService = usersService;
        this.rolsService = rolsService;
        this.codeService = codeService;
        this.logger = new common_1.Logger('AuthController');
        this.twilioClient = require('twilio')(env_config_1.ENV.TWILIO_ACCOUNT_SID, env_config_1.ENV.TWILIO_AUTH_TOKEN);
    }
    async validateNickEmail(res, query) {
        try {
            const result = await this.authService.validateNickEmail(query.email);
            if (!result.success) {
                return res.status(common_1.HttpStatus.OK).json(result);
            }
            return res.status(common_1.HttpStatus.OK).json(result);
        }
        catch (error) {
            return res.status(common_1.HttpStatus.FORBIDDEN).json();
        }
    }
    async register(res, userDto) {
        try {
            const resp = await this.usersService.findByEmail(userDto.email);
            if (resp.success) {
                const response = {
                    success: false,
                    message: 'user already exist',
                    user: null,
                };
                return res.status(common_1.HttpStatus.OK).json(response);
            }
            else {
                const rol = await this.rolsService.findRol({ name: 'customer' });
                const user = await this.usersService.register(userDto);
                await this.usersService.updateUser(user, rol);
                const response = {
                    success: true,
                    message: 'user registered sucesfull',
                    user,
                };
                return res.status(common_1.HttpStatus.OK).json(response);
            }
        }
        catch (err) {
            util_1.debuglog(err);
            const response = { success: false, message: err, user: null };
            return res.status(common_1.HttpStatus.OK).json(response);
        }
    }
    async registerAdmin(res, userDto) {
        try {
            const resp = await this.usersService.findByEmail(userDto.email);
            if (resp.success) {
                const response = {
                    success: false,
                    message: 'user already exist',
                    user: null,
                };
                return res.status(common_1.HttpStatus.OK).json(response);
            }
            else {
                const rol = await this.rolsService.findRol({ name: 'admin' });
                const user = await this.usersService.register(userDto);
                await this.usersService.updateUser(user, rol);
                const response = {
                    success: true,
                    message: 'user registered sucesfull',
                    user,
                };
                return res.status(common_1.HttpStatus.OK).json(response);
            }
        }
        catch (err) {
            util_1.debuglog(err);
            const response = { success: false, message: err, user: null };
            return res.status(common_1.HttpStatus.OK).json(response);
        }
    }
    async updateUser(res, param, userDto) {
        try {
            const userUpdate = await this.usersService.updateToken(userDto.firebaseRegistrationToken, param.id);
            return res.status(common_1.HttpStatus.OK).json(userUpdate);
        }
        catch (error) {
            console.log('error al modifcar user: ', error);
            return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el user', error });
        }
    }
    async login(res, login) {
        const resp = await this.authService.login(login);
        if (resp.success) {
            const token = this.authService.createToken(resp.user);
            return res.status(common_1.HttpStatus.OK).json(token);
        }
        else {
            res.status(common_1.HttpStatus.NOT_FOUND).json({ message: 'User o pass incorrect' });
        }
    }
    async googleCallback(req, res) {
        const token = this.authService.createToken(req.user);
        res.redirect(env_config_1.ENV.LANDING_URL + 'verifytoken/' + token.accessToken);
    }
    async googleSignIn() {
        console.log('get google debe andar el guard');
    }
    async validateMail(res, mail) {
        this.logger.log(mail.email);
        const result = await this.usersService.findByEmail(mail.email);
        let codigo = '';
        const ref = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        for (let i = 0; i < 6; i++) {
            codigo += ref.charAt(Math.floor(Math.random() * ref.length));
        }
        const code = {
            email: mail.email,
            nroCel: null,
            code: codigo,
            validate: false,
            expiredAt: moment().add(30, 'm').format('YYYY-MM-DD HH:mm'),
            uso: false,
        };
        if (result.success) {
            try {
                const mailer = {
                    to: mail.email,
                    from: env_config_1.ENV.USER_NO_REPLY,
                    subject: 'COMPRARTIR - Código de seguridad',
                    template: 'recovery',
                    context: {
                        code: codigo
                    }
                };
                try {
                    await this.authService.sendMail(mailer);
                }
                catch (error) {
                    return res.status(common_1.HttpStatus.BAD_REQUEST).json({ error, message: 'Ocurrio un error al enviar el mail' });
                }
                const result = await this.codeService.saveCode(code);
                if (!result) {
                    return res.status(common_1.HttpStatus.BAD_REQUEST).json(result);
                }
                return res.status(common_1.HttpStatus.OK).json('Se guardo el codigo correctamente');
            }
            catch (error) {
                return res.status(common_1.HttpStatus.NOT_FOUND).json({ message: 'No se encontre el email' });
            }
        }
        else {
            return res.status(common_1.HttpStatus.NOT_FOUND).json({ error: 'user not exist', message: 'usario no existente' });
        }
    }
    async validateCode(res, validate) {
        const resp = await this.codeService.validateMailandCodigo(validate);
        if (!resp.succeses) {
            res.status(common_1.HttpStatus.BAD_REQUEST).json(Object.assign(Object.assign({}, resp), { message: ' mail or code invalid, o code has expired' }));
        }
        else {
            await this.codeService.actualizarCodigo(resp.code);
            return res.status(common_1.HttpStatus.OK).json(Object.assign(Object.assign({}, resp), { message: ' el codigo se valido con exito' }));
        }
    }
    async changuePass(res, credentials) {
        const resp = await this.codeService.findOne(credentials.codeID);
        const respuesta = await this.codeService.validateMailandCodigo(credentials);
        if ((credentials.pass === credentials.repeatPass)) {
            if (resp && resp.validate === true && resp.uso === false) {
                await this.usersService.updatePassword(credentials);
                await this.codeService.actualizarUsoCodigo(respuesta.code);
                return res.status(common_1.HttpStatus.OK).json({ message: 'se cambio la contraseña con exito' });
            }
            else {
                return res.status(common_1.HttpStatus.NOT_FOUND).json({ message: 'Error al cambiar la contraseña, codigo no valido o codigo ya usado' });
            }
        }
        else {
            return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'Error: Contraseña distintas' });
        }
    }
    async validateEmail(res, param) {
        const result = await this.usersService.findByEmailForValidate(param.email);
        if (result) {
            return res.status(common_1.HttpStatus.OK).json({
                validate: true,
            });
        }
        else {
            return res.status(common_1.HttpStatus.NOT_FOUND).json({
                statusCode: 404,
                error: 'email_not_found',
                message: 'Email not found',
            });
        }
    }
    async validateNick(res, nickname) {
        const result = await this.usersService.findByNickName(nickname);
        if (result) {
            return res.status(common_1.HttpStatus.OK).json({
                validate: true,
            });
        }
        else {
            return res.status(common_1.HttpStatus.NOT_FOUND).json({
                statusCode: 404,
                error: 'nickname_not_found',
                message: 'Nickname not found',
            });
        }
    }
    async validateNumber(res, numberRecovery) {
        this.logger.log(numberRecovery.nroCel);
        const result = await this.usersService.findByNumCel(numberRecovery.nroCel);
        let codigo = '';
        const ref = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZ0123456789';
        for (let i = 0; i < 6; i++) {
            codigo += ref.charAt(Math.floor(Math.random() * ref.length));
        }
        const code = {
            email: null,
            nroCel: numberRecovery.nroCel,
            code: codigo,
            validate: false,
            expiredAt: moment().add(30, 'm').format('YYYY-MM-DD HH:mm'),
            uso: false,
        };
        if (result.success) {
            this.twilioClient.messages
                .create({
                body: 'codigo de restablecimiento de contraseña: ' + codigo,
                from: '+12054489977',
                to: numberRecovery.nroCel,
            })
                .then(message => {
                console.log(message.sid);
                const result = this.codeService.saveCode(code);
                return res.status(common_1.HttpStatus.OK).json('Se guardo el codigo correctamente');
            })
                .then(err => {
                console.log('error: ', err);
                return res.status(common_1.HttpStatus.BAD_REQUEST).json({ err, message: 'Ocurrio un error al enviar el sms' });
            });
        }
        else {
            return res.status(common_1.HttpStatus.NOT_FOUND).json({ error: 'user not exist', message: 'usario no existente' });
        }
    }
};
__decorate([
    common_1.Get('validate'),
    swagger_1.ApiOperation({ summary: 'End point para validar email o nick name', description: 'validate email' }),
    swagger_1.ApiOkResponse({ description: 'El usuario o mail existe.', type: [user_dto_1.UserDto], status: 200 }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 403, description: "The logged user has no rights" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: "Internal server error" }),
    __param(0, common_1.Response()), __param(1, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, validation_dto_1.validateNickEmailQueryDto]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "validateNickEmail", null);
__decorate([
    common_1.Post('register'),
    swagger_1.ApiOperation({ summary: 'Registra una cuenta' }),
    swagger_1.ApiCreatedResponse({ description: 'The Account has been successfully created.', type: user_dto_1.UserDto }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 403, description: "The logged user has no rights" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: "internal server error" }),
    swagger_1.ApiBody({ type: user_dto_1.UserDto }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, user_dto_1.UserDto]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "register", null);
__decorate([
    common_1.Post('registerAdmin'),
    swagger_1.ApiOperation({ summary: 'Registra una cuenta' }),
    swagger_1.ApiCreatedResponse({ description: 'The Account has been successfully created.', type: user_dto_1.UserDto }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 403, description: "The logged user has no rights" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: "internal server error" }),
    swagger_1.ApiBody({ type: user_dto_1.UserDto }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, user_dto_1.UserDto]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "registerAdmin", null);
__decorate([
    common_1.Put('/edit/user/:id'),
    __param(0, common_1.Response()), __param(1, common_1.Param()), __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, user_firebase_dto_1.UserFirebaseDto]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "updateUser", null);
__decorate([
    common_1.Post('login'),
    swagger_1.ApiOperation({ summary: 'Se loguea una cuenta' }),
    swagger_1.ApiCreatedResponse({ description: 'The Account has been successfully login.', type: login_dto_1.LoginUserDto, status: 200 }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 403, description: "Forbidden" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: "internal server error" }),
    swagger_1.ApiBody({ type: login_dto_1.LoginUserDto }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, login_dto_1.LoginUserDto]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "login", null);
__decorate([
    common_1.Get('google/callback'),
    common_1.UseGuards(passport_1.AuthGuard('google')),
    swagger_1.ApiOperation({ summary: 'Se loguea una cuenta con google', description: 'login account google' }),
    swagger_1.ApiResponse({ status: 200, description: 'Los datos han sido devueltos correctamente.' }),
    swagger_1.ApiResponse({ status: 400, description: "bad request" }),
    swagger_1.ApiResponse({ status: 403, description: "forbidden" }),
    swagger_1.ApiResponse({ status: 404, description: "Not found" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Request()), __param(1, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "googleCallback", null);
__decorate([
    common_1.Get('google'),
    common_1.UseGuards(passport_1.AuthGuard('google')),
    swagger_1.ApiResponse({ status: 200, description: 'Los datos han sido devueltos correctamente.' }),
    swagger_1.ApiResponse({ status: 400, description: "bad request" }),
    swagger_1.ApiResponse({ status: 403, description: "Forbidden" }),
    swagger_1.ApiResponse({ status: 404, description: "Not found" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "googleSignIn", null);
__decorate([
    common_1.Get('recovery/:email'),
    swagger_1.ApiOperation({ summary: 'End point para enviar el email' }),
    swagger_1.ApiOkResponse({ description: 'the email was sent correctly', type: [param_recovery_dto_1.ParamRecoveryDto], status: 200 }),
    swagger_1.ApiResponse({ status: 400, description: "bad request" }),
    swagger_1.ApiResponse({ status: 403, description: "forbidden" }),
    swagger_1.ApiResponse({ status: 404, description: "Email Not found" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    __param(0, common_1.Response()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, param_recovery_dto_1.ParamRecoveryDto]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "validateMail", null);
__decorate([
    common_1.Post('validate-code'),
    swagger_1.ApiOperation({ summary: 'Validation the code the recovery the  password' }),
    swagger_1.ApiOkResponse({ description: 'The code has been checked successfully.', type: responseDto_dto_1.ValidateResponseDto, status: 200 }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 403, description: "ProhibidoForbidden" }),
    swagger_1.ApiResponse({ status: 404, description: "Not found" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: "internal server error" }),
    swagger_1.ApiBody({ type: validateDto_dto_1.ValidateDto }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, validateDto_dto_1.ValidateDto]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "validateCode", null);
__decorate([
    common_1.Post('change-password'),
    swagger_1.ApiOperation({ summary: 'Cambiar contraseña' }),
    swagger_1.ApiCreatedResponse({ description: 'The password has been successfully change.', type: credentials_dto_1.CredentialsDto, status: 200 }),
    swagger_1.ApiBadRequestResponse({ description: 'Contraseña distintas' }),
    swagger_1.ApiResponse({ status: 400, description: "password different" }),
    swagger_1.ApiResponse({ status: 403, description: "ProhibidoForbidden" }),
    swagger_1.ApiResponse({ status: 404, description: "Not found" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: "internal server error" }),
    swagger_1.ApiBody({ type: credentials_dto_1.CredentialsDto }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, credentials_dto_1.CredentialsDto]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "changuePass", null);
__decorate([
    common_1.Get('validate-email/:email'),
    swagger_1.ApiOperation({ summary: 'End point para validar email' }),
    swagger_1.ApiOkResponse({ description: 'Email existe.', status: 200 }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 403, description: "Forbidden" }),
    swagger_1.ApiResponse({ status: 404, description: "Not found" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: "internal server error" }),
    __param(0, common_1.Response()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, param_validate_mail_dto_1.ValidateMailDto]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "validateEmail", null);
__decorate([
    common_1.Get('validate-nickname/:nickname'),
    swagger_1.ApiOperation({ summary: 'End point para validations nickName' }),
    swagger_1.ApiResponse({ description: 'nickName existe.', status: 200 }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 403, description: "Forbidden" }),
    swagger_1.ApiResponse({ status: 404, description: "nickName Not found" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: "internal server error" }),
    __param(0, common_1.Response()), __param(1, common_1.Param('nickname')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "validateNick", null);
__decorate([
    common_1.Get('recovery/sms/:nroCel'),
    swagger_1.ApiOperation({ summary: 'End point para enviar el numero' }),
    swagger_1.ApiResponse({ description: 'the sms was sent successfully.', status: 200 }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 403, description: "Forbidden" }),
    swagger_1.ApiResponse({ status: 404, description: "number Not found" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: "internal server error" }),
    __param(0, common_1.Response()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, param_recovery_dto_1.NumberRecoveryDto]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "validateNumber", null);
AuthController = __decorate([
    crud_1.Crud({
        model: {
            type: user_entity_1.User,
        },
        params: {
            id: {
                field: 'id',
                type: 'uuid',
                primary: true,
            },
        },
    }),
    swagger_1.ApiTags('Auth'),
    common_1.Controller('auth'),
    __metadata("design:paramtypes", [auth_service_1.AuthService,
        user_service_1.UsersService,
        rols_service_1.RolsService,
        code_service_1.CodeService])
], AuthController);
exports.AuthController = AuthController;
//# sourceMappingURL=auth.controller.js.map