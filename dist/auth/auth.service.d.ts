import { UsersService } from '../users/user.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { User } from '../users/user.entity';
import { NodeMailerService } from '../mailer/mailer.service';
import { LoginUserDto } from './dto/login.dto';
import { ISendMailOptions } from '@nest-modules/mailer';
export declare class AuthService {
    private readonly usersService;
    private readonly mailerService;
    constructor(usersService: UsersService, mailerService: NodeMailerService);
    private readonly logger;
    createToken(user: User): {
        expiresIn: number;
        accessToken: string;
    };
    validateUserToken(payload: JwtPayload): Promise<any>;
    validateUser(email: string, password: string): Promise<any>;
    findUserByMail(email: string): Promise<any>;
    validateNickEmail(nickEmail: string): Promise<any>;
    login(login: LoginUserDto): Promise<any>;
    sendMail(mailer: ISendMailOptions): Promise<any>;
}
