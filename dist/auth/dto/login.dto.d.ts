export declare class LoginUserDto {
    readonly email: string;
    readonly nickName: string;
    readonly password: string;
}
