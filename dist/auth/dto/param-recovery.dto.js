"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NumberRecoveryDto = exports.ParamRecoveryDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class ParamRecoveryDto {
}
__decorate([
    swagger_1.ApiProperty({ description: 'email de recuperacion de contraseña' }),
    class_validator_1.IsEmail(),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], ParamRecoveryDto.prototype, "email", void 0);
exports.ParamRecoveryDto = ParamRecoveryDto;
class NumberRecoveryDto {
}
__decorate([
    swagger_1.ApiProperty({ description: 'nro de recuperacion de contraseña' }),
    __metadata("design:type", String)
], NumberRecoveryDto.prototype, "nroCel", void 0);
exports.NumberRecoveryDto = NumberRecoveryDto;
//# sourceMappingURL=param-recovery.dto.js.map