export declare class LoginResponseDto {
    success: boolean;
    message: string;
}
