import { CodeDto } from '../../code/dto/code.dto';
export declare class ValidateResponseDto {
    readonly succeses: boolean;
    readonly code: CodeDto;
    readonly message: string;
}
