"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CredentialsDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class CredentialsDto {
}
__decorate([
    swagger_1.ApiProperty({ description: 'ID codigo de recuperacion' }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CredentialsDto.prototype, "codeID", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'Email del usuario que necesita recuperar la contraseña' }),
    __metadata("design:type", String)
], CredentialsDto.prototype, "email", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'Nro de celular del usuario que necesita recuperar la contraseña' }),
    __metadata("design:type", String)
], CredentialsDto.prototype, "nroCel", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Codigo de recuperacion' }),
    __metadata("design:type", String)
], CredentialsDto.prototype, "code", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Nueva password' }),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], CredentialsDto.prototype, "pass", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Se repite la nueva password' }),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], CredentialsDto.prototype, "repeatPass", void 0);
exports.CredentialsDto = CredentialsDto;
//# sourceMappingURL=credentials.dto.js.map