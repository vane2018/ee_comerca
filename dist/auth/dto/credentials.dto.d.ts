export declare class CredentialsDto {
    codeID: string;
    email: string;
    nroCel: string;
    code: string;
    pass: string;
    repeatPass: string;
}
