"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DetalleService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const crud_typeorm_1 = require("@nestjsx/crud-typeorm");
const detalle_entity_1 = require("./detalle.entity");
const products_service_1 = require("../products/products.service");
let DetalleService = class DetalleService extends crud_typeorm_1.TypeOrmCrudService {
    constructor(repo, productService) {
        super(repo);
    }
    async saveDetalle(detalle) {
        let detalles = this.repo.create(detalle);
        return await this.repo.save(detalles);
    }
};
DetalleService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(detalle_entity_1.Detalle)),
    __metadata("design:paramtypes", [Object, products_service_1.ProductsService])
], DetalleService);
exports.DetalleService = DetalleService;
//# sourceMappingURL=detalle.service.js.map