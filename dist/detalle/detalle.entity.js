"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Detalle = void 0;
const typeorm_1 = require("typeorm");
const purchase_entity_1 = require("../purchases/purchase.entity");
const products_entity_1 = require("../products/products.entity");
let Detalle = class Detalle {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Detalle.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Detalle.prototype, "productoId", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Detalle.prototype, "purchaseId", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Detalle.prototype, "productoName", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Detalle.prototype, "productoPrice", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], Detalle.prototype, "imagenProducto", void 0);
__decorate([
    typeorm_1.ManyToOne(() => purchase_entity_1.Purchase, purchase => purchase.detalle),
    __metadata("design:type", purchase_entity_1.Purchase)
], Detalle.prototype, "purchase", void 0);
__decorate([
    typeorm_1.ManyToOne(() => products_entity_1.Product, product => product.detalle),
    __metadata("design:type", products_entity_1.Product)
], Detalle.prototype, "producto", void 0);
Detalle = __decorate([
    typeorm_1.Entity('DETALLE')
], Detalle);
exports.Detalle = Detalle;
//# sourceMappingURL=detalle.entity.js.map