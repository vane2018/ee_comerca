export declare class detalleDto {
    productoId: string;
    productoName: string;
    productoPrice: number;
    purchaseId: string;
    imagenProducto: string;
}
