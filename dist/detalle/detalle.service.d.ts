import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Detalle } from './detalle.entity';
import { detalleDto } from './dto/detalleDto.dto';
import { ProductsService } from '../products/products.service';
export declare class DetalleService extends TypeOrmCrudService<Detalle> {
    constructor(repo: any, productService: ProductsService);
    saveDetalle(detalle: detalleDto): Promise<detalleDto>;
}
