import { Purchase } from '../purchases/purchase.entity';
import { Product } from '../products/products.entity';
export declare class Detalle {
    id: string;
    productoId: string;
    purchaseId: string;
    productoName: string;
    productoPrice: number;
    imagenProducto: string;
    purchase: Purchase;
    producto: Product;
}
