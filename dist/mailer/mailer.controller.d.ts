import { NodeMailerService } from './mailer.service';
import { MailerRecommendDto } from './dto/mailerRecommend.dto';
export declare class MailerController {
    private mailerService;
    constructor(mailerService: NodeMailerService);
    postMailerComprartir(res: any, bodys: MailerRecommendDto): Promise<any>;
}
