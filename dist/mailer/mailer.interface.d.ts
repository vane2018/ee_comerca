export interface MailerOptions {
    to: string;
    from: string;
    subject: string;
    text: string;
    html: string;
}
