"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MailerController = void 0;
const common_1 = require("@nestjs/common");
const mailer_service_1 = require("./mailer.service");
const swagger_1 = require("@nestjs/swagger");
const mailerRecommend_dto_1 = require("./dto/mailerRecommend.dto");
const env_config_1 = require("../common/env.config");
let MailerController = class MailerController {
    constructor(mailerService) {
        this.mailerService = mailerService;
    }
    async postMailerComprartir(res, bodys) {
        const mailer = {
            to: env_config_1.ENV.USER_COBRO_COMISION,
            from: env_config_1.ENV.USER_COBRO_COMISION,
            subject: 'De: ' + env_config_1.ENV.USER_COBRO_COMISION + ' - Motivo: ' + bodys.subject,
            text: `${bodys.message}.`,
            html: `${bodys.message}.`,
        };
        try {
            const mail = await this.mailerService.sendMail(mailer);
            return res.status(common_1.HttpStatus.OK).json(mail);
        }
        catch (error) {
            console.log('error', error);
            return res.status(common_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ err: true, msg: 'error al enviar mail' });
        }
    }
};
__decorate([
    common_1.Post('/notificar'),
    common_1.UsePipes(new common_1.ValidationPipe({ transform: true })),
    swagger_1.ApiResponse({ status: 201, description: 'The record has been successfully created.' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, mailerRecommend_dto_1.MailerRecommendDto]),
    __metadata("design:returntype", Promise)
], MailerController.prototype, "postMailerComprartir", null);
MailerController = __decorate([
    swagger_1.ApiTags('Mailers'),
    common_1.Controller('mailers'),
    __metadata("design:paramtypes", [mailer_service_1.NodeMailerService])
], MailerController);
exports.MailerController = MailerController;
//# sourceMappingURL=mailer.controller.js.map