export declare class NotificarDto {
    name: string;
    apellido: string;
    cbu: string;
    message: string;
    subject: string;
    monto: string;
    nickname: string;
}
