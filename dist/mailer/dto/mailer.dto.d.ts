export declare class MailerDto {
    name: string;
    email: string;
    subject: string;
    message: string;
}
