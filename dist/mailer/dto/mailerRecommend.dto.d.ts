export declare class MailerRecommendDto {
    nick: string;
    subject: string;
    iduser: string;
    message: string;
}
