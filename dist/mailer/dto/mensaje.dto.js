"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotificarDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class NotificarDto {
}
__decorate([
    swagger_1.ApiProperty(),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], NotificarDto.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Email del usuario a enviar el codigo' }),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], NotificarDto.prototype, "apellido", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Cuerpo para el mensaje' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], NotificarDto.prototype, "cbu", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'email body' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], NotificarDto.prototype, "message", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Cuerpo para el mensaje' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], NotificarDto.prototype, "subject", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Mensaje del email' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], NotificarDto.prototype, "monto", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Mensaje del email' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], NotificarDto.prototype, "nickname", void 0);
exports.NotificarDto = NotificarDto;
//# sourceMappingURL=mensaje.dto.js.map