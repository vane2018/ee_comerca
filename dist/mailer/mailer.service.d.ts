import { MailerService, ISendMailOptions } from '@nest-modules/mailer';
export declare class NodeMailerService {
    private readonly mailerService;
    constructor(mailerService: MailerService);
    sendMail(mailer: ISendMailOptions): Promise<any>;
}
