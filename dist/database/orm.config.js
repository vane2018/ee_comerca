"use strict";
const ormconfig = {
    type: "postgres",
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT),
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    entities: ["dist/**/*.entity{.ts,.js}"],
    synchronize: process.env.DB_SINCRONIZE === 'true' ? true : false,
    migrationsRun: process.env.DB_MIGRATION_RUN === 'true' ? true : false,
    logging: false,
    migrations: ['dist/migrations/*{.ts,.js}'],
    cli: {
        migrationsDir: 'dist/migrations',
    },
};
module.exports = ormconfig;
//# sourceMappingURL=orm.config.js.map