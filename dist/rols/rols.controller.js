"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RolsController = void 0;
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const rols_entity_1 = require("./rols.entity");
const rols_service_1 = require("./rols.service");
const swagger_1 = require("@nestjs/swagger");
const rol_dto_1 = require("./dto/rol.dto");
let RolsController = class RolsController {
    constructor(service) {
        this.service = service;
    }
    async getRols(res, nameRol) {
        const rol = await this.service.findRol(nameRol.id);
        return res.status(common_1.HttpStatus.OK).json(rol);
    }
    async crearRols(res, nameRol) {
        const rol = await this.service.findRol(nameRol.id);
        return res.status(common_1.HttpStatus.OK).json(rol);
    }
};
__decorate([
    common_1.Get('porNombre/:name'),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, rol_dto_1.RolDto]),
    __metadata("design:returntype", Promise)
], RolsController.prototype, "getRols", null);
__decorate([
    common_1.Post('/register'),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    swagger_1.ApiBody({ type: rol_dto_1.RolDto }),
    __param(0, common_1.Response()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, rol_dto_1.RolDto]),
    __metadata("design:returntype", Promise)
], RolsController.prototype, "crearRols", null);
RolsController = __decorate([
    crud_1.Crud({
        model: {
            type: rols_entity_1.Rols,
        },
        params: {
            id: {
                field: 'id',
                type: 'uuid',
                primary: true,
            },
        },
    }),
    swagger_1.ApiTags('Rols'),
    common_1.Controller('rols'),
    __metadata("design:paramtypes", [rols_service_1.RolsService])
], RolsController);
exports.RolsController = RolsController;
//# sourceMappingURL=rols.controller.js.map