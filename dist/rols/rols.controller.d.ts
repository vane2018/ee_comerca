import { RolsService } from './rols.service';
import { RolDto } from './dto/rol.dto';
export declare class RolsController {
    service: RolsService;
    constructor(service: RolsService);
    getRols(res: any, nameRol: RolDto): Promise<any>;
    crearRols(res: any, nameRol: RolDto): Promise<any>;
}
