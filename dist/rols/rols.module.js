"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RolsModule = void 0;
const common_1 = require("@nestjs/common");
const rols_controller_1 = require("./rols.controller");
const rols_service_1 = require("./rols.service");
const typeorm_1 = require("@nestjs/typeorm");
const rols_entity_1 = require("./rols.entity");
let RolsModule = class RolsModule {
};
RolsModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([rols_entity_1.Rols])],
        controllers: [rols_controller_1.RolsController],
        providers: [rols_service_1.RolsService],
        exports: [rols_service_1.RolsService],
    })
], RolsModule);
exports.RolsModule = RolsModule;
//# sourceMappingURL=rols.module.js.map