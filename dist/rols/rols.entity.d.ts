import { User } from '../users/user.entity';
import { Permission } from '../permissions/permissions.entity';
export declare class Rols {
    id: string;
    name: string;
    description: string;
    users: User[];
    permission: Permission[];
}
