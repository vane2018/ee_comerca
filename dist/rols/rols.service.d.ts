import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { RolDto } from './dto/rol.dto';
export declare class RolsService extends TypeOrmCrudService<RolDto> {
    constructor(repo: any);
    findRol(name: any): Promise<any>;
}
