"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Rols = void 0;
const typeorm_1 = require("typeorm");
const user_entity_1 = require("../users/user.entity");
const permissions_entity_1 = require("../permissions/permissions.entity");
let Rols = class Rols {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", String)
], Rols.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Rols.prototype, "name", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Rols.prototype, "description", void 0);
__decorate([
    typeorm_1.ManyToMany(() => user_entity_1.User, (user) => user.rols),
    __metadata("design:type", Array)
], Rols.prototype, "users", void 0);
__decorate([
    typeorm_1.ManyToMany(() => permissions_entity_1.Permission, (permission) => permission.rols),
    typeorm_1.JoinTable({
        name: 'ROLS_PERMISSIONS',
        joinColumns: [
            { name: 'rolId' },
        ],
        inverseJoinColumns: [
            { name: 'permissionId' },
        ],
    }),
    __metadata("design:type", Array)
], Rols.prototype, "permission", void 0);
Rols = __decorate([
    typeorm_1.Entity('ROLS')
], Rols);
exports.Rols = Rols;
//# sourceMappingURL=rols.entity.js.map