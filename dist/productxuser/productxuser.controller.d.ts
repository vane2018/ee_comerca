import { ProductsViewService } from "./productxuser.service";
import { productViewDto } from "./dto/productxUserDto.dto";
export declare class ProductsViewController {
    service: ProductsViewService;
    constructor(service: ProductsViewService);
    getProvincias(res: any, prodViewDto: productViewDto): Promise<any>;
    comparacion(res: any, prodViewDto: productViewDto): Promise<any>;
}
