import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { productView } from "./productUser.entity";
import { productViewDto } from "./dto/productxUserDto.dto";
export declare class ProductsViewService extends TypeOrmCrudService<productView> {
    constructor(repo: any);
    guardarView(prod: productViewDto): Promise<productView>;
    comparar(prod: productViewDto): Promise<true | productView>;
}
