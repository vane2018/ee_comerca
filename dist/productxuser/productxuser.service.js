"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsViewService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const crud_typeorm_1 = require("@nestjsx/crud-typeorm");
const productUser_entity_1 = require("./productUser.entity");
const typeorm_2 = require("typeorm");
let ProductsViewService = class ProductsViewService extends crud_typeorm_1.TypeOrmCrudService {
    constructor(repo) {
        super(repo);
    }
    async guardarView(prod) {
        try {
            let prodV = this.repo.create(prod);
            return await this.repo.save(prodV);
        }
        catch (error) {
            console.log('error', error);
        }
    }
    async comparar(prod) {
        let band = false;
        let r = await typeorm_2.getRepository(productUser_entity_1.productView)
            .createQueryBuilder("productView")
            .where("productView.idUser = :prod", { prod: prod.idUser })
            .getMany();
        for (let index = 0; index < r.length; index++) {
            if (r[index].idProduct === prod.idProduct) {
                band = true;
            }
        }
        if (band) {
            return band;
        }
        else {
            let prodV = this.repo.create(prod);
            return await this.repo.save(prodV);
        }
    }
};
ProductsViewService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(productUser_entity_1.productView)),
    __metadata("design:paramtypes", [Object])
], ProductsViewService);
exports.ProductsViewService = ProductsViewService;
//# sourceMappingURL=productxuser.service.js.map