"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductxuserModule = void 0;
const common_1 = require("@nestjs/common");
const productUser_entity_1 = require("./productUser.entity");
const typeorm_1 = require("@nestjs/typeorm");
const productxuser_controller_1 = require("./productxuser.controller");
const productxuser_service_1 = require("./productxuser.service");
let ProductxuserModule = class ProductxuserModule {
};
ProductxuserModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([productUser_entity_1.productView])
        ],
        controllers: [productxuser_controller_1.ProductsViewController],
        providers: [productxuser_service_1.ProductsViewService],
        exports: [productxuser_service_1.ProductsViewService]
    })
], ProductxuserModule);
exports.ProductxuserModule = ProductxuserModule;
//# sourceMappingURL=productxuser.module.js.map