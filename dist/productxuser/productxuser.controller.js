"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsViewController = void 0;
const crud_1 = require("@nestjsx/crud");
const swagger_1 = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const productUser_entity_1 = require("./productUser.entity");
const productxuser_service_1 = require("./productxuser.service");
const productxUserDto_dto_1 = require("./dto/productxUserDto.dto");
let ProductsViewController = class ProductsViewController {
    constructor(service) {
        this.service = service;
    }
    async getProvincias(res, prodViewDto) {
        try {
            let provincias = await this.service.guardarView(prodViewDto);
            return res.status(common_1.HttpStatus.OK).json(provincias);
        }
        catch (error) {
            console.log('error', error);
        }
    }
    async comparacion(res, prodViewDto) {
        try {
            let estado = await this.service.comparar(prodViewDto);
            if (estado) {
                return res.status(common_1.HttpStatus.OK).json({ "mensaje": estado });
            }
            else {
                return res.status(common_1.HttpStatus.OK).json({ "mensaje": estado });
            }
        }
        catch (error) {
            console.log("error", error);
        }
    }
};
__decorate([
    common_1.Post('viewxuser'),
    swagger_1.ApiOperation({ summary: 'comprueba si el producto fue visto por usuario' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiBody({ type: productxUserDto_dto_1.productViewDto }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, productxUserDto_dto_1.productViewDto]),
    __metadata("design:returntype", Promise)
], ProductsViewController.prototype, "getProvincias", null);
__decorate([
    common_1.Post('/comp'),
    swagger_1.ApiOperation({ summary: 'compara los view' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiBody({ type: productxUserDto_dto_1.productViewDto }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, productxUserDto_dto_1.productViewDto]),
    __metadata("design:returntype", Promise)
], ProductsViewController.prototype, "comparacion", null);
ProductsViewController = __decorate([
    crud_1.Crud({
        model: {
            type: productUser_entity_1.productView,
        },
        params: {
            id: {
                field: 'id',
                type: 'uuid',
                primary: true,
            },
        }
    }),
    swagger_1.ApiTags('ProductsViewxUser'),
    common_1.Controller('productsViewxuser'),
    __metadata("design:paramtypes", [productxuser_service_1.ProductsViewService])
], ProductsViewController);
exports.ProductsViewController = ProductsViewController;
//# sourceMappingURL=productxuser.controller.js.map