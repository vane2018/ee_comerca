"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const swagger_1 = require("@nestjs/swagger");
const app_module_1 = require("./app.module");
const basicAuth = require("express-basic-auth");
const express_1 = require("express");
const validation_pipe_1 = require("@nestjs/common/pipes/validation.pipe");
const fbase = require("firebase-admin");
const env_config_1 = require("./common/env.config");
const API_DEFAULT_PREFIX = '/api/v1';
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    app.useGlobalPipes(new validation_pipe_1.ValidationPipe());
    app.use(express_1.json({ limit: '50mb' }));
    app.enableCors();
    app.setGlobalPrefix(process.env.API_PREFIX || API_DEFAULT_PREFIX);
    const options = new swagger_1.DocumentBuilder()
        .setTitle('Comprartir Api')
        .setDescription('The API for e-commerce')
        .setVersion('1.0')
        .build();
    const adminConfig = {
        projectId: env_config_1.ENV.FIREBASE_PROJECT_ID,
        clientEmail: env_config_1.ENV.FIREBASE_CLIENT_EMAIL,
        privateKey: env_config_1.ENV.FIREBASE_PRIVATE_KEY.replace(/\\n/g, '\n')
    };
    if (!fbase.apps.length) {
        fbase.initializeApp({
            credential: fbase.credential.cert(adminConfig)
        });
    }
    app.use('/doc', basicAuth({
        challenge: true,
        users: { [process.env.USER_SWAGGER]: process.env.USER_SWAGGER_PASSWORD },
    }));
    const document = swagger_1.SwaggerModule.createDocument(app, options);
    swagger_1.SwaggerModule.setup('/doc', app, document);
    await app.listen(3000);
}
bootstrap();
//# sourceMappingURL=main.js.map