"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileService = void 0;
const common_1 = require("@nestjs/common");
const uuid_1 = require("uuid");
const storage_1 = require("@google-cloud/storage");
const env_config_1 = require("../common/env.config");
const FOLDER_NAME = 'upload';
const CREDENTIAL = {
    private_key: env_config_1.ENV.G_STORAGE_CREDENTIAL_PRIVATE_KEY.replace(/\\n/g, '\n'),
    client_email: env_config_1.ENV.G_STORAGE_CREDENTIAL_CLIENT_EMAIL
};
let FileService = class FileService {
    constructor() {
        this.storage = new storage_1.Storage({ credentials: CREDENTIAL });
        this.bucket = this.storage.bucket(env_config_1.ENV.G_STORAGE_BUCKET_NAME);
    }
    async getReadSignedUrl(fileName, cfg) {
        const now = new Date();
        const nextDay = now.setHours(now.getHours() + 24);
        const defaultReadConfig = {
            action: 'read',
            expires: nextDay
        };
        const file = this.bucket.file(`${FOLDER_NAME}/${fileName}`);
        const config = cfg ? cfg : defaultReadConfig;
        return await file.getSignedUrl(config);
    }
    async getWriteSignedUrl(fileName, cfg) {
        const now = new Date();
        const nextHour = now.setHours(now.getHours() + 1);
        const defaultWriteConfig = {
            action: 'write',
            expires: nextHour
        };
        const name = fileName ? fileName : uuid_1.v4();
        const file = this.bucket.file(`${FOLDER_NAME}/${name}`);
        const config = cfg ? cfg : defaultWriteConfig;
        const signedUrl = await file.getSignedUrl(config);
        return { fileName: name, signedUrl: signedUrl[0] };
    }
};
FileService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [])
], FileService);
exports.FileService = FileService;
//# sourceMappingURL=file.service.js.map