import { UploadedFileMetadata } from '@aginix/nestjs-gcloud-storage';
import { FileService } from './file.service';
export declare class FileController {
    fileService: FileService;
    constructor(fileService: FileService);
    UploadedFilesUsingInterceptor(file: UploadedFileMetadata): void;
    getReadSignedUrl(res: any, fileName: any): Promise<any>;
    getWriteSignedUrl(res: any): Promise<any>;
}
