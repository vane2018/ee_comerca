"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileController = void 0;
const common_1 = require("@nestjs/common");
const nestjs_gcloud_storage_1 = require("@aginix/nestjs-gcloud-storage");
const swagger_1 = require("@nestjs/swagger");
const file_service_1 = require("./file.service");
let FileController = class FileController {
    constructor(fileService) {
        this.fileService = fileService;
    }
    UploadedFilesUsingInterceptor(file) {
        console.log(`Storage URL: ${file.storageUrl}`, 'AppController');
    }
    async getReadSignedUrl(res, fileName) {
        const signed = await this.fileService.getReadSignedUrl(fileName);
        return res.status(common_1.HttpStatus.OK).json(signed);
    }
    async getWriteSignedUrl(res) {
        const signed = await this.fileService.getWriteSignedUrl();
        return res.status(common_1.HttpStatus.OK).json(signed);
    }
};
__decorate([
    common_1.Post('/upload'),
    common_1.UseInterceptors(nestjs_gcloud_storage_1.GCloudStorageFileInterceptor('file', undefined, { prefix: 'upload/app' })),
    __param(0, common_1.UploadedFile()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], FileController.prototype, "UploadedFilesUsingInterceptor", null);
__decorate([
    common_1.Get('/signed-url/read/:fileName'),
    swagger_1.ApiOperation({ summary: 'test get signed url' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Param('fileName')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], FileController.prototype, "getReadSignedUrl", null);
__decorate([
    common_1.Get('/signed-url/write'),
    swagger_1.ApiOperation({ summary: 'test get signed url' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], FileController.prototype, "getWriteSignedUrl", null);
FileController = __decorate([
    swagger_1.ApiTags('File'),
    common_1.Controller('file'),
    __metadata("design:paramtypes", [file_service_1.FileService])
], FileController);
exports.FileController = FileController;
//# sourceMappingURL=file.controller.js.map