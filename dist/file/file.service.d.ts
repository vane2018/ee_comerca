import { GetSignedUrlConfig, GetSignedUrlResponse } from '@google-cloud/storage';
export declare class FileService {
    private storage;
    private bucket;
    constructor();
    getReadSignedUrl(fileName: string, cfg?: GetSignedUrlConfig): Promise<GetSignedUrlResponse>;
    getWriteSignedUrl(fileName?: string, cfg?: GetSignedUrlConfig): Promise<any>;
}
