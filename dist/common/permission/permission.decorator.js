"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Permision = void 0;
const common_1 = require("@nestjs/common");
const Permision = (...permission) => common_1.SetMetadata('permission', permission);
exports.Permision = Permision;
//# sourceMappingURL=permission.decorator.js.map