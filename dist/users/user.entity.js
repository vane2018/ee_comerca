"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const typeorm_1 = require("typeorm");
const class_validator_1 = require("class-validator");
const bcrypt = require("bcrypt");
const commerce_entity_1 = require("../commerce/commerce.entity");
const rols_entity_1 = require("../rols/rols.entity");
const purchase_entity_1 = require("../purchases/purchase.entity");
const commissions_entity_1 = require("../commissions/commissions.entity");
const chat_entity_1 = require("../chat/chat.entity");
const cobrarcomision_entity_1 = require("../cobrarcomision/cobrarcomision.entity");
let User = class User {
    async hashPassword() {
        this.password = await bcrypt.hash(this.password, 10);
    }
    async comparePassword(attempt) {
        return await bcrypt.compare(attempt, this.password);
    }
    async compareNickname(nick) {
        if (nick === this.nickName) {
            return true;
        }
    }
    toResponseObject(showToken = true) {
        const { email, password, firstName, lastName, dni, gender, nickName, phone, commisions, commerce, isCustomerAndSeller, profilePic, rols, nickReferrer, calle, numero, latitud, longitud, localidad, pais, provincia, fechanacimiento, pisodepartamento, firebaseRegistrationToken } = this;
        const responseObject = {
            email,
            password,
            firstName,
            lastName,
            dni,
            firebaseRegistrationToken,
            gender,
            nickReferrer,
            nickName,
            phone,
            profilePic,
            commisions,
            calle,
            numero,
            localidad,
            provincia,
            pais,
            latitud,
            longitud,
            commerce,
            fechanacimiento,
            pisodepartamento,
            isCustomerAndSeller,
            rols,
        };
        return responseObject;
    }
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], User.prototype, "id", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 300, unique: true }),
    __metadata("design:type", String)
], User.prototype, "email", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 300 }),
    __metadata("design:type", String)
], User.prototype, "password", void 0);
__decorate([
    class_validator_1.IsOptional(),
    typeorm_1.Column('varchar', { length: 300, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "firstName", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 300, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "lastName", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 300, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "nickReferrer", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 300, unique: true, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "nickName", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 300, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "phone", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 30, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "gender", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 300, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "dni", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 300, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "cbu", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 300, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "profilePic", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 10, nullable: true, default: false }),
    __metadata("design:type", Boolean)
], User.prototype, "isCustomerAndSeller", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 10, nullable: true, default: "customer" }),
    __metadata("design:type", String)
], User.prototype, "lastAccess", void 0);
__decorate([
    typeorm_1.Column("text", { array: true, default: () => 'array [] :: text []' }),
    __metadata("design:type", Array)
], User.prototype, "favorite", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "firebaseRegistrationToken", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "pais", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "provincia", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "localidad", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "latitud", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "longitud", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "calle", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "numero", void 0);
__decorate([
    typeorm_1.Column({ type: "float8", nullable: true, default: 0 }),
    __metadata("design:type", Number)
], User.prototype, "montoextraido", void 0);
__decorate([
    typeorm_1.Column({ type: "float8", nullable: true }),
    __metadata("design:type", String)
], User.prototype, "estadocobrocomision", void 0);
__decorate([
    typeorm_1.Column({ type: Date, default: new Date("2017-03-15T19:47:47.975Z") }),
    __metadata("design:type", Date)
], User.prototype, "fechanacimiento", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], User.prototype, "pisodepartamento", void 0);
__decorate([
    typeorm_1.OneToOne(() => commerce_entity_1.Commerce, (commerce) => commerce.user),
    __metadata("design:type", commerce_entity_1.Commerce)
], User.prototype, "commerce", void 0);
__decorate([
    typeorm_1.OneToMany(() => chat_entity_1.Chat, (chat) => chat.remitente),
    __metadata("design:type", Array)
], User.prototype, "chat", void 0);
__decorate([
    typeorm_1.ManyToMany(() => rols_entity_1.Rols, (rol) => rol.users),
    typeorm_1.JoinTable({
        name: 'USERS_ROLS',
        joinColumns: [
            { name: 'userId' },
        ],
        inverseJoinColumns: [
            { name: 'rolId' },
        ],
    }),
    __metadata("design:type", Array)
], User.prototype, "rols", void 0);
__decorate([
    typeorm_1.OneToMany(type => purchase_entity_1.Purchase, (purchase) => purchase.user),
    __metadata("design:type", purchase_entity_1.Purchase)
], User.prototype, "purchase", void 0);
__decorate([
    typeorm_1.OneToMany(type => commissions_entity_1.Commision, commision => commision.user),
    __metadata("design:type", Array)
], User.prototype, "commisions", void 0);
__decorate([
    typeorm_1.OneToMany(type => cobrarcomision_entity_1.Cobro, cobro => cobro.user),
    __metadata("design:type", Array)
], User.prototype, "cobro", void 0);
__decorate([
    typeorm_1.BeforeInsert(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], User.prototype, "hashPassword", null);
User = __decorate([
    typeorm_1.Entity('USERS')
], User);
exports.User = User;
//# sourceMappingURL=user.entity.js.map