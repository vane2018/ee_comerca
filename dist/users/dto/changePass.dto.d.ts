export declare class ChangePassDto {
    backPassword: string;
    newPassword: string;
    repeatPassword: string;
}
