import { Commision } from '../../commissions/commissions.entity';
import { Commerce } from '../../commerce/commerce.entity';
export declare class UserDto {
    readonly id?: string;
    readonly password: string;
    readonly email: string;
    readonly firstName: string;
    readonly calle: string;
    readonly numero: string;
    readonly localidad: string;
    readonly pais: string;
    readonly provincia: string;
    readonly latitud: string;
    readonly longitud: string;
    readonly lastName: string;
    readonly nickReferrer: string;
    readonly nickName: string;
    readonly phone: string;
    readonly firebaseRegistrationToken: string;
    readonly gender: string;
    readonly dni: string;
    readonly profilePic: string;
    readonly isCustomerAndSeller: boolean;
    readonly fechanacimiento: Date;
    readonly pisodepartamento: string;
    commisions: Commision[];
    commerce: Commerce;
    rols: any;
}
