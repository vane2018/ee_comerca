"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserUpdateDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class UserUpdateDto {
}
__decorate([
    swagger_1.ApiProperty({ description: 'user id' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], UserUpdateDto.prototype, "id", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'Password del Usuario' }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], UserUpdateDto.prototype, "password", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Email' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], UserUpdateDto.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Nombres del usuario' }),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UserUpdateDto.prototype, "firstName", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Apellido del usuario' }),
    __metadata("design:type", String)
], UserUpdateDto.prototype, "lastName", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'Nick del usuario referente' }),
    __metadata("design:type", String)
], UserUpdateDto.prototype, "nickReferrer", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'Sobre nombre del usuario o Nick name' }),
    __metadata("design:type", String)
], UserUpdateDto.prototype, "nickName", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Telefono del usuario' }),
    __metadata("design:type", String)
], UserUpdateDto.prototype, "phone", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Genero del usuario' }),
    __metadata("design:type", String)
], UserUpdateDto.prototype, "gender", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'Dni del Usuario' }),
    __metadata("design:type", String)
], UserUpdateDto.prototype, "dni", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'Foto duuuuuuuuuuu' }),
    __metadata("design:type", String)
], UserUpdateDto.prototype, "profilePic", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'Variable para ver si es customer o seller o los dos', example: 'false' }),
    __metadata("design:type", Boolean)
], UserUpdateDto.prototype, "isCustomerAndSeller", void 0);
exports.UserUpdateDto = UserUpdateDto;
//# sourceMappingURL=userUpdate.dto.js.map