export declare class UserUpdateDto {
    readonly id: string;
    readonly password?: string;
    readonly email: string;
    readonly firstName: string;
    readonly lastName: string;
    readonly nickReferrer: string;
    readonly nickName: string;
    readonly phone: string;
    readonly gender: string;
    readonly dni: string;
    readonly profilePic: string;
    readonly isCustomerAndSeller: boolean;
}
