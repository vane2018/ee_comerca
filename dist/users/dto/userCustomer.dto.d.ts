import { User } from '../user.entity';
import { UserDto } from './user.dto';
export declare class UserCustomerDto extends UserDto {
    readonly customer: User;
}
