"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const commerce_entity_1 = require("../../commerce/commerce.entity");
class UserDto {
}
__decorate([
    swagger_1.ApiProperty({ description: 'Password del Usuario' }),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], UserDto.prototype, "password", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Email' }),
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], UserDto.prototype, "email", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Nombres del usuario' }),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UserDto.prototype, "firstName", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Nombres del usuario' }),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UserDto.prototype, "calle", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Nombres del usuario' }),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UserDto.prototype, "numero", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Nombres del usuario' }),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UserDto.prototype, "localidad", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Nombres del usuario' }),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UserDto.prototype, "pais", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Nombres del usuario' }),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UserDto.prototype, "provincia", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Nombres del usuario' }),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UserDto.prototype, "latitud", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Nombres del usuario' }),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UserDto.prototype, "longitud", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Apellido del usuario' }),
    __metadata("design:type", String)
], UserDto.prototype, "lastName", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'Nick del usuario referente' }),
    __metadata("design:type", String)
], UserDto.prototype, "nickReferrer", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'Sobre nombre del usuario o Nick name' }),
    __metadata("design:type", String)
], UserDto.prototype, "nickName", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Telefono del usuario' }),
    __metadata("design:type", String)
], UserDto.prototype, "phone", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Telefono del usuario' }),
    __metadata("design:type", String)
], UserDto.prototype, "firebaseRegistrationToken", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Genero del usuario' }),
    __metadata("design:type", String)
], UserDto.prototype, "gender", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'Dni del Usuario' }),
    __metadata("design:type", String)
], UserDto.prototype, "dni", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'Foto duuuuuuuuuuu' }),
    __metadata("design:type", String)
], UserDto.prototype, "profilePic", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'Variable para ver si es customer o seller o los dos', example: 'false' }),
    __metadata("design:type", Boolean)
], UserDto.prototype, "isCustomerAndSeller", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'fecha de nacimiento date', type: 'string', format: 'date-time', default: new Date("2019-03-15T19:47:47.975Z") }),
    class_validator_1.IsString(),
    __metadata("design:type", Date)
], UserDto.prototype, "fechanacimiento", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Nombres del usuario' }),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UserDto.prototype, "pisodepartamento", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'commission del usuario' }),
    __metadata("design:type", Array)
], UserDto.prototype, "commisions", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'Commercio del usuario' }),
    __metadata("design:type", commerce_entity_1.Commerce)
], UserDto.prototype, "commerce", void 0);
__decorate([
    swagger_1.ApiProperty({ description: 'Rol de dicho usuario' }),
    __metadata("design:type", Object)
], UserDto.prototype, "rols", void 0);
exports.UserDto = UserDto;
//# sourceMappingURL=user.dto.js.map