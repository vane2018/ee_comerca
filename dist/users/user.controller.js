"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersController = void 0;
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const user_dto_1 = require("../users/dto/user.dto");
const swagger_1 = require("@nestjs/swagger");
const user_entity_1 = require("./user.entity");
const user_service_1 = require("./user.service");
const userFavorite_dto_1 = require("./dto/userFavorite.dto");
const paramsUserDto_1 = require("./dto/paramsUserDto");
const changePass_dto_1 = require("./dto/changePass.dto");
const errorChangePass_dto_1 = require("./dto/errorChangePass.dto");
const user_firebase_dto_1 = require("./dto/user-firebase.dto");
const cbu_dto_1 = require("./dto/cbu.dto");
const consulta_cbu_dto_1 = require("./dto/consulta-cbu.dto");
const modif_cbu_dto_1 = require("./dto/modif-cbu.dto");
const consultar_extraccion_dto_1 = require("./dto/consultar-extraccion.dto");
const notificar_dto_1 = require("./dto/notificar.dto");
const datos_user_rol_dto_1 = require("./dto/datos-user-rol.dto");
const rols_service_1 = require("../rols/rols.service");
let UsersController = class UsersController {
    constructor(service, serviceRol) {
        this.service = service;
        this.serviceRol = serviceRol;
    }
    async getUsers(res) {
        const users = await this.service.getAllUsers();
        return res.status(common_1.HttpStatus.OK).json(users);
    }
    async getCommerce(res, param) {
        const user = await this.service.getUsuarioById(param.userId);
        return res.status(common_1.HttpStatus.OK).json(user);
    }
    async getUser(res, cuerpo, param) {
        const monto = await this.service.consultaMonto(cuerpo.id);
        return res.status(common_1.HttpStatus.OK).json(monto);
    }
    async getReferrers(res, param) {
        const listReferrer = await this.service.getReferrers(param.nickName);
        return res.status(common_1.HttpStatus.OK).json(listReferrer);
    }
    async updateUser(res, param, userDto) {
        try {
            let product = await this.service.findOne(param.id);
            if (product) {
                let productMod = {
                    firebaseRegistrationToken: userDto.firebaseRegistrationToken
                };
                const userUpdate = await this.service.updateToken(param.id, productMod.firebaseRegistrationToken);
                return res.status(common_1.HttpStatus.OK).json(userUpdate);
            }
            else {
                return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: ' user no encontrados' });
            }
        }
        catch (error) {
            console.log('error al modifcar user: ', error);
            return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el user', error });
        }
    }
    async updateCbu(res, param, userDto) {
        try {
            let user = await this.service.findOne(param.id);
            if (user) {
                user.cbu = userDto.cbu;
                const userUpdate = await this.service.modificoUser(user);
                return res.status(common_1.HttpStatus.OK).json(userUpdate);
            }
            else {
                return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: ' user no encontrados' });
            }
        }
        catch (error) {
            console.log('error al modifcar user: ', error);
            return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el user', error });
        }
    }
    async comprobarCbu(res, datosCbu) {
        try {
            let us = await this.service.comprueboCbu(datosCbu.id);
            if (us) {
                return res.status(common_1.HttpStatus.OK).json(us);
            }
            else {
                return res.status(common_1.HttpStatus.OK).json("false");
            }
        }
        catch (_a) {
            return false;
        }
    }
    async ChangePassword(res, param, changePassDto) {
        try {
            const resUser = await this.service.findOne({ id: param.userId });
            if (resUser) {
                const isValidPass = await resUser.comparePassword(changePassDto.backPassword);
                const backSamePass = await this.service.comparePassword(changePassDto.backPassword, changePassDto.newPassword);
                const repatPassSame = await this.service.comparePassword(changePassDto.newPassword, changePassDto.repeatPassword);
                if (!isValidPass) {
                    return res.status(common_1.HttpStatus.BAD_REQUEST).json({ error: 'invalid password' });
                }
                if (backSamePass) {
                    return res.status(common_1.HttpStatus.CONFLICT).json({ error: 'new and back password not should same' });
                }
                if (!repatPassSame) {
                    return res.status(common_1.HttpStatus.CONFLICT).json({ error: 'new and repeat password should same' });
                }
                await this.service.changePass(resUser, changePassDto.newPassword);
                return res.status(common_1.HttpStatus.OK).json({ message: 'pass changed succesfull' });
            }
            else {
                return res.status(common_1.HttpStatus.NOT_FOUND).json({
                    statusCode: 404,
                    error: 'user not found',
                    message: 'user not found',
                });
            }
        }
        catch (error) {
            console.log('error: ', error);
            return res.status(common_1.HttpStatus.INTERNAL_SERVER_ERROR).json({ error: 'Internal Server Error' });
        }
    }
    async saveFavorite(res, updateUserDto, param) {
        try {
            let users = await this.service.findOne(param.id);
            if (users) {
                users.favorite = updateUserDto.favorite;
                let usuario = await this.service.saveProduct(users);
                return res.status(common_1.HttpStatus.OK).json(usuario);
            }
            else {
                return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'users no encontrado' });
            }
        }
        catch (error) {
            console.log('error al crear user: ', error);
            return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el user', error });
        }
    }
    async guardarCbu(res, datosCbu) {
        try {
            this.service.saveCbuReferido(datosCbu.user, datosCbu.cbu);
            return res.status(common_1.HttpStatus.OK).json({ mensaje: 'se guardo correctamente', statusCode: 200 });
        }
        catch (_a) {
            console.log("error");
        }
    }
    async notificar(res, datos) {
        try {
            let user = await this.service.getUser();
            await this.service.notificar(user.id, datos);
            return res.status(common_1.HttpStatus.OK).json({ message: 'notificacion enviada' });
        }
        catch (error) {
            console.log("error");
            return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al enviar notificacion', error });
        }
    }
    async agregorol(res, datos) {
        const rol = await this.serviceRol.findRol({ name: 'empleado' });
        let usuario = await this.service.updateUserRol(datos.idUser, rol);
        return res.status(common_1.HttpStatus.OK).json(usuario);
    }
    async agregorolSuperAdmin(res, datos) {
        const rol = await this.serviceRol.findRol({ name: 'super' });
        let usuario = await this.service.updateUserRol(datos.idUser, rol);
        return res.status(common_1.HttpStatus.OK).json(usuario);
    }
};
__decorate([
    common_1.Get(),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getUsers", null);
__decorate([
    common_1.Get('/:userId'),
    swagger_1.ApiOperation({ summary: 'Obtiene un usuario por su Id' }),
    swagger_1.ApiResponse({ status: 200, description: 'Los datos han sido devueltos correctamente.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getCommerce", null);
__decorate([
    common_1.Post('/monto/consulta/extraccion/userId'),
    swagger_1.ApiOperation({ summary: 'Obtiene un usuario por su Id' }),
    swagger_1.ApiResponse({ status: 200, description: 'Los datos han sido devueltos correctamente.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Body()), __param(2, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, consultar_extraccion_dto_1.cunsultaDto, Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getUser", null);
__decorate([
    common_1.Get('referrers/:nickName'),
    swagger_1.ApiParam({ type: 'string', name: 'nickName' }),
    __param(0, common_1.Response()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getReferrers", null);
__decorate([
    common_1.Put('/editUser/:id'),
    __param(0, common_1.Response()), __param(1, common_1.Param()), __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, user_firebase_dto_1.UserFirebaseDto]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "updateUser", null);
__decorate([
    common_1.Put('/editCbu/:id'),
    __param(0, common_1.Response()), __param(1, common_1.Param()), __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, modif_cbu_dto_1.modificarCbuDto]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "updateCbu", null);
__decorate([
    common_1.Post('comprobarCbu'),
    swagger_1.ApiOperation({ summary: 'password change' }),
    swagger_1.ApiResponse({ status: 200, description: 'was changed password' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 403, description: "Forbidden" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiConflictResponse({ description: 'conflict with password form', status: 409, type: errorChangePass_dto_1.ErrorChangePassDto }),
    swagger_1.ApiResponse({ status: 500, description: "internal server error" }),
    swagger_1.ApiBody({ type: consulta_cbu_dto_1.consultaCbu }),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, consulta_cbu_dto_1.consultaCbu]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "comprobarCbu", null);
__decorate([
    common_1.Post('change-password/:userId'),
    swagger_1.ApiOperation({ summary: 'password change' }),
    swagger_1.ApiResponse({ status: 200, description: 'was changed password' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 403, description: "Forbidden" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiConflictResponse({ description: 'conflict with password form', status: 409, type: errorChangePass_dto_1.ErrorChangePassDto }),
    swagger_1.ApiResponse({ status: 500, description: "internal server error" }),
    swagger_1.ApiBody({ type: changePass_dto_1.ChangePassDto }),
    __param(0, common_1.Response()), __param(1, common_1.Param()), __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, paramsUserDto_1.ParamsUserDto, changePass_dto_1.ChangePassDto]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "ChangePassword", null);
__decorate([
    common_1.Post('favorito/:id'),
    __param(0, common_1.Res()), __param(1, common_1.Body()), __param(2, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, userFavorite_dto_1.UserFavoriteDto, Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "saveFavorite", null);
__decorate([
    common_1.Post('guardarCbu'),
    swagger_1.ApiOperation({ summary: 'password change' }),
    swagger_1.ApiResponse({ status: 200, description: 'was changed password' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 403, description: "Forbidden" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiConflictResponse({ description: 'conflict with password form', status: 409, type: errorChangePass_dto_1.ErrorChangePassDto }),
    swagger_1.ApiResponse({ status: 500, description: "internal server error" }),
    swagger_1.ApiBody({ type: cbu_dto_1.datosCbu }),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, cbu_dto_1.datosCbu]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "guardarCbu", null);
__decorate([
    common_1.Post('notificarUser'),
    swagger_1.ApiOperation({ summary: 'password change' }),
    swagger_1.ApiResponse({ status: 200, description: 'was changed password' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 403, description: "Forbidden" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiConflictResponse({ description: 'conflict with password form', status: 409, type: errorChangePass_dto_1.ErrorChangePassDto }),
    swagger_1.ApiResponse({ status: 500, description: "internal server error" }),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, notificar_dto_1.bodyMensajeDto]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "notificar", null);
__decorate([
    common_1.Post('addUserRol/empleado'),
    swagger_1.ApiOperation({ summary: 'asigna el rol de empleado, ingrese idUser' }),
    swagger_1.ApiResponse({ status: 200, description: 'was changed password' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 403, description: "Forbidden" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiConflictResponse({ description: 'conflict with password form', status: 409, type: errorChangePass_dto_1.ErrorChangePassDto }),
    swagger_1.ApiResponse({ status: 500, description: "internal server error" }),
    swagger_1.ApiBody({ type: datos_user_rol_dto_1.addRol }),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, datos_user_rol_dto_1.addRol]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "agregorol", null);
__decorate([
    common_1.Post('addUserRol/superAdmin'),
    swagger_1.ApiOperation({ summary: 'asigna el rol de SuperAdmin, ingrese idUser' }),
    swagger_1.ApiResponse({ status: 200, description: 'was changed password' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 403, description: "Forbidden" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiConflictResponse({ description: 'conflict with password form', status: 409, type: errorChangePass_dto_1.ErrorChangePassDto }),
    swagger_1.ApiResponse({ status: 500, description: "internal server error" }),
    swagger_1.ApiBody({ type: datos_user_rol_dto_1.addRol }),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, datos_user_rol_dto_1.addRol]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "agregorolSuperAdmin", null);
UsersController = __decorate([
    crud_1.Crud({
        model: {
            type: user_entity_1.User,
        },
        routes: {
            createOneBase: {
                decorators: [swagger_1.ApiBody({ type: user_dto_1.UserDto })]
            }
        },
        serialize: {
            create: user_dto_1.UserDto,
        },
        params: {
            id: {
                field: 'id',
                type: 'uuid',
                primary: true,
            },
        },
        query: {
            join: {
                commisions: {
                    allow: []
                },
                commerce: {}
            },
        }
    }),
    swagger_1.ApiTags('Users'),
    common_1.Controller('users'),
    __metadata("design:paramtypes", [user_service_1.UsersService,
        rols_service_1.RolsService])
], UsersController);
exports.UsersController = UsersController;
//# sourceMappingURL=user.controller.js.map