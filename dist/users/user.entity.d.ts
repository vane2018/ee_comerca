import { UserDto } from './dto/user.dto';
import { Commerce } from '../commerce/commerce.entity';
import { Rols } from '../rols/rols.entity';
import { Purchase } from '../purchases/purchase.entity';
import { Commision } from '../commissions/commissions.entity';
import { Chat } from '../chat/chat.entity';
import { Cobro } from '../cobrarcomision/cobrarcomision.entity';
declare const enum tipoUsuario {
    customer = "customer",
    seller = "seller",
    admin = "admin"
}
export declare class User {
    id: string;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    nickReferrer: string;
    nickName: string;
    phone: string;
    gender: string;
    dni: string;
    cbu: string;
    profilePic: string;
    isCustomerAndSeller: boolean;
    lastAccess: tipoUsuario;
    favorite: string[];
    firebaseRegistrationToken: string;
    pais: string;
    provincia: string;
    localidad: string;
    latitud: string;
    longitud: string;
    calle: string;
    numero: string;
    montoextraido: number;
    estadocobrocomision: string;
    fechanacimiento: Date;
    pisodepartamento: string;
    commerce: Commerce;
    chat: Chat[];
    rols: Rols[];
    purchase: Purchase;
    commisions: Commision[];
    user: any;
    cobro: Cobro[];
    hashPassword(): Promise<void>;
    comparePassword(attempt: string): Promise<any>;
    compareNickname(nick: string): Promise<boolean>;
    toResponseObject(showToken?: boolean): UserDto;
}
export {};
