"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const crud_typeorm_1 = require("@nestjsx/crud-typeorm");
const bcrypt = require("bcrypt");
const user_entity_1 = require("./user.entity");
const firebase = require("firebase-admin");
const chat_entity_1 = require("../chat/chat.entity");
let UsersService = class UsersService extends crud_typeorm_1.TypeOrmCrudService {
    constructor(repo) {
        super(repo);
        this.getAllUsers = async () => {
            return await this.repo.find({ relations: ['rols'] });
        };
        this.getUserValidateToken = async (id) => {
            return await this.repo.findOne(id, { relations: ['rols'] });
        };
    }
    async getUsuarioById(id) {
        let usuario = await this.repo.findOne(id, { relations: ['commisions', 'commerce', 'rols'] });
        return usuario;
    }
    async findByEmail(userEmail) {
        const status = {
            success: false,
            message: 'user not found',
            user: null,
        };
        try {
            const user = await this.repo.findOne({ email: userEmail });
            if (user) {
                const status = {
                    success: true,
                    message: 'user finded',
                    user,
                };
                return status;
            }
            return status;
        }
        catch (error) {
            console.log('error en buscar por email: ', error);
        }
    }
    async addCobro(idCustomer, comisionn) {
        let custom = await this.repo.findOne(idCustomer, { relations: ['cobro'] });
        custom.cobro.push(comisionn);
        await this.repo.save(custom);
    }
    async findByNumCel(nroCel) {
        const status = {
            success: false,
            message: 'user not found',
            user: null,
        };
        try {
            const user = await this.repo.findOne({ phone: nroCel });
            if (user) {
                const status = {
                    success: true,
                    message: 'user finded',
                    user,
                };
                return status;
            }
            return status;
        }
        catch (error) {
            console.log('error en buscar por nro: ', error);
        }
    }
    async guardarComision(user, total) {
        let usuario = await this.repo.findOne(user.id);
        usuario.montoextraido = total;
        return this.repo.save(usuario);
    }
    async login(login) {
        try {
            const user = await this.repo.findOne({ where: [{ email: login.email }, { nickName: login.nickName }], });
            if (user) {
                const pass = await user.comparePassword(login.password);
                const nick = await user.compareNickname(login.nickName);
                if (pass) {
                    return { success: true, user: user };
                }
                else {
                    return { success: pass, user: null };
                }
            }
            return { success: false, user: null };
        }
        catch (error) {
            console.log('pass o mail incorrectos');
        }
    }
    async findByNickName(nickName) {
        return await this.repo.findOne({ nickName });
    }
    async findByEmailForValidate(email) {
        return await this.repo.findOne({ email });
    }
    async findById(id) {
        const user = await this.repo.findOne(id);
        const status = {
            success: true,
            message: 'user finded',
            user,
        };
        return status;
    }
    async bucarporId(id) {
        let user = this.repo.findOne({ where: { id: id } });
        return user;
    }
    async register(userDto) {
        const { email } = userDto;
        let user = await this.repo.findOne({ where: { email } });
        if (user) {
            throw new common_1.HttpException('User already exists', common_1.HttpStatus.BAD_REQUEST);
        }
        try {
            userDto.rols = [];
            user = this.repo.create(userDto);
            return await this.repo.save(user);
        }
        catch (error) {
            console.log('error: ', error);
        }
    }
    async updateUser(userDto, rolDto) {
        const user = await this.repo.findOne({ where: { email: userDto.email }, relations: ['rols'] });
        if (user.rols.find(role => (role.name === "customer"))) {
            user.isCustomerAndSeller = !!userDto.isCustomerAndSeller;
        }
        user.rols.push(rolDto);
        return this.repo.save(user);
    }
    async updateUserRol(id, rolDto) {
        const user = await this.repo.findOne({ where: { id: id }, relations: ['rols'] });
        user.rols.push(rolDto);
        return this.repo.save(user);
    }
    async updatePassword(credential) {
        let user;
        if (credential.email) {
            user = await this.repo.findOne({ where: { email: credential.email } });
        }
        else {
            user = await this.repo.findOne({ where: { phone: credential.nroCel } });
        }
        user.password = await bcrypt.hash(credential.pass, 10);
        await this.repo.save(user);
    }
    async updateToken(token, idUser) {
        let user = await this.repo.findOne(idUser);
        user.firebaseRegistrationToken = token;
        return await this.repo.save(user);
    }
    async addCommisions(idCustomer, comisionn) {
        let custom = await this.repo.findOne(idCustomer, { relations: ['commisions'] });
        custom.commisions.push(comisionn);
        await this.repo.save(custom);
    }
    async changePass(user, password) {
        user.password = await bcrypt.hash(password, 10);
        return this.repo.save(user);
    }
    async comparePassword(password1, password2) {
        return password1 === password2;
    }
    async saveProduct(productDto) {
        return await this.repo.save(productDto);
    }
    async getReferrers(nickName) {
        const referrers = this.repo.find({ where: { nickReferrer: nickName } });
        return referrers;
    }
    async descontarComision(user) {
        user.commisions.push();
    }
    async notificar(id, datos) {
        try {
            var usuario = this.repo.findOne(id);
            const notificacionId = `nueva-comision`;
            if ((await usuario).firebaseRegistrationToken) {
                firebase.messaging().sendToDevice((await usuario).firebaseRegistrationToken, {
                    data: {
                        notificacionId: notificacionId,
                        tipo: chat_entity_1.TipoNotificacion.Comision
                    },
                    notification: {
                        tag: notificacionId,
                        title: `Tiene un mensaje cobro de comision`,
                        body: `nombre ${datos.nombre}, apellido ${datos.apellido}, nickname ${datos.montoExtraccioon}, cbu ${datos.cbu}`,
                    }
                });
            }
        }
        catch (error) {
            console.log(error);
        }
    }
    async saveCbuReferido(referer, cbu) {
        let user = await this.repo.findOne({ where: { id: referer } });
        user.cbu = cbu;
        this.repo.save(user);
    }
    async modificoUser(user) {
        return this.repo.save(user);
    }
    async consultaMonto(id) {
        let usuarios = await this.repo.findOne(id);
        let montoExtraccion = usuarios.montoextraido;
        return montoExtraccion;
    }
    async getUser() {
        let usuario = await this.repo.findOne({ email: "comprartir@gmail.com" });
        return usuario;
    }
    async comprueboCbu(id) {
        let user = await this.repo.findOne({ where: { id: id } });
        if (user.cbu !== "") {
            return user.cbu;
        }
        else {
            return false;
        }
    }
    async getCobroByUserId(userId) {
        let comisions = await this.repo.findOne({ where: { user: userId } });
        return comisions;
    }
};
UsersService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_entity_1.User)),
    __metadata("design:paramtypes", [Object])
], UsersService);
exports.UsersService = UsersService;
//# sourceMappingURL=user.service.js.map