import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { RegistrationStatus } from './interface/reponseStatus.interface';
import { CredentialsDto } from '../auth/dto/credentials.dto';
import { User } from './user.entity';
import { UserDto } from './dto/user.dto';
import { bodyMensajeDto } from './dto/notificar.dto';
export declare class UsersService extends TypeOrmCrudService<User> {
    constructor(repo: any);
    getAllUsers: () => Promise<any[]>;
    getUsuarioById(id: string): Promise<any>;
    getUserValidateToken: (id: string) => Promise<any>;
    findByEmail(userEmail: string): Promise<RegistrationStatus>;
    addCobro(idCustomer: string, comisionn: any): Promise<any>;
    findByNumCel(nroCel: string): Promise<RegistrationStatus>;
    guardarComision(user: User, total: number): Promise<User>;
    login(login: any): Promise<any>;
    findByNickName(nickName: string): Promise<User>;
    findByEmailForValidate(email: string): Promise<User>;
    findById(id: string): Promise<RegistrationStatus>;
    bucarporId(id: string): Promise<User>;
    register(userDto: UserDto): Promise<UserDto>;
    updateUser(userDto: UserDto, rolDto: any): Promise<User>;
    updateUserRol(id: string, rolDto: any): Promise<User>;
    updatePassword(credential: CredentialsDto): Promise<void>;
    updateToken(token: string, idUser: string): Promise<User>;
    addCommisions(idCustomer: string, comisionn: any): Promise<any>;
    changePass(user: User, password: any): Promise<User>;
    comparePassword(password1: string, password2: string): Promise<boolean>;
    saveProduct(productDto: UserDto): Promise<any>;
    getReferrers(nickName: string): Promise<any>;
    descontarComision(user: User): Promise<void>;
    notificar(id: string, datos: bodyMensajeDto): Promise<void>;
    saveCbuReferido(referer: string, cbu: string): Promise<void>;
    modificoUser(user: User): Promise<User>;
    consultaMonto(id: string): Promise<number>;
    getUser(): Promise<User>;
    comprueboCbu(id: string): Promise<string | false>;
    getCobroByUserId(userId: string): Promise<any>;
}
