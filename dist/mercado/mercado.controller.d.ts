import { mercadoPagoQueryDto } from './dto/mercado-pago-query.dto';
import { mercadoDto } from './dto/mercado-paggo.dto';
import { MercadoService } from './mercado.service';
import { CommerceService } from '../commerce/commerce.service';
export declare class MercadoController {
    serviceMercado: MercadoService;
    commerceService: CommerceService;
    constructor(serviceMercado: MercadoService, commerceService: CommerceService);
    gestionarPago(res: any, query: mercadoPagoQueryDto, credenciales: mercadoDto): Promise<any>;
    obtenerDatos(res: any): Promise<any>;
}
