"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Mercado = void 0;
const commerce_entity_1 = require("../commerce/commerce.entity");
const typeorm_1 = require("typeorm");
let Mercado = class Mercado {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Mercado.prototype, "id", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], Mercado.prototype, "email", void 0);
__decorate([
    typeorm_1.Column({ type: "float8", nullable: true }),
    __metadata("design:type", Number)
], Mercado.prototype, "transaction_amount", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], Mercado.prototype, "status", void 0);
__decorate([
    typeorm_1.Column({ type: "float8", nullable: true }),
    __metadata("design:type", Number)
], Mercado.prototype, "application_fee", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", Array)
], Mercado.prototype, "fee_details", void 0);
__decorate([
    typeorm_1.OneToOne(() => commerce_entity_1.Commerce, (commerce) => commerce.merc),
    __metadata("design:type", commerce_entity_1.Commerce)
], Mercado.prototype, "commerce", void 0);
Mercado = __decorate([
    typeorm_1.Entity("MERCADO")
], Mercado);
exports.Mercado = Mercado;
//# sourceMappingURL=mercado.entity.js.map