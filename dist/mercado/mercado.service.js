"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MercadoService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const axios_1 = require("axios");
const crud_typeorm_1 = require("@nestjsx/crud-typeorm");
const mercado_entity_1 = require("./mercado.entity");
const env_config_1 = require("../common/env.config");
let MercadoService = class MercadoService extends crud_typeorm_1.TypeOrmCrudService {
    constructor(repo) {
        super(repo);
    }
    async realizarPago(query, credenciales) {
        try {
            const token = credenciales.token;
            const payment_method_id = credenciales.payment_method_id;
            const installments = parseInt(credenciales.installments);
            const issuer_id = credenciales.issuer_id;
            const transaction_amoun = Number(query.transaction_amount);
            const shipping_amount = Number(query.shipping_amount);
            const product_price = transaction_amoun - shipping_amount;
            const comprartir_commission = (product_price * 0.07);
            const application_fee = shipping_amount + comprartir_commission;
            const email = query.email;
            const commerceId = query.commerceId;
            var payment_data = {
                transaction_amount: transaction_amoun,
                token: token,
                issuer_id: issuer_id,
                installments: installments,
                payment_method_id: payment_method_id,
                binary_mode: true,
                application_fee: application_fee,
                payer: {
                    email: email
                }
            };
            var commerce_id = {
                comerceId: commerceId
            };
            const mivar = await axios_1.default({
                method: 'post',
                url: env_config_1.ENV.URL_API + '/api/v1/mercado-pago/apis/v1/mercado-pago/exist/comercioId/{comercioId}',
                data: commerce_id,
                headers: {
                    'accept': 'application/json',
                    'content-type': 'application/json',
                }
            });
            var sellerAccessToken = mivar.data;
            const miletra = await axios_1.default({
                method: 'post',
                url: 'https://api.mercadopago.com/v1/payments',
                data: payment_data,
                headers: {
                    'accept': 'application/json',
                    'content-type': 'application/json',
                    'Authorization': `Bearer ${sellerAccessToken}`
                }
            });
            var productMod = {
                email: miletra.data.payer.email,
                transaction_amount: miletra.data.transaction_amount,
                status: miletra.data.status,
                application_fee: payment_data.application_fee,
                fee_details: miletra.data.fee_details
            };
            this.guardar(productMod);
            return miletra.data;
        }
        catch (error) {
            console.log("error", error);
        }
    }
    async guardar(midtao) {
        await this.repo.save(midtao);
    }
    async getdatos() {
        let vector = await this.repo.find();
        return vector;
    }
};
MercadoService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(mercado_entity_1.Mercado)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], MercadoService);
exports.MercadoService = MercadoService;
//# sourceMappingURL=mercado.service.js.map