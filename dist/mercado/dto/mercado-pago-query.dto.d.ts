export declare class mercadoPagoQueryDto {
    transaction_amount: string;
    email: string;
    commerceId: string;
    shipping_amount: string;
}
