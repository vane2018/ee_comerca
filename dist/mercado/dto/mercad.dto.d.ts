export declare class mercadDto {
    email: string;
    transaction_amount: number;
    status: string;
    application_fee: number;
    fee_details: [];
}
