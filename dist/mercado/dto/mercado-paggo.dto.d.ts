export declare class mercadoDto {
    token: string;
    payment_method_id: string;
    installments: string;
    issuer_id: string;
}
