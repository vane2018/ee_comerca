import { Commerce } from "../commerce/commerce.entity";
export declare class Mercado {
    id: string;
    email: string;
    transaction_amount: number;
    status: string;
    application_fee: number;
    fee_details: [];
    commerce: Commerce;
}
