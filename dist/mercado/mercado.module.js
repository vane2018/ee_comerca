"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MercadoModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const commerce_module_1 = require("../commerce/commerce.module");
const mercado_controller_1 = require("./mercado.controller");
const mercado_entity_1 = require("./mercado.entity");
const mercado_service_1 = require("./mercado.service");
let MercadoModule = class MercadoModule {
};
MercadoModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([mercado_entity_1.Mercado]), commerce_module_1.CommerceModule],
        controllers: [mercado_controller_1.MercadoController],
        providers: [mercado_service_1.MercadoService]
    })
], MercadoModule);
exports.MercadoModule = MercadoModule;
//# sourceMappingURL=mercado.module.js.map