import { Repository } from 'typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { mercadoPagoQueryDto } from './dto/mercado-pago-query.dto';
import { mercadoDto } from './dto/mercado-paggo.dto';
import { Mercado } from './mercado.entity';
import { mercadDto } from './dto/mercad.dto';
export declare class MercadoService extends TypeOrmCrudService<Mercado> {
    constructor(repo: Repository<Mercado>);
    realizarPago(query: mercadoPagoQueryDto, credenciales: mercadoDto): Promise<any>;
    guardar(midtao: mercadDto): Promise<void>;
    getdatos(): Promise<Mercado[]>;
}
