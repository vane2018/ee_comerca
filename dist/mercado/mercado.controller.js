"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MercadoController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const mercado_pago_query_dto_1 = require("./dto/mercado-pago-query.dto");
const mercado_paggo_dto_1 = require("./dto/mercado-paggo.dto");
const mercado_service_1 = require("./mercado.service");
const commerce_service_1 = require("../commerce/commerce.service");
const common_2 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const permission_guards_1 = require("../common/guards/permission.guards");
const permission_decorator_1 = require("../common/permission/permission.decorator");
const env_config_1 = require("../common/env.config");
let MercadoController = class MercadoController {
    constructor(serviceMercado, commerceService) {
        this.serviceMercado = serviceMercado;
        this.commerceService = commerceService;
    }
    async gestionarPago(res, query, credenciales) {
        let mercado = await this.serviceMercado.realizarPago(query, credenciales);
        if (mercado.status === 'approved') {
            return res.redirect(env_config_1.ENV.APP_FRONT_URL + '/mercado-pago/loading-sppiner/?success');
        }
        else {
            if (mercado.status === 'rejected') {
                this.serviceMercado.guardar(mercado);
                return res.redirect(env_config_1.ENV.APP_FRONT_URL + '/mercado-pago/loading-sppiner/?failure');
            }
        }
    }
    async obtenerDatos(res) {
        let datos = await this.serviceMercado.getdatos();
        return res.status(common_1.HttpStatus.OK).json(datos);
    }
};
__decorate([
    common_1.Post('/apis/v1/mercado-pago/gestion-pagos'),
    swagger_1.ApiOperation({ summary: 'crea comercio' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    swagger_1.ApiBody({ type: mercado_paggo_dto_1.mercadoDto && mercado_pago_query_dto_1.mercadoPagoQueryDto }),
    __param(0, common_1.Response()), __param(1, common_1.Query()), __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, mercado_pago_query_dto_1.mercadoPagoQueryDto, mercado_paggo_dto_1.mercadoDto]),
    __metadata("design:returntype", Promise)
], MercadoController.prototype, "gestionarPago", null);
__decorate([
    common_1.Get('/apis/v1/mercado-pago/gestion'),
    common_2.UseGuards(passport_1.AuthGuard('jwt'), permission_guards_1.PermissionGuard),
    permission_decorator_1.Permision('admin', 'super'),
    swagger_1.ApiOperation({ summary: 'crea comercio' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], MercadoController.prototype, "obtenerDatos", null);
MercadoController = __decorate([
    swagger_1.ApiTags('Mercado'),
    common_1.Controller('mercado'),
    __metadata("design:paramtypes", [mercado_service_1.MercadoService,
        commerce_service_1.CommerceService])
], MercadoController);
exports.MercadoController = MercadoController;
//# sourceMappingURL=mercado.controller.js.map