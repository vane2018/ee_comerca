"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommerceController = void 0;
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const commerce_service_1 = require("./commerce.service");
const swagger_1 = require("@nestjs/swagger");
const commerce_entity_1 = require("./commerce.entity");
const commerce_dto_1 = require("./dto/commerce.dto");
const createCommerce_dto_1 = require("./dto/createCommerce.dto");
const user_service_1 = require("../users/user.service");
const rols_service_1 = require("../rols/rols.service");
const localDTO_dto_1 = require("./dto/localDTO.dto");
const departamento_dto_1 = require("./dto/departamento.dto");
const provincia_dto_1 = require("./dto/provincia.dto");
const ubicacion_dto_1 = require("./dto/ubicacion.dto");
const update_dto_1 = require("./dto/update.dto");
const obtenerComercio_dto_1 = require("./dto/obtenerComercio.dto");
const localidadCadygo_dto_1 = require("./dto/localidadCadygo.dto");
let CommerceController = class CommerceController {
    constructor(service, userService, rolsService) {
        this.service = service;
        this.userService = userService;
        this.rolsService = rolsService;
    }
    async postCommerce(res, createCommerceDto) {
        try {
            let seller = await this.userService.findOne(createCommerceDto.sellerId);
            if (seller) {
                const commerceACrear = Object.assign({ seller }, createCommerceDto.commerce);
                commerceACrear.user = await this.userService.findOne(createCommerceDto.sellerId);
                const resp = await this.userService.findById(createCommerceDto.sellerId);
                const rol = await this.rolsService.findRol({ name: 'seller' });
                await this.userService.updateUser(resp.user, rol);
                commerceACrear.user = await this.userService.findOne(createCommerceDto.sellerId);
                commerceACrear.pisoDepto = createCommerceDto.commerce.pisoDepto;
                const commerce = await this.service.saveCommerce(commerceACrear);
                console.log("COMERCIO GUARDADO", commerce);
                return res.status(common_1.HttpStatus.OK).json(commerce);
            }
            else {
            }
        }
        catch (error) {
        }
    }
    async getSellers(res) {
        let commerces = await this.service.getAllCommerces();
        return res.status(common_1.HttpStatus.OK).json(commerces);
    }
    async obtenerLocalidadCadygo(res, localidadDTO) {
        let commerces = await this.service.obtenerlocalidades(localidadDTO.localidad);
        return res.status(common_1.HttpStatus.OK).json(commerces);
    }
    async crearComercios(res, comercio) {
        try {
            let comercios = await this.service.crearComercio(comercio);
            return res.status(common_1.HttpStatus.OK).json(comercios);
        }
        catch (error) {
            console.log(error);
            return res.status(common_1.HttpStatus.INTERNAL_SERVER_ERROR).json(error);
        }
    }
    async getProvincias(res, provDto) {
        let provincias = await this.service.obtenerprovincia(provDto.pais);
        return res.status(common_1.HttpStatus.OK).json(provincias);
    }
    async getPais(res) {
        let pais = await this.service.getpaises();
        return res.status(common_1.HttpStatus.OK).json(pais);
    }
    async getlocalidad(res, localidadDTO) {
        console.log("entro a localidades");
        let pais = await this.service.getlocalidades(localidadDTO.provincia);
        return res.status(common_1.HttpStatus.OK).json(pais);
    }
    async getDepartamentp(res, departamentoDTO) {
        let pais = await this.service.getdepartamento(departamentoDTO.provincia);
        return res.status(common_1.HttpStatus.OK).json(pais);
    }
    async getProductUbicacion(res, ubicDTO) {
        let pais = await this.service.searchUbicacion(ubicDTO.provincia);
        return res.status(common_1.HttpStatus.OK).json(pais);
    }
    async gettodos(res) {
        let tod = await this.service.obtenerTodo();
        return res.status(common_1.HttpStatus.OK).json(tod);
    }
    async getCommerce(res, param) {
        const commerce = await this.service.getCommerceById(param.commerceId);
        return res.status(common_1.HttpStatus.OK).json(commerce);
    }
    async putProducts(res, param, createProductDto) {
        try {
            let product = await this.service.findOne(param.id);
            if (product) {
                let productMod = {
                    id: param.id,
                    name: createProductDto.name,
                    adress: createProductDto.adress,
                    numero: createProductDto.numero,
                    cuit: createProductDto.cuit,
                    pais: createProductDto.pais,
                    provincia: createProductDto.provincia,
                    localidad: createProductDto.localidad,
                    latitud: createProductDto.latitud,
                    longitud: createProductDto.longitud,
                    profilePic: createProductDto.profilePic,
                    coverPic: createProductDto.coverPic,
                    pisoDepto: createProductDto.pisoDepto
                };
                const producto = await this.service.updateComerce(productMod);
                return res.status(common_1.HttpStatus.OK).json(producto);
            }
            else {
                return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: ' producto no encontrados' });
            }
        }
        catch (error) {
            console.log('error al modifcar producto: ', error);
            return res.status(common_1.HttpStatus.BAD_REQUEST).json({ message: 'ocurrio un error al guardar el producto', error });
        }
    }
};
__decorate([
    common_1.Post('register'),
    swagger_1.ApiOperation({ summary: 'Registra un commercio' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    swagger_1.ApiCreatedResponse({ description: 'The Commerce has been successfully created.', type: commerce_dto_1.CommerceDTO, status: 200 }),
    swagger_1.ApiBody({ type: createCommerce_dto_1.CreateCommerceDTO }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, createCommerce_dto_1.CreateCommerceDTO]),
    __metadata("design:returntype", Promise)
], CommerceController.prototype, "postCommerce", null);
__decorate([
    common_1.Get(),
    swagger_1.ApiOperation({ summary: 'Obtiene todos los comercios' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CommerceController.prototype, "getSellers", null);
__decorate([
    common_1.Post('/localidad/cadygo'),
    swagger_1.ApiOperation({ summary: 'Obtiene lacalidades cadygo' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, localidadCadygo_dto_1.localidadCadygo]),
    __metadata("design:returntype", Promise)
], CommerceController.prototype, "obtenerLocalidadCadygo", null);
__decorate([
    common_1.Post('/cadygo/crear-comercio'),
    swagger_1.ApiOperation({ summary: 'crea comercio' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    swagger_1.ApiBody({ type: obtenerComercio_dto_1.obtenerComercio }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, obtenerComercio_dto_1.obtenerComercio]),
    __metadata("design:returntype", Promise)
], CommerceController.prototype, "crearComercios", null);
__decorate([
    common_1.Post('provincias'),
    swagger_1.ApiOperation({ summary: 'Obtiene todas las provincias' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, provincia_dto_1.provinciaDto]),
    __metadata("design:returntype", Promise)
], CommerceController.prototype, "getProvincias", null);
__decorate([
    common_1.Get('pais'),
    swagger_1.ApiOperation({ summary: 'Obtiene todas los Paises' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CommerceController.prototype, "getPais", null);
__decorate([
    common_1.Post('localidad'),
    swagger_1.ApiOperation({ summary: 'Obtiene todas las localidades' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, localDTO_dto_1.localDto]),
    __metadata("design:returntype", Promise)
], CommerceController.prototype, "getlocalidad", null);
__decorate([
    common_1.Post('departamentos'),
    swagger_1.ApiOperation({ summary: 'Obtiene todos los departamentos' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, departamento_dto_1.depaDto]),
    __metadata("design:returntype", Promise)
], CommerceController.prototype, "getDepartamentp", null);
__decorate([
    common_1.Post('ubicacion'),
    swagger_1.ApiOperation({ summary: 'busquedax ubicacion' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, ubicacion_dto_1.ubicacionDto]),
    __metadata("design:returntype", Promise)
], CommerceController.prototype, "getProductUbicacion", null);
__decorate([
    common_1.Get('/paises/provincia'),
    swagger_1.ApiOperation({ summary: 'obtiene paises con provincias' }),
    swagger_1.ApiResponse({ status: 200, description: 'data returned correctly' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 400, description: "Bad Request" }),
    swagger_1.ApiResponse({ status: 404, description: "'The resource was not found'" }),
    swagger_1.ApiResponse({ status: 409, description: "Conflict" }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CommerceController.prototype, "gettodos", null);
__decorate([
    common_1.Get('/:id'),
    swagger_1.ApiOperation({ summary: 'Obtiene un comercio por su Id' }),
    swagger_1.ApiResponse({ status: 200, description: 'Los datos han sido devueltos correctamente.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    swagger_1.ApiResponse({ status: 500, description: 'Internal Server Error.' }),
    __param(0, common_1.Response()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CommerceController.prototype, "getCommerce", null);
__decorate([
    common_1.Put('/edit/:id'),
    __param(0, common_1.Response()), __param(1, common_1.Param()), __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, update_dto_1.updateDTO]),
    __metadata("design:returntype", Promise)
], CommerceController.prototype, "putProducts", null);
CommerceController = __decorate([
    crud_1.Crud({
        model: {
            type: commerce_entity_1.Commerce,
        },
        params: {
            id: {
                field: 'id',
                type: 'uuid',
                primary: true,
            },
        },
        query: {
            join: {
                users: {
                    allow: [],
                },
                products: {
                    allow: []
                }
            },
        },
    }),
    swagger_1.ApiTags('Commerces'),
    common_1.Controller('commerces'),
    __metadata("design:paramtypes", [commerce_service_1.CommerceService,
        user_service_1.UsersService,
        rols_service_1.RolsService])
], CommerceController);
exports.CommerceController = CommerceController;
//# sourceMappingURL=commerce.controller.js.map