import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Commerce } from './commerce.entity';
import { CommerceDTO } from './dto/commerce.dto';
import { updateDTO } from './dto/update.dto';
import { obtenerComercio } from './dto/obtenerComercio.dto';
import { UsersService } from '../users/user.service';
export declare class CommerceService extends TypeOrmCrudService<Commerce> {
    userService: UsersService;
    constructor(repo: any, userService: UsersService);
    saveCommerce(commerceDto: CommerceDTO): Promise<Commerce>;
    updateComerce(commerce: updateDTO): Promise<any>;
    getAllCommerces(): Promise<any[]>;
    compruebaExistencia(id: string): Promise<Commerce>;
    obtenerlocalidades(localidad: string): Promise<any>;
    crearComercio(comercce: obtenerComercio): Promise<any>;
    getCommerceById(id: string): Promise<any>;
    addProducts(idCommerce: string, product: any): Promise<any>;
    searchUserCommerce(ids: string[]): Promise<any>;
    searchUser(ids: string): Promise<any>;
    getDistanciaMetros(lat1: any, lon1: any, localidad: any): Promise<number>;
    obtenerprovincia(pais: string): Promise<any>;
    getpaises(): Promise<any>;
    getdepartamento(id: string): Promise<any>;
    getlocalidades(provin: string): Promise<any>;
    searchUbicacion(name: string): Promise<any>;
    obtenerTodo(): Promise<{
        name: string;
        iso3: string;
        iso2: string;
        phone_code: string;
        capital: string;
        currency: string;
        states: {
            id: number;
            name: string;
            state_code: string;
        }[];
    }[]>;
}
