"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Commerce = void 0;
const mercado_pago_entity_1 = require("../mercado-pago/mercado-pago.entity");
const typeorm_1 = require("typeorm");
const products_entity_1 = require("../products/products.entity");
const user_entity_1 = require("../users/user.entity");
const mercado_entity_1 = require("../mercado/mercado.entity");
let Commerce = class Commerce {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Commerce.prototype, "id", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, unique: true }),
    __metadata("design:type", String)
], Commerce.prototype, "name", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500 }),
    __metadata("design:type", String)
], Commerce.prototype, "adress", void 0);
__decorate([
    typeorm_1.Column('numeric'),
    __metadata("design:type", Number)
], Commerce.prototype, "cuit", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], Commerce.prototype, "profilePic", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], Commerce.prototype, "coverPic", void 0);
__decorate([
    typeorm_1.OneToOne(type => user_entity_1.User, (user) => user.commerce),
    typeorm_1.JoinColumn(),
    __metadata("design:type", user_entity_1.User)
], Commerce.prototype, "user", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], Commerce.prototype, "pais", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], Commerce.prototype, "provincia", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], Commerce.prototype, "localidad", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], Commerce.prototype, "latitud", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], Commerce.prototype, "longitud", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], Commerce.prototype, "numero", void 0);
__decorate([
    typeorm_1.Column('varchar', { length: 500, nullable: true }),
    __metadata("design:type", String)
], Commerce.prototype, "pisoDepto", void 0);
__decorate([
    typeorm_1.OneToMany(type => products_entity_1.Product, product => product.commerce),
    __metadata("design:type", Array)
], Commerce.prototype, "products", void 0);
__decorate([
    typeorm_1.OneToOne(type => mercado_pago_entity_1.MercadoPago, (mercado) => mercado.commerce),
    typeorm_1.JoinColumn(),
    __metadata("design:type", mercado_pago_entity_1.MercadoPago)
], Commerce.prototype, "mercado", void 0);
__decorate([
    typeorm_1.OneToOne(type => mercado_entity_1.Mercado, (mercado) => mercado.commerce),
    typeorm_1.JoinColumn(),
    __metadata("design:type", mercado_entity_1.Mercado)
], Commerce.prototype, "merc", void 0);
Commerce = __decorate([
    typeorm_1.Entity('COMMERCES')
], Commerce);
exports.Commerce = Commerce;
//# sourceMappingURL=commerce.entity.js.map