"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommerceService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const crud_typeorm_1 = require("@nestjsx/crud-typeorm");
const commerce_entity_1 = require("./commerce.entity");
const countries = require("../resource/countries.json");
const paises = require("../resource/paises.json");
const localidades = require("../resource/localidades.json");
const typeorm_2 = require("typeorm");
const user_service_1 = require("../users/user.service");
const env_config_1 = require("../common/env.config");
const fetch = require("node-fetch");
let CommerceService = class CommerceService extends crud_typeorm_1.TypeOrmCrudService {
    constructor(repo, userService) {
        super(repo);
        this.userService = userService;
    }
    async saveCommerce(commerceDto) {
        let commerce = this.repo.create(commerceDto);
        console.log("comercio creado", commerceDto);
        return await this.repo.save(commerce);
    }
    async updateComerce(commerce) {
        let comercio = this.repo.create(commerce);
        return await this.repo.save(comercio);
    }
    async getAllCommerces() {
        return await this.repo.find({ relations: ['user', 'products'] });
    }
    async compruebaExistencia(id) {
        let commerce = this.repo.findOne(id);
        return commerce;
    }
    async obtenerlocalidades(localidad) {
        var vector = [];
        return fetch(env_config_1.ENV.CADYGO_API + '/apis/localidades/v1', {
            method: 'GET'
        })
            .then((response) => {
            return response.json();
        })
            .then((localidades) => {
            let arrayL = localidades.items;
            var minuscula = localidad.toLowerCase();
            for (let index = 0; index < arrayL.length; index++) {
                let n = arrayL[index].nombre.toLowerCase();
                vector[index] = n;
            }
            for (let i = 0; i < vector.length; i++) {
                if (vector[i] == minuscula) {
                    var localid = arrayL[i];
                }
            }
            if (localid === undefined) {
                return [];
            }
            else {
                return localid;
            }
        });
    }
    async crearComercio(comercce) {
        const cuer = {
            commerceMarketplaceId: comercce.commerceMarketplaceId,
            marketplace: comercce.marketplace,
            name: comercce.name,
            localidadId: comercce.localidadId,
            streetName: comercce.streetName,
            streetNumber: comercce.streetNumber,
            directionLatitude: comercce.directionLatitude,
            directionLongitude: comercce.directionLongitude,
            phone: comercce.phone,
            adminEmail: comercce.adminEmail,
            adminName: comercce.adminName,
        };
        return fetch(env_config_1.ENV.CADYGO_API + '/apis/v1/external-marketplace/commerce', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + env_config_1.ENV.CADYGO_TOKEN,
            },
            body: JSON.stringify(cuer),
            cache: 'no-cache'
        })
            .then(function (response) {
            return response.json();
        })
            .then(function (data) {
            return data;
        })
            .catch(function (err) {
            console.error(err);
        });
    }
    async getCommerceById(id) {
        let product = [];
        let commerce = await this.repo.findOne(id, { relations: ['user', 'products', 'products.productType'] });
        console.log(commerce);
        for (let index = 0; index < commerce.products.length; index++) {
            if (commerce.products[index].deletedDate === null) {
                product.push(commerce.products[index]);
            }
        }
        commerce.products = product;
        return commerce;
    }
    async addProducts(idCommerce, product) {
        let commerce = await this.repo.findOne(idCommerce, { relations: ['products'] });
        commerce.products.push(product);
        let commerceCreate = this.repo.create(commerce);
        await this.repo.save(commerce);
    }
    async searchUserCommerce(ids) {
        let commerce = await this.repo.findByIds(ids, { relations: ['user'] });
        return commerce;
    }
    async searchUser(ids) {
        let commerce = await this.userService.findOne(ids);
        return commerce;
    }
    async getDistanciaMetros(lat1, lon1, localidad) {
        let distancias = [];
        let latitud = [];
        let i = 0;
        let j = 0;
        let comercios = await typeorm_2.getRepository(commerce_entity_1.Commerce)
            .createQueryBuilder("commerce")
            .getMany();
        for (let index = 0; index < comercios.length; index++) {
            if (comercios[index].localidad == localidad) {
                let rad = function (x) { return x * Math.PI / 180; };
                var R = 6378.137;
                let dLat = rad(parseFloat(comercios[index].latitud) - lat1);
                var dLong = rad(parseFloat(comercios[index].longitud) - lon1);
                var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(lat1)) *
                    Math.cos(rad(parseFloat(comercios[index].latitud))) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
                var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                var d = R * c * 1000;
                if (d < 10) {
                    distancias.push(d);
                }
            }
        }
        return d;
    }
    async obtenerprovincia(pais) {
        let provincia;
        for (let index = 0; index < countries.length; index++) {
            if (countries[index].name === pais) {
                provincia = countries[index].states;
            }
        }
        console.log(provincia);
        return provincia;
    }
    async getpaises() {
        let pais = paises.countries;
        return pais;
    }
    async getdepartamento(id) {
        return fetch('https://apis.datos.gob.ar/georef/api/departamentos?provincia=' + id + '&aplanar=true&campos=estandar&max=20&exacto=true')
            .then(function (response) {
            return response.json();
        })
            .then(function (data) {
            return data.departamentos;
        })
            .catch(function (err) {
            console.error(err);
        });
    }
    async getlocalidades(provin) {
        let provincia;
        let number = localidades.length;
        for (let index = 0; index < number; index++) {
            if (localidades[index].provincia === provin) {
                provincia = localidades[index].localidad;
                console.log("LAS LOCALIDADES DE LA PROVINCIA SON", provincia);
            }
        }
        return provincia;
    }
    async searchUbicacion(name) {
        let productos = [];
        var j = 0;
        let a = await typeorm_2.getRepository(commerce_entity_1.Commerce)
            .createQueryBuilder("commerce")
            .innerJoinAndSelect("commerce.products", "products")
            .where("commerce.provincia = :id", { id: name })
            .getMany();
        for (let index = 0; index < a.length; index++) {
            for (let j = 0; j < a[index].products.length; j++) {
                if (a[index].products[j].deletedDate === null) {
                    productos.push(a[index].products[j]);
                }
            }
        }
        return productos;
    }
    async obtenerTodo() {
        return countries;
    }
};
CommerceService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(commerce_entity_1.Commerce)),
    __metadata("design:paramtypes", [Object, user_service_1.UsersService])
], CommerceService);
exports.CommerceService = CommerceService;
//# sourceMappingURL=commerce.service.js.map