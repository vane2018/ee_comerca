import { MercadoPago } from '../mercado-pago/mercado-pago.entity';
import { Product } from '../products/products.entity';
import { User } from '../users/user.entity';
import { Mercado } from '../mercado/mercado.entity';
export declare class Commerce {
    id: string;
    name: string;
    adress: string;
    cuit: number;
    profilePic: string;
    coverPic: string;
    user: User;
    pais: string;
    provincia: string;
    localidad: string;
    latitud: string;
    longitud: string;
    numero: string;
    pisoDepto: string;
    products: Product[];
    mercado: MercadoPago;
    merc: Mercado;
}
