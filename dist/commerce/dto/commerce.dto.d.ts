import { User } from '../../users/user.entity';
export declare class CommerceDTO implements Readonly<CommerceDTO> {
    name: string;
    adress: string;
    cuit: number;
    pais: string;
    provincia: string;
    localidad: string;
    numero: string;
    latitud: string;
    longitud: string;
    profilePic: string;
    coverPic: string;
    user: User;
    pisoDepto: string;
}
