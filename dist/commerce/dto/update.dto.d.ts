export declare class updateDTO {
    id: string;
    name: string;
    adress: string;
    numero: string;
    cuit: number;
    pais: string;
    provincia: string;
    localidad: string;
    latitud: string;
    longitud: string;
    profilePic: string;
    coverPic: string;
    pisoDepto: string;
}
