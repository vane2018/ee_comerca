"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.obtenerComercio = void 0;
const swagger_1 = require("@nestjs/swagger");
class obtenerComercio {
}
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'Nombre del marketplace dado en acceso.' }),
    __metadata("design:type", String)
], obtenerComercio.prototype, "marketplace", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'id del vendedor' }),
    __metadata("design:type", String)
], obtenerComercio.prototype, "commerceMarketplaceId", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'id del vendedor' }),
    __metadata("design:type", String)
], obtenerComercio.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'id del vendedor' }),
    __metadata("design:type", String)
], obtenerComercio.prototype, "localidadId", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'id del vendedor' }),
    __metadata("design:type", String)
], obtenerComercio.prototype, "streetName", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'id del vendedor' }),
    __metadata("design:type", String)
], obtenerComercio.prototype, "streetNumber", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'id del vendedor' }),
    __metadata("design:type", String)
], obtenerComercio.prototype, "directionLatitude", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'id del vendedor' }),
    __metadata("design:type", String)
], obtenerComercio.prototype, "directionLongitude", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'id del vendedor' }),
    __metadata("design:type", String)
], obtenerComercio.prototype, "phone", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'id del vendedor' }),
    __metadata("design:type", String)
], obtenerComercio.prototype, "adminEmail", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'id del vendedor' }),
    __metadata("design:type", String)
], obtenerComercio.prototype, "adminName", void 0);
exports.obtenerComercio = obtenerComercio;
//# sourceMappingURL=obtenerComercio.dto.js.map