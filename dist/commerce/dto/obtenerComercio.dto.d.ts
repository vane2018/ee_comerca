export declare class obtenerComercio {
    marketplace: string;
    commerceMarketplaceId: string;
    name: string;
    localidadId: string;
    streetName: string;
    streetNumber: string;
    directionLatitude: string;
    directionLongitude: string;
    phone: string;
    adminEmail: string;
    adminName: string;
}
