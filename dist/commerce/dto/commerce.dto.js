"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommerceDTO = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const user_entity_1 = require("../../users/user.entity");
class CommerceDTO {
}
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'Nombre del comercio' }),
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], CommerceDTO.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'Direccion' }),
    class_validator_1.IsString(),
    __metadata("design:type", String)
], CommerceDTO.prototype, "adress", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'CUIT ' }),
    class_validator_1.IsNumber(),
    __metadata("design:type", Number)
], CommerceDTO.prototype, "cuit", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'PAIS' }),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], CommerceDTO.prototype, "pais", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'PROVINCIA' }),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], CommerceDTO.prototype, "provincia", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'PROVINCIA' }),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], CommerceDTO.prototype, "localidad", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'PROVINCIA' }),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], CommerceDTO.prototype, "numero", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'PROVINCIA' }),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], CommerceDTO.prototype, "latitud", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'PROVINCIA' }),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], CommerceDTO.prototype, "longitud", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'Foto de perfil del comercio' }),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], CommerceDTO.prototype, "profilePic", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'Foto de portada del comercio' }),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], CommerceDTO.prototype, "coverPic", void 0);
__decorate([
    swagger_1.ApiPropertyOptional({ description: 'Foto de portada del comercio' }),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", user_entity_1.User)
], CommerceDTO.prototype, "user", void 0);
__decorate([
    swagger_1.ApiProperty({ required: true, description: 'PROVINCIA' }),
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], CommerceDTO.prototype, "pisoDepto", void 0);
exports.CommerceDTO = CommerceDTO;
//# sourceMappingURL=commerce.dto.js.map