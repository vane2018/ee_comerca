import { CommerceDTO } from './commerce.dto';
export declare class CreateCommerceDTO implements Readonly<CreateCommerceDTO> {
    readonly sellerId: string;
    readonly commerce: CommerceDTO;
}
