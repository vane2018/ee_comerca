FROM node:14

RUN mkdir -p /app
WORKDIR /app

COPY . /app

RUN npm install

# RUN npm run typeorm:run

# RUN chmod 755 entrypoint.sh

EXPOSE 3001
# ENTRYPOINT ["/app/entrypoint.sh"]

ENTRYPOINT ["/bin/bash", "./entrypoint.sh"]
# CMD ["npm", "run", "start:dev"]
