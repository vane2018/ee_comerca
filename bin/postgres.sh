
#!/bin/bash

sudo -u postgres -H -- psql -d comprartir -c "
DELETE FROM public.\"ROLS\";
DELETE FROM public.\"PAYMENT_TYPES\";
DELETE FROM public.\"SHIPPING_TYPES\";
DELETE FROM public. \"CATEGORIA_GRAL_PRODUCTS\";
INSERT INTO public.\"ROLS\" (name, description) VALUES ('customer', 'can have acces to api customers'),
('seller', 'can have acces to api seller'), ('admin', 'can have acces all api'), ('super', 'can have acces all api'),('empleado', 'can have acces some api');
INSERT INTO public.\"PAYMENT_TYPES\" (name, description) VALUES ('Mercado Pago', 'tipo de pago con mercado pago'),
('Debito', 'tipo de pago con tarjeta de debito'), ('Credito', 'tipo de pago con tarjeta de credito');
INSERT INTO public.\"SHIPPING_TYPES\" (name, description) VALUES ('Acordar con el vendedor', 'Al terminar el proceso de compra se habilitar el chat de conversación con el vendedor.'),
('Cadygo', 'Empresa de logística');
INSERT INTO public.\"CATEGORIA_GRAL_PRODUCTS\" (name) VALUES ('Tecnologia'),
('Hogar y Electrodomesticos'), ('Deportes y Aire Libre'), ('Ropa y Accesorios');"

